<?php

namespace App\Models;

class Resign
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($id)
    {
        $qkar = $this->app->db->prepare("SELECT * FROM karyawan WHERE id = ?");
        $qkar->bind_param('s', $id);
        $qkar->execute();
        $c = $qkar->get_result();
        $data = $c->fetch_assoc();

        if(!empty($data)){
            $stmt = $this->app->db->prepare("INSERT INTO `resign` VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param('ssssssssssssssssssssssssssssssssss', 
                            $data['id'],
                            $data['ktp'],
                            $data['nama'],
                            $data['tempat_lhr'],
                            $data['tgl_lhr'],
                            $data['jk'],
                            $data['alamat'],
                            $data['kd_status'],
                            $data['tgl_in'],
                            $data['tgl_angkat'],
                            $data['gaji'],
                            $data['bpjs_ket'],
                            $data['bpjs_ket_plus'],
                            $data['bpjs_kes'],
                            $data['kelas'],
                            $data['bpjs_kes_plus'],
                            $data['kd_jabatan'],
                            $data['kd_departemen'],
                            $data['kd_bagian'],
                            $data['bagian'],
                            $data['kd_jamkerja'],
                            $data['jml_grup'],
                            $data['grup'],
                            $data['libur'],
                            $data['setengah'],
                            $data['kd_t_jabatan'],
                            $data['t_keahlian'],
                            $data['kd_t_prestasi'],
                            $data['spt'],
                            $data['ket_kar'],
                            $data['tgl_update'],
                            $data['ket'],
                            $data['kd_status_lama'],
                            $data['tgl_m']);
            $stmt->execute();
            $stmt->store_result();
            if($stmt->affected_rows == 1){
                if($id_history = $this->history('karyawan', $id, 'delete')) {
                    $stm = $this->app->db->prepare("DELETE FROM `karyawan` WHERE id = ?;");
                    $stm->bind_param('s', $id);
                    $stm->execute();
                    $stm->store_result();
                    var_dump($stm);
                    
                    if($stm->affected_rows == 1) return $id;
                    else {
                        $this->undo_history($id_history);

                        echo 'ngek'; die();
                        return false;
                    }
                }else{ 
                    return false; 
                }
            }else{ 
                return false;
            }
        }
    }

    public function get()
    {        
        $res = $this->app->db->query("SELECT r.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM resign r 
                                    JOIN jamkerja jm ON jm.id = r.kd_jamkerja 
                                    JOIN departemen d ON d.id = r.kd_departemen 
                                    JOIN bagian b ON b.id = r.kd_bagian 
                                    JOIN status s ON s.id = r.kd_status
                                    ORDER BY r.tgl_update DESC, r.kd_departemen, r.kd_bagian, r.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {

            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }
        return $container;
    }


    public function getIn()
    {        
        $res = $this->app->db->query("SELECT r.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM karyawan r 
                                    JOIN jamkerja jm ON jm.id = r.kd_jamkerja 
                                    JOIN departemen d ON d.id = r.kd_departemen 
                                    JOIN bagian b ON b.id = r.kd_bagian 
                                    JOIN status s ON s.id = r.kd_status
                                    WHERE kd_status = 4
                                    ORDER BY r.tgl_update DESC, r.kd_departemen, r.kd_bagian, r.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {

            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM resign k 
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    JOIN status s ON s.id = k.kd_status
                                    WHERE k.id = $id");
        $c = $res->fetch_assoc();
        //var_dump($id);die();
        //nominal Jabatan
        if(empty($c['kd_jabatan'])){
            $c['nm_jabatan'] = '';
        }else{
            $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
            $restj->bind_param('i', $c['kd_jabatan']);
            $restj->execute();
            $restj->store_result();
            $restj->bind_result($nm_jabatan);
            $restj->fetch();
            $c['nm_jabatan'] = $nm_jabatan;
        }
        //end
            
        //nominal Tunjangan Jabatan
        if(empty($c['kd_t_jabatan'])){
            $c['nom_jabatan'] = '0';
        }else{
            $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
            $restj->bind_param('i', $c['kd_t_jabatan']);
            $restj->execute();
            $restj->store_result();
            $restj->bind_result($nom_jabatan);
            $restj->fetch();
            $c['nom_jabatan'] = $nom_jabatan;
        }
        //end
        
        //nominal Tunjangan Prestasi
        if(empty($c['kd_prestasi'])){
            $c['nom_prestasi'] = '0';
        }else{
            $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
            $restps->bind_param('i', $c['kd_prestasi']);
            $restps->execute();
            $restps->store_result();
            $restps->bind_result($nom_prestasi);
            $restps->fetch();
            $c['nom_prestasi'] = $nom_prestasi;
        }
        //end
        
        //nominal Tunjangan Keahlian
        if(empty($c['kd_t_keahlian'])){
            $c['nom_keahlian'] = '0';
        }else{
            $c['nom_keahlian'] = $c['kd_t_keahlian'];
        }
        //end
        
        return $c;
    }

    public function update($data)
    {
        $stmt = $this->app->db->prepare("UPDATE `resign` SET ktp = ?, nama = ?, tempat_lhr = ?, tgl_lhr = ?, jk = ?, alamat = ?, bpjs_ket =?, bpjs_ket_plus =?, bpjs_kes =?, kelas=?, bpjs_kes_plus=? WHERE id = ?");
        $stmt->bind_param('sssssssssss', 
                            $data['ktp'],
                            $data['nama'],
                            $data['tempat'],
                            $data['tgl_lhr'],
                            $data['jk'],
                            $data['alamat'],
                            $data['bpjsket'],
                            $data['bpjsket_plus'],
                            $data['bpjskes'],
                            $data['kelas'],
                            $data['bpjskesplus'],
                            $data['nik']
                        );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }


    public function delete($id)
    {
        if($id_history = $this->history('resign', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `resign` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

    public function cekResign()
    {        
        $a = -1;
        $tgl = date('Y-m-d');
        $bulan = date('m');
        $thn = date('Y');
        $bulann = date('m', strtotime("-1 month", strtotime($tgl)));
        $thnn = date('Y', strtotime("-1 month", strtotime($tgl)));
        $tgl_aw = date($thnn.'-'.$bulann.'-21');
        $tgl_ak = date($thn.'-'.$bulan.'-20');

        if($tgl>=$tgl_aw && $tgl<=$tgl_ak){
            $bulan = date('m', strtotime("-1 month", strtotime($tgl)));
            $thn = date('Y', strtotime("-1 month", strtotime($tgl)));
            $bulann = date('m', strtotime("-2 month", strtotime($tgl)));
            $thnn = date('Y', strtotime("-2 month", strtotime($tgl)));
            $tgl_aw = date($thnn.'-'.$bulann.'-21');
            $tgl_ak = date($thn.'-'.$bulan.'-20');
        }

        $sql = $this->app->db->prepare("SELECT id FROM karyawan WHERE kd_status = 4 AND tgl_update BETWEEN ? AND ?");
        $sql->bind_param('ss', $tgl_aw, $tgl_ak);
        $sql->execute();
        $res = $sql->get_result();

        $con = [];
        while($c = $res->fetch_assoc()) {
            $a = 0;
            $con[] = $c;
        }

        for ($i=0; $i < count($con); $i++) { 
            $qkar = $this->app->db->prepare("SELECT * FROM karyawan WHERE id = ?");
            $qkar->bind_param('s', $con[$i]['id']);
            $qkar->execute();
            $ckar = $qkar->get_result();
            $data = $ckar->fetch_assoc();

            if(!empty($data)){
                $stmt = $this->app->db->prepare("INSERT INTO `resign` VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param('ssssssssssssssssssssssssssssssssss', 
                                $data['id'],
                                $data['ktp'],
                                $data['nama'],
                                $data['tempat_lhr'],
                                $data['tgl_lhr'],
                                $data['jk'],
                                $data['alamat'],
                                $data['kd_status'],
                                $data['tgl_in'],
                                $data['tgl_angkat'],
                                $data['gaji'],
                                $data['bpjs_ket'],
                                $data['bpjs_ket_plus'],
                                $data['bpjs_kes'],
                                $data['kelas'],
                                $data['bpjs_kes_plus'],
                                $data['kd_jabatan'],
                                $data['kd_departemen'],
                                $data['kd_bagian'],
                                $data['bagian'],
                                $data['kd_jamkerja'],
                                $data['jml_grup'],
                                $data['grup'],
                                $data['libur'],
                                $data['setengah'],
                                $data['kd_t_jabatan'],
                                $data['t_keahlian'],
                                $data['kd_t_prestasi'],
                                $data['spt'],
                                $data['ket_kar'],
                                $data['tgl_update'],
                                $data['ket'],
                                $data['kd_status_lama'],
                                $data['tgl_m']);
                $stmt->execute();
                $stmt->store_result();
                if($stmt->affected_rows == 1){
                    if($id_history = $this->history('karyawan', $id, 'delete')) {
                        $stm = $this->app->db->prepare("DELETE FROM `karyawan` WHERE id = ?");
                        $stm->bind_param('s', $id);
                        $stm->execute();
                        $stm->store_result();
                        //var_dump($stm);
                        
                        if($stm->affected_rows == 1){ 
                            $a++; 
                        }else{
                            $this->undo_history($id_history);

                            echo 'gagal delete in karyawan'; die();
                        }
                    }else{
                        echo 'gagal history'; die();
                    }
                }else{ 
                    echo 'gagal insert in resign'; die();
                }
            }
        }
        if($a==count($con)){
            return 1;
        }elseif($a==-1){
            return 0;
        }else{
            return 2;
        }
    }
}