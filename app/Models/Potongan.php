<?php

namespace App\Models;

class Potongan
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($pot, $nom, $ket=null)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `potongan`(nm_potongan, nominal, ket) VALUES(?,?,?)");
        $stmt->bind_param('sss', $pot, $nom, $ket);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM potongan");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM potongan WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM potongan WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $pot, $nom, $ket=null)
    {
        $stmt = $this->app->db->prepare("UPDATE potongan SET nm_potongan = ?, nominal = ?, ket = ? WHERE id = ?");
        $stmt->bind_param('sssi', $pot, $nom, $ket, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('potongan', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `potongan` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

    public function addKoperasi($data)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `koperasi`(nik, nominal, tgl) VALUES(?,?,?)");
        $stmt->bind_param('sss', 
                           $data['nik'],
                           $data['nominal'],
                           $data['tgl']
                        );

        //var_dump($data);echo"<br>";die();
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function getKoperasi()
    {
        $tgl = date('Y-m-01');
        $res = $this->app->db->query("SELECT ko.*, k.nama, k.jk, d.nm_departemen, b.nm_bagian FROM koperasi ko
                                    JOIN karyawan k ON k.id = ko.nik
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian
                                    WHERE ko.tgl = '{$tgl}'
                                    ORDER BY k.kd_departemen, k.kd_bagian, k.id ASC
                                    ");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getKoperasiById($id)
    {
        $res = $this->app->db->query("SELECT * FROM koperasi WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function updateKoperasi($data)
    {
        //var_dump($data);echo"<br>";die();
        $stmt = $this->app->db->prepare("UPDATE `koperasi` SET nominal = ? WHERE id = ? AND tgl = ?");

        $stmt->bind_param('sss', 
                           $data['nominal'],
                           $data['id'],
                           $data['tgl']
                        );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function updateKoperasiMinus($data)
    {
        //var_dump($data);echo"<br>";die();
        $stmt = $this->app->db->prepare("UPDATE `koperasi` SET nominal = ?, sisa = ?, pot_lama = ? WHERE id = ?");

        $stmt->bind_param('ssss', 
                           $data['bayar'],
                           $data['sisa'],
                           $data['kop'],
                           $data['id']
                        );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteKop($id)
    {
        if($id_history = $this->history('koperasi', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `koperasi` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }


    public function addLain($data)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `potongan_lain`(nik, nominal, tgl) VALUES(?,?,?)");
        $stmt->bind_param('sss', 
                           $data['nik'],
                           $data['nominal'],
                           $data['tgl']
                        );

        //var_dump($data);echo"<br>";die();
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function getLain()
    {
        $tgl = date('Y-m-01');
        $res = $this->app->db->query("SELECT po.*, k.nama, k.jk, d.nm_departemen, b.nm_bagian FROM potongan_lain po
                                    JOIN karyawan k ON k.id = po.nik
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian
                                    WHERE po.tgl = '{$tgl}'
                                    ORDER BY k.kd_departemen, k.kd_bagian, k.id ASC
                                    ");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getLainById($id)
    {
        $res = $this->app->db->query("SELECT * FROM potongan_lain WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function updateLain($data)
    {
        //var_dump($data);echo"<br>";die();
        $stmt = $this->app->db->prepare("UPDATE `potongan_lain` SET nominal = ? WHERE id = ? AND tgl = ?");

        $stmt->bind_param('sss', 
                           $data['nominal'],
                           $data['id'],
                           $data['tgl']
                        );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteLain($id)
    {
        if($id_history = $this->history('potongan_lain', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `potongan_lain` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

}