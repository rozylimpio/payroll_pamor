<?php

namespace App\Models;

class TTD
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM manager");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM manager WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $nm, $jab)
    {
        $stmt = $this->app->db->prepare("UPDATE manager SET nama = ?, jabatan = ? WHERE id = ?");
        $stmt->bind_param('ssi', $nm, $jab, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

}