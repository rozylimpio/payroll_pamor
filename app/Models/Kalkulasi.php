<?php

namespace App\Models;

class Kalkulasi
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }    

    public function cek($bln, $thn)
    {
        $qs = $this->app->db->prepare("SELECT DISTINCT bulan FROM absensi_kalkulasi WHERE bulan = ? AND tahun = ?");
        $qs->bind_param('ss', $bln, $thn);
        $qs->execute();
        $resqs = $qs->get_result();
        $qsc = $resqs->fetch_assoc();

        if(!empty($qsc)){
            $del = $this->app->db->prepare("DELETE FROM `absensi_kalkulasi` WHERE `bulan` = ? AND `tahun` = ?");
            $del->bind_param('ss', $bln, $thn);
            $del->execute();
        }
    }

    public function add($d)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `absensi_kalkulasi`(bulan, tahun, kd_departemen, kd_bagian, nik, gaji, persen, tarif_l, tarif_ai, masuk, pagi, siang, malam, libur, ijin, sdr, cuti, cuti_h, cuti_r, rumah, alpha, alpha_r, kk, pijin, t_ijin, lembur_w, t_lembur_w, t_lbiasa, t_llibur, t_lembur_lain, t_lembur, nt_jabatan, nt_keahlian, nt_prestasi, nt_masakerja, nt_premi, nt_insentif, nt_lwajib, nt_lbiasa, nt_llibur, nt_lembur, n_rumah, n_ai, n_psw, n_bpjs_ket, n_bpjs_kes, tn_lain, n_koperasi, n_kspn, n_spt, n_spt_p, gaji_kotor, n_potongan, total_gaji, sd, u_makan) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        $stmt->bind_param('ssssssssssssssssssssssssssssssssssssssssssssssssssssssss', 
                        $d['bulan'],
                        $d['tahun'],
                        $d['kd_departemen'],
                        $d['kd_bagian'],
                        $d['nik'],
                        $d['gaji'],
                        $d['persen'],
                        $d['tarif_l'],
                        $d['tarif_ai'],
                        $d['masuk'],
                        $d['pagi'],
                        $d['siang'],
                        $d['malam'],
                        $d['libur'],
                        $d['ijin'],
                        $d['sdr'],
                        $d['cuti'],
                        $d['cuti_h'],
                        $d['cuti_r'],
                        $d['rumah'],
                        $d['alpha'],
                        $d['alpha_r'],
                        $d['kk'],
                        $d['pijin'],
                        $d['t_ijin'],
                        $d['lembur_w'],
                        $d['t_lembur_w'],
                        $d['t_biasa'],
                        $d['t_libur'],
                        $d['t_lembur_lain'],
                        $d['t_lembur'],
                        $d['nt_jabatan'],
                        $d['nt_keahlian'],
                        $d['nt_prestasi'],
                        $d['nt_masakerja'],
                        $d['nt_premi'],
                        $d['nt_insentif'],
                        $d['nt_l_wajib'],
                        $d['nt_l_biasa'],
                        $d['nt_l_libur'],
                        $d['nt_lembur'],
                        $d['n_rumah'],
                        $d['n_ai'],
                        $d['n_psw'],
                        $d['n_bpjs_ket'],
                        $d['n_bpjs_kes'],
                        $d['tn_lain'],
                        $d['n_koperasi'],
                        $d['n_kspn'],
                        $d['n_spt'],
                        $d['n_spt_p'],
                        $d['gaji_kotor'],
                        $d['n_potongan'],
                        $d['total_gaji'],
                        $d['sd'],
                        $d['tmakan']
                    );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1){ 
            //echo $d['nik'].' - Insert<br>';
            return $stmt->insert_id; 
        } else { 
            echo $d['nik'].' - Gagal<br>';
            return null; 
        }
    }

    public function getData($datar)
    {
        $dqm = null;
        $dlmbb = null;
        $dlmbl = null;
        $dqij = null;
        $dqh = null;
        $durasi = null;
        $dtja = null;
        $dtkea = null;
        $dtpre = null;
        $djmke = null;
        $dtmke = null;
        $dtprem = null;
        $dpket = null;
        $dpkes = null;
        $dpkspn = null;
        $dplain = null;
        $dpkop = null;
        $sspt = 0;
        $ssptp = 0;

        $stmt = $this->app->db->prepare("SELECT DISTINCT a.nik, k.nama, k.gaji, k.persen, k.spt, k.kd_status, k.kd_status_lama, k.tgl_update, 
                                        k.kd_departemen, k.kd_bagian, k.tgl_in
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tgl_absensi BETWEEN ? AND ?
                                        ORDER BY k.grup, a.nik ASC");
        $stmt->bind_param('ss', $datar['tgl_aw'],
                                  $datar['tgl_ak']
                                );
        $stmt->execute();
        $res = $stmt->get_result();

        $content = []; 
        $data = [];     
        $totalgaji = 0;  
        while($c = $res->fetch_assoc()) {
            $content[] = $c;
        }

        //print_r($content);die();

        for($a=0;$a<count($content);$a++){
            $data['nik'][]=$content[$a]['nik'];
            $data['nama'][]=$content[$a]['nama'];
            $data['gaji'][]=$content[$a]['gaji'];
            $data['persen'][]=$content[$a]['persen'];
            $data['departemen'][]=$content[$a]['kd_departemen'];
            $data['bagian'][]=$content[$a]['kd_bagian'];
            $data['status'][]=$content[$a]['kd_status'];
            $data['status_lama'][]=$content[$a]['kd_status_lama'];
        }

        for($i=0;$i<count($content);$i++){
            $nik = $content[$i]['nik'];
            $gaji = $content[$i]['gaji'];
            $spt = $content[$i]['spt'];
            $tgl_in = $content[$i]['tgl_in'];
            //--------------------------------------------------------------------------------------------------------------------------------//
            //                                            HITUNG JUMLAH MASUK, JAM IJIN, JAM LEMBUR
            //--------------------------------------------------------------------------------------------------------------------------------//
            $days = 0;
            
            if(strtotime($tgl_in)>strtotime($datar['tgl_aw'])){ 
                $data[$nik]['newbie'] = "yes";           
                $jmlhari_bln = get_object_vars(date_diff(date_create($datar['tgl_aw']), date_create($tgl_in)));
                $days = $jmlhari_bln['days'];
            }else{
                $data[$nik]['newbie'] = "no";           
            }
            /**/

            $qtgl = $this->app->db->prepare("SELECT tgl_absensi FROM absensi WHERE tgl_absensi BETWEEN ? AND ? AND nik = ? 
                                            ORDER BY tgl_absensi DESC");
            $qtgl->bind_param('sss', $datar['tgl_aw'],
                                     $datar['tgl_ak'],
                                     $nik
                                    );
            $qtgl->execute();
            $dqtgl = $qtgl->get_result();
            $data[$nik]['sd'] = $dqtgl->fetch_assoc();


            $qmsk = $this->app->db->prepare("SELECT sum(kehadiran = 'masuk') as tmasuk, 
                                            sum(lembur_wajib = 'wajib') as wajib, sum(t_ijin) as tijin, 
                                            sum(t_lembur) as tlembur, sum(pot_ijin_hari)  as pot_ijin, sum(ijin != '') as pijin
                                            , sum(u_makan) as tmakan
                                            FROM absensi 
                                            WHERE tgl_absensi BETWEEN ? AND ? AND kehadiran = 'masuk' AND nik = ?");
            $qmsk->bind_param('sss', $datar['tgl_aw'],
                                     $datar['tgl_ak'],
                                     $nik
                                    );
            $qmsk->execute();
            $dqmsk = $qmsk->get_result();
            while ($dqm = $dqmsk->fetch_assoc()) {
                $data[$nik]['masuk'] = $dqm;
            }


            $qlmbb = $this->app->db->prepare("SELECT IFNULL(sum(l.t_lembur),0) as l_biasa FROM absensi a
                                            JOIN absensi_lembur l ON l.id_absensi = a.id
                                            WHERE a.tgl_absensi BETWEEN ? AND ?
                                            AND (l.lembur = 'biasa' OR l.lembur = 'jumat' OR l.lembur = 'libur') AND nik = ?");
            $qlmbb->bind_param('sss', $datar['tgl_aw'], $datar['tgl_ak'], $nik);
            $qlmbb->execute();
            $dqlmbb = $qlmbb->get_result();
            $dlmbb = $dqlmbb->fetch_assoc();

            $qlmbl = $this->app->db->prepare("SELECT IFNULL(sum(l.t_lembur),0) as l_libur FROM absensi a
                                            JOIN absensi_lembur l ON l.id_absensi = a.id
                                            WHERE a.tgl_absensi BETWEEN ? AND ?
                                            AND l.lembur = 'nasional' AND nik = ?");
            $qlmbl->bind_param('sss', $datar['tgl_aw'], $datar['tgl_ak'], $nik);
            $qlmbl->execute();
            $dqlmbl = $qlmbl->get_result();
            $dlmbl = $dqlmbl->fetch_assoc();   
            
            $data[$nik]['lembur'] = array_merge($dlmbb, $dlmbl);



            $qijin = $this->app->db->prepare("SELECT sum(jadwal_shift='Pagi') as pagi, 
                                            sum(jadwal_shift='Siang') as siang, sum(jadwal_shift='Malam') as malam
                                            FROM absensi 
                                            WHERE tgl_absensi BETWEEN ? AND ? AND kehadiran = 'masuk' AND nik = ? 
                                            AND (ijin = '' OR ijin IS NULL) ");
            $qijin->bind_param('sss', $datar['tgl_aw'],
                                     $datar['tgl_ak'],
                                     $nik
                                    );
            $qijin->execute();
            $dqijin = $qijin->get_result();
            while ($dqij = $dqijin->fetch_assoc()) {
                $data[$nik]['jadwal'] = $dqij;
            }
            //--------------------------------------------------------------------------------------------------------------------------------//
            
            



            //--------------------------------------------------------------------------------------------------------------------------------//
            //                                                      HITUNG JUMLAH KEHADIRAN
            //--------------------------------------------------------------------------------------------------------------------------------//
            $qhdr = $this->app->db->prepare("SELECT sum(kehadiran = 'libur') as libur, sum(kehadiran = 'ijin') as ijin, 
                                            sum(kehadiran = 'sdr') as sdr, sum(kehadiran = 'cuti') as cuti, 
                                            sum(kehadiran = 'cutihamil') as cutihamil, sum(kehadiran = 'dirumahkan') as dirumahkan,
                                            sum(kehadiran = 'rumah' OR kehadiran = 'Rumah') as rumahi, sum(kehadiran = 'unpaid') as unpaid,
                                            (sum(kehadiran = 'alpha' OR kehadiran = '')) as alpha, sum(kehadiran = 'kk') as kk,
                                            (coalesce(sum(kehadiran='alpha_r'), 0) + {$days}) as alpha_r, 
                                            sum(kehadiran = 'cuti_r') as cuti_r
                                            FROM absensi 
                                            WHERE tgl_absensi BETWEEN ? AND ? AND kehadiran != 'masuk' AND nik = ?");
            $qhdr->bind_param('sss', $datar['tgl_aw'],
                                     $datar['tgl_ak'],
                                     $nik
                                    );
            $qhdr->execute();
            $dqhdr = $qhdr->get_result();
            while ($dqh = $dqhdr->fetch_assoc()) {
                $data[$nik]['hadir'] = $dqh;
            }
            //--------------------------------------------------------------------------------------------------------------------------------//
            

            //--------------------------------------------------------------------------------------------------------------------------------//
            //                                                           HITUNG RESIGN
            //--------------------------------------------------------------------------------------------------------------------------------//
            /*
            if($content[$i]['kd_status']==4){
                $tgl_ak = $datar['tgl_ak'];
                $tgl_aw = $content[$i]['tgl_update'];

                $durasi = get_object_vars(date_diff(date_create($tgl_aw), date_create($tgl_ak)));

                $data[$nik]['hadir']['alpha_r'] = $data[$nik]['hadir']['alpha_r'] + $durasi['days'] + 1;
            }
            /**/
            //--------------------------------------------------------------------------------------------------------------------------------//
            



            //--------------------------------------------------------------------------------------------------------------------------------//
            //                                                           TUNJANGAN
            //--------------------------------------------------------------------------------------------------------------------------------//
            //Tunjangan Jabatan
            $dtja = null;
            $qtjab = $this->app->db->prepare("SELECT tj.nominal as t_jabatan FROM karyawan k
                                              JOIN t_jabatan tj ON tj.id = k.kd_t_jabatan
                                              WHERE k.id = ?");
            $qtjab->bind_param('s', $nik);
            $qtjab->execute();
            $dqtjab = $qtjab->get_result();
            $dtja = $dqtjab->fetch_assoc();
           
            if(!empty($dtja)){
                $data[$nik]['tunjangan']['t_jabatan'] = $dtja['t_jabatan'];
            }else{
                $data[$nik]['tunjangan']['t_jabatan'] = 0;
            }


            //Tunjangan Keahlian
            $qtkeah = $this->app->db->prepare("SELECT kd_t_keahlian as t_keahlian FROM karyawan k
                                              WHERE k.id = ?");
            $qtkeah->bind_param('s', $nik);
            $qtkeah->execute();
            $dqtkeah = $qtkeah->get_result();
            $dtkea = $dqtkeah->fetch_assoc();

            if(!empty($dtkea)){
                $data[$nik]['tunjangan']['t_keahlian'] = $dtkea['t_keahlian'];
            }else{
                $data[$nik]['tunjangan']['t_keahlian'] = 0;
            }


            //Tunjangan Prestasi            
            $qtpres = $this->app->db->prepare("SELECT tp.nominal as t_prestasi FROM karyawan k
                                               JOIN t_prestasi tp ON tp.id = k.kd_t_prestasi
                                               WHERE k.id = ?");
            $qtpres->bind_param('s', $nik);
            $qtpres->execute();
            $dqtpres = $qtpres->get_result();
            $dtpre = $dqtpres->fetch_assoc();

            if(!empty($dtpre)){
                $data[$nik]['tunjangan']['t_prestasi'] = $dtpre['t_prestasi'];
            }else{
                $data[$nik]['tunjangan']['t_prestasi'] = 0;
            }

            //Tunjangan Masa Kerja
            if($content[$i]['kd_status']==2 || $content[$i]['kd_status_lama']==2){
                //Masa Kerja brp tahun
                $qjmker = $this->app->db->prepare("SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) as masakerja 
                                                   FROM karyawan k WHERE id = ?");
                $qjmker->bind_param('ss', $datar['tgl_ak'], $nik);
                $qjmker->execute();
                $dqjmker = $qjmker->get_result();
                $djmke = $dqjmker->fetch_assoc();

                if(!empty($djmke)){
                    $data[$nik]['tunjangan']['j_masakerja'] = $djmke['masakerja'];
                }else{
                    $data[$nik]['tunjangan']['j_masakerja'] = 0;
                }

                //Tunjangan Masa Kerja
                $qtmker = $this->app->db->prepare("SELECT tm.nominal as t_masakerja FROM t_masakerja tm 
                                                   WHERE (SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) FROM karyawan WHERE id = ?)        
                                                   BETWEEN tm.awal AND tm.akhir");
                $qtmker->bind_param('ss', $datar['tgl_ak'], $nik);
                $qtmker->execute();
                $dqtmker = $qtmker->get_result();
                $dtmke = $dqtmker->fetch_assoc();

                if(!empty($dtmke)){
                    $data[$nik]['tunjangan']['t_masakerja'] = $dtmke['t_masakerja'];
                }else{
                    $data[$nik]['tunjangan']['t_masakerja'] = 0;
                }
            }else{
                $data[$nik]['tunjangan']['j_masakerja'] = 0;
                $data[$nik]['tunjangan']['t_masakerja'] = 0;
            }


            //Tunjangan Premi
            $qtpremi = $this->app->db->prepare("SELECT (sum(a.jadwal_shift='Malam')*pm.nominal) + (sum(a.jadwal_shift='Siang')*ps.nominal) 
                                                as t_premi
                                                FROM absensi a, premi pm, premi ps
                                                WHERE a.tgl_absensi BETWEEN ? AND ? 
                                                AND a.kehadiran = 'masuk' AND a.nik = ? AND ps.nm_premi = 'Siang' AND pm.nm_premi = 'Malam' 
                                                AND (a.ijin IS NULL OR a.ijin = '')");
            $qtpremi->bind_param('sss', $datar['tgl_aw'], 
                                        $datar['tgl_ak'], 
                                        $nik
                                        );
            $qtpremi->execute();
            $dqtpremi = $qtpremi->get_result();
            $dtprem = $dqtpremi->fetch_assoc();

            if(!empty($dtprem)){
                $data[$nik]['tunjangan']['t_premi'] = $dtprem['t_premi'];
            }else{
                $data[$nik]['tunjangan']['t_premi'] = 0;
            }


            //Tunjangan Insentif Absen via PHP
            //--------------------------------------------------------------------------------------------------------------------------------//

            



            //--------------------------------------------------------------------------------------------------------------------------------//
            //                                                           POTONGAN
            //--------------------------------------------------------------------------------------------------------------------------------//
            //BPJS Ketenagakerjaan
            $qpketp = $this->app->db->prepare("SELECT bpjs_ket_plus  FROM karyawan WHERE id = ?");
            $qpketp->bind_param('s', $nik);
            $qpketp->execute();
            $dqpketp = $qpketp->get_result();
            $dpketp = $dqpketp->fetch_assoc();

            if($dpketp['bpjs_ket_plus']==1){
                $qpket = $this->app->db->prepare("SELECT pt.nominal as bpjs_ket
                                                FROM karyawan k, potongan pt
                                                WHERE k.id = ? AND k.bpjs_ket != ''
                                                AND pt.nm_potongan = 'BPJS Ketenagakerjaan Plus'");
                $qpket->bind_param('s', $nik);
                $qpket->execute();
                $dqpket = $qpket->get_result();
                $dpket = $dqpket->fetch_assoc();
            }else{
                $qpket = $this->app->db->prepare("SELECT pt.nominal as bpjs_ket
                                                FROM karyawan k, potongan pt
                                                WHERE k.id = ? AND k.bpjs_ket != ''
                                                AND pt.nm_potongan = 'BPJS Ketenagakerjaan'");
                $qpket->bind_param('s', $nik);
                $qpket->execute();
                $dqpket = $qpket->get_result();
                $dpket = $dqpket->fetch_assoc();
            }
            if(!empty($dpket)){
                $data[$nik]['potongan']['bpjs_ket'] = $dpket['bpjs_ket'];
            }else{
                $data[$nik]['potongan']['bpjs_ket'] = 0;
            }
            //BPJS Kesehatan
            $qpkes = $this->app->db->prepare("SELECT ps.nominal as bpjs_kes
                                            FROM karyawan k, potongan ps
                                            WHERE k.id = ? AND k.bpjs_kes IS NOT NULL 
                                            AND ps.nm_potongan = 'BPJS Kesehatan' 
                                            AND ps.ket LIKE CONCAT(CASE WHEN k.bpjs_kes != '' THEN '%' ELSE '' END, 
                                            (SELECT kelas FROM karyawan WHERE id=? AND kelas IS NOT NULL) , 
                                            CASE WHEN k.bpjs_kes != '' THEN '%' ELSE '' END)");
            $qpkes->bind_param('ss', $nik, $nik);
            $qpkes->execute();
            $dqpkes = $qpkes->get_result();
            $dpkes = $dqpkes->fetch_assoc();

            if(!empty($dpkes)){
                $data[$nik]['potongan']['bpjs_kes'] = $dpkes['bpjs_kes'];
            }else{
                $data[$nik]['potongan']['bpjs_kes'] = 0;
            }

            //BPJS Kesehatan Plus
            $qpkesjp = $this->app->db->prepare("SELECT bpjs_kes_plus  FROM karyawan WHERE id = ?");
            $qpkesjp->bind_param('s', $nik);
            $qpkesjp->execute();
            $dqpkesjp = $qpkesjp->get_result();
            $dpkesjp = $dqpkesjp->fetch_assoc();

            $data[$nik]['potongan']['jml_bpjs_kes_plus'] = $dpkesjp['bpjs_kes_plus'];

            if($dpkesjp['bpjs_kes_plus']>0){
                $qpkesp = $this->app->db->query("SELECT nominal FROM potongan WHERE nm_potongan = 'BPJS Kesehatan Plus'");
                $dpkesp = $qpkesp->fetch_assoc();

                $data[$nik]['potongan']['bpjs_kes_plus'] = $dpkesp['nominal'];
            }else{
                $data[$nik]['potongan']['bpjs_kes_plus'] = 0;
            }

            $qpkesp = $this->app->db->prepare("SELECT pt.nominal as bpjs_kes_plus
                                            FROM karyawan k, potongan pt
                                            WHERE k.id = ? AND k.bpjs_kes_plus > 0
                                            AND pt.nm_potongan = 'BPJS Kesehatan Plus'");
            $qpkesp->bind_param('s', $nik);
            $qpkesp->execute();
            $dqpkesp = $qpkesp->get_result();
            $dpkesp = $dqpkesp->fetch_assoc();

            if(!empty($dpkesp)){
                $data[$nik]['potongan']['bpjs_kes_plus'] = $dpkesp['bpjs_kes_plus'];
            }else{
                $data[$nik]['potongan']['bpjs_kes_plus'] = 0;
            }

            //KSPN
            $qpkspn = $this->app->db->prepare("SELECT p.nominal as kspn FROM potongan p, karyawan k WHERE k.id = ? 
                                            AND p.nm_potongan LIKE '%KSPN%' 
                                            AND p.ket LIKE CONCAT('%',(SELECT ket_kar FROM karyawan WHERE id=?),'%')
                                            ");
            $qpkspn->bind_param('ss', $nik, $nik);
            $qpkspn->execute();
            $dqkspn = $qpkspn->get_result();
            $dpkspn = $dqkspn->fetch_assoc();

            if(!empty($dpkspn)){
                /*
                if($content[$i]['kd_status']==4){
                    $data[$nik]['potongan']['kspn'] = 0;
                }else{
                    */
                $data[$nik]['potongan']['kspn'] = $dpkspn['kspn'];
                //}
            }else{
                $data[$nik]['potongan']['kspn'] = 0;
            }


            //Lainnya        
            $qplainn = $this->app->db->prepare("SELECT nominal FROM `potongan_lain` WHERE nik = ? AND MONTH(tgl) = MONTH(?)");
            $qplainn->bind_param('ss', $nik, $datar['tgl_ak']);
            $qplainn->execute();
            $dqplainn = $qplainn->get_result();
            $dplainn = $dqplainn->fetch_assoc();

            if(!empty($dplainn)){
                $data[$nik]['potongan']['lainnya'] = $dplainn['nominal'];
            }else{
                $data[$nik]['potongan']['lainnya'] = 0;
            }

            //var_dump($data[$nik]['potongan']['lainnya']); echo '<br><br>'; die();
            
            //Koperasi            
            $qpkop = $this->app->db->prepare("SELECT nominal FROM `koperasi` WHERE nik = ? 
                                              AND MONTH(tgl) = MONTH(?) AND YEAR(tgl) = YEAR(?)");
            $qpkop->bind_param('sss', $nik, $datar['tgl_ak'], $datar['tgl_ak']);
            $qpkop->execute();
            $dqpkop = $qpkop->get_result();
            $dpkop = $dqpkop->fetch_assoc();

            if(!empty($dpkop)){
                $data[$nik]['potongan']['koperasi'] = $dpkop['nominal'];
            }else{
                $data[$nik]['potongan']['koperasi'] = 0;
            }

            //SPT Karyawan dan Perusahaan
            if(!empty($spt)){
                $dspt = '';
                $qspt = $this->app->db->prepare("SELECT nominal, spt_pers FROM `spt` WHERE nik=? ORDER BY berlaku DESC");
                $qspt->bind_param('s', $nik);
                $qspt->execute();
                $dqspt = $qspt->get_result();
                $dspt = $dqspt->fetch_assoc();

                if(!empty($dspt)){
                    $sspt = $dspt['nominal'];
                    $ssptp = $dspt['spt_pers'];
                }else{
                    $sspt = 0;
                    $ssptp = 0;
                }

                $data[$nik]['potongan']['spt_karyawan'] = $sspt;
                $data[$nik]['potongan']['spt_perusahaan'] = $ssptp;
            }else{
                $data[$nik]['potongan']['spt_karyawan'] = 0;
                $data[$nik]['potongan']['spt_perusahaan'] = 0;
            }
            //--------------------------------------------------------------------------------------------------------------------------------//
            

        }
        //print_r($data);die();
        return $data;
    }

    public function getTahun()
    {
        $stmt = $this->app->db->query("SELECT DISTINCT tahun FROM absensi_kalkulasi"); 
        $content = [];
        while($c = $stmt->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }


    public function getTahunTHR()
    {
        $stmt = $this->app->db->query("SELECT DISTINCT tahun FROM kalkulasi_thr"); 
        $content = [];
        while($c = $stmt->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }


    public function getTahunTA()
    {
        $stmt = $this->app->db->query("SELECT DISTINCT tahun FROM kalkulasi_ta"); 
        $content = [];
        while($c = $stmt->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }

    public function getFullData($thn, $bln=null, $dep=null, $bag=null)
    {
        $where = '';
        if(empty($bln) && empty($dep) && empty($bag)){
            $where = ' WHERE a.tahun = "'.$thn.'" ';
        }elseif(!empty($bln) && empty($dep) && empty($bag)){
            $where = ' WHERE a.tahun = "'.$thn.'" AND a.bulan = "'.$bln.'" ';
        }elseif(!empty($bln) && !empty($dep) && empty($bag)){
            $where = ' WHERE a.tahun = "'.$thn.'" AND a.bulan = "'.$bln.'" AND a.kd_departemen = "'.$dep.'" ';
        }elseif(!empty($bln) && !empty($dep) && !empty($bag)){
            $where = ' WHERE a.tahun = "'.$thn.'" AND a.bulan = "'.$bln.'" AND a.kd_departemen = "'.$dep.'" AND a.kd_bagian = "'.$bag.'" ';
        }
        $stmt = $this->app->db->query("SELECT a.*, k.nama, k.tgl_in, k.tgl_angkat, d.nm_departemen, b.nm_bagian
                                       FROM absensi_kalkulasi a 
                                       JOIN karyawan k ON k.id = a.nik
                                       JOIN departemen d ON d.id = a.kd_departemen
                                       JOIN bagian b ON b.id = a.kd_bagian
                                       $where
                                       ORDER BY a.bulan, d.id, b.id, k.grup, k.libur, 
                                       a.nt_jabatan DESC,
                                       a.nik ASC");
        $content = [];
        while($c = $stmt->fetch_assoc()) {
            $content[] = $c;
        }

        $stmtr = $this->app->db->query("SELECT a.*, k.nama, k.tgl_in, k.tgl_angkat, d.nm_departemen, b.nm_bagian
                                       FROM absensi_kalkulasi a 
                                       JOIN resign k ON k.id = a.nik
                                       JOIN departemen d ON d.id = a.kd_departemen
                                       JOIN bagian b ON b.id = a.kd_bagian
                                       $where
                                       ORDER BY a.bulan, d.id, b.id, k.grup, k.libur, 
                                       a.nt_jabatan DESC,
                                       a.nik ASC");
        while($cr = $stmtr->fetch_assoc()) {
            $content[] = $cr;
        }

        return $content;
    }

    public function cekGaji($bln, $thn)
    {
        $qs = $this->app->db->prepare("SELECT DISTINCT bln FROM gaji_bruto WHERE bln = ? AND thn = ?");
        $qs->bind_param('ss', $bln, $thn);
        $qs->execute();
        $resqs = $qs->get_result();
        $qsc = $resqs->fetch_assoc();

        if(!empty($qsc)){
            $del = $this->app->db->prepare("DELETE FROM `gaji_bruto` WHERE `bln` = ? AND `thn` = ?");
            $del->bind_param('ss', $bln, $thn);
            $del->execute();
        }

        $qsk = $this->app->db->prepare("SELECT DISTINCT bln FROM gaji_kar WHERE bln = ? AND thn = ?");
        $qsk->bind_param('ss', $bln, $thn);
        $qsk->execute();
        $resqsk = $qsk->get_result();
        $qskc = $resqsk->fetch_assoc();

        if(!empty($qskc)){
            $delk = $this->app->db->prepare("DELETE FROM `gaji_kar` WHERE `bln` = ? AND `thn` = ?");
            $delk->bind_param('ss', $bln, $thn);
            $delk->execute();
        }

        $qsr = $this->app->db->prepare("SELECT DISTINCT bulan FROM rekap_lembur WHERE bulan = ? AND tahun = ?");
        $qsr->bind_param('ss', $bln, $thn);
        $qsr->execute();
        $resqsr = $qsr->get_result();
        $qsrc = $resqsr->fetch_assoc();

        if(!empty($qsrc)){
            $delr = $this->app->db->prepare("DELETE FROM `rekap_lembur` WHERE `bulan` = ? AND `tahun` = ?");
            $delr->bind_param('ss', $bln, $thn);
            $delr->execute();
        }
    }

    public function addGaji($d)
    {
        //cari jumlah grup di produksi
        $sjgpro = $this->app->db->prepare("SELECT DISTINCT k.jml_grup 
                                          FROM absensi_kalkulasi a
                                          JOIN karyawan k ON k.id = a.nik
                                          WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 95
                                        ");
        $sjgpro->bind_param('ss', $d['bulan'], $d['tahun']);
        $sjgpro->execute();
        $dsjgpro = $sjgpro->get_result();
        $djgpro = $dsjgpro->fetch_assoc();

        for ($i='A'; $i <= chr(64 + $djgpro['jml_grup']); $i++) { 
            //$g[] = $i;
            //cari total semua per grup PRODUKSI shift
            $sqlp = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                    IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                    IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                    IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                    IFNULL(sum(a.t_lembur),0) as t_lembur, 
                    IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                    IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                    IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                    FROM absensi_kalkulasi a
                    JOIN karyawan k ON k.id = a.nik
                    WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 95 AND k.jml_grup = ? AND k.grup = ?");
            $sqlp->bind_param('ssss', $d['bulan'], $d['tahun'], $djgpro['jml_grup'], $i);
            $sqlp->execute();
            $dsqlp = $sqlp->get_result();
            $dp = $dsqlp->fetch_assoc();

            $data[] = array_merge(array('departemen'=>'produksi', 'grup' => $i), $dp);
        }

        //cari total semua PRODUKSI dayshift
        $sqlpn = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian != 95 AND k.kd_bagian != 115");
        $sqlpn->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlpn->execute();
        $dsqlpn = $sqlpn->get_result();
        $dpn = $dsqlpn->fetch_assoc();

        $sqlmtc = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 15");
        $sqlmtc->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlmtc->execute();
        $dsqlmtc = $sqlmtc->get_result();
        $dmtc = $dsqlmtc->fetch_assoc();

        $dtpd = array('jml_l' => $dpn['jml_l']+$dmtc['jml_l'], 'jml_p' => $dpn['jml_p']+$dmtc['jml_p'], 'gp' => $dpn['gp']+$dmtc['gp'], 
                    'mk' => $dpn['mk']+$dmtc['mk'], 'insentif' => $dpn['insentif']+$dmtc['insentif'], 
                    'n_lembur' => $dpn['n_lembur']+$dmtc['n_lembur'], 'premi' => $dpn['premi']+$dmtc['premi'], 
                    'tjab' => $dpn['tjab']+$dmtc['tjab'], 'tahli' => $dpn['tahli']+$dmtc['tahli'], 't_pres' => $dpn['t_pres']+$dmtc['t_pres'], 
                    'gaji_bruto' => $dpn['gaji_bruto']+$dmtc['gaji_bruto'],'t_lembur' => $dpn['t_lembur']+$dmtc['t_lembur'],
                    'bpjs_ket' => $dpn['bpjs_ket']+$dmtc['bpjs_ket'], 'bpjs_kes' => $dpn['bpjs_kes']+$dmtc['bpjs_kes'],
                    'lain' => $dpn['lain']+$dmtc['lain'], 'kspn' => $dpn['kspn']+$dmtc['kspn'], 'alpha' => $dpn['alpha']+$dmtc['alpha'],
                    'psw' => $dpn['psw']+$dmtc['psw'], 'spt' => $dpn['spt']+$dmtc['spt'], 'koperasi' => $dpn['koperasi']+$dmtc['koperasi'],
                    't_gaji' => $dpn['t_gaji']+$dmtc['t_gaji'],'rumah' => $dpn['rumah']+$dmtc['rumah'],'tmakan' => $dpn['tmakan']+$dmtc['tmakan']
                );


        $data[] = array_merge(array('departemen'=>'produksi n', 'grup' => 'N'), $dtpd);


        //cari total UTILITY SHIFT
        $sqlus = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 110 ");
        $sqlus->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlus->execute();
        $dsqlus = $sqlus->get_result();
        $dus = $dsqlus->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'utility', 'grup' => 'S'), $dus);

        //cari total UTILITY N
        $sqlun = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 111 ");
        $sqlun->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlun->execute();
        $dsqlun = $sqlun->get_result();
        $dun = $dsqlun->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'utility', 'grup' => 'N'), $dun);

        //cari total PEMASARAN GUDANG        
        $sqlpg = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 11 ");
        $sqlpg->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlpg->execute();
        $dsqlpg = $sqlpg->get_result();
        $dpg = $dsqlpg->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'pemasaran gudang', 'grup' => 'N'), $dpg);

        //cari total PEMASARAN ADM   
        $sqlpa = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 10 ");
        $sqlpa->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlpa->execute();
        $dsqlpa = $sqlpa->get_result();
        $dpa = $dsqlpa->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'pemasaran adm', 'grup' => 'N'), $dpa);

        //cari total KEUANGAN
        $sqlkeu = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 1");
        $sqlkeu->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlkeu->execute();
        $dsqlkeu = $sqlkeu->get_result();
        $dkeu = $dsqlkeu->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'keuangan', 'grup' => 'N'), $dkeu);

        //cari total LOGISTIK GUDANG
        $sqllog = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 6 AND k.kd_bagian = 9 ");
        $sqllog->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqllog->execute();
        $dsqllog = $sqllog->get_result();
        $dlog = $dsqllog->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'logistik gudang', 'grup' => 'N'), $dlog);
        
        //cari total SIPIL
        $sqlsip = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 108 ");
        $sqlsip->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlsip->execute();
        $dsqlsip = $sqlsip->get_result();
        $dsip = $dsqlsip->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'sipil', 'grup' => 'N'), $dsip);

        //cari total KEAMANAN
        $sqlkea = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 104 ");
        $sqlkea->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlkea->execute();
        $dsqlkea = $sqlkea->get_result();
        $dkea = $dsqlkea->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'keamanan', 'grup' => 'N'), $dkea);

        //cari total EXIM
        $sqlex = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 115 ");
        $sqlex->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlex->execute();
        $dsqlex = $sqlex->get_result();
        $dex = $dsqlex->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'exim', 'grup' => 'N'), $dex);

        //cari total E.D.P
        $sqledp = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 116 ");
        $sqledp->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqledp->execute();
        $dsqledp = $sqledp->get_result();
        $dedp = $dsqledp->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'e.d.p', 'grup' => 'N'), $dedp);

        //cari total HRD
        $sqlhr = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 3 AND k.kd_bagian = 6 ");
        $sqlhr->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlhr->execute();
        $dsqlhr = $sqlhr->get_result();
        $dhr = $dsqlhr->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'personalia', 'grup' => 'N'), $dhr);

        //cari total UMUM
        $sqlum = $this->app->db->prepare("SELECT IFNULL(sum(k.jk='L'),0) as jml_l, IFNULL(sum(k.jk='P'),0) as jml_p, 
                IFNULL(sum(a.gaji),0) as gp, IFNULL(sum(a.nt_masakerja),0) as mk, IFNULL(sum(a.nt_insentif),0) as insentif, 
                IFNULL(sum(a.nt_lembur),0) as n_lembur, IFNULL(sum(a.nt_premi),0) as premi, IFNULL(sum(a.nt_jabatan),0) as tjab, 
                IFNULL(sum(a.nt_keahlian),0) as tahli, IFNULL(sum(a.nt_prestasi),0) as t_pres, IFNULL(sum(a.gaji_kotor),0) as gaji_bruto,
                IFNULL(sum(a.t_lembur),0) as t_lembur, 
                IFNULL(sum(a.n_bpjs_ket),0) as bpjs_ket, IFNULL(sum(a.n_bpjs_kes),0) as bpjs_kes, IFNULL(sum(a.tn_lain),0) as lain, 
                IFNULL(sum(a.n_kspn),0) as kspn, IFNULL(sum(a.n_ai),0) as alpha, IFNULL(sum(a.n_psw),0) as psw, 
                IFNULL(sum(a.n_spt),0) as spt, IFNULL(sum(a.n_koperasi),0) as koperasi, IFNULL(sum(total_gaji),0) as t_gaji,
                    IFNULL(sum(a.n_rumah),0) as rumah, IFNULL(sum(a.u_makan),0) as tmakan
                FROM absensi_kalkulasi a
                JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 
                AND k.kd_bagian != 116 AND k.kd_bagian != 108 AND k.kd_bagian != 104");
        $sqlum->bind_param('ss', $d['bulan'], $d['tahun']);
        $sqlum->execute();
        $dsqlum = $sqlum->get_result();
        $dum = $dsqlum->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'umum', 'grup' => 'N'), $dum);

        //print_r($data); die();
        $a=0;
        for ($i=0; $i < count($data); $i++) { 
            
            $jml_kar = $data[$i]['jml_p']+$data[$i]['jml_l'];

            $sqlgk = $this->app->db->prepare("INSERT INTO gaji_bruto (bln, thn, dept, grup, jml_kar, gp, masakerja, insentif, lembur, premi, tjab, tahli, tprestasi, gj_bruto, jml_lembur, u_makan) 
                                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $sqlgk->bind_param('ssssssssssssssss',
                                $d['bulan'], 
                                $d['tahun'],
                                $data[$i]['departemen'],
                                $data[$i]['grup'],
                                $jml_kar,
                                $data[$i]['gp'],
                                $data[$i]['mk'],
                                $data[$i]['insentif'],
                                $data[$i]['n_lembur'],
                                $data[$i]['premi'],
                                $data[$i]['tjab'],
                                $data[$i]['tahli'],
                                $data[$i]['t_pres'],
                                $data[$i]['gaji_bruto'],
                                $data[$i]['t_lembur'],
                                $data[$i]['tmakan']
                            );

            $sqlgk->execute();
            $sqlgk->store_result();

            if($sqlgk->affected_rows == 1){ 
                
                $gaji_t = $data[$i]['gaji_bruto']-($data[$i]['bpjs_ket']+$data[$i]['bpjs_kes']+$data[$i]['alpha']+$data[$i]['psw']+$data[$i]['rumah']);
                //$tgaji = $gaji_t - ($data[$i]['spt']+$data[$i]['kspn']+$data[$i]['lain']+$data[$i]['koperasi']);
                empty($data[$i]['t_gaji']) ? $tgaji = 0 : $tgaji = $data[$i]['t_gaji'];//pembulatan($data[$i]['t_gaji']);

                $sqlg = $this->app->db->prepare("INSERT INTo gaji_kar (bln, thn, dept, grup, jml_l, jml_p, gaji_bruto, bpjs_ket, bpjs_kes, absen, psw, rumah, gaji_terima, spt, kspn, lain, koperasi, gaji_bersih) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $sqlg->bind_param('ssssssssssssssssss',
                                $d['bulan'], 
                                $d['tahun'],
                                $data[$i]['departemen'],
                                $data[$i]['grup'],
                                $data[$i]['jml_l'],
                                $data[$i]['jml_p'],
                                $data[$i]['gaji_bruto'],
                                $data[$i]['bpjs_ket'],
                                $data[$i]['bpjs_kes'],
                                $data[$i]['alpha'],
                                $data[$i]['psw'],
                                $data[$i]['rumah'],
                                $gaji_t,
                                $data[$i]['spt'],
                                $data[$i]['kspn'],
                                $data[$i]['lain'],
                                $data[$i]['koperasi'],
                                $tgaji
                                );
                $sqlg->execute();
                $sqlg->store_result();

                if($sqlg->affected_rows == 1){
                    $a += 1;
                }
            }
        }
        if($a==count($data)){
            return true;
        }else{
            return false;
        }
    }

    public function getRekapPot($thn, $bln)
    {
        $stmt = $this->app->db->prepare("SELECT * FROM gaji_bruto WHERE thn = ? AND bln = ?");
        $stmt->bind_param('ss', $thn, $bln);
        $stmt->execute();
        $res = $stmt->get_result();

        $content = [];
        while($c = $res->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }

    public function getRekapGaji($thn, $bln)
    {
        $stmt = $this->app->db->prepare("SELECT * FROM gaji_kar WHERE thn = ? AND bln = ?");
        $stmt->bind_param('ss', $thn, $bln);
        $stmt->execute();
        $res = $stmt->get_result();

        $content = [];
        while($c = $res->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }

    public function getRekapTHRTA($thn, $kode)
    {
        $tabel = 'rekap_'.$kode;
        $stmt = $this->app->db->prepare("SELECT * FROM `$tabel` WHERE tahun = ?");
        $stmt->bind_param('s', $thn);
        $stmt->execute();
        $res = $stmt->get_result();

        $content = [];
        while($c = $res->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }

    public function getMasterGaji($thn, $bln)
    {
        $stmt = $this->app->db->prepare("SELECT k.nama, k.id, k.grup, k.libur, b.nm_bagian, k.tgl_lhr, k.tgl_in, k.tgl_angkat, k.gaji, 
                                        a.nt_masakerja, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.n_spt, a.n_spt_p, a.u_makan
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE a.tahun = ? AND a.bulan = ?
                                        ORDER BY a.kd_departemen, a.kd_bagian, k.bagian, k.grup, k.libur, k.id ASC");
        $stmt->bind_param('ss', $thn, $bln);
        $stmt->execute();
        $res = $stmt->get_result();

        $content = [];
        while($c = $res->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }

    public function getMasterThrTa($thn, $kode)
    {
        $tabel = 'kalkulasi_'.$kode;
        $stmt = $this->app->db->prepare("SELECT k.nama, k.id, k.grup, b.nm_bagian, k.libur, a.gaji, a.gaji_kotor, 
                                        a.t_masakerja, a.t_jabatan, a.t_keahlian, a.t_prestasi,  a.rule, a.total
                                        FROM `$tabel` a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE a.tahun = ?
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.bagian, k.grup, k.libur, k.id ASC");
        $stmt->bind_param('s', $thn);
        $stmt->execute();
        $res = $stmt->get_result();

        $content = [];
        while($c = $res->fetch_assoc()) {
            $content[] = $c;
        }

        return $content;
    }

    public function getRincian($thn, $bln)
    {
        $data = [];
        $bulan = $bln;
        $bulann = $bln - 1;
        if($bulann == 0){
          $bulann = 12;
          $thnn = $thn - 1;
        }else{
          $thnn = $thn;
        }
        $tgl_aw = date($thnn.'-'.$bulann.'-21');
        $tgl_ak = date($thn.'-'.$bulan.'-20');
        $jmlhari_bln = get_object_vars(date_diff(date_create($tgl_aw), date_create($tgl_ak)));

        //cari jumlah grup
        $sjgpro = $this->app->db->prepare("SELECT DISTINCT k.jml_grup FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                                          WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 95");
        $sjgpro->bind_param('ss', $bln, $thn);
        $sjgpro->execute();
        $dsjgpro = $sjgpro->get_result();
        $djgpro = $dsjgpro->fetch_assoc();

        //cari total semua per grup PRODUKSI shift
        for ($i='A'; $i <= chr(64 + $djgpro['jml_grup']); $i++) { 
            $sqlp = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, 
                    a.tarif_l, 
                    a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                    a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                    FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                    WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 95 AND k.jml_grup = ? AND k.grup = ?
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
            $sqlp->bind_param('ssss', $bln, $thn, $djgpro['jml_grup'], $i);
            $sqlp->execute();
            $dsqlp = $sqlp->get_result();
            $c = [];
            while($dp = $dsqlp->fetch_assoc()){
                $c[] = $dp;
            }
            $data[] = array('bagian'=>'produksi', 'grup'=> $i, 'data' => $c);
        }

        //cari total semua PRODUKSI dayshift
        $sqlpn = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 80
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlpn->bind_param('ss', $bln, $thn);
        $sqlpn->execute();
        $dsqlpn = $sqlpn->get_result();
        $c = [];
        while($dpn = $dsqlpn->fetch_assoc()){
            $c[] = $dpn;
        }
        $data[] = array('bagian'=>'adm produksi', 'grup'=> 'N', 'data' => $c);

        //cari total semua MAINTENANCE
        $sqlmtc = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 15
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlmtc->bind_param('ss', $bln, $thn);
        $sqlmtc->execute();
        $dsqlmtc = $sqlmtc->get_result();
        $c = [];
        while($dmtc = $dsqlmtc->fetch_assoc()){
            $c[] = $dmtc;
        }
        $data[] = array('bagian'=>'maintenance', 'grup'=> 'N', 'data' => $c);

        //cari total semua QC/PACK/CC/B
        $sqlqc = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 98
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlqc->bind_param('ss', $bln, $thn);
        $sqlqc->execute();
        $dsqlqc = $sqlqc->get_result();
        $c = [];
        while($dqc = $dsqlqc->fetch_assoc()){
            $c[] = $dqc;
        }
        $data[] = array('bagian'=>'QC/PACK/CC/B STORE', 'grup'=> 'N', 'data' => $c);

        //cari total semua PEMASARAN GUDANG
        $sqlpg = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 11
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlpg->bind_param('ss', $bln, $thn);
        $sqlpg->execute();
        $dsqlpg = $sqlpg->get_result();
        $c = [];
        while($dpg = $dsqlpg->fetch_assoc()){
            $c[] = $dpg;
        }
        $data[] = array('bagian'=>'pemasaran gudang', 'grup'=> 'N', 'data' => $c);

        //cari total semua LOGISTIK GUDANG
        $sqllg = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan 
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 6 AND k.kd_bagian = 9
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqllg->bind_param('ss', $bln, $thn);
        $sqllg->execute();
        $dsqllg = $sqllg->get_result();
        $c = [];
        while($dlg = $dsqllg->fetch_assoc()){
            $c[] = $dlg;
        }
        $data[] = array('bagian'=>'logistik gudang', 'grup'=> 'N', 'data' => $c);

        //cari total semua EXIM
        $sqlex = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan 
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 115
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlex->bind_param('ss', $bln, $thn);
        $sqlex->execute();
        $dsqlex = $sqlex->get_result();
        $c = [];
        while($dex = $dsqlex->fetch_assoc()){
            $c[] = $dex;
        }
        $data[] = array('bagian'=>'exim', 'grup'=> 'N', 'data' => $c);

        //cari total semua UTILITY SHIFT
        $sqlus = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 110
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlus->bind_param('ss', $bln, $thn);
        $sqlus->execute();
        $dsqlus = $sqlus->get_result();
        $c = [];
        while($dus = $dsqlus->fetch_assoc()){
            $c[] = $dus;
        }
        $data[] = array('bagian'=>'utility', 'grup'=> 'S', 'data' => $c);

        //cari total semua UTILITY N
        $sqlun = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 111
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlun->bind_param('ss', $bln, $thn);
        $sqlun->execute();
        $dsqlun = $sqlun->get_result();
        $c = [];
        while($dun = $dsqlun->fetch_assoc()){
            $c[] = $dun;
        }
        $data[] = array('bagian'=>'utility', 'grup'=> 'N', 'data' => $c);

        //cari total semua Pemasaran Adm
        $sqlpa = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 10
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlpa->bind_param('ss', $bln, $thn);
        $sqlpa->execute();
        $dsqlpa = $sqlpa->get_result();
        $c = [];
        while($dpa = $dsqlpa->fetch_assoc()){
            $c[] = $dpa;
        }
        $data[] = array('bagian'=>'pemasaran adm', 'grup'=> 'N', 'data' => $c);

        //cari total semua Keuangan
        $sqlkeu = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 1
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlkeu->bind_param('ss', $bln, $thn);
        $sqlkeu->execute();
        $dsqlkeu = $sqlkeu->get_result();
        $c = [];
        while($dkeu = $dsqlkeu->fetch_assoc()){
            $c[] = $dkeu;
        }
        $data[] = array('bagian'=>'keuangan', 'grup'=> 'N', 'data' => $c);

        //cari total semua UMUM
        $sqlum = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 105
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlum->bind_param('ss', $bln, $thn);
        $sqlum->execute();
        $dsqlum = $sqlum->get_result();
        $c = [];
        while($dum = $dsqlum->fetch_assoc()){
            $c[] = $dum;
        }
        $sqlumu = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 101
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlumu->bind_param('ss', $bln, $thn);
        $sqlumu->execute();
        $dsqlumu = $sqlumu->get_result();
        
        while($dumu = $dsqlumu->fetch_assoc()){
            $c[] = $dumu;
        }
        $sqlumum = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 117
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlumum->bind_param('ss', $bln, $thn);
        $sqlumum->execute();
        $dsqlumum = $sqlumum->get_result();
        
        while($dumum = $dsqlumum->fetch_assoc()){
            $c[] = $dumum;
        }
        $data[] = array('bagian'=>'umum', 'grup'=> 'N', 'data' => $c);

        //cari total semua HRD
        $sqlhr = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 3 AND k.kd_bagian = 6
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlhr->bind_param('ss', $bln, $thn);
        $sqlhr->execute();
        $dsqlhr = $sqlhr->get_result();
        $c = [];
        while($dhr = $dsqlhr->fetch_assoc()){
            $c[] = $dhr;
        }
        $data[] = array('bagian'=>'personalia', 'grup'=> 'N', 'data' => $c);

        //cari total semua sipil
        $sqlsip = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 108
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlsip->bind_param('ss', $bln, $thn);
        $sqlsip->execute();
        $dsqlsip = $sqlsip->get_result();
        $c = [];
        while($dsip = $dsqlsip->fetch_assoc()){
            $c[] = $dsip;
        }
        $data[] = array('bagian'=>'sipil', 'grup'=> 'N', 'data' => $c);

        //cari total semua keamanan
        $sqlkea = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 104
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqlkea->bind_param('ss', $bln, $thn);
        $sqlkea->execute();
        $dsqlkea = $sqlkea->get_result();
        $c = [];
        while($dkea = $dsqlkea->fetch_assoc()){
            $c[] = $dkea;
        }
        $data[] = array('bagian'=>'keamanan', 'grup'=> 'N', 'data' => $c);

        //cari total semua edp
        $sqledp = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.nt_masakerja, a.nt_insentif, a.t_lembur, a.tarif_l, 
                a.nt_lembur, a.nt_premi, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.gaji_kotor, a.n_bpjs_ket, a.n_bpjs_kes, a.n_spt, 
                a.n_kspn, a.n_ai, a.n_psw, a.n_koperasi, a.tn_lain, a.total_gaji, a.alpha, a.ijin, a.sdr, a.pijin, k.tgl_angkat,
                    a.rumah, a.n_rumah, a.cuti, a.cuti_h, a.u_makan
                FROM absensi_kalkulasi a JOIN karyawan k ON k.id = a.nik
                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 116
                    ORDER BY a.nt_jabatan DESC, a.nik ASC");
        $sqledp->bind_param('ss', $bln, $thn);
        $sqledp->execute();
        $dsqledp = $sqledp->get_result();
        $c = [];
        while($dedp = $dsqledp->fetch_assoc()){
            $c[] = $dedp;
        }
        $data[] = array('bagian'=>'e.d.p', 'grup'=> 'N', 'data' => $c);

        
        return $data;
    }

    public function getRincianThrTa($thn, $tbl)
    {
        $tabel = 'kalkulasi_'.$tbl;
        $data = [];

        //cari jumlah grup
        $sjgpro = $this->app->db->prepare("SELECT DISTINCT k.jml_grup FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                          WHERE a.tahun = ? AND k.kd_bagian = 95");
        $sjgpro->bind_param('s', $thn);
        $sjgpro->execute();
        $dsjgpro = $sjgpro->get_result();
        $djgpro = $dsjgpro->fetch_assoc();

        //cari total semua per grup PRODUKSI shift
        for ($i='A'; $i <= chr(64 + $djgpro['jml_grup']); $i++) { 
            $sqlp = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                            a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                            FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                            WHERE a.tahun = ? AND k.kd_bagian = 95 AND k.jml_grup = ? AND k.grup = ?
                                            ORDER BY a.t_jabatan DESC, a.nik ASC");
            $sqlp->bind_param('sss', $thn, $djgpro['jml_grup'], $i);
            $sqlp->execute();
            $dsqlp = $sqlp->get_result();
            $c = [];
            while($dp = $dsqlp->fetch_assoc()){
                $c[] = $dp;
            }
            $data[] = array('bagian'=>'produksi', 'grup'=> $i, 'data' => $c);
        }

        //cari total semua PRODUKSI dayshift
        $sqlpn = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 80
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlpn->bind_param('s', $thn);
        $sqlpn->execute();
        $dsqlpn = $sqlpn->get_result();
        $c = [];
        while($dpn = $dsqlpn->fetch_assoc()){
            $c[] = $dpn;
        }
        $data[] = array('bagian'=>'adm produksi', 'grup'=> 'N', 'data' => $c);

        //cari total semua MAINTENANCE
        $sqlmtc = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 15
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlmtc->bind_param('s', $thn);
        $sqlmtc->execute();
        $dsqlmtc = $sqlmtc->get_result();
        $c = [];
        while($dmtc = $dsqlmtc->fetch_assoc()){
            $c[] = $dmtc;
        }
        $data[] = array('bagian'=>'maintenance', 'grup'=> 'N', 'data' => $c);

        //cari total semua QC/PACK/CC/B
        $sqlqc = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 98
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlqc->bind_param('s', $thn);
        $sqlqc->execute();
        $dsqlqc = $sqlqc->get_result();
        $c = [];
        while($dqc = $dsqlqc->fetch_assoc()){
            $c[] = $dqc;
        }
        $data[] = array('bagian'=>'QC/PACK/CC/B STORE', 'grup'=> 'N', 'data' => $c);

        //cari total semua PEMASARAN GUDANG
        $sqlpg = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 11
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlpg->bind_param('s', $thn);
        $sqlpg->execute();
        $dsqlpg = $sqlpg->get_result();
        $c = [];
        while($dpg = $dsqlpg->fetch_assoc()){
            $c[] = $dpg;
        }
        $data[] = array('bagian'=>'pemasaran gudang', 'grup'=> 'N', 'data' => $c);

        //cari total semua LOGISTIK GUDANG
        $sqllg = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total 
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 6 AND k.kd_bagian = 9
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqllg->bind_param('s', $thn);
        $sqllg->execute();
        $dsqllg = $sqllg->get_result();
        $c = [];
        while($dlg = $dsqllg->fetch_assoc()){
            $c[] = $dlg;
        }
        $data[] = array('bagian'=>'logistik gudang', 'grup'=> 'N', 'data' => $c);

        //cari total semua EXIM
        $sqlex = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total 
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 115
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlex->bind_param('s', $thn);
        $sqlex->execute();
        $dsqlex = $sqlex->get_result();
        $c = [];
        while($dex = $dsqlex->fetch_assoc()){
            $c[] = $dex;
        }
        $data[] = array('bagian'=>'exim', 'grup'=> 'N', 'data' => $c);

        //cari total semua UTILITY SHIFT
        $sqlus = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 110
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlus->bind_param('s', $thn);
        $sqlus->execute();
        $dsqlus = $sqlus->get_result();
        $c = [];
        while($dus = $dsqlus->fetch_assoc()){
            $c[] = $dus;
        }
        $data[] = array('bagian'=>'utility', 'grup'=> 'S', 'data' => $c);

        //cari total semua UTILITY N
        $sqlun = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 111
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlun->bind_param('s', $thn);
        $sqlun->execute();
        $dsqlun = $sqlun->get_result();
        $c = [];
        while($dun = $dsqlun->fetch_assoc()){
            $c[] = $dun;
        }
        $data[] = array('bagian'=>'utility', 'grup'=> 'N', 'data' => $c);

        //cari total semua Pemasaran Adm
        $sqlpa = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 10
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlpa->bind_param('s', $thn);
        $sqlpa->execute();
        $dsqlpa = $sqlpa->get_result();
        $c = [];
        while($dpa = $dsqlpa->fetch_assoc()){
            $c[] = $dpa;
        }
        $data[] = array('bagian'=>'pemasaran adm', 'grup'=> 'N', 'data' => $c);

        //cari total semua Keuangan
        $sqlkeu = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 1
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlkeu->bind_param('s', $thn);
        $sqlkeu->execute();
        $dsqlkeu = $sqlkeu->get_result();
        $c = [];
        while($dkeu = $dsqlkeu->fetch_assoc()){
            $c[] = $dkeu;
        }
        $data[] = array('bagian'=>'keuangan', 'grup'=> 'N', 'data' => $c);

        //cari total semua UMUM
        $sqlum = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 105
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlum->bind_param('s', $thn);
        $sqlum->execute();
        $dsqlum = $sqlum->get_result();
        $c = [];
        while($dum = $dsqlum->fetch_assoc()){
            $c[] = $dum;
        }
        $sqlumu = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 101
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlumu->bind_param('s', $thn);
        $sqlumu->execute();
        $dsqlumu = $sqlumu->get_result();
        
        while($dumu = $dsqlumu->fetch_assoc()){
            $c[] = $dumu;
        }
        $sqlumum = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 117
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlumum->bind_param('s', $thn);
        $sqlumum->execute();
        $dsqlumum = $sqlumum->get_result();
        
        while($dumum = $dsqlumum->fetch_assoc()){
            $c[] = $dumum;
        }
        $data[] = array('bagian'=>'umum', 'grup'=> 'N', 'data' => $c);

        //cari total semua HRD
        $sqlhr = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 3 AND k.kd_bagian = 6
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlhr->bind_param('s', $thn);
        $sqlhr->execute();
        $dsqlhr = $sqlhr->get_result();
        $c = [];
        while($dhr = $dsqlhr->fetch_assoc()){
            $c[] = $dhr;
        }
        $data[] = array('bagian'=>'personalia', 'grup'=> 'N', 'data' => $c);

        //cari total semua sipil
        $sqlsip = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 108
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlsip->bind_param('s', $thn);
        $sqlsip->execute();
        $dsqlsip = $sqlsip->get_result();
        $c = [];
        while($dsip = $dsqlsip->fetch_assoc()){
            $c[] = $dsip;
        }
        $data[] = array('bagian'=>'sipil', 'grup'=> 'N', 'data' => $c);

        //cari total semua keamanan
        $sqlkea = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 104
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqlkea->bind_param('s', $thn);
        $sqlkea->execute();
        $dsqlkea = $sqlkea->get_result();
        $c = [];
        while($dkea = $dsqlkea->fetch_assoc()){
            $c[] = $dkea;
        }
        $data[] = array('bagian'=>'keamanan', 'grup'=> 'N', 'data' => $c);

        //cari total semua edp
        $sqledp = $this->app->db->prepare("SELECT a.nik, k.nama, k.tgl_in, a.gaji, a.t_masakerja, a.t_jabatan, a.t_keahlian, 
                                        a.t_prestasi, a.gaji_kotor, k.tgl_angkat, a.rule, a.total
                                        FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                                        WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 116
                                        ORDER BY a.t_jabatan DESC, a.nik ASC");
        $sqledp->bind_param('s', $thn);
        $sqledp->execute();
        $dsqledp = $sqledp->get_result();
        $c = [];
        while($dedp = $dsqledp->fetch_assoc()){
            $c[] = $dedp;
        }
        $data[] = array('bagian'=>'e.d.p', 'grup'=> 'N', 'data' => $c);

        
        return $data;
    }

    public function addRekap($bln, $thn)
    {
        $a=0;
        //cari jumlah grup di produksi
        $sjgpro = $this->app->db->prepare("SELECT DISTINCT k.jml_grup 
                                          FROM absensi_kalkulasi a
                                          JOIN karyawan k ON k.id = a.nik
                                          WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 95
                                        ");
        $sjgpro->bind_param('ss', $bln, $thn);
        $sjgpro->execute();
        $dsjgpro = $sjgpro->get_result();
        $djgpro = $dsjgpro->fetch_assoc();


        $res = $this->app->db->query("SELECT b.*, d.nm_departemen FROM bagian b JOIN departemen d ON d.id = b.kd_departemen 
                                      ORDER BY d.id, b.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $co[] = $c;
        }

        $data = [];
        for ($i=0; $i < count($co); $i++) { 
            $sd = $this->app->db->prepare("SELECT k.grup, 
                                        IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                                        IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                                        IFNULL(SUM(a.nt_lembur),0) as tlembur,
                                        IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                                        IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                                        FROM absensi_kalkulasi a 
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        JOIN karyawan k ON k.id = a.nik
                                        WHERE a.bulan = ? AND a.tahun = ? AND a.kd_bagian = ?");
            $sd->bind_param('sss', $bln, $thn, $co[$i]['id']);
            $sd->execute();
            $dsdd = $sd->get_result();
            $d = $dsdd->fetch_assoc();
            $data[] = array_merge(array('bagian' => $co[$i]['nm_bagian'], 'id' => $co[$i]['id']), $d);
        }

        //print_r($data);echo'<br><br>';

        for ($j=0; $j < count($data); $j++) { 
            if($data[$j]['id'] == 95){//95 = produksi
                unset($data[$j]);
            }elseif($data[$j]['id'] == 9){//9 = Logistik Gudang
                unset($data[$j]);
            }elseif($data[$j]['id'] == 11){//11 = Pemasaran Gudang
                unset($data[$j]);
            }elseif($data[$j]['id'] == 73){//73 = MTC Dayshift
                unset($data[$j]);
            }elseif($data[$j]['id'] == 90){//90 = MTC Shift
                unset($data[$j]);
            }elseif($data[$j]['id'] == 80){//80 = Adm Produksi
                unset($data[$j]);
            }elseif($data[$j]['id'] == 98){//98 = QC
                unset($data[$j]);
            }elseif($data[$j]['id'] == 115){//115 = Exim
                unset($data[$j]);
            }elseif($data[$j]['id'] == 1){//1 = akutansi
                unset($data[$j]);
            }elseif($data[$j]['id'] == 2){//2 = keuangan
                unset($data[$j]);
            }
        }
        $data = array_values($data);

        $bulan = $bln;
        $bulann = $bln - 1;
        if($bulann == 0){
          $bulann = 12;
          $thnn = $thn - 1;
        }else{
          $thnn = $thn;
        }
        $tgl_aw = date($thnn.'-'.$bulann.'-21');
        $tgl_ak = date($thn.'-'.$bulan.'-20');
        $jmlhari_bln = get_object_vars(date_diff(date_create($tgl_aw), date_create($tgl_ak)));

        for ($k='A'; $k <= chr(64 + $djgpro['jml_grup']); $k++) { 
            $sdd = $this->app->db->prepare("SELECT b.id, 
                                IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                                IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                                IFNULL(SUM(a.nt_lembur),0) as tlembur,
                                IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                                IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                                FROM absensi_kalkulasi a 
                                JOIN bagian b ON b.id = a.kd_bagian
                                JOIN karyawan k ON k.id = a.nik
                                WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 95 AND k.jml_grup = ? AND k.grup = ?");
            $sdd->bind_param('ssss', $bln, $thn, $djgpro['jml_grup'], $k);
            $sdd->execute();
            $dsddd = $sdd->get_result();            
            $dd = $dsddd->fetch_assoc();
            $nmbag = 'Produksi';
            $data[] = array_merge(array('bagian' => $nmbag, 'grup' => $k), $dd);
            
        }

        //LOGISTIK GUDANG
        $slg = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 9");
        $slg->bind_param('ss', $bln, $thn);
        $slg->execute();
        $dslg = $slg->get_result();
        $dlg = $dslg->fetch_assoc();
        $nmbag = 'Logistik Gudang';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $dlg);

        //PEMASARAN GUDANG
        $spg = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 11");
        $spg->bind_param('ss', $bln, $thn);
        $spg->execute();
        $dspg = $spg->get_result();
        $dpg = $dspg->fetch_assoc();
        $nmbag = 'Pemasaran Gudang';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $dpg);

        //ADM PRODUKSI
        $sap = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 80");
        $sap->bind_param('ss', $bln, $thn);
        $sap->execute();
        $dsap = $sap->get_result();
        $dap = $dsap->fetch_assoc();
        $nmbag = 'Adm Produksi';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $dap);

        //QC
        $sqc = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 98");
        $sqc->bind_param('ss', $bln, $thn);
        $sqc->execute();
        $dsqc = $sqc->get_result();
        $dqc = $dsqc->fetch_assoc();
        $nmbag = 'QC/Lab/B Store';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $dqc);

        //MTC
        $smtc = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND (k.kd_bagian = 73 OR k.kd_bagian = 90)");
        $smtc->bind_param('ss', $bln, $thn);
        $smtc->execute();
        $dsmtc = $smtc->get_result();
        $dmtc = $dsmtc->fetch_assoc();
        $nmbag = 'Maintenance';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $dmtc);

        //Exim
        $se = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND k.kd_bagian = 115");
        $se->bind_param('ss', $bln, $thn);
        $se->execute();
        $dse = $se->get_result();
        $de = $dse->fetch_assoc();
        $nmbag = 'Exim';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $de);


        //Akutansi/Keuangan
        $spak = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? AND k.kd_departemen = 1");
        $spak->bind_param('ss', $bln, $thn);
        $spak->execute();
        $dspak = $spak->get_result();
        $dpak = $dspak->fetch_assoc();
        $nmbag = 'Akuntansi/Keuangan';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'N'), $dpak);
        /*------------------------------------------------------------DIRUMAHKAN-----------------------------------------------------------//
        $sddr = $this->app->db->prepare("SELECT b.id, 
                            IFNULL(COUNT(k.jk),0) as jml, IFNULL(SUM(a.nt_lwajib),0) as lwajib, 
                            IFNULL(SUM(a.nt_lbiasa),0) as lbiasa,  IFNULL(SUM(a.nt_llibur),0) as llibur, 
                            IFNULL(SUM(a.nt_lembur),0) as tlembur,
                            IFNULL(sum(a.gaji_kotor),0) as gaji_bruto, IFNULL(sum(a.alpha),0) as alpha,  
                            IFNULL(sum(a.ijin),0) as ijin, IFNULL(sum(a.sdr),0) as sdr,  IFNULL(sum(a.pijin),0) as psw
                            FROM absensi_kalkulasi a 
                            JOIN bagian b ON b.id = a.kd_bagian
                            JOIN karyawan k ON k.id = a.nik
                            WHERE a.bulan = ? AND a.tahun = ? 
                            AND (k.kd_bagian = 95 OR k.kd_bagian = 9 OR k.kd_bagian = 11
                            OR k.kd_bagian = 80 OR k.kd_bagian = 98 OR k.kd_bagian = 73 OR k.kd_bagian = 90 OR k.kd_bagian = 115) 
                            AND a.rumah >= 22");
        $sddr->bind_param('ss', $bln, $thn);
        $sddr->execute();
        $dsdddr = $sddr->get_result();
        $dr = $dsdddr->fetch_assoc();
        $nmbag = 'Produksi_R';
        $data[] = array_merge(array('bagian' => $nmbag, 'grup' => 'R'), $dr);
        */
        //print_r($data);echo'<br><br>';

        for ($m=0; $m < count($data); $m++) {
            if($data[$m]['id'] == '101'){ //RT
                $umr = $data[$m]; unset($data[$m]);
            }elseif($data[$m]['id'] == '105'){ //sekretariat
                $ums = $data[$m]; unset($data[$m]);
            }elseif($data[$m]['id'] == '117'){ //umum
                $umu = $data[$m]; unset($data[$m]);
            }
        }
        $data = array_values($data);
        
        //print_r($data);echo'<br><br>';

        $data[] = ['bagian' => 'Umum & Rumah Tangga', 'grup' => 'N', 
                    'jml' => $umr['jml']+$ums['jml']+$umu['jml'],
                    'lwajib' => $umr['lwajib']+$ums['lwajib']+$umu['lwajib'],
                    'lbiasa' => $umr['lbiasa']+$ums['lbiasa']+$umu['lbiasa'],
                    'llibur' => $umr['llibur']+$ums['llibur']+$umu['llibur'],
                    'tlembur' => $umr['tlembur']+$ums['tlembur']+$umu['tlembur'],
                    'gaji_bruto' => $umr['gaji_bruto']+$ums['gaji_bruto']+$umu['gaji_bruto'],
                    'alpha' => $umr['alpha']+$ums['alpha']+$umu['alpha'],
                    'ijin' => $umr['ijin']+$ums['ijin']+$umu['ijin'],
                    'sdr' => $umr['sdr']+$ums['sdr']+$umu['sdr'],
                    'psw' => $umr['psw']+$ums['psw']+$umu['psw'],
                  ];


        //print_r($data);die();
        
        for ($l=0; $l < count($data); $l++) { 
            //hitung persentasi lembur/gajikotor*100
            $p_lwajib = 0; $p_lbiasa = 0; $p_llibur = 0; $p_ntlembur = 0;

            empty($data[$l]['gaji_bruto']) ? $p_lwajib = 0 : $p_lwajib = ($data[$l]['lwajib']/$data[$l]['gaji_bruto'])*100;
            empty($data[$l]['gaji_bruto']) ? $p_lbiasa = 0 : $p_lbiasa = ($data[$l]['lbiasa']/$data[$l]['gaji_bruto'])*100;
            empty($data[$l]['gaji_bruto']) ? $p_llibur = 0 : $p_llibur = ($data[$l]['llibur']/$data[$l]['gaji_bruto'])*100;
            empty($data[$l]['gaji_bruto']) ? $p_lembur = 0 : $p_lembur = ($data[$l]['tlembur']/$data[$l]['gaji_bruto'])*100;


            $sql = $this->app->db->prepare("INSERT INTO rekap_lembur (tahun, bulan, bagian, grup, kar, n_lwajib, p_lwajib, n_lbiasa, 
                                            p_lbiasa, n_llibur, p_llibur, tn_lembur, tp_lembur, gaji_kotor, a, i, s, p) 
                                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $sql->bind_param('ssssssssssssssssss',
                                $thn, 
                                $bln,
                                $data[$l]['bagian'],
                                $data[$l]['grup'],
                                $data[$l]['jml'],
                                $data[$l]['lwajib'],
                                $p_lwajib,
                                $data[$l]['lbiasa'],
                                $p_lbiasa,
                                $data[$l]['llibur'],
                                $p_llibur,
                                $data[$l]['tlembur'],
                                $p_lembur,
                                $data[$l]['gaji_bruto'],
                                $data[$l]['alpha'],
                                $data[$l]['ijin'],
                                $data[$l]['sdr'],
                                $data[$l]['psw']
                                );
            $sql->execute();
            $sql->store_result();

            if($sql->affected_rows == 1){
                $a += 1;
            }
        }
        
        if($a==count($data)){
            return true;
        }else{
            return false;
        }
    }

    public function getRekap($thn, $bln)
    {
        $sql = $this->app->db->prepare("SELECT * FROM rekap_lembur WHERE tahun = ? AND bulan = ?");
        $sql->bind_param('ss', $thn, $bln);
        $sql->execute();
        $da = $sql->get_result();
        $c = [];
        while($d = $da->fetch_assoc()){
            $c[] = $d;
        }
        return $c;
    }

    public function getKaLem($tahun, $bulan, $departemen, $bagian)
    {
        $sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, 
                                        a.masuk, a.libur, a.pagi, a.siang, a.malam, 
                                        a.lembur_w, a.t_lembur_w, a.t_lbiasa, a.t_llibur, a.t_lembur, 
                                        a.ijin, a.sdr, a.cuti, a.cuti_h, a.alpha, a.pijin, a.t_ijin, a.rumah, a.nt_premi,
                                        k.grup, k.libur as libur_s, k.kd_status, k.tgl_update, a.sd
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        WHERE a.tahun = ? AND a.bulan = ? AND a.kd_departemen = ? AND a.kd_bagian = ?
                                        ORDER BY a.kd_bagian, k.grup, k.libur, k.bagian, k.id ASC");
        $sql->bind_param('ssss', $tahun, $bulan, $departemen, $bagian);
        $sql->execute();
        $da = $sql->get_result();
        $c = [];
        while($d = $da->fetch_assoc()){
            $c[] = $d;
        }

        $sqlr = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, 
                                        a.masuk, a.libur, a.pagi, a.siang, a.malam, 
                                        a.lembur_w, a.t_lbiasa, a.t_llibur, a.t_lembur, 
                                        a.ijin, a.sdr, a.cuti, a.cuti_h, a.alpha, a.pijin, a.t_ijin, a.rumah, a.nt_premi,
                                        k.grup, k.libur as libur_s, k.kd_status, k.tgl_update
                                        FROM absensi_kalkulasi a
                                        JOIN resign k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        WHERE a.tahun = ? AND a.bulan = ? AND a.kd_departemen = ? AND a.kd_bagian = ?
                                        ORDER BY a.kd_bagian, k.grup, k.libur, k.bagian, k.id ASC");
        $sqlr->bind_param('ssss', $tahun, $bulan, $departemen, $bagian);
        $sqlr->execute();
        $dar = $sqlr->get_result();
        while($dr = $dar->fetch_assoc()){
            $c[] = $dr;
        }

        return $c;
    }

    public function getKaLemId($tahun, $bulan, $nik)
    {
        $sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, 
                                        a.masuk, a.libur, a.pagi, a.siang, a.malam, 
                                        a.lembur_w, a.t_lembur_w, a.t_lbiasa, a.t_llibur, a.t_lembur, 
                                        a.ijin, a.sdr, a.cuti, a.cuti_h, a.cuti_r, a.alpha, a.alpha_r, a.pijin, a.t_ijin, a.n_psw, a.rumah, 
                                        a.nt_premi, k.grup, k.libur as libur_s, k.kd_status, k.tgl_update, a.u_makan
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        WHERE a.tahun = ? AND a.bulan = ? AND a.nik = ?");
        $sql->bind_param('sss', $tahun, $bulan, $nik);
        $sql->execute();
        $da = $sql->get_result();
        $d = $da->fetch_assoc();

        if(empty($d)){
            $sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, 
                                        a.masuk, a.libur, a.pagi, a.siang, a.malam, 
                                        a.lembur_w, a.t_lembur_w, a.t_lbiasa, a.t_llibur, a.t_lembur, 
                                        a.ijin, a.sdr, a.cuti, a.cuti_h, a.cuti_r, a.alpha, a.alpha_r, a.pijin, a.t_ijin, a.n_psw, a.rumah, 
                                        a.nt_premi, k.grup, k.libur as libur_s, k.kd_status, k.tgl_update, a.u_makan
                                        FROM absensi_kalkulasi a
                                        JOIN resign k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        WHERE a.tahun = ? AND a.bulan = ? AND a.nik = ?");
            $sql->bind_param('sss', $tahun, $bulan, $nik);
            $sql->execute();
            $da = $sql->get_result();
            $d = $da->fetch_assoc();
        }
        
        return $d;
    }

    public function cekMinus($tgl, $bln, $thn)
    {
        $sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, k.bagian, k.grup, k.libur, a.n_koperasi, 
                                        a.n_potongan, a.gaji_kotor, a.total_gaji, kop.id as id_koperasi, kop.nominal as pot_koperasi,
                                        kop.sisa, kop.pot_lama 
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        JOIN koperasi kop ON kop.nik = a.nik
                                        WHERE a.total_gaji <= 0 AND a.bulan = ? AND a.tahun = ? AND kop.tgl = ?
                                        ORDER BY a.kd_departemen, a.kd_bagian, k.bagian, k.grup, k.libur, k.id ASC");
        $sql->bind_param('sss', $bln, $thn, $tgl);
        /*$sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, k.bagian, k.grup, k.libur, a.n_koperasi, 
                                        a.n_potongan, a.gaji_kotor, a.total_gaji, kop.id as id_koperasi, kop.nominal as pot_koperasi,
                                        kop.sisa, kop.pot_lama 
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        JOIN koperasi kop ON kop.nik = a.nik
                                        WHERE a.total_gaji < 0 AND a.bulan = ? AND a.tahun = ?
                                        ORDER BY a.kd_departemen, a.kd_bagian, k.bagian, k.grup, k.libur, k.id ASC");
        $sql->bind_param('ss', $bln, $thn);*/
        $sql->execute();
        $da = $sql->get_result();
        $c = [];
        while($d = $da->fetch_assoc()){
            $c[] = $d;
        }
        return $c;
    }

    public function cekMinusG($tgl, $bln, $thn)
    {
        $sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, k.bagian, k.grup, k.libur, a.n_koperasi, 
                                        a.n_potongan, a.gaji_kotor, a.total_gaji
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        WHERE a.total_gaji <= 0 AND a.bulan = ? AND a.tahun = ?
                                        ORDER BY a.kd_departemen, a.kd_bagian, k.bagian, k.grup, k.libur, k.id ASC");
        $sql->bind_param('ss', $bln, $thn);
        $sql->execute();
        $da = $sql->get_result();
        $c = [];
        while($d = $da->fetch_assoc()){
            $c[] = $d;
        }
        return $c;
    }

    public function lapMinus($bln, $thn)
    {
        $tgl = $thn.'-'.$bln.'-01';
        $sql = $this->app->db->prepare("SELECT a.nik, k.nama, d.nm_departemen, b.nm_bagian, k.bagian, k.grup, k.libur, a.n_koperasi, 
                                        a.n_potongan, a.gaji_kotor, a.total_gaji, kop.id as id_koperasi, kop.nominal as pot_koperasi,
                                        kop.sisa, kop.pot_lama 
                                        FROM absensi_kalkulasi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = a.kd_departemen
                                        JOIN bagian b ON b.id = a.kd_bagian
                                        JOIN koperasi kop ON kop.nik = a.nik
                                        WHERE a.bulan = ? AND a.tahun = ? AND kop.sisa > 0  AND kop.tgl = ?
                                        ORDER BY a.kd_departemen, a.kd_bagian, k.bagian, k.grup, k.libur, k.id ASC");
        $sql->bind_param('iis', $bln, $thn, $tgl);
        $sql->execute();
        $da = $sql->get_result();
        $c = [];
        while($d = $da->fetch_assoc()){
            $c[] = $d;
        }
        return $c;
    }

    public function kalThr($bln, $thn, $tglmk)
    {
        $d = [];
        $res = $this->app->db->query("SELECT * FROM karyawan WHERE kd_status = 1 OR kd_status = 2");
        $dk = [];
        while($c = $res->fetch_assoc()) {
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $nom_jabatan = 0;
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $nom_prestasi = 0;
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $dk[] = $c;
        }

        //print_r($dk); die();
        $i = 0;
        foreach ($dk as $dkar) {
        //for ($i=0; $i < count($dk); $i++) {
            $idkar = $dkar['id'];
            //masa kerja
            if($dkar['kd_status']==2){
                //Masa Kerja brp tahun
                $qjmker = $this->app->db->prepare("SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) as masakerja 
                                                   FROM karyawan k WHERE id = ?");
                $qjmker->bind_param('ss', $tglmk, $idkar);
                $qjmker->execute();
                $dqjmker = $qjmker->get_result();
                $djmke = $dqjmker->fetch_assoc();

                if(!empty($djmke)){
                    $j_masakerja = $djmke['masakerja'];
                }else{
                    $j_masakerja = 0;
                }

                //Tunjangan Masa Kerja
                $qtmker = $this->app->db->prepare("SELECT tm.nominal as t_masakerja FROM t_masakerja tm 
                                                   WHERE (SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) FROM karyawan WHERE id = ?)        
                                                   BETWEEN tm.awal AND tm.akhir");
                $qtmker->bind_param('ss', $tglmk, $idkar);
                $qtmker->execute();
                $dqtmker = $qtmker->get_result();
                $dtmke = $dqtmker->fetch_assoc();

                if(!empty($dtmke)){
                    $t_masakerja = $dtmke['t_masakerja'];
                }else{
                    $t_masakerja = 0;
                }
            }else{
                $j_masakerja = 0;
                $t_masakerja = 0;
            }
            $jmasakerja = $j_masakerja*$t_masakerja;

            //kalkulasi thr
            $d[$i]['tahun'] = $thn;
            $d[$i]['nik'] = $idkar;
            $d[$i]['gaji'] = $dkar['gaji'];
            $d[$i]['nt_jabatan'] = $dkar['nom_jabatan'];
            $d[$i]['nt_keahlian'] = $dkar['nom_keahlian'];
            $d[$i]['nt_prestasi'] = $dkar['nom_prestasi'];
            $d[$i]['nt_masakerja'] = $jmasakerja;
            $d[$i]['gaji_kotor'] = $dkar['gaji']+$dkar['nom_jabatan']+$dkar['nom_keahlian']+$dkar['nom_prestasi']+$jmasakerja;

            $jmlb = date_diff(date_create($dkar['tgl_in']), date_create($tglmk));
            $jmlt = date_diff(date_create($dkar['tgl_in']), date_create($tglmk));
            $selisih = $jmlb->format("%m") + ($jmlb->format("%y")*12);
            $d[$i]['rule'] = $selisih <= 0 ? 0 : $selisih;

            if($selisih > 0 && $selisih < 12){
                $d[$i]['total'] = ($d[$i]['gaji_kotor']*$selisih)/12;
            }elseif($selisih<=0){
                $d[$i]['total'] = 0;
            }elseif($selisih>=12){
                $d[$i]['total'] = $d[$i]['gaji_kotor'];
            }

            //print_r($d); die();
            //
            // $sql = $this->app->db->prepare("SELECT a.tahun, a.nik, a.gaji, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.nt_masakerja, 
            //                                sum(a.gaji+a.nt_jabatan+a.nt_keahlian+a.nt_prestasi+a.nt_masakerja) as gaji_kotor,
            //                                 (CASE 
            //                                     WHEN TIMESTAMPDIFF(MONTH, k.tgl_in, ?) <= 0
            //                                         THEN '0'
            //                                     ELSE TIMESTAMPDIFF(MONTH, k.tgl_in, ?)
            //                                 END) as rule,
            //                                 (CASE
            //                                     WHEN TIMESTAMPDIFF(MONTH, k.tgl_in, ?) > 0 AND TIMESTAMPDIFF(MONTH, k.tgl_in, ?) < 12
            //                                         THEN sum(a.gaji+a.nt_jabatan+a.nt_keahlian+a.nt_prestasi+a.nt_masakerja)*(TIMESTAMPDIFF(MONTH, k.tgl_in, ?)/12)
            //                                     WHEN TIMESTAMPDIFF(MONTH, k.tgl_in, ?) <= 0
            //                                         THEN '0'
            //                                     ELSE sum(a.gaji+a.nt_jabatan+a.nt_keahlian+a.nt_prestasi+a.nt_masakerja)
            //                                 END) as total
            //                                 FROM absensi_kalkulasi a
            //                                 JOIN karyawan k ON k.id = a.nik
            //                                 WHERE a.bulan = ? AND a.tahun = ? AND a.nik = $idkar");
            // $sql->bind_param('ssssssii', $tglmk, $tglmk, $tglmk, $tglmk, $tglmk, $tglmk, $bln, $thn);
            // $sql->execute();
            // $da = $sql->get_result();
            // $d[] = $da->fetch_assoc();
            $i++;
        }
        return $d;
    }

    public function cekThr($thn)
    {
        $qs = $this->app->db->prepare("SELECT DISTINCT tahun FROM kalkulasi_thr WHERE tahun = ?");
        $qs->bind_param('s', $thn);
        $qs->execute();
        $resqs = $qs->get_result();
        $qsc = $resqs->fetch_assoc();

        if(!empty($qsc)){
            $del = $this->app->db->prepare("DELETE FROM `kalkulasi_thr` WHERE `tahun` = ?");
            $del->bind_param('s', $thn);
            $del->execute();
        }

        $qs = $this->app->db->prepare("SELECT DISTINCT tahun FROM rekap_thr WHERE tahun = ?");
        $qs->bind_param('s', $thn);
        $qs->execute();
        $resqs = $qs->get_result();
        $qsc = $resqs->fetch_assoc();

        if(!empty($qsc)){
            $del = $this->app->db->prepare("DELETE FROM `rekap_thr` WHERE `tahun` = ?");
            $del->bind_param('s', $thn);
            $del->execute();
        }
    }

    public function addThr($d)
    {

        $total = pembulatan($d['total']);
        $stmt = $this->app->db->prepare("INSERT INTO `kalkulasi_thr`(tahun, nik, gaji, t_masakerja, t_jabatan, t_keahlian, t_prestasi, gaji_kotor, rule, total) 
            VALUES(?,?,?,?,?,?,?,?,?,?)");

        $stmt->bind_param('ssssssssss', 
                        $d['tahun'],
                        $d['nik'],
                        $d['gaji'],
                        $d['nt_masakerja'],
                        $d['nt_jabatan'],
                        $d['nt_keahlian'],
                        $d['nt_prestasi'],
                        $d['gaji_kotor'],
                        $d['rule'],
                        $total
                    );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1){ 
            //echo $d['nik'].' - Insert<br>';
            return $stmt->insert_id; 
        } else { 
            echo $d['nik'].' - Gagal<br>';
            return null; 
        }
    }

    public function addRekapTHRTA($d, $tabel, $tabelrekap)
    {
        $data = [];
        //print_r($d);echo "_<br><br>";
        //echo $tabel."_<br><br>";
        //echo $tabelrekap."_<br><br>";
        //cari jumlah grup di produksi
        $sjgpro = $this->app->db->prepare("SELECT DISTINCT k.jml_grup 
                                          FROM `$tabel` a
                                          JOIN karyawan k ON k.id = a.nik
                                          WHERE a.tahun = ? AND k.kd_bagian = 95
                                        ");
        $sjgpro->bind_param('s', $d[0]['tahun']);
        $sjgpro->execute();
        $dsjgpro = $sjgpro->get_result();
        $djgpro = $dsjgpro->fetch_assoc();

        //print_r($djgpro);echo "_<br><br>";

        for ($i='A'; $i <= chr(64 + $djgpro['jml_grup']); $i++) { 
            //$g[] = $i;
            //cari total semua per grup PRODUKSI shift
            $sqlp = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                    WHERE a.tahun = ? AND k.kd_bagian = 95 AND k.jml_grup = ? AND k.grup = ?");
            $sqlp->bind_param('sss', $d[0]['tahun'], $djgpro['jml_grup'], $i);
            $sqlp->execute();
            $dsqlp = $sqlp->get_result();
            $dp = $dsqlp->fetch_assoc();

            $data[] = array_merge(array('departemen'=>'produksi', 'grup' => $i), $dp);
        }

        //print_r($data);die();

        //cari total semua PRODUKSI dayshift
        $sqlpn = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian != 95 AND k.kd_bagian != 115");
        $sqlpn->bind_param('s', $d[0]['tahun']);
        $sqlpn->execute();
        $dsqlpn = $sqlpn->get_result();
        $dpn = $dsqlpn->fetch_assoc();

        $sqlmtc = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 15");
        $sqlmtc->bind_param('s', $d[0]['tahun']);
        $sqlmtc->execute();
        $dsqlmtc = $sqlmtc->get_result();
        $dmtc = $dsqlmtc->fetch_assoc();

        $dtpd = array('total' => $dpn['total']+$dmtc['total']);


        $data[] = array_merge(array('departemen'=>'produksi n', 'grup' => 'N'), $dtpd);


        //cari total UTILITY SHIFT
        $sqlus = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 110 ");
        $sqlus->bind_param('s', $d[0]['tahun']);
        $sqlus->execute();
        $dsqlus = $sqlus->get_result();
        $dus = $dsqlus->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'utility', 'grup' => 'S'), $dus);

        //cari total UTILITY N
        $sqlun = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 13 AND k.kd_bagian = 111 ");
        $sqlun->bind_param('s', $d[0]['tahun']);
        $sqlun->execute();
        $dsqlun = $sqlun->get_result();
        $dun = $dsqlun->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'utility', 'grup' => 'N'), $dun);

        //cari total PEMASARAN GUDANG        
        $sqlpg = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 11 ");
        $sqlpg->bind_param('s', $d[0]['tahun']);
        $sqlpg->execute();
        $dsqlpg = $sqlpg->get_result();
        $dpg = $dsqlpg->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'pemasaran gudang', 'grup' => 'N'), $dpg);

        //cari total PEMASARAN ADM   
        $sqlpa = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 7 AND k.kd_bagian = 10 ");
        $sqlpa->bind_param('s', $d[0]['tahun']);
        $sqlpa->execute();
        $dsqlpa = $sqlpa->get_result();
        $dpa = $dsqlpa->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'pemasaran adm', 'grup' => 'N'), $dpa);

        //cari total KEUANGAN
        $sqlkeu = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 1");
        $sqlkeu->bind_param('s', $d[0]['tahun']);
        $sqlkeu->execute();
        $dsqlkeu = $sqlkeu->get_result();
        $dkeu = $dsqlkeu->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'keuangan', 'grup' => 'N'), $dkeu);

        //cari total LOGISTIK GUDANG
        $sqllog = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 6 AND k.kd_bagian = 9 ");
        $sqllog->bind_param('s', $d[0]['tahun']);
        $sqllog->execute();
        $dsqllog = $sqllog->get_result();
        $dlog = $dsqllog->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'logistik gudang', 'grup' => 'N'), $dlog);
        
        //cari total SIPIL
        $sqlsip = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 108 ");
        $sqlsip->bind_param('s', $d[0]['tahun']);
        $sqlsip->execute();
        $dsqlsip = $sqlsip->get_result();
        $dsip = $dsqlsip->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'sipil', 'grup' => 'N'), $dsip);

        //cari total KEAMANAN
        $sqlkea = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 104 ");
        $sqlkea->bind_param('s', $d[0]['tahun']);
        $sqlkea->execute();
        $dsqlkea = $sqlkea->get_result();
        $dkea = $dsqlkea->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'keamanan', 'grup' => 'N'), $dkea);

        //cari total EXIM
        $sqlex = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 11 AND k.kd_bagian = 115 ");
        $sqlex->bind_param('s', $d[0]['tahun']);
        $sqlex->execute();
        $dsqlex = $sqlex->get_result();
        $dex = $dsqlex->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'exim', 'grup' => 'N'), $dex);

        //cari total E.D.P
        $sqledp = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 12 AND k.kd_bagian = 116 ");
        $sqledp->bind_param('s', $d[0]['tahun']);
        $sqledp->execute();
        $dsqledp = $sqledp->get_result();
        $dedp = $dsqledp->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'e.d.p', 'grup' => 'N'), $dedp);

        //cari total HRD
        $sqlhr = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 3 AND k.kd_bagian = 6 ");
        $sqlhr->bind_param('s', $d[0]['tahun']);
        $sqlhr->execute();
        $dsqlhr = $sqlhr->get_result();
        $dhr = $dsqlhr->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'personalia', 'grup' => 'N'), $dhr);

        //cari total UMUM
        $sqlum = $this->app->db->prepare("SELECT IFNULL(sum(total),0) as total FROM `$tabel` a JOIN karyawan k ON k.id = a.nik
                WHERE a.tahun = ? AND k.kd_departemen = 12 
                AND k.kd_bagian != 116 AND k.kd_bagian != 108 AND k.kd_bagian != 104");
        $sqlum->bind_param('s', $d[0]['tahun']);
        $sqlum->execute();
        $dsqlum = $sqlum->get_result();
        $dum = $dsqlum->fetch_assoc();
        $data[] = array_merge(array('departemen'=>'umum', 'grup' => 'N'), $dum);

        //print_r($data); die();
        $a=0;
        for ($i=0; $i < count($data); $i++) { 

            $sqlgk = $this->app->db->prepare("INSERT INTO `$tabelrekap` (tahun, dept, grup, total) VALUES (?,?,?,?)");
            $sqlgk->bind_param('ssss',
                                $d[0]['tahun'],
                                $data[$i]['departemen'],
                                $data[$i]['grup'],
                                $data[$i]['total']
                            );

            $sqlgk->execute();
            $sqlgk->store_result();

            if($sqlgk->affected_rows == 1){ 
                $a += 1;
            }
        }
        if($a==count($data)){
            return true;
        }else{
            die();
            return false;
        }
    }

    public function kalTA($bln, $thn, $persen)
    {
        $d = [];
        $sqlkar = $this->app->db->query("SELECT id FROM karyawan WHERE kd_status = 2");
        $dk = [];
        while($ck = $sqlkar->fetch_assoc()) {
            $dk[] = $ck;
        }

        //print_r($dk);
        foreach ($dk as $dkar) {
        //for ($i=0; $i < count($dk); $i++) {
            $idkar = $dkar['id'];
            $sql = $this->app->db->prepare("SELECT a.tahun, a.nik, a.gaji, a.nt_jabatan, a.nt_keahlian, a.nt_prestasi, a.nt_masakerja, 
                                           sum(a.gaji+a.nt_jabatan+a.nt_keahlian+a.nt_prestasi+a.nt_masakerja) as gaji_kotor,
                                           (sum(a.gaji+a.nt_jabatan+a.nt_keahlian+a.nt_prestasi+a.nt_masakerja) * (?/100)) as total
                                            FROM absensi_kalkulasi a
                                            WHERE a.bulan = ? AND a.tahun = ? AND a.nik = $idkar");
            $sql->bind_param('sii', $persen, $bln, $thn);
            $sql->execute();
            $da = $sql->get_result();
            $d[] = $da->fetch_assoc();
        }
        return $d;
    }

    public function cekTA($thn)
    {
        $qs = $this->app->db->prepare("SELECT DISTINCT tahun FROM kalkulasi_ta WHERE tahun = ?");
        $qs->bind_param('s', $thn);
        $qs->execute();
        $resqs = $qs->get_result();
        $qsc = $resqs->fetch_assoc();

        if(!empty($qsc)){
            $del = $this->app->db->prepare("DELETE FROM `kalkulasi_ta` WHERE `tahun` = ?");
            $del->bind_param('s', $thn);
            $del->execute();
        }

        $qs = $this->app->db->prepare("SELECT DISTINCT tahun FROM rekap_ta WHERE tahun = ?");
        $qs->bind_param('s', $thn);
        $qs->execute();
        $resqs = $qs->get_result();
        $qsc = $resqs->fetch_assoc();

        if(!empty($qsc)){
            $del = $this->app->db->prepare("DELETE FROM `rekap_ta` WHERE `tahun` = ?");
            $del->bind_param('s', $thn);
            $del->execute();
        }
    }

    public function addTA($d, $persen)
    {
        $total = pembulatan($d['total']);
        $stmt = $this->app->db->prepare("INSERT INTO `kalkulasi_ta`(tahun, nik, gaji, t_masakerja, t_jabatan, t_keahlian, t_prestasi, gaji_kotor, rule, total) 
            VALUES(?,?,?,?,?,?,?,?,?,?)");

        $stmt->bind_param('ssssssssss', 
                        $d['tahun'],
                        $d['nik'],
                        $d['gaji'],
                        $d['nt_masakerja'],
                        $d['nt_jabatan'],
                        $d['nt_keahlian'],
                        $d['nt_prestasi'],
                        $d['gaji_kotor'],
                        $persen,
                        $total
                    );
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1){ 
            //echo $d['nik'].' - Insert<br>';
            return $stmt->insert_id; 
        } else { 
            echo $d['nik'].' - Gagal<br>';
            return null; 
        }
    }

}