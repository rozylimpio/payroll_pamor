<?php

namespace App\Models;

class Libur
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($tgl, $ket)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `libur_nasional`(tgl_libur, ket) VALUES(?,?)");
        $stmt->bind_param('ss', $tgl, $ket);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM libur_nasional ORDER BY tgl_libur DESC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM libur_nasional WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM libur_nasional WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $tgl, $ket)
    {
        $stmt = $this->app->db->prepare("UPDATE libur_nasional SET tgl_libur = ?, ket = ? WHERE id = ?");
        $stmt->bind_param('ssi', $tgl, $ket, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('libur_nasional', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `libur_nasional` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }


    public function getThr($thn)
    {
        //$thn = date('Y');
        $res = $this->app->db->query("SELECT * FROM libur_nasional WHERE ket like '%Hari Raya Idul Fitri%' 
                                    AND DATE_FORMAT(tgl_libur, '%Y')='$thn' ORDER BY tgl_libur ASC");
        $c = $res->fetch_assoc();
        return $c;
    }
    
}