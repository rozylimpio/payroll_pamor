<?php

namespace App\Models;

class Insentif
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($insentif, $nom)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `insentif`(nm_insentif, nominal) VALUES(?,?)");
        $stmt->bind_param('ss', $insentif, $nom);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM insentif");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM insentif WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM insentif WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $insentif, $nom)
    {
        $stmt = $this->app->db->prepare("UPDATE insentif SET nm_insentif = ?, nominal = ? WHERE id = ?");
        $stmt->bind_param('ssi', $insentif, $nom, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('insentif', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `insentif` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}