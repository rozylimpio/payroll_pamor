<?php

namespace App\Models;

class Jadwal7GrupU
{
   use THistory;

   protected $app;

   public function __construct($app) 
   {
      $this->app = $app;
   }

   public function add($hari)
   {
    for ($mm=0; $mm < count($hari) ; $mm++) { 
       for ($kk=0; $kk < count($hari[$mm]) ; $kk++) { 
          //print_r($hari[$mm][$kk]);echo '<br>';
          $stmt = $this->app->db->prepare("INSERT INTO `jadwal_7_shift_u`(harike, grup, shift, berlaku) VALUES(?,?,?,?)");
          $stmt->bind_param('ssss', $hari[$mm][$kk]['harike'], 
                                      $hari[$mm][$kk]['grup'],
                                      $hari[$mm][$kk]['shift'],
                                      $hari[$mm][$kk]['berlaku']
                           );
          $stmt->execute();
          $stmt->store_result();
       }
    }
    if($stmt->affected_rows == 1) return $stmt->insert_id;
    else return null;
   }

   public function get()
   {
      $resb = $this->app->db->query("SELECT * FROM jadwal_7_shift_u ORDER BY berlaku DESC, id ASC");
      $container = [];
      while($cb = $resb->fetch_assoc()) {
         $container[] = $cb;
      }
      return $container;
   }

}