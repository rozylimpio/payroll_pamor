<?php

namespace App\Models;

class Persen
{
    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($ket, $status, $persen)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `persen`(ket, status, persentase) VALUES(?,?,?)");
        $stmt->bind_param('sii', $ket, $status, $persen);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM persen");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM persen WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $persen)
    {
        $stmt = $this->app->db->prepare("UPDATE persen SET persentase = ? WHERE id = ?");
        $stmt->bind_param('ii', $persen, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        $stmt = $this->app->db->prepare("DELETE FROM `persen` WHERE id = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else {
            return false;
        }
    }

    public function getpersen($ket, $status)
    {
        $stmt = $this->app->db->prepare("SELECT persentase FROM `persen` WHERE ket LIKE ? AND status = ?");
        $stmt->bind_param('si', $ket, $status);
        $stmt->execute();
        $res = $stmt->get_result();

        $c = $res->fetch_assoc();

        return $c['persentase'];
    }
}