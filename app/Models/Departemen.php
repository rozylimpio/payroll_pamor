<?php

namespace App\Models;

class Departemen
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($dep)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `departemen`(nm_departemen) VALUES(?)");
        $stmt->bind_param('s', $dep);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM departemen");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM departemen WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM departemen WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $dep)
    {
        $stmt = $this->app->db->prepare("UPDATE departemen SET nm_departemen = ? WHERE id = ?");
        $stmt->bind_param('si', $dep, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('departemen', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `departemen` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}