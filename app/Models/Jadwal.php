<?php

namespace App\Models;

class Jadwal
{
   use THistory;

   protected $app;

   public function __construct($app) 
   {
      $this->app = $app;
   }

   public function add($hari)
   {
      $minggu_del = $hari[0][0]['minggu'];
      $berlaku_del = $hari[0][0]['berlaku'];

      $this->app->db->query("DELETE FROM `jadwal_3_shift` WHERE minggu > $minggu_del AND berlaku<'$berlaku_del'");

      for ($mm=0; $mm < count($hari) ; $mm++) { 
         for ($kk=0; $kk < count($hari[$mm]) ; $kk++) { 
            //print_r($hari[$mm][$kk]);echo '<br>';
            $stmt = $this->app->db->prepare("INSERT INTO `jadwal_3_shift`(bulan, tglaw, tglak, grup, shift, berlaku) VALUES(?,?,?,?,?,?)");
            $stmt->bind_param('ssssss', $hari[$mm][$kk]['bulan_ke'], 
                                        $hari[$mm][$kk]['tglaw'],
                                        $hari[$mm][$kk]['tglak'],
                                        $hari[$mm][$kk]['grup'],
                                        $hari[$mm][$kk]['shift'],
                                        $hari[$mm][$kk]['berlaku']
                             );
            $stmt->execute();
            $stmt->store_result();
         }
      }
      if($stmt->affected_rows == 1) return $stmt->insert_id;
      else return null;
   }

   public function get()
   {      
      $cek = $this->app->db->query("SELECT DISTINCT(bulan) FROM jadwal_3_shift ORDER BY bulan, tglaw, grup ASC");
      $cont = [];
      if (!empty($cek->fetch_assoc())){
         $resb = $this->app->db->query("SELECT DISTINCT(bulan) FROM jadwal_3_shift ORDER BY tglaw, grup ASC");
         $container = [];
         while($cb = $resb->fetch_assoc()) {
            //$container[] = $cb;
            $bln = $cb['bulan'];
            $res = $this->app->db->query("SELECT bulan, tglaw, tglak, grup, shift, berlaku FROM jadwal_3_shift 
                                       WHERE bulan = '$bln' ORDER BY berlaku DESC, tglaw, grup ASC");
            while($c = $res->fetch_assoc()) {
               $cont[$bln][] = $c;
            }
         }
      }
      return $cont;
   }

}