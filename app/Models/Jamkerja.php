<?php

namespace App\Models;

class Jamkerja
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM jamkerja");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM jamkerja WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM jamkerja WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function getAktif()
    {
        $res = $this->app->db->query("SELECT ja.*, j.nm_jamkerja FROM jamkerja_aktif ja JOIN jamkerja j ON j.id = ja.kd_jamkerja");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getAktifById($id)
    {
        $res = $this->app->db->query("SELECT ja.*, j.nm_jamkerja FROM jamkerja_aktif ja JOIN jamkerja j ON j.id = ja.kd_jamkerja WHERE ja.id = {$id}");
        return $res->fetch_assoc();
    }

    public function addAktif($kd, $bagian, $shift, $hari, $ket, $mulai, $akhir, $tgl)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `jamkerja_aktif`(kd_jamkerja, bagian, shift, hari, ket, mulai, akhir, berlaku) VALUES(?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssssss', $kd, $bagian, $shift, $hari, $ket, $mulai, $akhir, $tgl);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function updateAktif($id, $kd, $bagian, $shift, $hari, $ket, $mulai, $akhir, $tgl)
    {
        $stmt = $this->app->db->prepare("UPDATE jamkerja_aktif SET kd_jamkerja = ?, bagian = ?, shift = ?, hari = ?, ket = ?, mulai = ?, akhir = ?, berlaku = ? WHERE id = ?");
        $stmt->bind_param('ssssssssi', $kd, $bagian, $shift, $hari, $ket, $mulai, $akhir, $tgl, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteAktif($id)
    {
        if($id_history = $this->history('jamkerja_aktif', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `jamkerja_aktif` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }


    public function getIstirahat()
    {
        $res = $this->app->db->query("SELECT ji.*, j.nm_jamkerja FROM jamkerja_istirahat ji 
                                      JOIN jamkerja j ON j.id = ji.kd_jamkerja");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getIstirahatById($id)
    {
        $res = $this->app->db->query("SELECT ji.*, j.nm_jamkerja FROM jamkerja_istirahat ji 
                                      JOIN jamkerja j ON j.id = ji.kd_jamkerja
                                      WHERE ji.id = {$id}");
        return $res->fetch_assoc();
    }

    public function addIstirahat($kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `jamkerja_istirahat`(kd_jamkerja, hari, shift, ist_ke, ket, mulai, akhir, berlaku) VALUES(?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssssss', $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function updateIstirahat($id, $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl)
    {
        $stmt = $this->app->db->prepare("UPDATE jamkerja_istirahat SET kd_jamkerja = ?, hari = ?, shift = ?, ist_ke = ?, ket = ?, mulai = ?, akhir = ?, berlaku = ? WHERE id = ?");
        $stmt->bind_param('ssssssssi', $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteIstirahat($id)
    {
        if($id_history = $this->history('jamkerja_istirahat', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `jamkerja_istirahat` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

    public function getStatus()
    {
        $res = $this->app->db->query("SELECT * FROM status");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }


    public function getIstirahatS()
    {
        $res = $this->app->db->query("SELECT ji.*, j.nm_jamkerja FROM jamkerja_istirahat_s ji 
                                      JOIN jamkerja j ON j.id = ji.kd_jamkerja");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getIstirahatByIdS($id)
    {
        $res = $this->app->db->query("SELECT ji.*, j.nm_jamkerja FROM jamkerja_istirahat_s ji 
                                      JOIN jamkerja j ON j.id = ji.kd_jamkerja
                                      WHERE ji.id = {$id}");
        return $res->fetch_assoc();
    }

    public function addIstirahatS($kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `jamkerja_istirahat_s`(kd_jamkerja, hari, shift, ist_ke, ket, mulai, akhir, berlaku) VALUES(?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssssss', $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function updateIstirahatS($id, $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl)
    {
        $stmt = $this->app->db->prepare("UPDATE jamkerja_istirahat_s SET kd_jamkerja = ?, hari = ?, shift = ?, ist_ke = ?, ket = ?, mulai = ?, akhir = ?, berlaku = ? WHERE id = ?");
        $stmt->bind_param('ssssssssi', $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteIstirahatS($id)
    {
        if($id_history = $this->history('jamkerja_istirahat_s', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `jamkerja_istirahat_s` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

}