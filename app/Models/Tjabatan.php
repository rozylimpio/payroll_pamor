<?php

namespace App\Models;

class Tjabatan
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($jab, $jam, $nom)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `t_jabatan`(kd_jabatan, kd_jamkerja, nominal) VALUES(?,?,?)");
        $stmt->bind_param('sss', $jab, $jam, $nom);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT t.*, j.nm_jabatan, j.ket, jk.nm_jamkerja FROM t_jabatan t 
                                      JOIN jabatan j ON j.id = t.kd_jabatan
                                      JOIN jamkerja jk ON jk.id = t.kd_jamkerja
                                      ORDER BY t.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT t.*, j.nm_jabatan, j.ket, jk.nm_jamkerja FROM t_jabatan t 
                                      JOIN jabatan j ON j.id = t.kd_jabatan
                                      JOIN jamkerja jk ON jk.id = t.kd_jamkerja WHERE t.id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT t.*, j.nm_jabatan, j.ket, jk.nm_jamkerja FROM t_jabatan t 
                                      JOIN jabatan j ON j.id = t.kd_jabatan
                                      JOIN jamkerja jk ON jk.id = t.kd_jamkerja WHERE t.id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $jab, $jam, $nom)
    {
        $stmt = $this->app->db->prepare("UPDATE t_jabatan SET kd_jabatan = ?, kd_jamkerja = ?, nominal = ? WHERE id = ?");
        $stmt->bind_param('sssi', $jab, $jam, $nom, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('t_jabatan', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `t_jabatan` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}