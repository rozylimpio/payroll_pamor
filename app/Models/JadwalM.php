<?php

namespace App\Models;

class JadwalM
{
   use THistory;

   protected $app;

   public function __construct($app) 
   {
      $this->app = $app;
   }

   public function add($hari)
   {
    for ($mm=0; $mm < count($hari) ; $mm++) { 
      $stmt = $this->app->db->prepare("INSERT INTO `jadwal_shift_m`(harike, grup, shift, berlaku) VALUES(?,?,?,?)");
      $stmt->bind_param('ssss', $hari[$mm]['harike'], 
                                $hari[$mm]['grup'],
                                $hari[$mm]['shift'],
                                $hari[$mm]['berlaku']
                       );
      $stmt->execute();
      $stmt->store_result();
    }
    if($stmt->affected_rows == 1) return $stmt->insert_id;
    else return null;
   }

   public function get()
   {
      $resb = $this->app->db->query("SELECT * FROM jadwal_shift_m ORDER BY berlaku DESC, harike ASC, grup asc");
      $container = [];
      while($cb = $resb->fetch_assoc()) {
         $container[] = $cb;
      }
      return $container;
   }

}