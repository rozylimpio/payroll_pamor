<?php

namespace App\Models;

class Absensi
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }    

    public function getDepartemen($tgl)
    {
        $stmt = $this->app->db->prepare("SELECT DISTINCT d.id, d.nm_departemen FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        WHERE a.tgl_absensi >= ?");
        $stmt->bind_param('s', $tgl);
        $stmt->execute();

        $res = $stmt->get_result();

        $container = [];        
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getBagian($tgl)
    {
        $stmt = $this->app->db->prepare("SELECT DISTINCT b.id, b.nm_bagian, d.id as kd_departemen FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE a.tgl_absensi >= ?");
        $stmt->bind_param('s', $tgl);
        $stmt->execute();

        $res = $stmt->get_result();

        $container = [];        
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        //var_dump($container);die();
        return $container;
    }

    public function getData($data)
    {
        $and = '';
        $bind = '';
        $s = '';

        if(!empty($data['jgrup'])){
            if(!empty($data['grup'])){
                if(!empty($data['libur'])){
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'" AND k.libur = '.$data['libur'].' AND k.kd_jamkerja = '.$data['jamkerja'];
                }else{
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'"  AND k.kd_jamkerja = '.$data['jamkerja'];
                }
            }else{
                $and = ' AND k.jml_grup = '.$data['jgrup'].'  AND k.kd_jamkerja = '.$data['jamkerja'];
            }
        }else{
            $and = 'AND k.kd_jamkerja = '.$data['jamkerja'];
        }
        $stmt = $this->app->db->prepare("SELECT a.*, k.nama, 
                                               k.kd_departemen, d.nm_departemen,
                                               k.kd_bagian, b.nm_bagian, k.bagian,
                                               k.kd_jamkerja, jm.nm_jamkerja, k.jml_grup,
                                               k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi = ? AND k.kd_departemen = ? AND k.kd_bagian = ? {$and}
                                        ");
        $stmt->bind_param('sss', $data['tanggal'],
                                    $data['departemen'],
                                    $data['bagian']//,
                                    //$data['ket_bag']
                                );
        $stmt->execute();
        $res = $stmt->get_result();

        $container = [];        
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        //var_dump($container);die();
        return $container;
    }

    public function getDataAbs($tgl_aw, $tgl_ak, $nik)
    {
        $stmt = $this->app->db->prepare("SELECT a.*, k.nama, 
                                               k.kd_departemen, d.nm_departemen,
                                               k.kd_bagian, b.nm_bagian, k.bagian,
                                               k.kd_jamkerja, jm.nm_jamkerja, k.jml_grup,
                                               k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? AND a.nik = ?
                                        ORDER BY a.tgl_absensi ASC
                                        ");
        $stmt->bind_param('sss', $tgl_aw, $tgl_ak, $nik);
        $stmt->execute();
        $res = $stmt->get_result();

        $container = [];        
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        //var_dump($container);die();
        return $container;
    }

    public function getDataAbsFull($data)
    {
        $and = '';
        $bind = '';
        $s = '';

        if(!empty($data['jgrup'])){
            if(!empty($data['grup'])){
                if(!empty($data['libur'])){
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'" AND k.libur = '.$data['libur'].' AND k.kd_jamkerja = '.$data['jamkerja'];
                }else{
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'"  AND k.kd_jamkerja = '.$data['jamkerja'];
                }
            }else{
                $and = ' AND k.jml_grup = '.$data['jgrup'].'  AND k.kd_jamkerja = '.$data['jamkerja'];
            }
        }else{
            $and = 'AND k.kd_jamkerja = '.$data['jamkerja'];
        }
        $stmtk = $this->app->db->prepare("SELECT k.id
                                        FROM karyawan k
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE k.kd_departemen = ? AND k.kd_bagian = ? {$and}
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.bagian, k.kd_jamkerja, k.jml_grup, k.grup, k.libur, k.id ASC
                                        ");
        $stmtk->bind_param('ss', $data['departemen'], $data['bagian']);
        $stmtk->execute();
        $resk = $stmtk->get_result();
        $nik = [];        
        while($c = $resk->fetch_assoc()) {
            $nik[] = $c['id'];
        }
        //print_r($nik);die();
        $container = [];        
        for ($i=0; $i < count($nik); $i++) { 
            $id = $nik[$i];
            $stmt = $this->app->db->prepare("SELECT a.*, k.nama, 
                                               k.kd_departemen, d.nm_departemen,
                                               k.kd_bagian, b.nm_bagian, k.bagian,
                                               k.kd_jamkerja, jm.nm_jamkerja, k.jml_grup,
                                               k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? AND a.nik = ?
                                        ORDER BY a.tgl_absensi ASC
                                        ");
            $stmt->bind_param('sss', $data['tgl_aw'], $data['tgl_ak'], $id);
            $stmt->execute();
            $res = $stmt->get_result();

            while($ck = $res->fetch_assoc()) {
                $container[$i][] = $ck;
            }
        }
        //var_dump($container);die();
        return $container;
    }

    public function delete($id)
    {
        if($id_history = $this->history('absensi', $id, 'delete')) {
            $stmtl = $this->app->db->prepare("DELETE FROM `absensi_lembur` WHERE id_absensi = ?");
            $stmtl->bind_param('i', $id);
            $stmtl->execute();

            $stmtm = $this->app->db->prepare("DELETE FROM `absensi_mtk` WHERE id_absensi = ?");
            $stmtm->bind_param('i', $id);
            $stmtm->execute();


            $stmt = $this->app->db->prepare("DELETE FROM `absensi` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

    public function deleteAbs($tgl)
    {
        $stmtl = $this->app->db->prepare("DELETE al FROM absensi_lembur al JOIN absensi a ON a.id = al.id_absensi WHERE a.tgl_absensi = ?");
        $stmtl->bind_param('s', $tgl);
        $stmtl->execute();

        $stmtm = $this->app->db->prepare("DELETE am FROM absensi_mtk am JOIN absensi a ON a.id = am.id_absensi WHERE a.tgl_absensi = ?");
        $stmtm->bind_param('s', $tgl);
        $stmtm->execute();

        $stmt = $this->app->db->prepare("DELETE FROM `absensi` WHERE tgl_absensi = ?");
        $stmt->bind_param('s', $tgl);
        $stmt->execute();
        $stmt->store_result();

        //proses mengembalikan sisa cuti
        $query = $this->app->db->query("SELECT id, cuti, sisa_cuti FROM `karyawan` WHERE kd_status = 4");
        $container = [];
        while($c = $query->fetch_assoc()) {
            $container[] = $c;
        }
        $i=0;
        if($stmt->affected_rows > 0){
            foreach ($container as $index => $data) {
                $sisa_cuti = 0;
                $sisa_cuti = $data['sisa_cuti']+1;

                if ($sisa_cuti > $data['cuti']) {
                    $sisa_cuti = $data['cuti'];
                }

                $update = $this->app->db->prepare("UPDATE karyawan SET sisa_cuti = ? WHERE id = ?");
                $update->bind_param('ii', $sisa_cuti, $data['id']);
                $update->execute();
                $update->store_result();

                if($update->affected_rows > 0){
                    $i++;
                }
            }

            if($i==count($container)){
                return true;
            }else{
                return false;    
            }
        }else{
            return false;
        }
    }

    public function deleteAbsBetween($tgl_aw, $tgl_ak)
    {
        $stmtl = $this->app->db->prepare("DELETE al FROM absensi_lembur al JOIN absensi a ON a.id = al.id_absensi 
                                          WHERE a.tgl_absensi BETWEEN ? AND ?");
        $stmtl->bind_param('ss', $tgl_aw, $tgl_ak);
        $stmtl->execute();

        $stmtm = $this->app->db->prepare("DELETE am FROM absensi_mtk am JOIN absensi a ON a.id = am.id_absensi 
                                          WHERE a.tgl_absensi BETWEEN ? AND ?");
        $stmtm->bind_param('ss', $tgl_aw, $tgl_ak);
        $stmtm->execute();

        $stmt = $this->app->db->prepare("DELETE FROM `absensi` WHERE tgl_absensi BETWEEN ? AND ?");
        $stmt->bind_param('ss', $tgl_aw, $tgl_ak);
        $stmt->execute();
        $stmt->store_result();

        //proses mengembalikan sisa cuti
        $query = $this->app->db->query("SELECT id, cuti, sisa_cuti FROM `karyawan` WHERE kd_status = 4");
        $container = [];
        while($c = $query->fetch_assoc()) {
            $container[] = $c;
        }
        
        $i=0;
        if($stmt->affected_rows > 0){
            $jmld = get_object_vars(date_diff(date_create($tgl_aw), date_create($tgl_ak)));
            $jmlhr = $jmld['d']+1;
            foreach ($container as $index => $data) {
                $sisa_cuti = 0;
                $sisa_cuti = $data['sisa_cuti']+$jmlhr;

                if ($sisa_cuti > $data['cuti']) {
                    $sisa_cuti = $data['cuti'];
                }

                $update = $this->app->db->prepare("UPDATE karyawan SET sisa_cuti = ? WHERE id = ?");
                $update->bind_param('ii', $sisa_cuti, $data['id']);
                $update->execute();
                $update->store_result();

                if($update->affected_rows > 0){
                    $i++;
                }
            }

            if($i==count($container)){
                return true;
            }else{
                return false;    
            }
        }else{
            return false;
        }
    }

    public function tmasakerja($content, $tgl)
    {
        if($content['kd_status']==2 || $content['kd_status_lama']==2){
            //Masa Kerja brp tahun
            $qjmker = $this->app->db->prepare("SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) as masakerja 
                                               FROM karyawan WHERE id = ?");
            $qjmker->bind_param('ss', $tgl, $content['id']);
            $qjmker->execute();
            $dqjmker = $qjmker->get_result();
            $djmke = $dqjmker->fetch_assoc();
            $data['j_masakerja'] = $djmke['masakerja'];

            //Tunjangan Masa Kerja
            $qtmker = $this->app->db->prepare("SELECT tm.nominal as t_masakerja FROM t_masakerja tm 
                                               WHERE (SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) FROM karyawan WHERE id = ?)        
                                               BETWEEN tm.awal AND tm.akhir");
            $qtmker->bind_param('ss', $tgl, $content['id']);
            $qtmker->execute();
            $dqtmker = $qtmker->get_result();
            $dtmke = $dqtmker->fetch_assoc();

            if(!empty($dtmke)){
                $data['t_masakerja'] = $dtmke['t_masakerja'];
            }else{
                $data['t_masakerja'] = 0;
            }
        }else{
            $data['j_masakerja'] = 0;
            $data['t_masakerja'] = 0;
        }

        return $data;
    }

    public function update($data)
    {
        //print_r($data);echo'<br>';
        $a = 0;
        $stmt = $this->app->db->prepare("UPDATE absensi SET jadwal_shift = ?, kehadiran = ?, ijin = ?, lembur_wajib = ?, lembur_lain = ?, 
                                        waktu_masuk = ?, ist_keluar = ?, ist_masuk = ?, ist_ke = ?, waktu_pulang = ?, t_jam = ?, t_ijin = ?,
                                        t_lembur = ?, pot_ijin_hari = ?, u_makan = ? WHERE id = ?");
        $stmt->bind_param('sssssssssssssssi', 
                           $data['jadwal'],
                           $data['hadir'],
                           $data['ket_i'],
                           $data['lembur_wajib'],
                           $data['lembur_lain'],
                           $data['masuk'],
                           $data['istaw'],
                           $data['istak'],
                           $data['istke'],
                           $data['pulang'],
                           $data['totmasuk'],
                           $data['tijin'],
                           $data['totlembur'],
                           $data['pijin'],
                           $data['uangmakan'],
                           $data['id']
                          );
        $stmt->execute();
        $stmt->store_result();
        // print_r($data);echo "<br><br>";
        // var_dump($stmt);die();

        if($stmt->affected_rows == 1){
            $a = 0; $al = 0;
            if(!empty($data['imulai'][0]) && !empty($data['iakhir'][0])){
                
                //array_pop($data['imulai']);
                //array_pop($data['iakhir']);
                
                $sql = $this->app->db->prepare("SELECT * FROM absensi_mtk WHERE id_absensi = ?");
                $sql->bind_param('i',$data['id']);
                $sql->execute();
                $res = $sql->get_result();
                        
                if(!empty($res->fetch_array())){
                    $sd = $this->app->db->prepare("DELETE FROM `absensi_mtk` WHERE id_absensi = ?");
                    $sd->bind_param('i', $data['id']);
                    $sd->execute();
                    $sd->store_result();
                }

                for ($i=0; $i < count($data['imulai']); $i++) { 
                    $stm = $this->app->db->prepare("INSERT INTO absensi_mtk (id_absensi, jam_keluar, jam_masuk) VALUES (?,?,?)");
                    $stm->bind_param('iss', 
                                      $data['id'],
                                      $data['imulai'][$i],
                                      $data['iakhir'][$i]
                                    );
                    $stm->execute();
                    $stm->store_result();

                    if($stm->affected_rows == 1){
                        $a += 1;
                    }
                }
            }

            
            $sqll = $this->app->db->prepare("SELECT * FROM absensi_lembur WHERE id_absensi = ?");
            $sqll->bind_param('i',$data['id']);
            $sqll->execute();
            $resl = $sqll->get_result();
                    
            if(!empty($resl->fetch_array())){
                $sdl = $this->app->db->prepare("DELETE FROM `absensi_lembur` WHERE id_absensi = ?");
                $sdl->bind_param('i', $data['id']);
                $sdl->execute();
                $sdl->store_result();
            }
            if(!empty($data['totlembur'])){
                for ($l=0; $l < count($data['detail_lembur']); $l++) { 
                    $stml = $this->app->db->prepare("INSERT INTO absensi_lembur (id_absensi, lembur, jl_mulai, jl_akhir, t_lembur) 
                                                     VALUES (?,?,?,?,?)");
                    $stml->bind_param('issss', 
                                      $data['id'],
                                      $data['detail_lembur'][$l]['lembur'],
                                      $data['detail_lembur'][$l]['mulai'],
                                      $data['detail_lembur'][$l]['akhir'],
                                      $data['detail_lembur'][$l]['total']
                                    );
                    $stml->execute();
                    $stml->store_result();

                    if($stml->affected_rows == 1){
                        $al += 1;
                    }
                }
            }

            if($a==count($data['imulai']) && $al==count($data['detail_lembur'])){
                return "ok";
            }else{
                return "failed";         
            }

        }else{ 
            return "no"; 
        }
    }

    public function lamamtk($id, $tgl)
    {
        $sql = $this->app->db->prepare("SELECT * FROM absensi WHERE id = ?");
        $sql->bind_param('s',$id);
        $sql->execute();
        $sqlr = $sql->get_result();
        $d = $sqlr->fetch_assoc();

        $jam = carijam($d['t_ijin']);
        $menit = carimenit($d['t_ijin'], $jam);

        return $jam." jam ".$menit." menit";
    }

    public function cariAbsensi($tgl_aw, $tgl_ak, $nik)
    {
        $stmtk = $this->app->db->prepare("SELECT id FROM karyawan
                                        WHERE id = ?
                                        ");
        $stmtk->bind_param('s', $nik);
        $stmtk->execute();
        $resk = $stmtk->get_result();
        $ck = $resk->fetch_assoc();

        if(empty($ck)){
            $stmt = $this->app->db->prepare("SELECT a.*, k.nama, k.grup, k.libur, d.nm_departemen, b.nm_bagian FROM absensi a
                                            JOIN resign k ON k.id = a.nik
                                            JOIN departemen d ON d.id = k.kd_departemen
                                            JOIN bagian b ON b.id = k.kd_bagian
                                            WHERE a.nik = ? AND a.tgl_absensi BETWEEN ? AND ? 
                                            ORDER by a.tgl_absensi ASC
                                            ");
            $stmt->bind_param('sss', $nik, $tgl_aw, $tgl_ak);
            $stmt->execute();
            $res = $stmt->get_result();

            $container = [];        
            while($c = $res->fetch_assoc()) {
                $container[] = $c;
            }
        }else{
            $stmt = $this->app->db->prepare("SELECT a.*, k.nama, k.grup, k.libur, d.nm_departemen, b.nm_bagian FROM absensi a
                                            JOIN karyawan k ON k.id = a.nik
                                            JOIN departemen d ON d.id = k.kd_departemen
                                            JOIN bagian b ON b.id = k.kd_bagian
                                            WHERE a.nik = ? AND a.tgl_absensi BETWEEN ? AND ? 
                                            ORDER by a.tgl_absensi ASC
                                            ");
            $stmt->bind_param('sss', $nik, $tgl_aw, $tgl_ak);
            $stmt->execute();
            $res = $stmt->get_result();

            $container = [];        
            while($c = $res->fetch_assoc()) {
                $container[] = $c;
            }
        }
        return $container;
    }

    public function getCekAbsKehadiran($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }
        //kehadiran is null
        $stmtn = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran='' OR a.kehadiran IS NULL) 
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtn->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtn->execute();
        $resn = $stmtn->get_result();
    
        while($cn = $resn->fetch_assoc()) {
            $container[] = $cn;
        } 

        // jam masuk < 0
        $stmttj = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND t_jam<0
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmttj->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmttj->execute();
        $restj = $stmttj->get_result();
        
        while($ctj = $restj->fetch_assoc()) {
            $container[] = $ctj;
        }

        return $container;

    }

    public function getCekAbsMasuk($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }
        
        //kehadiran masuk, finger kosong, lembur 0
        $stmtm = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                               k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'masuk' AND (a.waktu_masuk='00:00:00' AND a.waktu_pulang='00:00:00'))
                                        AND a.t_lembur = 0
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtm->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);
        $stmtm->execute();
        $resm = $stmtm->get_result();
        
        while($cm = $resm->fetch_assoc()) {
            $container[] = $cm;
        }

        //masuk tp tidak finger dan lembur 0
        $stmtaw = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'masuk' AND (a.waktu_masuk='00:00:00' OR a.waktu_pulang='00:00:00'))
                                        AND a.t_lembur = 0
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtaw->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);
        $stmtaw->execute();
        $resaw = $stmtaw->get_result();
        
        while($caw = $resaw->fetch_assoc()) {
            $container[] = $caw;
        }
            
        return $container;
    }

    public function getCekAbsAlpha($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }
        
        // kehadiran alpha finger ada isi
        $stmta = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'alpha' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmta->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmta->execute();
        $resa = $stmta->get_result();
        
        while($ca = $resa->fetch_assoc()) {
            $container[] = $ca;
        }

        return $container;
    }

    public function getCekAbsIjin($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }
        
        // kehadiran ijin finger isi
        $stmti = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'ijin' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmti->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmti->execute();
        $resi = $stmti->get_result();
        
        while($ci = $resi->fetch_assoc()) {
            $container[] = $ci;
        }

        return $container;
    }

    public function getCekAbsCuti($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        
        
        // kehadiran cuti finger isi
        $stmtc = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'cuti' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtc->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtc->execute();
        $resc = $stmtc->get_result();
        
        while($cc = $resc->fetch_assoc()) {
            $container[] = $cc;
        }

        return $container;
    }

    public function getCekAbsCutihamil($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        
        
         //cuti hamil tp finger
        $stmtch = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'cutihamil' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtch->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtch->execute();
        $resch = $stmtch->get_result();
        
        while($cch = $resch->fetch_assoc()) {
            $container[] = $cch;
        }

        return $container;
    }
    
    public function getCekAbsKk($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        
        
        // kk tp finger
        $stmtk = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'kk' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtk->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtk->execute();
        $resk = $stmtk->get_result();
        
        while($ck = $resk->fetch_assoc()) {
            $container[] = $ck;
        }

        return $container;
    }
    
    public function getCekAbsSdr($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        
    
        // sdr tp finger
        $stmtsd = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'sdr' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtsd->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtsd->execute();
        $ress = $stmtsd->get_result();
        
        while($cs = $ress->fetch_assoc()) {
            $container[] = $cs;
        }

        return $container;
    }
    
    public function getCekAbsDirumahkan($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        
 
        // dirumahkan tp finger
        $stmtr = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'dirumahkan' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtr->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtr->execute();
        $resr = $stmtr->get_result();
        
        while($cr = $resr->fetch_assoc()) {
            $container[] = $cr;
        }

        return $container;
    }
    
    public function getCekAbsLibur($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        

        //libur tp finger
        $stmtl = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.kehadiran = 'libur' AND (a.waktu_masuk!='00:00:00' AND a.waktu_pulang!='00:00:00'))
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtl->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtl->execute();
        $resl = $stmtl->get_result();
        
        while($cl = $resl->fetch_assoc()) {
            $container[] = $cl;
        }

        //libur tp lembur ada isi
        $stmtle = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND a.kehadiran = 'libur' AND (a.lembur_lain != NULL OR a.lembur_lain != '')
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtle->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtle->execute();
        $resle = $stmtle->get_result();
        
        while($cle = $resle->fetch_assoc()) {
            $container[] = $cle;
        }

        return $container;
    }
    
    public function getCekAbsTmkPsw($data)
    {
        $container = []; 
        $where = '';
        if($data['departemen']!='all'){
            $where = 'AND k.kd_departemen = '.$data['departemen'];
        }        

        // psw/mtk kehadiran tidak masuk
        $stmtpt = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.ijin='psw' OR a.ijin='tmk') AND a.kehadiran != 'masuk'
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtpt->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtpt->execute();
        $respt = $stmtpt->get_result();
        
        while($cpt = $respt->fetch_assoc()) {
            $container[] = $cpt;
        }

        // psw/mtk total ijin > 7jam
        $stmtptp = $this->app->db->prepare("SELECT a.*, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian,
                                                k.kd_jamkerja, jm.nm_jamkerja, k.grup, k.libur, s.nm_status
                                        FROM absensi a
                                        JOIN karyawan k ON k.id = a.nik JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE a.tgl_absensi BETWEEN ? AND ? $where
                                        AND (a.ijin='psw' OR a.ijin='tmk') AND a.t_ijin >= 25200
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.libur ASC
                                        ");
        $stmtptp->bind_param('ss', $data['tgl_aw'],$data['tgl_ak']);            
        $stmtptp->execute();
        $resptp = $stmtptp->get_result();
        
        while($cptp = $resptp->fetch_assoc()) {
            $container[] = $cptp;
        }

        return $container;
    }
    
    public function getDataCekAbs($data)
    {
        $datar = '';
        if($data['cek'] == 'kehadiran'){
            $datar = self::getCekAbsKehadiran($data);
        }elseif($data['cek'] == 'masuk'){
            $datar = self::getCekAbsMasuk($data);
        }elseif($data['cek'] == 'alpha'){
            $datar = self::getCekAbsAlpha($data);
        }elseif($data['cek'] == 'ijin'){
            $datar = self::getCekAbsIjin($data);
        }elseif($data['cek'] == 'cuti'){
            $datar = self::getCekAbsCuti($data);
        }elseif($data['cek'] == 'cutihamil'){
            $datar = self::getCekAbsCutihamil($data);
        }elseif($data['cek'] == 'kk'){
            $datar = self::getCekAbsKk($data);
        }elseif($data['cek'] == 'sdr'){
            $datar = self::getCekAbsSdr($data);
        }elseif($data['cek'] == 'dirumahkan'){
            $datar = self::getCekAbsDirumahkan($data);
        }elseif($data['cek'] == 'libur'){
            $datar = self::getCekAbsLibur($data);
        }elseif($data['cek'] == 'tmkpsw'){
            $datar = self::getCekAbsTmkPsw($data);
        }

        return $datar;
    }

    public function cekJmlKar($d)
    {
        $sql = $this->app->db->query("SELECT count(id) as jmlkar FROM karyawan");
        $jks = $sql->fetch_assoc();        
        $jmlhari = get_object_vars(date_diff(date_create($d['tgl_aw']), date_create($d['tgl_ak'])));

        $sqlj = $this->app->db->prepare("SELECT count(id) as jmlkar FROM absensi WHERE tgl_absensi BETWEEN ? AND ?");
        $sqlj->bind_param('ss', $d['tgl_aw'], $d['tgl_ak']);
        $sqlj->execute();
        $res = $sqlj->get_result();
        $jka = $res->fetch_assoc();

        if(($jks['jmlkar']*($jmlhari['days']+1)) == $jka['jmlkar']){
            return true;
        }else{
            return false;
        }
    }

    public function cariLemburbyId($id)
    {
        $stmt = $this->app->db->prepare("SELECT * FROM absensi_lembur WHERE id_absensi = ?");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getFile($thn, $bln = null, $bag = null, $tbl, $id = null)
    {
        if(empty($bln) && empty($id) && empty($bag)){
            $stmt = $this->app->db->prepare("SELECT * FROM $tbl WHERE tahun = ?");
            $stmt->bind_param('i', $thn);
        }elseif(!empty($bln) && empty($id) && empty($bag)){
            $stmt = $this->app->db->prepare("SELECT * FROM $tbl WHERE tahun = ? AND bulan = ?");
            $stmt->bind_param('ii', $thn, $bln);
        }elseif(!empty($bln) && empty($id) && !empty($bag)){
            $stmt = $this->app->db->prepare("SELECT * FROM $tbl WHERE tahun = ? AND bulan = ? AND bagian = ?");
            $stmt->bind_param('iis', $thn, $bln, $bag);
        }else{
            $stmt = $this->app->db->prepare("SELECT * FROM $tbl WHERE id = ?");
            $stmt->bind_param('i', $id);
        }
        $stmt->execute();
        $res = $stmt->get_result();
        $c = $res->fetch_assoc();
        return $c;
    }

    public function deleteFile($id, $tbl)
    {        
        $stmt = $this->app->db->prepare("DELETE FROM $tbl WHERE id = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else {
            return false;
        }
    }

    public function getUnpaid($tglaw, $tglak, $nik)
    {
        $stmt = $this->app->db->prepare("SELECT id, nik, tgl_absensi, kehadiran FROM `absensi` WHERE nik = ? AND tgl_absensi BETWEEN ? AND ? 
                                         AND (kehadiran = 'dirumahkan' OR kehadiran = 'rumah' OR kehadiran = 'unpaid' OR kehadiran = 'alpha' OR kehadiran = 'ijin')");
        $stmt->bind_param('iss', $nik, $tglaw, $tglak);
        $stmt->execute();
        $res = $stmt->get_result();
        $container = [];
        while ( $c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function updateAbsensi($d, $nik, $ket)
    {
        $a=0;
        for ($i=0; $i < count($d); $i++) { 
            $stmt = $this->app->db->prepare("UPDATE `absensi` SET kehadiran = ? WHERE nik = ? AND tgl_absensi = ?");
            $stmt->bind_param('sss', $ket, $nik, $d[$i]);
            $stmt->execute();
            $stmt->store_result();
            if($stmt->affected_rows > 0){
                $a += 1;
            }
        }

        if(count($d)==$a){
            return true;
        }else{
            return false;
        }
    }

    public function addSisa($nik, $jml, $sisa)
    {
        $stmt = $this->app->db->prepare("INSERT INTO sisa (nik, jml_aw, sisa) VALUES (?,?,?)");
        $stmt->bind_param('iii', $nik, $jml, $sisa);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->affected_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    public function getSisa()
    {
        $res = $this->app->db->query("SELECT s.*, k.id, k.nama, k.jk, k.grup, k.libur, jm.nm_jamkerja, 
                                    d.nm_departemen, b.nm_bagian, st.nm_status FROM sisa s
                                    JOIN karyawan k ON k.id = s.nik
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    JOIN status st ON st.id = k.kd_status
                                    ORDER BY k.kd_departemen, k.kd_bagian, k.grup, k.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getLembur($id)
    {
      $sql = $this->app->db->prepare("SELECT lembur_lain FROM absensi
                                    WHERE id = ? 
                                    AND (lembur_lain LIKE '%libur%' OR lembur_lain LIKE '%nasional%') ");
      $sql->bind_param('s', $id);
      $sql->execute();
      $res = $sql->get_result();
      $c = $res->fetch_assoc();
      
      return count($c);
    }

    public function delete3grup($tgl, $tbl)
    {        
        $stmt = $this->app->db->prepare("DELETE FROM $tbl WHERE berlaku = ?");
        $stmt->bind_param('s', $tgl);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows >= 1) return true;
        else {
            return false;
        }
    }

}

