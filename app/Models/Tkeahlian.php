<?php

namespace App\Models;

class Tkeahlian
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($dep, $jam, $nom)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `t_keahlian`(kd_departemen, kd_jamkerja, nominal) VALUES(?,?,?)");
        $stmt->bind_param('sss', $dep, $jam, $nom);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT t.*, d.nm_departemen, jk.nm_jamkerja FROM t_keahlian t 
                                      JOIN departemen d ON d.id = t.kd_departemen
                                      JOIN jamkerja jk ON jk.id = t.kd_jamkerja
                                      ORDER BY t.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT t.*, d.nm_departemen, jk.nm_jamkerja FROM t_keahlian t 
                                      JOIN departemen d ON d.id = t.kd_departemen
                                      JOIN jamkerja jk ON jk.id = t.kd_jamkerja WHERE t.id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT t.*, d.nm_departemen, jk.nm_jamkerja FROM t_keahlian t 
                                      JOIN departemen d ON d.id = t.kd_departemen
                                      JOIN jamkerja jk ON jk.id = t.kd_jamkerja WHERE t.id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $dep, $jam, $nom)
    {
        $stmt = $this->app->db->prepare("UPDATE t_keahlian SET kd_departemen = ?, kd_jamkerja = ?, nominal = ? WHERE id = ?");
        $stmt->bind_param('sssi', $dep, $jam, $nom, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('t_keahlian', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `t_keahlian` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}