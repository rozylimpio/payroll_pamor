<?php

return (object) [
    'db' => (object) [
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'dbname' => 'payroll_pamor'
    ],

    'base_url' => 'http://localhost/payroll_pamor/public',
    'base_url_client' => 'http://rz/payroll_pamor/public',
    'base_url_client_ip' => 'http://192.168.55.23/payroll_pamor/public'
];