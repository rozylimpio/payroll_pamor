<?php 
require '_base_head.php';
$mlibur = new \App\Models\Libur($app);
$liburs = $mlibur->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mlibur->getById($app->input->get('edit'));
}

$redirect = url('c/libur');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Libur Nasional</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('c/libur' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'libur';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Tanggal
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Tanggal Libur Nasional" value="<?php echo $edit ? dateResolver($edit['tgl_libur']) : '' ;?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Keterangan 
          </label>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <textarea name="ket" class="form-control" required><?php echo $edit ? $edit['ket'] : '' ;?></textarea>
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'libur_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Tanggal</th>
                      <th>Keterangan</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($liburs as $index => $libur) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo dateFormat(dateResolver($libur['tgl_libur']));?></td>
                      <td><?php echo $libur['ket'];?></td>
                      <td>
                        <a href="<?php echo url('c/libur?edit=' . $libur['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('c/libur?_method=delete&id=' . $libur['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();

        $('#tgl').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3",
          locale: {
            format: "DD-MM-YYYY",
            separator: "-",
          }
        });
      });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>