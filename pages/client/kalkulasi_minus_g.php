<?php 
require '_base_head.php';
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Recheck Gaji Minus</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group col-md-10 col-sm-8 col-xs-12">
                  <button name="tinjau" type="submit" id="tinjau" class="btn btn-info">
                    <i class="glyphicon glyphicon-check"></i>
                    &nbsp;Proses&nbsp;
                  </button>
                </div>
              </div>
              
            </form>            
            <?php 
            $defmsg_category = 'kalkulasi_minus';
            require '../pages/defmsg.php'; 
            ?>
            <?php
            if (isset($_GET['tinjau'])) {
              $bulan = $app->input->get('bulan');
              $thn = date('Y');
              $tgl = date($thn.'-'.$bulan.'-01');

              $mkal = new \App\Models\Kalkulasi($app);
              $list = $mkal->cekMinusG($tgl, $bulan, $thn);

            ?>
              <table width="100%" class="table-responsive">
                <tr>
                  <td align="center">
                    <h4>
                      <b>
                        Daftar Karyawan 
                      </b><br>

                  </td>
                </tr>
              </table>

              <table class="table table-bordered table-hover table-striped" id="myTable" style="width: 100%">
                <thead>
                  <tr>
                    <th width="5%" rowspan="2">No</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">Departemen</th>
                    <th rowspan="2">Gaji Kotor</th>
                    <th rowspan="2">Potongan</th>
                    <th rowspan="2">Gaji Bersih</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $index => $isi) { ?>
                  <tr>
                    <td><?php echo $index+1;?></td>
                    <td><?php echo $isi['nik']."<br>".$isi['nama'];?></td>
                    <td>
                      <?php 
                        echo $isi['nm_departemen']."<br>".$isi['nm_bagian'];
                        echo !empty($isi['grup']) ? " - ".$isi['grup'] : "";
                        echo !empty($isi['libur']) ? $isi['libur'] : "";
                        echo '<br><small>'.$isi['bagian'].'</small>';
                      ?>    
                    </td>
                    <td><?php echo idr($isi['gaji_kotor']); ?></td>
                    <td><?php echo idr($isi['n_potongan']); ?></td>
                    <td><?php echo idr($isi['total_gaji']); ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            <?php
            }
            ?>
             
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<script>
$(document).ready(function() {

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });
  
});

</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>