<?php 
require '_base_head.php';

$mpot = new \App\Models\Potongan($app);
$kars = $mpot->getLain();
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Upload Potongan Lain</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('c/pot_lain')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'pot_lain';
        require '../pages/defmsg.php';
        ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="tgl">
            Bulan
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php for ($i=1; $i < 13; $i++) { ?>
              <option value="<?php echo $i;?>"><?php echo namaBulan($i);?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="foto">
            File Potongan Lain
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" name="filename" id="filename" accept=".csv"  class="form-control col-md-7 col-xs-12">
            <small>*.csv only</small>
          </div>
        </div>  
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        <div class="ln_solid"></div>
        
        </form>
        <!-- End SmartWizard Content -->                    

        <div class="x_content">        
          <?php
          $defmsg_category = 'pot_list';
          require '../pages/defmsg.php';
          ?>
          <?php
          //print_r($kars);
          ?>
          <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
            <!-- TABLE -->
                <br><br>
                <div align="center" class="row">
                    <h2>Data Karyawan</h2>
                </div>
                <div class="row table-responsive" align="center">
                  <hr>
                  <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                    <thead>
                      <tr>
                        <th width="5%">No</th>
                        <th>Nama</th>
                        <th>Bagian</th>
                        <th>Potongan Lain</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($kars as $index => $kar) { ?>
                      <tr>
                        <td><?php echo $index+1;?></td>
                        <td><?php echo $kar['nama'];?>
                            <br><?php echo "(".$kar['nik'].")";?>
                            <br><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
                        </td>
                        <td>
                            <?php echo $kar['nm_departemen']." - ".$kar['nm_bagian'];?> 
                        </td>
                        <td><?php echo indo_number($kar['nominal']);?></td>
                        <td>
                          <a href="<?php echo url('c/add_pot_lain?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                          <button type="button" data-url="<?php echo url('c/pot_lain?_method=delete&id=' . $kar['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>    
                <div class="ln_solid"></div>            
                <a href="<?php echo url('c/add_pot_lain');?>" class="btn btn-success pull-left">
                  <i class="glyphicon glyphicon-plus"></i> Input Manual
                </a>
            </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();

    $("#bulan.select2_single").select2({
        placeholder: "Pilih Bulan",
        allowClear: true,
        disabled: false
    });
  });
</script>

<?php require '_base_foot.php';?>