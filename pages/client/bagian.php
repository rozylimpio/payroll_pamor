<?php 
require '_base_head.php';
$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mbag->getById($app->input->get('edit'));
}

$redirect = url('c/bagian');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Bagian</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('c/bagian' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'bagian';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Departemen
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="departemen" id="departemen" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($deps as $dep) { ?>
              <option <?php echo $edit['kd_departemen'] == $dep['id'] ? 'selected' : '' ;?> value="<?php echo $dep['id'];?>"><?php echo $dep['nm_departemen'];?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Bagian
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="bagian" id="bagian" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nm_bagian'] : '' ;?>">
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'bagian_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Departemen</th>
                      <th>Bagian</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($bags as $index => $bag) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo $bag['nm_departemen'];?></td>
                      <td><?php echo $bag['nm_bagian'];?></td>
                      <td>
                        <a href="<?php echo url('c/bagian?edit=' . $bag['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('c/bagian?_method=delete&id=' . $bag['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $("#departemen.select2_single").select2({
            placeholder: "Pilih Departemen",
            allowClear: true
        });
      });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>