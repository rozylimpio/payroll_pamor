<!DOCTYPE html>
<html lang="id">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pamor Payroll</title>
    
    <!-- jQuery -->
    <script src="<?php echo url();?>js/jquery.min.js"></script>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo url();?>img/logo_.ico">
    
    <!-- Bootstrap -->
    <link href="<?php echo url();?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo url();?>css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo url();?>css/nprogress.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo url();?>css/select2.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo url();?>css/iCheck-green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo url();?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo url();?>css/daterangepicker.css" rel="stylesheet">    
    <!-- Switchery -->
    <link href="<?php echo url();?>css/switchery.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo url();?>css/custom.css" rel="stylesheet">
    <link href="<?php echo url();?>css/admin.css" rel="stylesheet">
    
    <script src="<?php echo url();?>js/lodash.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo url();?>js/select2.full.min.js"></script>
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo url('i/home');?>" class="site_title text-center">
                <img src="<?php echo url();?>/img/logo.png" width="70%" class="img-circle">
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_info">
                <span>ONESystem Payroll</span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <div class="clear"></div>
                <ul class="nav side-menu"> 
                  <li><a><i class="fa fa-keyboard-o"></i> Input <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo url('c/umk');?>">Input UMK</a></li>
                      <li><a href="<?php echo url('c/libur');?>">Input Libur Nasional</a></li>
                      <li><a href="<?php echo url('c/jabatan');?>">Input Jabatan</a></li>
                      <li><a href="<?php echo url('c/departemen');?>">Input Departemen</a></li>
                      <li><a href="<?php echo url('c/bagian');?>">Input Bagian</a></li>
                      <li><a>Input Tunjangan dan Potongan<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a>Input Tunjangan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <li><a href="<?php echo url('c/t_jabatan');?>">Tunjangan Jabatan</a></li>
                              <li><a href="<?php echo url('c/t_masakerja');?>">Tunjangan Masa Kerja</a></li>
                              <li><a href="<?php echo url('c/t_prestasi');?>">Tunjangan Prestasi</a></li>
                              <li><a href="<?php echo url('c/t_keahlian');?>">Tunjangan Keahlian</a></li>
                            </ul>
                          </li>                          
                          <li><a>Input Potongan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <li><a href="<?php echo url('c/koperasi');?>">Input Potongan Koperasi</a></li>
                              <li><a>Potongan Lain<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                  <li><a href="<?php echo url('c/potongan');?>">Input Potongan Lainnya</a></li>
                                  <li><a href="<?php echo url('c/pot_lain');?>">Upload Potongan Lainnya</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li><a href="<?php echo url('c/premi');?>">Input Premi Absen Shift</a></li>
                          <li><a href="<?php echo url('c/insentif');?>">Input Insentif Absen</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-group"></i>Karyawan<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo url('c/karyawan_sp');?>">Daftar SP Karyawan</a></li>
                        <li><a href="<?php echo url('c/karyawan_mutasi_daftar');?>">Daftar Mutasi Karyawan</a></li>
                        <li><a>Data Karyawan Resign<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="<?php echo url('c/resign_in');?>">In Periode Gaji</a></li>
                            <li><a href="<?php echo url('c/resign');?>">Seluruh Data Karyawan Resign</a></li>
                          </ul>
                        </li>
                        <li><a href="<?php echo url('c/cari');?>">Cari Karyawan</a></li>
                      </ul>
                  </li>
                  <li><a><i class="fa fa-money"></i> Penggajian <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>Recheck Gaji Minus <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('c/kalkulasi_minus');?>">Koperasi</a></li>
                          <li><a href="<?php echo url('c/kalkulasi_minus_g');?>">Gaji</a></li>
                        </ul>
                      </li>
                      <li><a href="<?php echo url('c/kalkulasi_lembur');?>">Rekap Lembur</a></li>
                      <li><a href="<?php echo url('c/kalkulasi_view');?>">Daftar Gaji Karyawan</a></li>
                      <li><a href="<?php echo url('c/kalkulasi_rekap');?>">Daftar Rekap Gaji</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-files-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>Cetak Rincian Penggajian <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('c/rincian_banding_gaji');?>">Cetak Rincian Perbandingan Gaji</a></li>
                          <li><a href="<?php echo url('c/rincian_gaji_kar');?>">Cetak Daftar Gaji Karyawan</a></li>
                          <li><a href="<?php echo url('c/rekap');?>">Cetak Rekap Lembur Wajib</a></li>
                        </ul>
                      </li>
                      <li><a href="<?php echo url('c/rincian_thr_kar');?>">Cetak Rincian THR Karyawan</a></li>
                      <li><a href="<?php echo url('c/rincian_ta_kar');?>">Cetak Rincian Tali Asih Karyawan</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <!--<div class="nav navbar-nav">
                    <b>DINAS TENAGA KERJA DAN TRANSMIGRASI</b>
              </div>-->

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $app->sess->nama; ?> &nbsp;
                    <img src="<?php echo url();?>img/img.png" alt="">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo url('edit_akun?redirect=' . redirect_url());?>"><i class="fa fa-sliders pull-right"></i> Kelola Akun</a></li>
                    <li><a href="<?php echo url('logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div>
            <input type="hidden" name="baseurl" id="baseurl" value="<?php echo url();?>" />