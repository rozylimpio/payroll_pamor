<?php 
require '_base_head.php';

$tgll = date('Y-m-d', strtotime('-6 month', strtotime(date('Y-m-d'))));

$mkal = new \App\Models\Kalkulasi($app);
$ys = $mkal->getTahun();

$mab = new \App\Models\Absensi($app);
$deps = $mab->getDepartemen($tgll);
$bags = $mab->getBagian($tgll);
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Lembur dan Absensi Karyawan</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php foreach($ys as $y) { ?>
                    <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
                      <?php echo $y['tahun']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                  <select name="departemen" id="departemen" class="form-control select2_single" style="cursor:pointer">
                    <option value=""></option>
                    <?php foreach($deps as $dep) { ?>
                    <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
                      <?php echo $dep['nm_departemen']?></option>
                    <?php } ?>
                  </select>  
                </div>         
                <div class="col-md-3 col-sm-5 col-xs-12">
                  <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
                  <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                  </select>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-12">
                  <button name="tinjau" type="submit" class="btn btn-info">
                    <i class="glyphicon glyphicon-search"></i>
                    &nbsp;view&nbsp;
                  </button>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-12">
                  <button name="import" type="submit" class="btn btn-success" formtarget="_blank" formaction="excel_lembur">
                    <i class="glyphicon glyphicon-save"></i>
                    &nbsp;Excel&nbsp;
                  </button>
                </div>
              </div>
              
            </form>

            
            <?php 
            $defmsg_category = 'kalkulasi';
            require '../pages/defmsg.php'; 
            ?>
              <?php
              if(isset($_GET['tinjau'])){
                $tahun = $app->input->get('tahun');
                $bulan = $app->input->get('bulan');
                $departemen = $app->input->get('departemen');
                $bagian = $app->input->get('bagian');

                $d = $mkal->getKaLem($tahun, $bulan, $departemen, $bagian);
                ?>
                <br><br><br>
                <div align="center">
                  <b>
                    DAFTAR PERINCIAN LEMBUR DAN ABSENSI<br>
                    <?php echo !empty($bagian) && !empty($departemen)?'Bagian : '.$d[0]['nm_bagian'].'<br>':'' ?>
                    <?php echo empty($bagian) && !empty($departemen)?'Departemen : '.$d[0]['nm_departemen'].'<br>':'' ?>
                  </b>
                    <?php echo !empty($d[0]['sd']) ? 's/d Tanggal '.dateFormatSingkat(dateResolver($d[0]['sd'])).'<br>' : ''; ?>
                    <?php echo !empty($bulan)?'Bulan : '.namaBulan($bulan).' - ':'Tahun ' ?> <?php echo $tahun ?>
                </div>
                <br><br>
                <table id="myTablex" class="display table-bordered table-hover table-striped hoverr" style="width:100%; font-size: 11px;">
                  <thead align="center">
                    <tr>
                      <th rowspan="2">No</th>
                      <th rowspan="2">No Induk</th>
                      <th rowspan="2">Nama Karyawan</th>
                      <th colspan="6">Masuk</th>
                      <th rowspan="2">Premi Shift</th>
                      <th colspan="5">Lembur</th>
                      <th colspan="7">Absensi</th>
                    </tr>
                    <tr>
                      <th>P</th>
                      <th>S</th>
                      <th>M</th>
                      <th>J.M</th>
                      <th>L</th>
                      <th>R</th>
                      <th>W</th>
                      <th>TW</th>
                      <th>P</th>
                      <th>N</th>
                      <th>TL</th>
                      <th>A</th>
                      <th>I</th>
                      <th>S</th>
                      <th>C</th>
                      <th>Ch</th>
                      <th>P</th>
                      <th>J.P</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    for($i=0;$i<count($d);$i++){
                    ?>
                    <tr>
                      <td><?php echo $i+1 ?></td>
                      <td><?php echo $d[$i]['nik'] ?></td>
                      <td><?php 
                          echo $d[$i]['nama'].'<br>'.$d[$i]['nm_bagian'].'<br>'.$d[$i]['grup'].' '.$d[$i]['libur_s'];
                          if($d[$i]['kd_status']==4){
                            echo '<br><small><i>Resign Tgl. '.$d[$i]['tgl_update'].'</i></small>';
                          }
                      ?></td>
                      <td><?php echo $d[$i]['pagi'] ?></td>
                      <td><?php echo $d[$i]['siang'] ?></td>
                      <td><?php echo $d[$i]['malam'] ?></td>
                      <td><?php echo $d[$i]['masuk'] ?></td>
                      <td><?php echo $d[$i]['libur'] ?></td>
                      <td><?php echo $d[$i]['rumah'] ?></td>
                      <td><?php echo $d[$i]['nt_premi'] ?></td>
                      <td><?php echo $d[$i]['lembur_w'] ?></td>
                      <td><?php 
                        $ttlwj = carijam($d[$i]['t_lembur_w']);
                        $ttlwm = carimenit($d[$i]['t_lembur_w'], $ttlwj);
                        $ttlwm == 30 ? $ttlwm = 5 : $ttlwm = 0;
                        echo idr($ttlwj).'.'.$ttlwm;
                      ?></td>
                      <td><?php echo $d[$i]['t_lbiasa'] ?></td>
                      <td><?php echo $d[$i]['t_llibur'] ?></td>
                      <td><?php 
                        $ttlj = carijam($d[$i]['t_lembur']);
                        $ttlm = carimenit($d[$i]['t_lembur'], $ttlj);
                        $ttlm == 30 ? $ttlm = 5 : $ttlm = 0;
                        echo idr($ttlj).'.'.$ttlm;
                      ?></td>
                      <td><?php echo $d[$i]['alpha'] ?></td>
                      <td><?php echo $d[$i]['ijin'] ?></td>
                      <td><?php echo $d[$i]['sdr'] ?></td>
                      <td><?php echo $d[$i]['cuti'] ?></td>
                      <td><?php echo $d[$i]['cuti_h'] ?></td>
                      <td><?php echo $d[$i]['pijin'] ?></td>
                      <td><?php 
                        $ttljp = carijam($d[$i]['t_ijin']);
                        $ttlmp = carimenit($d[$i]['t_ijin'], $ttljp);                        
                        $ttlmp == 30 ? $ttlmp = 5 : $ttlmp = 0;
                        echo idr($ttljp).'.'.$ttlmp;
                      ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                  </tbody>
                </table>
            <?php
            }
            ?>
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<script>
$(document).ready(function() {
  var emptyOption = '<option value=""></option>';
  var bags = JSON.parse('<?php echo json_encode($bags);?>');

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

  $("#departemen.select2_single").select2({
    placeholder: "Pilih Departemen",
    allowClear: true
  });


  
  $("#departemen.select2_single").select2({
      placeholder: "Pilih Departemen",
      allowClear: true
  }).on('change', function(e) {
    var kd_departemen = parseInt(e.currentTarget.value);
    var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
    
    $('#bagian').html(emptyOption);
    $(list_bagian).each(function(i, item) {
      $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
    });

    $("#bagian.select2_single").select2('destroy').select2({
      placeholder: "Pilih Bagian",
      allowClear: true,
      disabled: false
    });
  });



  $("#bagian.select2_single").select2({
    placeholder: "Pilih Bagian",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  }).show(function(e){
    var bagi = parseInt(document.getElementById('bagi').value);
    var ed = document.getElementById('departemen');
    var kd_departemen = parseInt(ed.options[ed.selectedIndex].value);
    var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
    $('#bagian').html(emptyOption);
    $(list_bagian).each(function(i, item) {
      $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
    });
  });


  var table = $('#myTablex').DataTable();
  $('#myTablex tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('row_selected') ) {
          $(this).removeClass('row_selected');
      }
      else {
          table.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
      }
  } );

});
</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>