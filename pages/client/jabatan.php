<?php 
require '_base_head.php';
$mjab = new \App\Models\Jabatan($app);
$jabs = $mjab->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mjab->getById($app->input->get('edit'));
}

$redirect = url('c/jabatan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Jabatan Pekerjaan</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('c/jabatan' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'jabatan';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="jabatan">
            Jabatan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="jabatan" id="jabatan" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nm_jabatan'] : '' ;?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="ket">
            Keterangan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="ket" id="ket" class="form-control"><?php echo $edit ? $edit['ket'] : '' ;?></textarea>
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'jabatan_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Jabatan</th>
                      <th>Keterangan</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($jabs as $index => $jab) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo $jab['nm_jabatan'];?></td>
                      <td><?php echo $jab['ket'];?></td>
                      <td>
                        <a href="<?php echo url('c/jabatan?edit=' . $jab['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('c/jabatan?_method=delete&id=' . $jab['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php require '_base_foot.php';?>