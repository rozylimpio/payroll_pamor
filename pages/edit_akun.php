<?php
$redirect = $app->input->get('redirect');
$nik = $app->sess->nik;

$makun = new \App\Models\User($app);
$edit = $makun->getByNik($nik);

//print_r($edit);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Akun</title>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo url();?>img/logo_.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo url();?>css/materialize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo url();?>css/login.css">
	<script type="text/javascript" src="<?php echo url();?>js/jquery.min.js"></script>
	<script>
	$(document).ready(function() {
	   $('select').attr("class", "browser-default")
	});
	</script>
</head>
<body>
<div class="row" style="margin-top: 2%;">
	<div class="col 12 m4 s12 offset-12 offset-m4" >
		<h3 class="center">Kelola <span style="color:#66CDAA">Akun</span></h3>

		<?php foreach($app->errors('edit_akun') as $error) { ?>
		<div class="msgbag-item row mepet card-panel red lighten-2 red-text text-lighten-5">
			<?php echo $error;?>
		</div>
		<?php } ?>
		<?php foreach($app->messages('edit_akun') as $message) { ?>
		<div class="msgbag-item row mepet card-panel green lighten-2 green-text text-lighten-5">
			<?php echo $message;?>
		</div>
		<?php } ?>

		<div class="card z-depth-2">
			<div class="card-content">
				<form action="<?php echo url('akun');?>" method="post" class="col s12">
        			<input type="hidden" name="_method" value="put">
					<div class="row mepet">
						<div class="input-field col s12">
							<i class="mdi-action-account-balance prefix"></i>
							<input type="text" name="nik" required="" readonly value="<?php echo $edit['nik'] ?>">
							<label>NIK</label>
						</div>
					</div>
					<div class="row mepet">
						<div class="input-field col s12">
							<i class="mdi-action-account-circle prefix"></i>
							<input type="text" name="nama" required="" readonly value="<?php echo $edit['nama'] ?>">
							<label>Nama</label>
						</div>
					</div>
					<div class="row mepet">
						<div class="input-field col s12">
							<i class="mdi-action-room prefix"></i>
							<input type="text" name="level" required="" readonly value="<?php echo $edit['posisi'] ?>">
							<label>Level</label>
						</div>
					</div>
					<div class="row mepet">
						<div class="input-field col s12">
							<i class="mdi-action-lock prefix"></i>
							<input type="password" name="pass" id="pass" required="" autofocus="">
							<label>Password</label>
							<a href="#" style="font-size:12px" onclick="myFunction()">Show Password</a>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="input-field col s12">
							<?php if(empty($redirect)){ ?>
							<a href="<?php echo url('logout')?>" style="color:#66CDAA;font-size:16px">Logout</a>
							<?php }else{ ?>
							<a href="<?php echo $redirect;?>" style="color:#66CDAA;font-size:16px"><- Back</a>
							<?php }?>
							<input type="submit" value="Submit" class="btn right">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo url();?>js/materialize.js"></script>
<script type="text/javascript" src="<?php echo url();?>js/msgbag.js"></script>

</body>
</html>

<script type="text/javascript">
	
	function myFunction() {
	  var x = document.getElementById("pass");
	  if (x.type === "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}
</script>