<?php
$mtki = new \App\Models\Karyawan($app);

$ijin = $app->input->post('ijin');
$tgl = date("d-m-Y");

$judul = 'Upload_'.$ijin.'_-_'.$tgl;


if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('sdr', 'File harus .csv');
        header('Location: ' . url('a/sdr'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    for($j=0;$j<count($data);$j++){
      $datar[$j]['nik'] = $data[$j][0];
      $datar[$j]['tgl_aw'] = dateCreate(str_replace('/', '-', $data[$j][1]));
      $datar[$j]['tgl_ak'] = dateCreate(str_replace('/', '-', $data[$j][2]));
      $datar[$j]['tgl_up'] = dateCreate(str_replace('/', '-', $data[$j][3]));
    }

    //print_r($datar);die();
    if ($ijin == 'kk') {
        $a = $mtki->kkUpload($datar);
    }else{
        $a = $mtki->sdrUpload($datar);
    }

    if($a == count($datar)){
        $app->addMessage('sdr', 'Berhasil Upload');
        header('Location: ' . url('a/sdr_u'));
    }else{
        $app->addError('sdr', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/sdr_u'));    
    }
    
} else {
    $app->addError('sdr', 'File Upload Gagal');
    header('Location: ' . url('a/sdr_u'));
}
?>