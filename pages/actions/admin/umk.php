<?php

$umk = str_replace('.', '', $app->input->post('umk'));
$tgl = dateCreate($app->input->post('tgl'));
$tambah = $app->input->post('tambah');
//echo $tgl;
$mumk = new \App\Models\Umk($app);

if($insert_id = $mumk->add($umk, $tgl, $tambah)) {
    $app->addMessage('umk', 'UMK Telah Berhasil Disimpan');
}
else {
    $app->addError('umk', 'UMK Gagal Disimpan');
}

header('Location: ' . url('a/umk'));