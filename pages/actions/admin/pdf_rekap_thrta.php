<?php
$thn = $app->input->post('thn');
$kode = $app->input->post('kode');

$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();

$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

ob_start();
?><html>
<head>
<meta charset='utf-8'>
<style>
    @page { margin: 20px 10px 20px 40px; }
    body {
        line-height: 1.1;
        font-size: 12pt;
        font-family: "Arial Narrow", Arial, sans-serif;
        color: black;
        margin: 20px 10px 20px 40px;
    }
  .kar{
    line-height: 1.1;
    font-family: Calibri;
  }
    table {
	  	border-collapse: collapse;
	  }
    table, tr, th, td { 
    	border: solid 1px; 
    	padding: 2.5px;
    }
  .sisi{
    border-left: solid 1px;
    border-right: solid 1px;
    border-top: 0px;
    border-bottom: 0px;
  }
  .kosong{
    border-left: 0px;
    border-right: 0px;
    border-top: 0px;
    border-bottom: 0px;
  }
    .page_break { page-break-before: always; }
    /*br{
        display: block; /* makes it have a width */
      /*  content: ""; /* clears default height */
       /*  margin-top: 20; /* change this to whatever height you want it */
    /* }*/
</style>
</head>
<body>
	<?php
	$datas = $mkal->getRekapTHRTA($thn, $kode);

	$da = [];
	for($w=0; $w<count($datas);$w++){
		if($datas[$w]['dept'] == 'produksi'){
		    $da['produksi'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'produksi n'){
		    $da['produksi'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'pemasaran gudang'){
		    $da['produksi'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'logistik gudang'){
		    $da['produksi'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'exim'){
		    $da['produksi'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'utility'){
		    $da['utility'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'pemasaran adm'){
		    $da['pemasaran'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'keuangan'){
		    $da['keuangan'][] = $datas[$w];
		}elseif($datas[$w]['dept'] == 'personalia'){
		    $da['personalia'][] = $datas[$w];
		}else{
		    $da['umum'][] = $datas[$w];
		}
	}
	$gbbp=0;$gbbu=0;$gbbi=0;$gbba=0;$gbbk=0;$gbbe=0;
	$ttp=0;$ttu=0;$tti=0;$tta=0;$ttk=0;$tte=0;
	?>
	<div style="font-size: 12px">
	PT. Pamor Spinning Mills<br>
	Jaten, Karanganyar<br>
	<u>S U R A K A R T A</u>
	</div>

	<br><br><br>
	<div align="center" style="font-size: 13pt">
	<b>
	  REKAP <?php echo $kode == 'thr' ? 'T.H.R' : 'TALI ASIH' ?> KARYAWAN PT. PAMOR SPINNING MILLS<br>
	  <?php echo 'TAHUN '.$thn; ?>
	</b>
	</div>      
	<br><br>
	<table width="100% auto" style="font-size: 12px;">
        <thead>
          <tr>
            <th align="center" width="5%"><br>NO<br><br></th>
            <th align="center" width="45%"><br>BAGIAN<br><br></th>
            <th align="center"><br>THR<br><br></th>
            <th align="center"><br>TOTAL<br><br></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="sisi" align="center">1</td>
            <td class="sisi" width="15%">PRODUKSI</td>
            <td class="sisi"></td><td class="sisi"></td>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['produksi']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi">
              <?php 
                echo ' &nbsp; - &nbsp;'.strtoupper($da['produksi'][$q]['dept']);
                if($da['produksi'][$q]['dept']=='produksi'){ 
                  if($da['produksi'][$q]['grup']!='N'){
                    echo ' SHIFT '.$da['produksi'][$q]['grup'];
                  }else{
                    echo ' '.$da['produksi'][$q]['grup'];
                  }
                }else{ 
                  echo ''; 
                }
              ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['total']); $gbbp += $da['produksi'][$q]['total']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td class="sisi"></td>
            <td class="sisi"></td><td class="sisi"></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttp += $gbbp; echo idr($ttp); ?></b></td>
          </tr>
          <tr>
            <td class="sisi" align="center">2</td>
            <td class="sisi">UTILITY</td>
            <td class="sisi"></td><td class="sisi"></td>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['utility']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi">
              <?php 
                echo ' &nbsp; - &nbsp;'.strtoupper($da['utility'][$q]['dept']);
                echo $da['utility'][$q]['grup']=='S' ? ' SHIFT' : ' N';
              ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['total']); $gbbu += $da['utility'][$q]['total']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td class="sisi"></td>
            <td class="sisi"></td><td class="sisi"></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttu += $gbbu; echo idr($ttu); ?></b></td>
          </tr>
          <tr>
            <td class="sisi" align="center">3</td>
            <td class="sisi"><?php echo strtoupper($da['pemasaran'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['pemasaran'][0]['total']); $gbba += $da['pemasaran'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $tta += $gbba; echo idr($tta); ?></b></td>
          </tr>                    
          <tr>
            <td class="sisi" align="center">4</td>
            <td class="sisi"><?php echo strtoupper($da['keuangan'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['keuangan'][0]['total']); $gbbk += $da['keuangan'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttk += $gbbk; echo idr($ttk); ?></b></td>
          </tr>                   
          <tr>
            <td class="sisi" align="center">5</td>
            <td class="sisi"><?php echo strtoupper($da['personalia'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['personalia'][0]['total']); $gbbe += $da['personalia'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $tte += $gbbe; echo idr($tte); ?></b></td>
          </tr>                    
          <tr>
            <td class="sisi" align="center">6</td>
            <td class="sisi">UMUM DLL</td>
            <td class="sisi"></td><td class="sisi"></td>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['umum']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi"><?php echo ' &nbsp; - &nbsp;'.strtoupper($da['umum'][$q]['dept']);?></td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['total']); $gbbi += $da['umum'][$q]['total']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td class="sisi"></td>
            <td class="sisi"></td><td class="sisi"></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $tti += $gbbi; echo idr($tti); ?></b></td>
          </tr>
        </tbody>
        <tfoot style="font-size:11pt">
          <tr>
            <th colspan="2"></th>
            <th align="right"><br><?php echo /*'9.999.999.999'*/idr($gbbp+$gbbu+$gbbi+$gbba+$gbbk+$gbbe); ?><br>&nbsp;</th>
            <th align="right"><br><?php echo /*'9.999.999.999'*/idr($ttp+$ttu+$tti+$tta+$ttk+$tte); ?><br>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
      <br>
      <div style="font-size: 10pt; float: right; margin-right: 5px">
        <i>( <?php echo terbilang($ttp+$ttu+$tti+$tta+$ttk+$tte); ?> )</i>
      </div>
      <br><br><br>
      <table border="0" width="100% auto" style="font-size:10pt; border: none 0px;">
        <tr>
          <td align="center" class="kosong">
            <br>
            Mengetahui,
            <br><br><br><br><br>
            <u>H. Dewanto Kusuma Wibowo,SE</u>
            <br>
            Direktur Utama
          </td>
          <td align="center" class="kosong">
            <br>
            Mengetahui,
            <br><br><br><br><br>
            <u><?php echo $mng[0]['nama']; ?></u><br>
            <?php echo $mng[0]['jabatan']; ?>
          </td>
          <td align="center" class="kosong">
            Jaten, <?php echo date('d').' '.namaBulan(date('m')).' '.date('Y');?>
            <br>
            Bag. Personalia
            <br><br><br><br><br><br><br><br>
          </td>
        </tr>
    </table>		

</body>
</html>
<?php

$html = ob_get_clean();

$subdir = 'pdf/rincian/';
$dir = $pubdir . '/' . $subdir;

$exists = false;
do {
    $name = $thn.'_Rekap_'.strtoupper($kode).'.pdf';
    $exists = file_exists($dir . $name);
    if($exists){ unlink($dir . $name); $exists = false; }
} while($exists);

$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml($html);
$dompdf->setPaper('F4', 'portrait');
$dompdf->render();

$pdf_gen = $dompdf->output();

if(!file_put_contents($dir . $name, $pdf_gen)){
    header("HTTP/1.0 500 Internal Server Error");
    echo 'Generate PDF Failed';
    exit();
} else {
    $mval->addRekapTHRTA($thn, $subdir . $name, $kode);

    header("Content-Type: application/json");
    echo json_encode([
        'filename' => $name,
        'filepath' => url($subdir . $name)
    ]);
}
?>