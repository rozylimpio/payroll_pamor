<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$thn = $app->input->post('tahun');
$bln = $app->input->post('bulan');

$mval = new \App\Models\Validasi($app);
$mkal = new \App\Models\Kalkulasi($app);


require $sysdir . '/excelstyle.php';

$tgl = date($thn.'-'.$bln.'-14');
$blnn = date('m', strtotime("-1 month", strtotime($tgl)));
$thnn = date('Y', strtotime("-1 month", strtotime($tgl)));
$datar = $mkal->getRekapPot($thnn, $blnn);
$data = $mkal->getRekapPot($thn, $bln);

$d = [];
for($l=0; $l<count($data);$l++){
	if($data[$l]['dept'] == 'produksi'){
		$d['produksi'][] = $data[$l];
	}elseif($data[$l]['dept'] == 'produksi n'){
		$d['produksi_l'][] = $data[$l];
	}elseif($data[$l]['dept'] == 'pemasaran gudang'){
		$d['produksi_l'][] = $data[$l];
	}elseif($data[$l]['dept'] == 'logistik gudang'){
		$d['produksi_l'][] = $data[$l];
	}elseif($data[$l]['dept'] == 'exim'){
		$d['produksi_l'][] = $data[$l];
	}elseif($data[$l]['dept'] == 'utility'){
		$d['utility'][] = $data[$l];
	}else{
		$d['umum'][] = $data[$l];
	}
}

$dd = [];
for($n=0; $n<count($datar);$n++){
	if($datar[$n]['dept'] == 'produksi'){
		$dd['produksi'][] = $datar[$n];
	}elseif($datar[$n]['dept'] == 'produksi n'){
		$dd['produksi_l'][] = $datar[$n];
	}elseif($datar[$n]['dept'] == 'pemasaran gudang'){
		$dd['produksi_l'][] = $datar[$n];
	}elseif($datar[$n]['dept'] == 'logistik gudang'){
		$dd['produksi_l'][] = $datar[$n];
	}elseif($datar[$n]['dept'] == 'exim'){
		$dd['produksi_l'][] = $datar[$n];
	}elseif($datar[$n]['dept'] == 'utility'){
		$dd['utility'][] = $datar[$n];
	}else{
		$dd['umum'][] = $datar[$n];
	}
}

$jkp=0;$gpp=0;$mkp=0;$insp=0;$lemp=0;$premp=0;$tjabp=0;$tahp=0;$tpresp=0;$gbp=0;$jmp=0;$jump=0;
$jkpp=0;$gppp=0;$mkpp=0;$inspp=0;$lempp=0;$prempp=0;$tjabpp=0;$tahpp=0;$tprespp=0;$gbpp=0;$jmpp=0;$jumpp=0;
$jku=0;$gpu=0;$mku=0;$insu=0;$lemu=0;$premu=0;$tjabu=0;$tahu=0;$tpresu=0;$gbu=0;$jmu=0;$jumu=0;
$jkuu=0;$gpuu=0;$mkuu=0;$insuu=0;$lemuu=0;$premuu=0;$tjabuu=0;$tahuu=0;$tpresuu=0;$gbuu=0;$jmuu=0;$jumuu=0;
$jki=0;$gpi=0;$mki=0;$insi=0;$lemi=0;$premi=0;$tjabi=0;$tahi=0;$tpresi=0;$gbi=0;$jmi=0;$jumi=0;
$jkii=0;$gpii=0;$mkii=0;$insii=0;$lemii=0;$premii=0;$tjabii=0;$tahii=0;$tpresii=0;$gbii=0;$jmii=0;$jumii=0;

$filename = $thn.'_'.$bln.'_Lap_Perbandingan_Gaji';
$formname = '';
$title = 'PERBANDINGAN GAJI BRUTO KARYAWAN PT. PAMOR SPINNING MILLS'; 
$sub = ''; 
$subtitle = 'BULAN : '.strtoupper(namaBulan($blnn)).' '.$thnn.' - '.strtoupper(namaBulan($bln)).' '.$thn;

$excel_data = [];
$total_data = [];
if(!empty($d) || !empty($dd)){
	!empty($d) ? $jmlg = count($d['produksi']) : $jmlg = 0;
	!empty($dd) ? $jmlgg = count($dd['produksi']) : $jmlgg = 0;
	$jmlg>$jmlgg ? $count = $jmlg : $count = $jmlgg;



	$excel_data[] = ['PRODUKSI', '','','','','','','','','','','','','','','','','','','','','','','',''];
	for ($k=0; $k < $count; $k++) {
		if($jmlg>$jmlgg){
		  $dept = '  -  '.strtoupper($d['produksi'][$k]['dept']).' '.$d['produksi'][$k]['grup']; 
		}else{
		  $dept = '  -  '.strtoupper($dd['produksi'][$k]['dept']).' '.$dd['produksi'][$k]['grup']; 
		}

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['jml_kar'])){ $jkpp += $dd['produksi'][$k]['jml_kar']; 
		$ddjkpp = $dd['produksi'][$k]['jml_kar'];} else { $ddjkpp = '0'; } }else { $ddjkpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['jml_kar'])){ $jkp += $d['produksi'][$k]['jml_kar']; 
        $djkp = $d['produksi'][$k]['jml_kar'];} else { $djkp = '0'; } }else { $djkp = '0'; }

        if(!empty($dd)){ if(!empty($dd['produksi'][$k]['gp'])){ $gppp += $dd['produksi'][$k]['gp']; 
		$ddgppp = $dd['produksi'][$k]['gp'];}else{ $ddgppp = '0'; } }else { $ddgppp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['gp'])){ $gpp += $d['produksi'][$k]['gp']; 
		$dgpp = $d['produksi'][$k]['gp'];}else{ $dgpp = '0'; } }else { $dgpp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['masakerja'])){ $mkpp += $dd['produksi'][$k]['masakerja']; 
		$ddmkpp = $dd['produksi'][$k]['masakerja'];}else{ $ddmkpp = '0'; } }else { $ddmkpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['masakerja'])){ $mkp += $d['produksi'][$k]['masakerja']; 
		$dmkp = $d['produksi'][$k]['masakerja'];}else{ $dmkp = '0'; } }else { $dmkp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['insentif'])){ $inspp += $dd['produksi'][$k]['insentif']; 
		$ddinspp = $dd['produksi'][$k]['insentif'];}else{ $ddinspp = '0'; } }else { $ddinspp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['insentif'])){ $insp += $d['produksi'][$k]['insentif']; 
		$dinsp = $d['produksi'][$k]['insentif'];}else{ $dinsp = '0'; } }else { $dinsp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['lembur'])){ $lempp += $dd['produksi'][$k]['lembur']; 
		$ddlempp = $dd['produksi'][$k]['lembur'];}else{ $ddlempp = '0'; } }else { $ddlempp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['lembur'])){ $lemp += $d['produksi'][$k]['lembur']; 
		$dlemp = $d['produksi'][$k]['lembur'];}else{ $dlemp = '0'; } }else { $dlemp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['premi'])){ $prempp += $dd['produksi'][$k]['premi']; 
		$ddprempp = $dd['produksi'][$k]['premi'];}else{ $ddprempp = '0'; } }else { $ddprempp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['premi'])){ $premp += $d['produksi'][$k]['premi']; 
		$dpremp = $d['produksi'][$k]['premi'];}else{ $dpremp = '0'; } }else { $dpremp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['tjab'])){ $tjabpp += $dd['produksi'][$k]['tjab']; 
		$ddtjabpp = $dd['produksi'][$k]['tjab'];}else{ $ddtjabpp = '0'; } }else { $ddtjabpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['tjab'])){ $tjabp += $d['produksi'][$k]['tjab']; 
		$dtjabp = $d['produksi'][$k]['tjab'];}else{ $dtjabp = '0'; } }else { $dtjabp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['tahli'])){ $tahpp += $dd['produksi'][$k]['tahli']; 
		$ddtahpp = $dd['produksi'][$k]['tahli'];}else{ $ddtahpp = '0'; } }else { $ddtahpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['tahli'])){ $tahp += $d['produksi'][$k]['tahli']; 
		$dtahp = $d['produksi'][$k]['tahli'];}else{ $dtahp = '0'; } }else { $dtahp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['tprestasi'])){ $tprespp += $dd['produksi'][$k]['tprestasi']; 
		$ddtprespp = $dd['produksi'][$k]['tprestasi'];}else{ $ddtprespp = '0'; } }else { $ddtprespp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['tprestasi'])){ $tpresp += $d['produksi'][$k]['tprestasi']; 
		$dtpresp = $d['produksi'][$k]['tprestasi'];}else{ $dtpresp = '0'; } }else { $dtpresp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['u_makan'])){ $jumpp += $dd['produksi'][$k]['u_makan']; 
		$ddjumpp = $dd['produksi'][$k]['u_makan'];}else{ $ddjumpp = '0'; } }else { $ddjumpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['u_makan'])){ $jump += $d['produksi'][$k]['u_makan']; 
		$djump = $d['produksi'][$k]['u_makan'];}else{ $djump = '0'; } }else { $djump = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi'][$k]['gj_bruto'])){ $gbpp += $dd['produksi'][$k]['gj_bruto']; 
		$ddgbpp = $dd['produksi'][$k]['gj_bruto'];}else{ $ddgbpp = '0'; } }else { $ddgbpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi'][$k]['gj_bruto'])){ $gbp += $d['produksi'][$k]['gj_bruto']; 
		$dgbp = $d['produksi'][$k]['gj_bruto'];}else{ $dgbp = '0'; } }else { $dgbp = '0'; }

    	if(empty($dd)){ $ddjmpp = '0,0'; }else{ if(empty($dd['produksi'][$k]['jml_lembur'])){ $ddjmpp = '0,0'; }else{
        $ttljpp = carijam($dd['produksi'][$k]['jml_lembur']);
        $ttlmpp = carimenit($dd['produksi'][$k]['jml_lembur'], $ttljpp);
        $ttlmpp == 30 ? $ttlmpp = 5 : $ttlmpp = 0;
        $ddjmpp = $ttljpp.','.$ttlmpp; 
        $jmpp += $dd['produksi'][$k]['jml_lembur']; } }

        if(empty($d)){ $djmp = '0,0'; }else{ if(empty($d['produksi'][$k]['jml_lembur'])){ $djmp = '0,0'; }else{
		$ttljp = carijam($d['produksi'][$k]['jml_lembur']); 
		$ttlmp = carimenit($d['produksi'][$k]['jml_lembur'], $ttljp);
        $ttlmp == 30 ? $ttlmp = 5 : $ttlmp = 0;
		$djmp = $ttljp.','.$ttlmp; 
		$jmp += $d['produksi'][$k]['jml_lembur']; } }


	    $excel_data[] = [
	        $dept,
	        $ddjkpp,
	        $djkp,
	        $ddgppp,
	        $dgpp,
	        $ddmkpp,
	        $dmkp,
	        $ddinspp,
	        $dinsp,
	        $ddlempp,
	        $dlemp,
	        $ddprempp,
	        $dpremp,
	        $ddtjabpp,
	        $dtjabp,
	        $ddtahpp,
	        $dtahp,
	        $ddtprespp,
	        $dtpresp,
	        $ddjumpp,
	        $djump,
	        $ddgbpp,
	        $dgbp,
	        $ddjmpp,
	        $djmp
	    ];
	}
    
    for ($k=0; $k < count($d['produksi_l']); $k++) {
		$dept = '  -  '.strtoupper($d['produksi_l'][$k]['dept']).' ';
        if($d['produksi_l'][$k]['dept']=='produksi'){ $dept .= $d['produksi_l'][$k]['grup']; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['jml_kar'])){ $jkpp += $dd['produksi_l'][$k]['jml_kar']; 
		$ddjkpp = $dd['produksi_l'][$k]['jml_kar'];} else { $ddjkpp = '0'; } }else { $ddjkpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['jml_kar'])){ $jkp += $d['produksi_l'][$k]['jml_kar']; 
        $djkp = $d['produksi_l'][$k]['jml_kar'];} else { $djkp = '0'; } }else { $djkp = '0'; }

        if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['gp'])){ $gppp += $dd['produksi_l'][$k]['gp']; 
		$ddgppp = $dd['produksi_l'][$k]['gp'];}else{ $ddgppp = '0'; } }else { $ddgppp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['gp'])){ $gpp += $d['produksi_l'][$k]['gp']; 
		$dgpp = $d['produksi_l'][$k]['gp'];}else{ $dgpp = '0'; } }else { $dgpp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['masakerja'])){ $mkpp += $dd['produksi_l'][$k]['masakerja']; 
		$ddmkpp = $dd['produksi_l'][$k]['masakerja'];}else{ $ddmkpp = '0'; } }else { $ddmkpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['masakerja'])){ $mkp += $d['produksi_l'][$k]['masakerja']; 
		$dmkp = $d['produksi_l'][$k]['masakerja'];}else{ $dmkp = '0'; } }else { $dmkp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['insentif'])){ $inspp += $dd['produksi_l'][$k]['insentif']; 
		$ddinspp = $dd['produksi_l'][$k]['insentif'];}else{ $ddinspp = '0'; } }else { $ddinspp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['insentif'])){ $insp += $d['produksi_l'][$k]['insentif']; 
		$dinsp = $d['produksi_l'][$k]['insentif'];}else{ $dinsp = '0'; } }else { $dinsp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['lembur'])){ $lempp += $dd['produksi_l'][$k]['lembur']; 
		$ddlempp = $dd['produksi_l'][$k]['lembur'];}else{ $ddlempp = '0'; } }else { $ddlempp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['lembur'])){ $lemp += $d['produksi_l'][$k]['lembur']; 
		$dlemp = $d['produksi_l'][$k]['lembur'];}else{ $dlemp = '0'; } }else { $dlemp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['premi'])){ $prempp += $dd['produksi_l'][$k]['premi']; 
		$ddprempp = $dd['produksi_l'][$k]['premi'];}else{ $ddprempp = '0'; } }else { $ddprempp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['premi'])){ $premp += $d['produksi_l'][$k]['premi']; 
		$dpremp = $d['produksi_l'][$k]['premi'];}else{ $dpremp = '0'; } }else { $dpremp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['tjab'])){ $tjabpp += $dd['produksi_l'][$k]['tjab']; 
		$ddtjabpp = $dd['produksi_l'][$k]['tjab'];}else{ $ddtjabpp = '0'; } }else { $ddtjabpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['tjab'])){ $tjabp += $d['produksi_l'][$k]['tjab']; 
		$dtjabp = $d['produksi_l'][$k]['tjab'];}else{ $dtjabp = '0'; } }else { $dtjabp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['tahli'])){ $tahpp += $dd['produksi_l'][$k]['tahli']; 
		$ddtahpp = $dd['produksi_l'][$k]['tahli'];}else{ $ddtahpp = '0'; } }else { $ddtahpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['tahli'])){ $tahp += $d['produksi_l'][$k]['tahli']; 
		$dtahp = $d['produksi_l'][$k]['tahli'];}else{ $dtahp = '0'; } }else { $dtahp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['tprestasi'])){ $tprespp += $dd['produksi_l'][$k]['tprestasi']; 
		$ddtprespp = $dd['produksi_l'][$k]['tprestasi'];}else{ $ddtprespp = '0'; } }else { $ddtprespp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['tprestasi'])){ $tpresp += $d['produksi_l'][$k]['tprestasi']; 
		$dtpresp = $d['produksi_l'][$k]['tprestasi'];}else{ $dtpresp = '0'; } }else { $dtpresp = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['u_makan'])){ $jumpp += $dd['produksi_l'][$k]['u_makan']; 
		$ddjumpp = $dd['produksi_l'][$k]['u_makan'];}else{ $ddjumpp = '0'; } }else { $ddjumpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['u_makan'])){ $jump += $d['produksi_l'][$k]['u_makan']; 
		$djump = $d['produksi_l'][$k]['u_makan'];}else{ $djump = '0'; } }else { $djump = '0'; }

		if(!empty($dd)){ if(!empty($dd['produksi_l'][$k]['gj_bruto'])){ $gbpp += $dd['produksi_l'][$k]['gj_bruto']; 
		$ddgbpp = $dd['produksi_l'][$k]['gj_bruto'];}else{ $ddgbpp = '0'; } }else { $ddgbpp = '0'; }

		if(!empty($d)){ if(!empty($d['produksi_l'][$k]['gj_bruto'])){ $gbp += $d['produksi_l'][$k]['gj_bruto']; 
		$dgbp = $d['produksi_l'][$k]['gj_bruto'];}else{ $dgbp = '0'; } }else { $dgbp = '0'; }

    	if(empty($dd)){ $ddjmpp = '0,0'; }else{ if(empty($dd['produksi_l'][$k]['jml_lembur'])){ $ddjmpp = '0,0'; }else{
        $ttljpp = carijam($dd['produksi_l'][$k]['jml_lembur']);
        $ttlmpp = carimenit($dd['produksi_l'][$k]['jml_lembur'], $ttljpp);
        $ttlmpp == 30 ? $ttlmpp = 5 : $ttlmpp = 0;
        $ddjmpp = $ttljpp.','.$ttlmpp; $jmpp += $dd['produksi_l'][$k]['jml_lembur']; } }

        if(empty($d)){ $djmp = '0,0'; }else{ if(empty($d['produksi_l'][$k]['jml_lembur'])){ $djmp = '0,0'; }else{
		$ttljp = carijam($d['produksi_l'][$k]['jml_lembur']); 
		$ttlmp = carimenit($d['produksi_l'][$k]['jml_lembur'], $ttljp);		
        $ttlmp == 30 ? $ttlmp = 5 : $ttlmp = 0;
		$djmp = $ttljp.','.$ttlmp; 
		$jmp += $d['produksi_l'][$k]['jml_lembur']; } }


	    $excel_data[] = [
	        $dept,
	        $ddjkpp,
	        $djkp,
	        $ddgppp,
	        $dgpp,
	        $ddmkpp,
	        $dmkp,
	        $ddinspp,
	        $dinsp,
	        $ddlempp,
	        $dlemp,
	        $ddprempp,
	        $dpremp,
	        $ddtjabpp,
	        $dtjabp,
	        $ddtahpp,
	        $dtahp,
	        $ddtprespp,
	        $dtpresp,
	        $ddjumpp,
	        $djump,
	        $ddgbpp,
	        $dgbp,
	        $ddjmpp,
	        $djmp
	    ];
	}

	$ttljtpp = carijam($jmpp);
	$ttlmtpp = carimenit($jmpp, $ttljtpp);
	empty($ttlmtpp) ? $ttlmtpp = 0 : $ttlmtpp = 5;
	$ddtjmpp = $ttljtpp.','.$ttlmtpp;		
	
	$ttljtp = carijam($jmp);
	$ttlmtp = carimenit($jmp, $ttljtp);
	empty($ttlmtp) ? $ttlmtp = 0 : $ttlmtp = 5;
	$dtjmp = $ttljtp.','.$ttlmtp;

	$excel_data[] = [
		'',
        $jkpp,
        $jkp,
        $gppp,
        $gpp,
        $mkpp,
        $mkp,
        $inspp,
        $insp,
        $lempp,
        $lemp,
        $prempp,
        $premp,
        $tjabpp,
        $tjabp,
        $tahpp,
        $tahp,
        $tprespp,
        $tpresp,
        $jumpp,
        $jump,
        $gbpp,
        $gbp,
        $ddtjmpp,
        $dtjmp    
	];

	$excel_data[] = ['UTILITY', '','','','','','','','','','','','','','','','','','','','','','','',''];
	for ($k=0; $k < count($d['utility']); $k++) {
		$dept = '  -  '.strtoupper($d['utility'][$k]['dept']).' ';
		if($d['utility'][$k]['grup'] == 'S'){ $dept .= 'SHIFT'; }else{ $dept .= 'N'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['jml_kar'])){ $jkuu += $dd['utility'][$k]['jml_kar']; 
		$ddjkuu = $dd['utility'][$k]['jml_kar'];} else { $ddjkuu = '0'; } }else { $ddjkuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['jml_kar'])){ $jku += $d['utility'][$k]['jml_kar']; 
        $djku = $d['utility'][$k]['jml_kar'];} else { $djku = '0'; } }else { $djku = '0'; }

        if(!empty($dd)){ if(!empty($dd['utility'][$k]['gp'])){ $gpuu += $dd['utility'][$k]['gp']; 
		$ddgpuu = $dd['utility'][$k]['gp'];}else{ $ddgpuu = '0'; } }else { $ddgpuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['gp'])){ $gpu += $d['utility'][$k]['gp']; 
		$dgpu = $d['utility'][$k]['gp'];}else{ $dgpu = '0'; } }else { $dgpu = '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['masakerja'])){ $mkuu += $dd['utility'][$k]['masakerja']; 
		$ddmkuu = $dd['utility'][$k]['masakerja'];}else{ $ddmkuu = '0'; } }else { $ddmkuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['masakerja'])){ $mku+= $d['utility'][$k]['masakerja']; 
		$dmku= $d['utility'][$k]['masakerja'];}else{ $dmku= '0'; } }else { $dmku= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['insentif'])){ $insuu += $dd['utility'][$k]['insentif']; 
		$ddinsuu = $dd['utility'][$k]['insentif'];}else{ $ddinsuu = '0'; } }else { $ddinsuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['insentif'])){ $insu+= $d['utility'][$k]['insentif']; 
		$dinsu= $d['utility'][$k]['insentif'];}else{ $dinsu= '0'; } }else { $dinsu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['lembur'])){ $lemuu += $dd['utility'][$k]['lembur']; 
		$ddlemuu = $dd['utility'][$k]['lembur'];}else{ $ddlemuu = '0'; } }else { $ddlemuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['lembur'])){ $lemu+= $d['utility'][$k]['lembur']; 
		$dlemu= $d['utility'][$k]['lembur'];}else{ $dlemu= '0'; } }else { $dlemu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['premi'])){ $premuu += $dd['utility'][$k]['premi']; 
		$ddpremuu = $dd['utility'][$k]['premi'];}else{ $ddpremuu = '0'; } }else { $ddpremuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['premi'])){ $premu+= $d['utility'][$k]['premi']; 
		$dpremu= $d['utility'][$k]['premi'];}else{ $dpremu= '0'; } }else { $dpremu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['tjab'])){ $tjabuu += $dd['utility'][$k]['tjab']; 
		$ddtjabuu = $dd['utility'][$k]['tjab'];}else{ $ddtjabuu = '0'; } }else { $ddtjabuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['tjab'])){ $tjabu+= $d['utility'][$k]['tjab']; 
		$dtjabu= $d['utility'][$k]['tjab'];}else{ $dtjabu= '0'; } }else { $dtjabu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['tahli'])){ $tahuu += $dd['utility'][$k]['tahli']; 
		$ddtahuu = $dd['utility'][$k]['tahli'];}else{ $ddtahuu = '0'; } }else { $ddtahuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['tahli'])){ $tahu+= $d['utility'][$k]['tahli']; 
		$dtahu= $d['utility'][$k]['tahli'];}else{ $dtahu= '0'; } }else { $dtahu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['tprestasi'])){ $tpresuu += $dd['utility'][$k]['tprestasi']; 
		$ddtpresuu = $dd['utility'][$k]['tprestasi'];}else{ $ddtpresuu = '0'; } }else { $ddtpresuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['tprestasi'])){ $tpresu+= $d['utility'][$k]['tprestasi']; 
		$dtpresu= $d['utility'][$k]['tprestasi'];}else{ $dtpresu= '0'; } }else { $dtpresu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['u_makan'])){ $jumuu += $dd['utility'][$k]['u_makan']; 
		$ddjumuu = $dd['utility'][$k]['u_makan'];}else{ $ddjumuu = '0'; } }else { $ddjumuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['u_makan'])){ $jumu+= $d['utility'][$k]['u_makan']; 
		$djumu= $d['utility'][$k]['u_makan'];}else{ $djumu= '0'; } }else { $djumu= '0'; }

		if(!empty($dd)){ if(!empty($dd['utility'][$k]['gj_bruto'])){ $gbuu += $dd['utility'][$k]['gj_bruto']; 
		$ddgbuu = $dd['utility'][$k]['gj_bruto'];}else{ $ddgbuu = '0'; } }else { $ddgbuu = '0'; }

		if(!empty($d)){ if(!empty($d['utility'][$k]['gj_bruto'])){ $gbu+= $d['utility'][$k]['gj_bruto']; 
		$dgbu= $d['utility'][$k]['gj_bruto'];}else{ $dgbu= '0'; } }else { $dgbu= '0'; }

    	if(empty($dd)){ $ddjmuu = '0,0'; }else{ if(empty($dd['utility'][$k]['jml_lembur'])){ $ddjmuu = '0,0'; }else{
        $ttljuu= carijam($dd['utility'][$k]['jml_lembur']);
        $ttlmuu= carimenit($dd['utility'][$k]['jml_lembur'], $ttljuu);        
        $ttlmuu == 30 ? $ttlmuu = 5 : $ttlmuu = 0;
        $ddjmuu= $ttljuu.','.$ttlmuu; $jmuu+= $dd['utility'][$k]['jml_lembur']; } }

        if(empty($d)){ $djmu= '0,0'; }else{ if(empty($d['utility'][$k]['jml_lembur'])){ $djmu= '0,0'; }else{
		$ttlju= carijam($d['utility'][$k]['jml_lembur']); $ttlmu= carimenit($d['utility'][$k]['jml_lembur'], $ttlju);       
        $ttlmu == 30 ? $ttlmu = 5 : $ttlmu = 0;
		$djmu= $ttlju.','.$ttlmu; $jmu+= $d['utility'][$k]['jml_lembur']; } }


	    $excel_data[] = [
	        $dept,
	        $ddjkuu,
	        $djku,
	        $ddgpuu,
	        $dgpu,
	        $ddmkuu,
	        $dmku,
	        $ddinsuu,
	        $dinsu,
	        $ddlemuu,
	        $dlemu,
	        $ddpremuu,
	        $dpremu,
	        $ddtjabuu,
	        $dtjabu,
	        $ddtahuu,
	        $dtahu,
	        $ddtpresuu,
	        $dtpresu,
	        $ddjumuu,
	        $djumu,
	        $ddgbuu,
	        $dgbu,
	        $ddjmuu,
	        $djmu
	    ];
	}

	$ttljtuu = carijam($jmuu);
	$ttlmtuu = carimenit($jmuu, $ttljtuu);
	empty($ttlmtuu) ? $ttlmtuu = 0 : $ttlmtuu = 5;
	$ddtjmuu = $ttljtuu.','.$ttlmtuu;		
	
	$ttljtu = carijam($jmu);
	$ttlmtu = carimenit($jmu, $ttljtu);
	empty($ttlmtu) ? $ttlmtu = 0 : $ttlmtu = 5;
	$dtjmu = $ttljtu.','.$ttlmtu;

	$excel_data[] = [
		'',
        $jkuu,
        $jku,
        $gpuu,
        $gpu,
        $mkuu,
        $mku,
        $insuu,
        $insu,
        $lemuu,
        $lemu,
        $premuu,
        $premu,
        $tjabuu,
        $tjabu,
        $tahuu,
        $tahu,
        $tpresuu,
        $tpresu,
        $jumuu,
        $jumu,
        $gbuu,
        $gbu,
        $ddtjmuu,
        $dtjmu
	];

	$excel_data[] = ['UMUM', '','','','','','','','','','','','','','','','','','','','','','','',''];
	for ($k=0; $k < count($d['umum']); $k++) {
		$dept = '  -  '.strtoupper($d['umum'][$k]['dept']);

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['jml_kar'])){ $jkii += $dd['umum'][$k]['jml_kar']; 
		$ddjkii = $dd['umum'][$k]['jml_kar'];} else { $ddjkii = '0'; } }else { $ddjkii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['jml_kar'])){ $jki += $d['umum'][$k]['jml_kar']; 
        $djki = $d['umum'][$k]['jml_kar'];} else { $djki = '0'; } }else { $djki = '0'; }

        if(!empty($dd)){ if(!empty($dd['umum'][$k]['gp'])){ $gpii += $dd['umum'][$k]['gp']; 
		$ddgpii = $dd['umum'][$k]['gp'];}else{ $ddgpii = '0'; } }else { $ddgpii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['gp'])){ $gpi += $d['umum'][$k]['gp']; 
		$dgpi = $d['umum'][$k]['gp'];}else{ $dgpi = '0'; } }else { $dgpi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['masakerja'])){ $mkii += $dd['umum'][$k]['masakerja']; 
		$ddmkii = $dd['umum'][$k]['masakerja'];}else{ $ddmkii = '0'; } }else { $ddmkii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['masakerja'])){ $mki += $d['umum'][$k]['masakerja']; 
		$dmki = $d['umum'][$k]['masakerja'];}else{ $dmki = '0'; } }else { $dmki = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['insentif'])){ $insii += $dd['umum'][$k]['insentif']; 
		$ddinsii = $dd['umum'][$k]['insentif'];}else{ $ddinsii = '0'; } }else { $ddinsii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['insentif'])){ $insi += $d['umum'][$k]['insentif']; 
		$dinsi = $d['umum'][$k]['insentif'];}else{ $dinsi = '0'; } }else { $dinsi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['lembur'])){ $lemii += $dd['umum'][$k]['lembur']; 
		$ddlemii = $dd['umum'][$k]['lembur'];}else{ $ddlemii = '0'; } }else { $ddlemii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['lembur'])){ $lemi += $d['umum'][$k]['lembur']; 
		$dlemi = $d['umum'][$k]['lembur'];}else{ $dlemi = '0'; } }else { $dlemi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['premi'])){ $premii += $dd['umum'][$k]['premi']; 
		$ddpremii = $dd['umum'][$k]['premi'];}else{ $ddpremii = '0'; } }else { $ddpremii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['premi'])){ $premi += $d['umum'][$k]['premi']; 
		$dpremi = $d['umum'][$k]['premi'];}else{ $dpremi = '0'; } }else { $dpremi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['tjab'])){ $tjabii += $dd['umum'][$k]['tjab']; 
		$ddtjabii = $dd['umum'][$k]['tjab'];}else{ $ddtjabii = '0'; } }else { $ddtjabii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['tjab'])){ $tjabi += $d['umum'][$k]['tjab']; 
		$dtjabi = $d['umum'][$k]['tjab'];}else{ $dtjabi = '0'; } }else { $dtjabi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['tahli'])){ $tahii += $dd['umum'][$k]['tahli']; 
		$ddtahii = $dd['umum'][$k]['tahli'];}else{ $ddtahii = '0'; } }else { $ddtahii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['tahli'])){ $tahi += $d['umum'][$k]['tahli']; 
		$dtahi = $d['umum'][$k]['tahli'];}else{ $dtahi = '0'; } }else { $dtahi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['tprestasi'])){ $tpresii += $dd['umum'][$k]['tprestasi']; 
		$ddtpresii = $dd['umum'][$k]['tprestasi'];}else{ $ddtpresii = '0'; } }else { $ddtpresii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['tprestasi'])){ $tpresi += $d['umum'][$k]['tprestasi']; 
		$dtpresi = $d['umum'][$k]['tprestasi'];}else{ $dtpresi = '0'; } }else { $dtpresi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['u_makan'])){ $jumii += $dd['umum'][$k]['u_makan']; 
		$ddjumii = $dd['umum'][$k]['u_makan'];}else{ $ddjumii = '0'; } }else { $ddjumii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['u_makan'])){ $jumi += $d['umum'][$k]['u_makan']; 
		$djumi = $d['umum'][$k]['u_makan'];}else{ $djumi = '0'; } }else { $djumi = '0'; }

		if(!empty($dd)){ if(!empty($dd['umum'][$k]['gj_bruto'])){ $gbii += $dd['umum'][$k]['gj_bruto']; 
		$ddgbii = $dd['umum'][$k]['gj_bruto'];}else{ $ddgbii = '0'; } }else { $ddgbii = '0'; }

		if(!empty($d)){ if(!empty($d['umum'][$k]['gj_bruto'])){ $gbi += $d['umum'][$k]['gj_bruto']; 
		$dgbi = $d['umum'][$k]['gj_bruto'];}else{ $dgbi = '0'; } }else { $dgbi = '0'; }

    	if(empty($dd)){ $ddjmii = '0,0'; }else{ if(empty($dd['umum'][$k]['jml_lembur'])){ $ddjmii = '0,0'; }else{
        $ttljii= carijam($dd['umum'][$k]['jml_lembur']);$ttlmii= carimenit($dd['umum'][$k]['jml_lembur'], $ttljii);       
        $ttlmii == 30 ? $ttlmii = 5 : $ttlmii = 0;
        $ddjmii= $ttljii.','.$ttlmii; $jmii += $dd['umum'][$k]['jml_lembur']; } }

        if(empty($d)){ $djmi = '0,0'; }else{ if(empty($d['umum'][$k]['jml_lembur'])){ $djmi = '0,0'; }else{
		$ttlji= carijam($d['umum'][$k]['jml_lembur']); $ttlmi= carimenit($d['umum'][$k]['jml_lembur'], $ttlji);       
        $ttlmi == 30 ? $ttlmi = 5 : $ttlmi = 0;
		$djmi= $ttlji.','.$ttlmi; $jmi += $d['umum'][$k]['jml_lembur']; } }


	    $excel_data[] = [
	        $dept,
	        $ddjkii,
	        $djki,
	        $ddgpii,
	        $dgpi,
	        $ddmkii,
	        $dmki,
	        $ddinsii,
	        $dinsi,
	        $ddlemii,
	        $dlemi,
	        $ddpremii,
	        $dpremi,
	        $ddtjabii,
	        $dtjabi,
	        $ddtahii,
	        $dtahi,
	        $ddtpresii,
	        $dtpresi,
	        $ddjumii,
	        $djumi,
	        $ddgbii,
	        $dgbi,
	        $ddjmii,
	        $djmi
	    ];
	}

	$ttljtii = carijam($jmii);
	$ttlmtii = carimenit($jmii, $ttljtii);
	empty($ttlmtii) ? $ttlmtii = 0 : $ttlmtii = 5;
	$ddtjmii = $ttljtii.','.$ttlmtii;		
	
	$ttljti = carijam($jmi);
	$ttlmti = carimenit($jmi, $ttljti);
	empty($ttlmti) ? $ttlmti = 0 : $ttlmti = 5;
	$dtjmi = $ttljti.','.$ttlmti;

	$excel_data[] = [
		'',
        $jkii,
        $jki,
        $gpii,
        $gpi,
        $mkii,
        $mki,
        $insii,
        $insi,
        $lemii,
        $lemi,
        $premii,
        $premi,
        $tjabii,
        $tjabi,
        $tahii,
        $tahi,
        $tpresii,
        $tpresi,
        $jumii,
        $jumi,
        $gbii,
        $gbi,
        $ddtjmii,
        $dtjmi
	];

}
$ttljj = carijam($jmpp+$jmuu+$jmii);
$ttlmm = carimenit($jmpp+$jmuu+$jmii, $ttljj);
empty($ttlmm) ? $ttlmm = 0 : $ttlmm = 5;
$ddttljj = $ttljj.','.$ttlmm;

$ttlj = carijam($jmp+$jmu+$jmi);
$ttlm = carimenit($jmp+$jmu+$jmi, $ttlj);
empty($ttlm) ? $ttlm = 0 : $ttlm = 5;
$dttlj = $ttlj.','.$ttlm;

$total_data = [
	'T O T A L', 
	$jkii+$jkuu+$jkpp,
	$jki+$jku+$jkp,
	$gppp+$gpuu+$gpii,
	$gpp+$gpu+$gpi,
	$mkpp+$mkuu+$mkii,
	$mkp+$mku+$mki,
	$inspp+$insuu+$insii,
	$insp+$insu+$insi,
	$lempp+$lemuu+$lemii,
	$lemp+$lemu+$lemi,
	$prempp+$premuu+$premii,
	$premp+$premu+$premi,
	$tjabpp+$tjabuu+$tjabii,
	$tjabp+$tjabu+$tjabi,
	$tahpp+$tahuu+$tahii,
	$tahp+$tahu+$tahi,
	$tprespp+$tpresuu+$tpresii,
	$tpresp+$tpresu+$tpresi,
	$jumpp+$jumuu+$jumii,
	$jump+$jumu+$jumi,
	$gbpp+$gbuu+$gbii,
	$gbp+$gbu+$gbi,
	$ddttljj,
	$dttlj
];

$rowcount = count($excel_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$sheet = $spreadsheet->getActiveSheet()->setTitle($filename);

$sheet->mergeCells('A1:Y1');
$sheet->mergeCells('A2:Y2');
$sheet->mergeCells('A3:Y3');
$sheet->mergeCells('A4:Y4');

$sheet->getStyle('A1')->applyFromArray($formnameStyle)->getFont()->setSize(9);
$sheet->getCell('A1')->setValue($formname);
$sheet->getStyle('A2')->applyFromArray($titleStyle);
$sheet->getCell('A2')->setValue($title);
$sheet->getStyle('A3')->applyFromArray($subtitleStyle);
$sheet->getCell('A3')->setValue($subtitle);

// header
$sheet->mergeCells('A5:A7');//dept
$sheet->mergeCells('B5:C6');//jmlkar
$sheet->mergeCells('D5:E6');//GP
$sheet->mergeCells('F5:G6');//MK
$sheet->mergeCells('H5:I6');//INSENTIP
$sheet->mergeCells('J5:K6');//LEMBURAN
$sheet->mergeCells('L5:M6');//PREMI SHIFT
$sheet->mergeCells('N5:S5');//Tunjangan
$sheet->mergeCells('N6:O6');//T jab
$sheet->mergeCells('P6:Q6');//T keah
$sheet->mergeCells('R6:S6');//T pres
$sheet->mergeCells('T5:U6');//uang makan
$sheet->mergeCells('V5:W6');//gaji bruto
$sheet->mergeCells('X5:Y6');//jml lembur

$sheet->getStyle('A5:Y7')->applyFromArray($headerStyle);
$sheet->fromArray([
    'DEPT.',
    'JML KAR.',
    '',
    'GAJI POKOK',
    '',
    'MASA KERJA',
    '',
    'INSENTIF',
    '',
    'LEMBURAN',
    '',
    'PREMI SHIFT',
    '',
    'TUNJANGAN',
    '',
    '',
    '',
    '',
    '',
    'UANG MAKAN',
    '',
    'GAJI BRUTO',
    '',
    'JML LEMBUR',
    ''
], null, 'A5');

$sheet->fromArray([
	'JABATAN',
	'',
	'KEAHLIAN',
	'',
	'PRESTASI',
	''
], null, 'N6');

$sheet->fromArray([
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln),
	namaBulanSingkat($blnn),
	namaBulanSingkat($bln)
], null, 'B7');

// data
$sheet->getStyle('A8:A'.(8+$rowcount))->applyFromArray($contentStyle);
$sheet->getStyle('B8:Y' . (8 + $rowcount))->applyFromArray($contentStyleR);
$sheet->fromArray($excel_data, null, 'A8');

//footer
$sheet->getStyle('A'. (8+$rowcount+1))->applyFromArray($footerStyleC);
$sheet->getStyle('B'. (8+$rowcount+1) .':Y' . (8+$rowcount+1))->applyFromArray($footerStyleR);
$sheet->fromArray($total_data, null, 'A'. (8+$rowcount+1));

$sheet->mergeCells('A'. (8+$rowcount+2).':Y'. (8+$rowcount+2));
$sheet->getStyle('A'. (8+$rowcount+2))->applyFromArray($contentStyleN);
$sheet->getCell('A'. (8+$rowcount+2))->setValue('Jaten, '.date('d').' '.namaBulan(date('m')).' '.date('Y'));


$sheet->getColumnDimension('A')->setWidth(30);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);
$sheet->getColumnDimension('J')->setAutoSize(true);
$sheet->getColumnDimension('K')->setAutoSize(true);
$sheet->getColumnDimension('L')->setAutoSize(true);
$sheet->getColumnDimension('M')->setAutoSize(true);
$sheet->getColumnDimension('N')->setAutoSize(true);
$sheet->getColumnDimension('O')->setAutoSize(true);
$sheet->getColumnDimension('P')->setAutoSize(true);
$sheet->getColumnDimension('Q')->setAutoSize(true);
$sheet->getColumnDimension('R')->setAutoSize(true);
$sheet->getColumnDimension('S')->setAutoSize(true);
$sheet->getColumnDimension('T')->setAutoSize(true);
$sheet->getColumnDimension('U')->setAutoSize(true);
$sheet->getColumnDimension('V')->setAutoSize(true);
$sheet->getColumnDimension('W')->setAutoSize(true);
$sheet->getColumnDimension('X')->setAutoSize(true);
$sheet->getColumnDimension('Y')->setAutoSize(true);

// ERROR
// die();
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>