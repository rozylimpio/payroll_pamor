<?php

$tgl = dateCreate($app->input->post('tgl'));
$shif = $app->input->post('shift');
$grup = $app->input->post('grup');
$atur = $app->input->post('atur');
$jmll = $app->input->post('jmll');

$n = 6;
$tglaw = strtotime($tgl);

$jdwl = [];$a=0;
for ($q=0; $q < count($atur); $q++) { 
  for ($w=0; $w < $jmll[$q]; $w++) { 
    if($atur[$q] != 'Libur'){
      $jdwl[$a]['berlaku'] = $tgl;
      //==================================================//
      $jdwl[$a]['grup'] = $grup;
      $jdwl[$a]['harike'] = date('N', $tglaw);
      //==================================================//
      $jdwl[$a]['shift'] = $atur[$q];//."<br>";
      $a++;
    }
    $tglaw = strtotime('+1 day', $tglaw);
  }
}

//print_r($jdwl); die();

$jadwal = new \App\Models\JadwalM($app);
if($insert_id = $jadwal->add($jdwl)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal_maintenance')); 
