<?php
$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();


$thn = $app->input->post('thn');
$bln = $app->input->post('bln');
/**/
/*
$thn = $app->input->post('tahun');
$bln = $app->input->post('bulan');
/**/

$bulan = $bln;
$bulann = $bln - 1;
if($bulann == 0){
  $bulann = 12;
  $thnn = $thn - 1;
}else{
  $thnn = $thn;
}
$tgl_aw = date($thnn.'-'.$bulann.'-21');
$tgl_ak = date($thn.'-'.$bulan.'-20');

$file = $mval->getRekap($thn, $bln);

if(empty($file)){
    ob_start();
    ?><html>
    <head>
    <meta charset='utf-8'>
    <style>
        @page { margin: 20px 20px 10px 20px; }
        body {
            line-height: 1.3;
            font-size: 12pt;
            font-family: Calibri;
            color: black;
            margin: 20px 20px 10px 20px;
        }
      .kar{
        line-height: 1.1;
        font-family: Calibri;
      }
        table {
            border-collapse: collapse;
          }
        table, tr, th, td { 
            border: solid 1px; 
            padding: 2.5px;
        }
      .sisi{
        border-left: solid 1px;
        border-right: solid 1px;
        border-top: 0px;
        border-bottom: 0px;
      }
      .kosong{
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-bottom: 0px;
      }
        .page_break { page-break-before: always; }
        /*br{
            display: block; /* makes it have a width */
          /*  content: ""; /* clears default height */
           /*  margin-top: 20; /* change this to whatever height you want it */
        /* }*/
    </style>
    </head>
    <body>
        <?php
        $d = $mkal->getRekap($thn, $bln);
        //var_dump($d); die();
        $da = [];
        for ($i=0; $i < count($d); $i++) { 
            if($d[$i]['bagian']=='Produksi'){
                $da['produksi'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Maintenance'){
                $da['produksi_l'][] = $d[$i];
            }elseif($d[$i]['bagian']=='QC/Lab/B Store'){
                $da['produksi_l'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Adm Produksi'){
                $da['produksi_l'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Pemasaran Gudang'){
                $da['produksi_l'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Logistik Gudang'){
                $da['produksi_l'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Exim'){
                $da['produksi_l'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Utility Shift'){
                $da['utility'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Utility Dayshift'){
                $da['utility'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Akuntansi/Keuangan'){
                $da['umum'][] = $d[$i];
            }elseif($d[$i]['bagian']=='HRD/PSDM'){
                $da['umum'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Adm Pemasaran'){
                $da['umum'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Umum & Rumah Tangga'){
                $da['umum'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Sipil'){
                $da['umum'][] = $d[$i];
            }elseif($d[$i]['bagian']=='Security'){
                $da['umum'][] = $d[$i];
            }elseif($d[$i]['bagian']=='EDP-SIM'){
                $da['umum'][] = $d[$i];
            }
        }
        //print_r($da);echo'<br><br>';
        $ks=0;$lws=0;$lns=0;$lps=0;$tls=0;$as=0;$is=0;$ss=0;$ps=0;$gks=0;
        $ku=0;$lwu=0;$lnu=0;$lpu=0;$tlu=0;$au=0;$iu=0;$su=0;$pu=0;$gku=0;
        $ki=0;$lwi=0;$lni=0;$lpi=0;$tli=0;$ai=0;$ii=0;$si=0;$pi=0;$gki=0;
        ?>
        <div align="center" style="font-size:14px;font-weight: bold;">
            PT. PAMOR SPINNING MILLS<br>
            Rekapitulasi Lembur Wajib - Nasional - Diperintah<br>
            Periode : <?php echo dateResolver($tgl_aw).' s/d '.dateResolver($tgl_ak); ?><br><br>
        </div>
        <table width="100% auto" style="font-size:11.5px">
            <thead align="center">
                <tr><td colspan="14" style="padding: 1px;"></td></tr>
                <tr>
                    <th rowspan="2">Departemen</th>
                    <th rowspan="2" style="font-size:10px">Jml<br>Orang</th>
                    <th colspan="2">Lembur Wajib</th>
                    <th colspan="2">Lembur Diperintah</th>
                    <th colspan="2">Lembur Nasional</th>
                    <th colspan="2">Total Lembur</th>
                    <th colspan="4">Absensi</th>
                </tr>
                <tr style="font-size:10.5px">
                    <th>Rupiah</th>
                    <th>Persen</th>
                    <th>Rupiah</th>
                    <th>Persen</th>
                    <th>Rupiah</th>
                    <th>Persen</th>
                    <th>Rupiah</th>
                    <th>Persen</th>
                    <th>A</th>
                    <th>I</th>
                    <th>S</th>
                    <th>P</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="sisi" width="18.5%"><b>Spinning</b></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                </tr>
                <?php 
                for($j=0; $j<count($da['produksi']); $j++){
                    $gks += $da['produksi'][$j]['gaji_kotor'];
                ?>
                <tr>
                    <td class="sisi" style="padding-left: 12px"><?php 
                    echo 'Grup '.$da['produksi'][$j]['grup']; 
                    ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['kar']); $ks += $da['produksi'][$j]['kar']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['n_lwajib']); $lws += $da['produksi'][$j]['n_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi'][$j]['p_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['n_lbiasa']); $lps += $da['produksi'][$j]['n_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi'][$j]['p_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['n_llibur']); $lns += $da['produksi'][$j]['n_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi'][$j]['p_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['tn_lembur']); $tls+=$da['produksi'][$j]['tn_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi'][$j]['tp_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['a']); $as += $da['produksi'][$j]['a']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['i']); $is += $da['produksi'][$j]['i']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['s']); $ss += $da['produksi'][$j]['s']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi'][$j]['p']); $ps += $da['produksi'][$j]['p']; ?></td>
                </tr>
                <?php } 
                for($k=0; $k<count($da['produksi_l']); $k++){ 
                    $gks += $da['produksi_l'][$k]['gaji_kotor'];
                ?>
                <tr>
                    <td class="sisi" style="padding-left: 12px"><?php 
                    echo $da['produksi_l'][$k]['bagian']; 
                    ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['kar']); $ks += $da['produksi_l'][$k]['kar']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['n_lwajib']);$lws+=$da['produksi_l'][$k]['n_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi_l'][$k]['p_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['n_lbiasa']);$lps+=$da['produksi_l'][$k]['n_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi_l'][$k]['p_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['n_llibur']);$lns+=$da['produksi_l'][$k]['n_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi_l'][$k]['p_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['tn_lembur']);$tls+=$da['produksi_l'][$k]['tn_lembur'];?></td>
                    <td class="sisi" align="right"><?php echo $da['produksi_l'][$k]['tp_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['a']); $as += $da['produksi_l'][$k]['a']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['i']); $is += $da['produksi_l'][$k]['i']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['s']); $ss += $da['produksi_l'][$k]['s']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['produksi_l'][$k]['p']); $ps += $da['produksi_l'][$k]['p']; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td align="center">TOTAL</td>
                    <td align="right"><?php echo idr($ks); ?></td>
                    <td align="right"><?php echo idr($lws); ?></td>
                    <td align="right"><?php echo $gks==0 ? '0' : round(($lws/$gks)*100, 2); ?></td>
                    <td align="right"><?php echo idr($lps); ?></td>
                    <td align="right"><?php echo $gks==0 ? '0' : round(($lps/$gks)*100, 2); ?></td>
                    <td align="right"><?php echo idr($lns); ?></td>
                    <td align="right"><?php echo $gks==0 ? '0' : round(($lns/$gks)*100, 2); ?></td>
                    <td align="right"><?php echo idr($tls); ?></td>
                    <td align="right"><?php echo $gks==0 ? '0' : round(($tls/$gks)*100, 2); ?></td>
                    <td align="right"><?php echo idr($as); ?></td>
                    <td align="right"><?php echo idr($is); ?></td>
                    <td align="right"><?php echo idr($ss); ?></td>
                    <td align="right"><?php echo idr($ps); ?></td>
                </tr>
                <tr>
                    <td class="sisi"><b>Utility</b></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                </tr>
                <?php for($l=0; $l<count($da['utility']); $l++){ 
                    $gku += $da['utility'][$l]['gaji_kotor'];
                ?>
                <tr>
                    <td class="sisi" style="padding-left: 12px"><?php echo $da['utility'][$l]['bagian']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['kar']); $ku += $da['utility'][$l]['kar']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['n_lwajib']); $lwu += $da['utility'][$l]['n_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['utility'][$l]['p_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['n_lbiasa']); $lpu += $da['utility'][$l]['n_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['utility'][$l]['p_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['n_llibur']); $lnu += $da['utility'][$l]['n_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['utility'][$l]['p_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['tn_lembur']); $tlu+=$da['utility'][$l]['tn_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['utility'][$l]['tp_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['a']); $au += $da['utility'][$l]['a']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['i']); $iu += $da['utility'][$l]['i']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['s']); $su += $da['utility'][$l]['s']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['utility'][$l]['p']); $pu += $da['utility'][$l]['p']; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td align="center">TOTAL</td>
                    <td align="right"><?php echo idr($ku); ?></td>
                    <td align="right"><?php echo idr($lwu); ?></td>
                    <td align="right"><?php echo $gku==0 ? '0' : round(($lwu/$gku)*100, 2); ?></td>
                    <td align="right"><?php echo idr($lpu); ?></td>
                    <td align="right"><?php echo $gku==0 ? '0' : round(($lpu/$gku)*100, 2); ?></td>
                    <td align="right"><?php echo idr($lnu); ?></td>
                    <td align="right"><?php echo $gku==0 ? '0' : round(($lnu/$gku)*100, 2); ?></td>
                    <td align="right"><?php echo idr($tlu); ?></td>
                    <td align="right"><?php echo $gku==0 ? '0' : round(($tlu/$gku)*100, 2); ?></td>
                    <td align="right"><?php echo idr($au); ?></td>
                    <td align="right"><?php echo idr($iu); ?></td>
                    <td align="right"><?php echo idr($su); ?></td>
                    <td align="right"><?php echo idr($pu); ?></td>
                </tr>
                <tr>
                    <td class="sisi"><b>Lainnya</b></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                    <td class="sisi"></td><td class="sisi"></td><td class="sisi"></td>
                </tr>
                <?php for($m=0; $m<count($da['umum']); $m++){ 
                    $gki += $da['umum'][$m]['gaji_kotor'];
                ?>
                <tr>
                    <td class="sisi" style="padding-left: 12px"><?php echo $da['umum'][$m]['bagian']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['kar']); $ki += $da['umum'][$m]['kar']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['n_lwajib']); $lwi += $da['umum'][$m]['n_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['umum'][$m]['p_lwajib']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['n_lbiasa']); $lpi += $da['umum'][$m]['n_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['umum'][$m]['p_lbiasa']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['n_llibur']); $lni += $da['umum'][$m]['n_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['umum'][$m]['p_llibur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['tn_lembur']); $tli+=$da['umum'][$m]['tn_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo $da['umum'][$m]['tp_lembur']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['a']); $ai += $da['umum'][$m]['a']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['i']); $ii += $da['umum'][$m]['i']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['s']); $si += $da['umum'][$m]['s']; ?></td>
                    <td class="sisi" align="right"><?php echo idr($da['umum'][$m]['p']); $pi += $da['umum'][$m]['p']; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td align="center">TOTAL</td>
                    <td align="right"><?php echo idr($ki); ?></td>
                    <td align="right"><?php echo idr($lwi); ?></td>
                    <td align="right"><?php echo $gki==0 ? '0' : round(($lwi/$gki)*100, 2); ?></td>
                    <td align="right"><?php echo idr($lpi); ?></td>
                    <td align="right"><?php echo $gki==0 ? '0' : round(($lpi/$gki)*100, 2); ?></td>
                    <td align="right"><?php echo idr($lni); ?></td>
                    <td align="right"><?php echo $gki==0 ? '0' : round(($lni/$gki)*100, 2); ?></td>
                    <td align="right"><?php echo idr($tli); ?></td>
                    <td align="right"><?php echo $gki==0 ? '0' : round(($tli/$gki)*100, 2); ?></td>
                    <td align="right"><?php echo idr($ai); ?></td>
                    <td align="right"><?php echo idr($ii); ?></td>
                    <td align="right"><?php echo idr($si); ?></td>
                    <td align="right"><?php echo idr($pi); ?></td>
                </tr>
                <tr>
                    <td style="padding-top: 10px"></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($ks+$ku+$ki); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($lws+$lwu+$lwi); ?></td>
                    <td align="right" style="padding-top: 10px">
                        <?php $gk = 0; $gk = $gks+$gku+$gki; echo $gk==0 ? '0' : round((($lws+$lwu+$lwi)/$gk)*100, 2); ?>
                    </td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($lps+$lpu+$lpi); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo $gk==0 ? '0' : round((($lps+$lpu+$lpi)/$gk)*100, 2); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($lns+$lnu+$lni); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo $gk==0 ? '0' : round((($lns+$lnu+$lni)/$gk)*100, 2); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($tls+$tlu+$tli); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo $gk==0 ? '0' : round((($tls+$tlu+$tli)/$gk)*100, 2); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($as+$au+$ai); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($is+$iu+$ii); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($ss+$su+$si); ?></td>
                    <td align="right" style="padding-top: 10px"><?php echo idr($ps+$pu+$pi); ?></td>
                </tr>
                <tr><td colspan="14" style="padding: 1px;"></td></tr>
            </tbody> 
        </table>
        <br>
        <table border="0" width="100% auto" style="font-size:13px; border: none 0px;">
            <tr>
                <td align="center" class="kosong" width="30%">&nbsp;</td>
                <td align="center" class="kosong" width="30%">&nbsp;</td>
                <td align="center" class="kosong">
                    Jaten, .....................................<br>
                    Bag. Personalia
                    <br><br><br><br>
                    <u><?php echo $mng[0]['nama']; ?></u><br>
                    <?php echo $mng[0]['jabatan']; ?>
                </td>
            </tr>
        </table>
    </body>
    </html>
    <?php
    //die();
    $html = ob_get_clean();

    $subdir = 'pdf/rincian/';
    $dir = $pubdir . '/' . $subdir;

    $exists = false;
    do {
        $name = $thn.'_'.$bln.'_Rekapitulasi_Lembur.pdf';
        $exists = file_exists($dir . $name);
    } while($exists);

    $dompdf = new \Dompdf\Dompdf();
    $dompdf->loadHtml($html);
    $dompdf->setPaper('folio', 'portrait');
    $dompdf->render();

    $pdf_gen = $dompdf->output();

    if(!file_put_contents($dir . $name, $pdf_gen)){
        header("HTTP/1.0 500 Internal Server Error");
        echo 'Generate PDF Failed';
        exit();
    } else {
        $mval->addRekap($thn, $bln, $subdir . $name);

        header("Content-Type: application/json");
        echo json_encode([
            'filename' => $name,
            'filepath' => url($subdir . $name)
        ]);
    }

}else{
    header("Content-Type: application/json");
        echo json_encode([
            'filename' => 'x',
            'filepath' => url($file)
        ]);
}
/**/
?>