<?php 

$nomorSurat = $app->input->post('no');
$no = str_replace('/','-',$nomorSurat);
$nik = $app->input->post('id');
$ket = $app->input->post('ket');
$dep = $app->input->post('dep');
$bag = $app->input->post('bag');
$res = $app->input->post('res');
$tgl = dateFormat(date('d-n-Y'));

$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();

$surat = new \App\Models\Surat($app);
if($res==1){
    $mkar = new \App\Models\Resign($app);
}else{
    $mkar = new \App\Models\Karyawan($app);
}
$data = $mkar->getById($nik);

$nosur = $surat->getByNo($nomorSurat);
if(!empty($nosur)){
    echo url('a/karyawan?departemen='.$dep.'&bagian='.$bag.'&tinjau=');
    exit();
}else{

ob_start();
?><html>
<head>
<meta charset='utf-8'>
<style>
    @page { margin: 90px 40px 12px 40px; }
    body {
        line-height: 1.3;
        font-size: 12pt;
        font-family: "Arial Narrow", Arial, sans-serif;
        color: black;
        margin: 90px 40px 12px 40px;
    }
    /*br{
        display: block; /* makes it have a width */
      /*  content: ""; /* clears default height */
       /*  margin-top: 20; /* change this to whatever height you want it */
    /* }*/
</style>
</head>
<body>
        <div>
            No. : <?php echo $nomorSurat;?><br>
            Hal : Pemberitahuan
        </div>
        <table width="100% auto">
            <tr>
                <td width="30%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
                <td width="40%">
                    Kepada<br>
                    <br>                      
                    Yth. Kepala BPJS Ketenagakerjaan<br>
                    Kab. Karanganyar<br>
                    Di<br>
                    Karanganyar<br>
                </td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <td colspan="3" align="justify">
                    Dengan hormat,<br>
                    Yang bertanda tangan dibawah ini:
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td width="30%" align="justify">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    Nama
                </td>
                <td>: &nbsp;</td>
                <td width="80%"><?php echo $mng[0]['nama']; ?></td>
            </tr>
            <tr>
                <td>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    Jabatan
                </td>
                <td>: &nbsp;</td>
                <td><?php echo $mng[0]['jabatan']; ?></td>
            </tr>
            <tr>
                <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    Instansi    
                </td>
                <td>: &nbsp;</td>
                <td>PT. PAMOR SPINNING MILLS</td>
            </tr>
            <tr>
                <td colspan="3" align="justify">
                    <br>
                    Memberitahukan bahwa karyawan kami dibawah ini telah mengundurkan diri dari  
                    PT. Pamor Spinning Mills Jaten, Karanganyar per tanggal <?php echo dateFormat(dateResolver($data['tgl_update'])) ?>.
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td width="30%" align="justify">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    Nama        
                </td>
                <td>: &nbsp;</td>
                <td width="80%" ><?php echo $data['nama']; ?></td>
            </tr>
            <tr>
                <td>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    Tgl Lahir         
                </td>
                <td>: &nbsp;</td>
                <td><?php echo dateFormat(dateResolver($data['tgl_lhr'])); ?></td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    Alamat      
                </td>
                <td style="vertical-align: top">: &nbsp;</td>
                <td align="justify" style="vertical-align: top"><?php echo $data['alamat']; ?></td>
            </tr>
            <tr>
                <td colspan="3" align="justify">
                    <br>
                    Demikian pemberitahuan ini kami sampaikan sebagai syarat untuk mencairkan Jaminan Hari Tua (JHT) BPJS Ketenagakerjaan.
                    <br><br>
                    Atas perhatiannya kami ucapkan terima kasih
                </td>
            </tr>
        </table>
        <table width="130%">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="center">
                    <br><br>
                    Jaten,  <?php echo $tgl ?><br>
                    PT. PAMOR SPINNING MILLS
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <u><?php echo $mng[0]['nama']; ?></u><br>
                    <?php echo $mng[0]['jabatan']; ?>
                </td>
            </tr>
        </table>
        <div>
        <br>
        Tembusan :<br>
        &nbsp; &nbsp; &nbsp; - Arsip
        </div>
</body>
</html>
<?php

$html = ob_get_clean();

$subdir = 'pdf/surat/';
$dir = $pubdir . '/' . $subdir;

$exists = false;
do {
    $name = $no.'_'.substr($nik,1).'_'.$ket.'.pdf';
    $exists = file_exists($dir . $name);
} while($exists);

$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml($html);
$dompdf->setPaper('folio', 'portrait');
$dompdf->render();

$pdf_gen = $dompdf->output();

if(!file_put_contents($dir . $name, $pdf_gen)){
    header("HTTP/1.0 500 Internal Server Error");
    echo 'Generate PDF Failed';
    exit();
} else {
    $surat->add($nomorSurat, $nik, $ket, $subdir . $name);

    header("Content-Type: application/json");
    echo json_encode([
        'filename' => $name,
        'filepath' => url($subdir . $name)
    ]);
}
}