<?php
$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$thn = $app->input->post('thn');
$bln = $app->input->post('bln');
$bagian = $app->input->post('bag');
$bag_ex = explode('_', $bagian);
$bag = $bag_ex[0];
$grup = $bag_ex[1];
$file = '';

$file = $mval->getSlip($thn, $bln, $bagian);

if(empty($file)){
	ob_start(); 
	?><html>
	<head>
	<meta charset='utf-8'>
	<style>
	    @page { margin: 10px 0 0 0; }
	    body {
	        line-height: 1.3;
	        font-size: 12pt;
	        font-family: Helvetica, Arial, sans-serif;/*Georgia, serif;*/
	        color: black;
	        margin: 0px;
	    }
	    table {
		  	border-collapse: collapse;
		  }
	    table, tr, th, td { 
	    	line-height: 1.2;
	    	border: 0px; 
	    	padding: 2px;
	    }
	    td.tinggi{
	    	/*height: 726pt; 736pt*/
	    	width: 303pt !important;
	    	min-width: 303pt; /*fix 303pt*/
	    	max-width: 303pt;
	    	vertical-align: top;
	    	padding: 15px 20px 0px 20px;
	    }
	    .colom{
	    	height: 369pt; /*376pt*/
	    	margin: 0px 10px 0px 0px;
	    }
		.sisi{
			border-left: solid 1px;
			border-right: solid 1px;
			border-top: 0px;
			border-bottom: 0px;
		}
		.atas{
			border-left: 0px;
			border-right: 0px;
			border-top: dashed 1px;
			border-bottom: dashed 1px;	
		}
		.kosong{
			border-left: 0px;
			border-right: 0px;
			border-top: 0px;
			border-bottom: 0px;
		}
	    /*.page_break { page-break-after: always; }*/
	</style>
	</head>
	<body>
		<?php
	   $data = $mkal->getRincian($thn, $bln);
	   $i=1;
	   foreach ($data as $d) {
	    	if($d['bagian'] == $bag && $d['grup'] == $grup){
		      $br = count($d['data']);
		      $hall = ceil($br/3);
		      $hall==0 ? $hal = 1 : $hal = $hall;
		      $min = 0;

		      for ($j=1; $j <= $hal; $j++) { 
		        $max_data = 3*$j;
		        $max_data>count($d['data']) ? $max = count($d['data']) : $max = $max_data;
		        ?>
		        <div class="<?php echo $i<=count($data) ? 'page_break' : ''; ?>">
					<table width="100% auto" border="0" class="kosong" table-layout: fixed>
						<tr>
							<?php
							for ($k=$min; $k < $max ; $k++) {
							$gb=0;$tp=0;$gt=0;
			            ?>
							<td class="tinggi">
								<div class="colom">
									<br><br><br><br><br><br>
									<table width="100%" border="0" class="kosong">
										<tr>
											<td colspan="2" align="center" style="font-size: 18; padding: 10px;" class="atas">
												<B>PT. PAMOR SPINNING MILLS</B>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center" style="font-size: 14; padding: 7px;" class="atas">
												<B>GAJI BULAN : <?php echo strtoupper(namaBulan($bln)).' '.$thn ?></B>
											</td>
										</tr>
										<tr>
											<td class="kosong" width="30%" style="font-size:18px">
												<br style="margin-bottom: 10px;">
												No. Urut
											</td>
											<td class="kosong" style="font-size:21px">
												<br>
												: &nbsp; <?php echo $k+1; ?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:18px">
												N a m a
											</td>
											<td class="kosong" style="font-size:21px">
												: &nbsp; <?php echo strtoupper(substr($d['data'][$k]['nama'], 0, 17)); ?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:18px">
												N . I . K
											</td>
											<td class="kosong" style="font-size:21px">
												: &nbsp; <?php echo substr($d['data'][$k]['nik'], 1); ?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:18px">
												Departement
												<br><br>
											</td>
											<td class="kosong" style="font-size:21px">
												: &nbsp; <?php
											              echo strtoupper($d['bagian']);
											              if($d['grup']=='N'){
											                echo '';
											              }elseif ($d['grup']=='S') {
											                echo ' SHIFT';
											              }elseif ($d['grup']=='R') {
											                echo " DIRUMAHKAN";
											              }else{
											                echo ' '.$d['grup'];
											              }

											              $nm = '_'.strtoupper(str_replace('/', '-', $d['bagian']));
											              if($d['grup']=='N'){
											                $nm .= '';
											              }elseif ($d['grup']=='S') {
											                $nm .= '_SHIFT';
											              }elseif ($d['grup']=='R') {
											                $nm .= "_DIRUMAHKAN";
											              }else{
											                $nm .= '_'.$d['grup'];
											              }
											            ?>
												<br><br>
											</td>
										</tr>
										<tr><td colspan="2" class="atas"></td></tr>
									</table>
								</div>
								<div class="colom" style="margin-bottom: -7px">
									<div style="margin-bottom: 10px">&nbsp;</div>
									<table width="100%" border="0" class="kosong">
										<tr>
											<td colspan="5"align="center" style="padding: 5px;" class="atas">
												<B style="font-size: 12;">PT. PAMOR SPINNING MILLS</B><br>
												<B style="font-size: 10;">GAJI BULAN : <?php echo strtoupper(namaBulan($bln)).' '.$thn ?></B>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:13px" width="40%">
												<div style="height:0px"></div>
												No. Urut<br>
												N a m a<br>
												N . I . K<br>
												Departement
												<br>
											</td>
											<td colspan="4" class="kosong" style="font-size:13px">
												<div style="height:0px"></div>
												: &nbsp; <?php echo $k+1; ?><br>
												: &nbsp; <?php echo strtoupper(substr($d['data'][$k]['nama'], 0, 23)); ?><br>
												: &nbsp; <?php echo substr($d['data'][$k]['nik'],1); ?><br>
												: &nbsp; <?php
											              echo strtoupper($d['bagian']);
											              if($d['grup']=='N'){
											                echo '';
											              }elseif ($d['grup']=='S') {
											                echo ' SHIFT';
											              }else{
											                echo ' '.$d['grup'];
											              }
											            ?>
												<br>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:13px;" class="atas">
												<div style="height:0px"></div>
												Gaji<br>
												Masa Kerja<br>
												Insentif<br>
												Lembur <?php 	$ttljpp = carijam($d['data'][$k]['t_lembur']);
											                  $ttlmpp = carimenit($d['data'][$k]['t_lembur'], $ttljpp);
											                  echo idr($ttljpp).',';
											                  echo $ttlmpp == 30 ? '5 Jm ' : '0 jm ';
											                  echo 'x '.idr($d['data'][$k]['tarif_l'])  ?><br>
												Premi Shift<br>
												Tunjangan Jabatan<br>
												Tunjangan Keahlian<br>
												Insentif Prestasi Kerja<br>
												Uang Makan
											</td>
											<td style="font-size:13px;" class="atas">
												<div style="height:0px"></div>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.
											</td>
											<td style="font-size:13px" align="right" class="atas">
												<div style="height:0px"></div>
												<?php echo idr($d['data'][$k]['gaji']); $gb += $d['data'][$k]['gaji']; ?><br>
												<?php echo idr($d['data'][$k]['nt_masakerja']); $gb += $d['data'][$k]['nt_masakerja']; ?><br>
												<?php echo idr($d['data'][$k]['nt_insentif']); $gb += $d['data'][$k]['nt_insentif']; ?><br>
												<?php echo idr($d['data'][$k]['nt_lembur']); $gb += $d['data'][$k]['nt_lembur']; ?><br>
												<?php echo idr($d['data'][$k]['nt_premi']); $gb += $d['data'][$k]['nt_premi']; ?><br>
												<?php echo idr($d['data'][$k]['nt_jabatan']); $gb += $d['data'][$k]['nt_jabatan']; ?><br>
												<?php echo idr($d['data'][$k]['nt_keahlian']); $gb += $d['data'][$k]['nt_keahlian']; ?><br>
												<?php echo idr($d['data'][$k]['nt_prestasi']); $gb += $d['data'][$k]['nt_prestasi']; ?><br>
												<?php echo idr($d['data'][$k]['u_makan']); $gb += $d['data'][$k]['u_makan']; ?>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:13px" align="right">
												GAJI BRUTO&nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; 
											</td>
											<td style="font-size:13px;">
												Rp.
											</td>
											<td style="font-size:13px" align="right">
												<?php echo idr($gb); ?>
											</td>
										</tr>
										<tr>
											<td style="font-size:13px;" class="atas">
												<div style="height:0px"></div>
												Bpjs Ketenagakerjaan<br>
												Bpjs Kesehatan<br>
												S.P.T<br>
												Serikat Buruh<br>
												Pot. Absensi<br>
												Pot. P.S.W<br>
												Koperasi<br>
												Lain
											</td>
											<td style="font-size:13px;" class="atas">
												<div style="height:0px"></div>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.
											</td>
											<td style="font-size:13px" align="right" class="atas">
												<div style="height:0px"></div>
												<?php echo idr($d['data'][$k]['n_bpjs_ket']); $tp += $d['data'][$k]['n_bpjs_ket']; ?><br>
												<?php echo idr($d['data'][$k]['n_bpjs_kes']); $tp += $d['data'][$k]['n_bpjs_kes']; ?><br>
												<?php echo idr($d['data'][$k]['n_spt']); $tp += $d['data'][$k]['n_spt']; ?><br>
												<?php echo idr($d['data'][$k]['n_kspn']); $tp += $d['data'][$k]['n_kspn']; ?><br>
												<?php echo idr($d['data'][$k]['n_ai']); $tp += $d['data'][$k]['n_ai']; ?><br>
												<?php echo idr($d['data'][$k]['n_psw']); $tp += $d['data'][$k]['n_psw']; ?><br>
												<?php echo idr($d['data'][$k]['n_koperasi']); $tp += $d['data'][$k]['n_koperasi']; ?><br>
												<?php 

												if($d['data'][$k]['rumah']>0 || $d['data'][$k]['n_rumah']>0){
													echo idr($d['data'][$k]['tn_lain']+$d['data'][$k]['n_rumah']); 
													$tp = $tp + ($d['data'][$k]['tn_lain']+$d['data'][$k]['n_rumah']); 
												}else{
													echo idr($d['data'][$k]['tn_lain']); $tp += $d['data'][$k]['tn_lain'];
												} 
												?>
											</td>
											<td colspan="2" class="atas"> &nbsp;</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:13px" align="right" class="atas">
												JUMLAH POTONGAN&nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; 
											</td>
											<td style="font-size:13px;" class="atas">
												Rp.
											</td>
											<td style="font-size:13px" align="right" class="atas">
												<?php echo idr($tp); ?>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:15px" align="right">
												<div style="height:0px"></div>
												<b>DITERIMA BERSIH</b> &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; 
											</td>
											<td style="font-size:15px;">
												<div style="height:0px"></div>
												<b>Rp.</b>
											</td>
											<td style="font-size:15px" align="right">
												<div style="height:0px"></div>
												<b><u><?php echo idr(pembulatan($gb-$tp)); ?></u></b>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<?php
							}
							if ($j==$hal){
								$jtd = $max_data - count($d['data']);
								for ($n=0; $n < $jtd; $n++) { 
									?>
									<td class="tinggi"><div class="colom"></div></td>
									<?php
								}
							}
							?>
						</tr>
					</table>
				</div>
	        <?php
	        $i+=1;
	        $min+=3;

		     }
	      }

	   }
	   ?>
	</body>
	</html>
	<?php

	$html = ob_get_clean();

	$subdir = 'pdf/slip_gaji/';
	$dir = $pubdir . '/' . $subdir;

	$exists = false;
	do {
	    $name = $thn.'_'.$bln.'_Slip_Gaji'.$nm.'.pdf';
	    $exists = file_exists($dir . $name);
	} while($exists);
  	$customPaper = array(0,0,1006.29,791.811026); //dikurangi 0.17mm. awal 792.283465);//
	$dompdf = new \Dompdf\Dompdf();
	$dompdf->loadHtml($html);
	$dompdf->setPaper($customPaper);
	$dompdf->render();

	$pdf_gen = $dompdf->output();

	if(!file_put_contents($dir . $name, $pdf_gen)){
	    header("HTTP/1.0 500 Internal Server Error");
	    echo 'Generate PDF Failed';
	    exit();
	} else {
	    $mval->addSlip($thn, $bln, $subdir . $name, $bagian);

	    header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => $name,
	        'filepath' => url($subdir . $name)
	    ]);
	}
}else{
	header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => 'x',
	        'filepath' => url($file)
	    ]);
}
?>