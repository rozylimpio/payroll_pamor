<?php
$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$thn = $app->input->post('thn');
$bln = $app->input->post('bln');

$file = $mval->getRincian($thn, $bln);
/**/
if(empty($file)){
	ob_start();
/** */
	?>
<html>
	<head>
	<meta charset='utf-8'>
	<style>
	    /* @page { margin: 25px 10px 20px 40px; } */
      @page { margin: 25px 10px 20px 12px; }
	    body {
	        line-height: 1.1;
	        /*font-size: 12pt;*/
	        font-family: "Arial Narrow", Arial, sans-serif;
	        color: black;
	        margin: 25px 10px 20px 40px;
	    }
      .kar{
        line-height: 1.5;
        font-family: Arial, Helvetica, sans-serif;
      }
	    table {
		  	border-collapse: collapse;
		  }
	    table, tr, th, td { 
	    	border: solid 1px; 
	    	padding: 3.5px;
	    }

      .sisi{
        border-left: solid 1px;
        border-right: solid 1px;
        border-top: 0px;
        border-bottom: 0px;
      }
      .kosong{
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-bottom: 0px;
      }
	    .page_break { page-break-before: always; }
	    /*br{
	        display: block; /* makes it have a width */
	      /*  content: ""; /* clears default height */
	       /*  margin-top: 20; /* change this to whatever height you want it */
	    /* }*/
	</style>
	</head>
	<body>
			<?php
        $tgl = date($thn.'-'.$bln.'-14');
        $blnn = date('m', strtotime("-1 month", strtotime($tgl)));
        $thnn = date('Y', strtotime("-1 month", strtotime($tgl)));
				$datar = $mkal->getRekapPot($thnn, $blnn); 
				$data = $mkal->getRekapPot($thn, $bln);

				$d = [];
				for($l=0; $l<count($data);$l++){
					if($data[$l]['dept'] == 'produksi'){
					  	$d['produksi'][] = $data[$l];
					}elseif($data[$l]['dept'] == 'produksi n'){
              $d['produksi_l'][] = $data[$l];
          }elseif($data[$l]['dept'] == 'pemasaran gudang'){
              $d['produksi_l'][] = $data[$l];
          }elseif($data[$l]['dept'] == 'logistik gudang'){
              $d['produksi_l'][] = $data[$l];
          }elseif($data[$l]['dept'] == 'exim'){
              $d['produksi_l'][] = $data[$l];
          }elseif($data[$l]['dept'] == 'utility'){
					  	$d['utility'][] = $data[$l];
					}else{
					  	$d['umum'][] = $data[$l];
					}
				}

				$dd = [];
				for($n=0; $n<count($datar);$n++){
					if($datar[$n]['dept'] == 'produksi'){
					  	$dd['produksi'][] = $datar[$n];
					}elseif($datar[$n]['dept'] == 'produksi n'){
              $dd['produksi_l'][] = $datar[$n];
          }elseif($datar[$n]['dept'] == 'pemasaran gudang'){
					  	$dd['produksi_l'][] = $datar[$n];
					}elseif($datar[$n]['dept'] == 'logistik gudang'){
              $dd['produksi_l'][] = $datar[$n];
          }elseif($datar[$n]['dept'] == 'exim'){
              $dd['produksi_l'][] = $datar[$n];
          }elseif($datar[$n]['dept'] == 'utility'){
              $dd['utility'][] = $datar[$n];
          }else{
					  	$dd['umum'][] = $datar[$n];
					}
				}

              $jkp=0;$gpp=0;$mkp=0;$insp=0;$lemp=0;$premp=0;$tjabp=0;$tahp=0;$tpresp=0;$gbp=0;$jmp=0;$jump=0;
              $jkpp=0;$gppp=0;$mkpp=0;$inspp=0;$lempp=0;$prempp=0;$tjabpp=0;$tahpp=0;$tprespp=0;$gbpp=0;$jmpp=0;$jumpp=0;
              $jku=0;$gpu=0;$mku=0;$insu=0;$lemu=0;$premu=0;$tjabu=0;$tahu=0;$tpresu=0;$gbu=0;$jmu=0;$jumu=0;
              $jkuu=0;$gpuu=0;$mkuu=0;$insuu=0;$lemuu=0;$premuu=0;$tjabuu=0;$tahuu=0;$tpresuu=0;$gbuu=0;$jmuu=0;$jumuu=0;
              $jki=0;$gpi=0;$mki=0;$insi=0;$lemi=0;$premi=0;$tjabi=0;$tahi=0;$tpresi=0;$gbi=0;$jmi=0;$jumi=0;
              $jkii=0;$gpii=0;$mkii=0;$insii=0;$lemii=0;$premii=0;$tjabii=0;$tahii=0;$tpresii=0;$gbii=0;$jmii=0;$jumii=0;
            ?>
              <br><br><br><br><br><br><br><br>
              <div align="center">
                <b>
                  PERBANDINGAN GAJI BRUTO KARYAWAN PT. PAMOR SPINNING MILLS<br>
                  <?php echo 'BULAN : '.strtoupper(namaBulan($blnn)).' '.$thnn.' - '.strtoupper(namaBulan($bln)).' '.$thn; ?>
                </b>
              </div>
              <br>       
              <br>
              <table width="100% auto" style="font-size: 10.5px">
                <thead>
                  <tr>
                    <th style="font-size: 10.5px" align="center" rowspan="3" width="13%">DEPT.</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">JML KAR.</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">GAJI POKOK</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">MASA KERJA</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">INSENTIF</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">LEMBURAN</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">PREMI SHIFT</th>
                    <th style="font-size: 10.5px" align="center" colspan="6">TUNJANGAN</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">UANG MAKAN</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">GAJI BRUTO</th>
                    <th style="font-size: 10.5px" align="center" rowspan="2" colspan="2">JML LEMBUR</th>
                  </tr>
                  <tr>
                    <th style="font-size: 10.5px" align="center" colspan="2">JABATAN</th>
                    <th style="font-size: 10.5px" align="center" colspan="2">KEAHLIAN</th>
                    <th style="font-size: 10.5px" align="center" colspan="2">PRESTASI</th>
                  </tr>
                  <tr>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($blnn) ?></th>
                  	<th style="font-size: 10px" align="center"><?php echo namaBulanSingkat($bln) ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="font-size:10.5px;"><b>PRODUKSI</b></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td>
                  </tr>
                  <?php
                  if(!empty($d) || !empty($dd)){
                  !empty($d) ? $jmlg = count($d['produksi']) : $jmlg = 0;
                  !empty($dd) ? $jmlgg = count($dd['produksi']) : $jmlgg = 0;
                  $jmlg>$jmlgg ? $count = $jmlg : $count = $jmlgg;
                  for ($k=0; $k < $count; $k++) { 
                  ?>
                  <tr>
                    <td style="font-size:10px;"><?php 
                        if($jmlg>$jmlgg){
                          echo ' - '.strtoupper($d['produksi'][$k]['dept']).' '.$d['produksi'][$k]['grup']; 
                        }else{
                          echo ' - '.strtoupper($dd['produksi'][$k]['dept']).' '.$dd['produksi'][$k]['grup']; 
                        }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['jml_kar'])){ $jkpp += $dd['produksi'][$k]['jml_kar']; 
                          echo $dd['produksi'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['jml_kar'])){ $jkp += $d['produksi'][$k]['jml_kar']; 
                          echo $d['produksi'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['gp'])){ $gppp += $dd['produksi'][$k]['gp']; 
                          echo idr($dd['produksi'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['gp'])){ $gpp += $d['produksi'][$k]['gp']; 
                          echo idr($d['produksi'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['masakerja'])){ $mkpp += $dd['produksi'][$k]['masakerja']; 
                          echo idr($dd['produksi'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['masakerja'])){ $mkp += $d['produksi'][$k]['masakerja']; 
                          echo idr($d['produksi'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['insentif'])){ $inspp += $dd['produksi'][$k]['insentif']; 
                          echo idr($dd['produksi'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['insentif'])){ $insp += $d['produksi'][$k]['insentif']; 
                          echo idr($d['produksi'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['lembur'])){ $lempp += $dd['produksi'][$k]['lembur']; 
                          echo idr($dd['produksi'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['lembur'])){ $lemp += $d['produksi'][$k]['lembur']; 
                          echo idr($d['produksi'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['premi'])){ $prempp += $dd['produksi'][$k]['premi']; 
                          echo idr($dd['produksi'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['premi'])){ $premp += $d['produksi'][$k]['premi']; 
                          echo idr($d['produksi'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['tjab'])){ $tjabpp += $dd['produksi'][$k]['tjab']; 
                          echo idr($dd['produksi'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['tjab'])){ $tjabp += $d['produksi'][$k]['tjab']; 
                          echo idr($d['produksi'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['tahli'])){ $tahpp += $dd['produksi'][$k]['tahli']; 
                          echo idr($dd['produksi'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['tahli'])){ $tahp += $d['produksi'][$k]['tahli']; 
                          echo idr($d['produksi'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['tprestasi'])){ $tprespp += $dd['produksi'][$k]['tprestasi']; 
                          echo idr($dd['produksi'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['tprestasi'])){ $tpresp += $d['produksi'][$k]['tprestasi']; 
                          echo idr($d['produksi'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['u_makan'])){ $jumpp += $dd['produksi'][$k]['u_makan']; 
                          echo idr($dd['produksi'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['u_makan'])){ $jump += $d['produksi'][$k]['u_makan']; 
                          echo idr($d['produksi'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi'][$k]['gj_bruto'])){ $gbpp += $dd['produksi'][$k]['gj_bruto']; 
                          echo idr($dd['produksi'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi'][$k]['gj_bruto'])){ $gbp += $d['produksi'][$k]['gj_bruto']; 
                          echo idr($d['produksi'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right">
                      <?php 
                      	if(empty($dd)){
                      		echo '0,0';
                      	}else{
                          if(empty($dd['produksi'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
  	                        $ttljpp = carijam($dd['produksi'][$k]['jml_lembur']);
  	                        $ttlmpp = carimenit($dd['produksi'][$k]['jml_lembur'], $ttljpp);
                            $ttlmpp == 30 ? $ttlmpp = 5 : $ttlmpp = 0;
  	                        echo idr($ttljpp).','.$ttlmpp;
  	                        $jmpp += $dd['produksi'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        if(empty($d)){
                          echo '0,0';
                        }else{
                          if(empty($d['produksi'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttljp = carijam($d['produksi'][$k]['jml_lembur']);
                            $ttlmp = carimenit($d['produksi'][$k]['jml_lembur'], $ttljp);
                            $ttlmp == 30 ? $ttlmp = 5 : $ttlmp = 0;
                            echo idr($ttljp).','.$ttlmp;
                            $jmp += $d['produksi'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <?php
                  if(!empty($d) || !empty($dd)){
                  for ($k=0; $k < count($d['produksi_l']); $k++) { 
                  ?>
                  <tr>
                    <td style="font-size:10px;"><?php 
                    echo ' - '.strtoupper($d['produksi_l'][$k]['dept']).' ';
                    echo $d['produksi_l'][$k]['dept']=='produksi' ? $d['produksi_l'][$k]['grup'] : '';
                    ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['jml_kar'])){ $jkpp += $dd['produksi_l'][$k]['jml_kar']; 
                          echo $dd['produksi_l'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['jml_kar'])){ $jkp += $d['produksi_l'][$k]['jml_kar']; 
                          echo $d['produksi_l'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['gp'])){ $gppp += $dd['produksi_l'][$k]['gp']; 
                          echo idr($dd['produksi_l'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['gp'])){ $gpp += $d['produksi_l'][$k]['gp']; 
                          echo idr($d['produksi_l'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['masakerja'])){ $mkpp += $dd['produksi_l'][$k]['masakerja']; 
                          echo idr($dd['produksi_l'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['masakerja'])){ $mkp += $d['produksi_l'][$k]['masakerja']; 
                          echo idr($d['produksi_l'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['insentif'])){ $inspp += $dd['produksi_l'][$k]['insentif']; 
                          echo idr($dd['produksi_l'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['insentif'])){ $insp += $d['produksi_l'][$k]['insentif']; 
                          echo idr($d['produksi_l'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['lembur'])){ $lempp += $dd['produksi_l'][$k]['lembur']; 
                          echo idr($dd['produksi_l'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['lembur'])){ $lemp += $d['produksi_l'][$k]['lembur']; 
                          echo idr($d['produksi_l'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['premi'])){ $prempp += $dd['produksi_l'][$k]['premi']; 
                          echo idr($dd['produksi_l'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['premi'])){ $premp += $d['produksi_l'][$k]['premi']; 
                          echo idr($d['produksi_l'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['tjab'])){ $tjabpp += $dd['produksi_l'][$k]['tjab']; 
                          echo idr($dd['produksi_l'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['tjab'])){ $tjabp += $d['produksi_l'][$k]['tjab']; 
                          echo idr($d['produksi_l'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['tahli'])){ $tahpp += $dd['produksi_l'][$k]['tahli']; 
                          echo idr($dd['produksi_l'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['tahli'])){ $tahp += $d['produksi_l'][$k]['tahli']; 
                          echo idr($d['produksi_l'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['tprestasi'])){ $tprespp += $dd['produksi_l'][$k]['tprestasi']; 
                          echo idr($dd['produksi_l'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['tprestasi'])){ $tpresp += $d['produksi_l'][$k]['tprestasi']; 
                          echo idr($d['produksi_l'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['u_makan'])){ $jumpp += $dd['produksi_l'][$k]['u_makan']; 
                          echo idr($dd['produksi_l'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['u_makan'])){ $jump += $d['produksi_l'][$k]['u_makan']; 
                          echo idr($d['produksi_l'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['produksi_l'][$k]['gj_bruto'])){ $gbpp += $dd['produksi_l'][$k]['gj_bruto']; 
                          echo idr($dd['produksi_l'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['produksi_l'][$k]['gj_bruto'])){ $gbp += $d['produksi_l'][$k]['gj_bruto']; 
                          echo idr($d['produksi_l'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right">
                      <?php 
                        if(empty($dd)){
                          echo '0,0';
                        }else{
                          if(empty($dd['produksi_l'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttljpp = carijam($dd['produksi_l'][$k]['jml_lembur']);
                            $ttlmpp = carimenit($dd['produksi_l'][$k]['jml_lembur'], $ttljpp);
                            $ttlmpp == 30 ? $ttlmpp = 5 : $ttlmpp = 0;
                            echo idr($ttljpp).','.$ttlmpp;
                            $jmpp += $dd['produksi_l'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        if(empty($d)){
                          echo '0,0';
                        }else{
                          if(empty($d['produksi_l'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttljp = carijam($d['produksi_l'][$k]['jml_lembur']);
                            $ttlmp = carimenit($d['produksi_l'][$k]['jml_lembur'], $ttljp);
                            $ttlmp == 30 ? $ttlmp = 5 : $ttlmp = 0;
                            echo idr($ttljp).','.$ttlmp;
                            $jmp += $d['produksi_l'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <tr>
                    <td></td>
                    <td align="right"><?php echo $jkpp; ?></td>
                    <td align="right"><?php echo $jkp; ?></td>
                    <td align="right"><?php echo idr($gppp); ?></td>
                    <td align="right"><?php echo idr($gpp); ?></td>
                    <td align="right"><?php echo idr($mkpp); ?></td>
                    <td align="right"><?php echo idr($mkp); ?></td>
                    <td align="right"><?php echo idr($inspp); ?></td>
                    <td align="right"><?php echo idr($insp); ?></td>
                    <td align="right"><?php echo idr($lempp); ?></td>
                    <td align="right"><?php echo idr($lemp); ?></td>
                    <td align="right"><?php echo idr($prempp); ?></td>
                    <td align="right"><?php echo idr($premp); ?></td>
                    <td align="right"><?php echo idr($tjabpp); ?></td>
                    <td align="right"><?php echo idr($tjabp); ?></td>
                    <td align="right"><?php echo idr($tahpp); ?></td>
                    <td align="right"><?php echo idr($tahp); ?></td>
                    <td align="right"><?php echo idr($tprespp); ?></td>
                    <td align="right"><?php echo idr($tpresp); ?></td>
                    <td align="right"><?php echo idr($jumpp); ?></td>
                    <td align="right"><?php echo idr($jump); ?></td>
                    <td align="right"><?php echo idr($gbpp); ?></td>
                    <td align="right"><?php echo idr($gbp); ?></td>
                    <td align="right">
                      <?php 
                        $ttljtpp = carijam($jmpp);
                        $ttlmtpp = carimenit($jmpp, $ttljtpp);
                        $ttlmtpp == 30 ? $ttlmtpp = 5 : $ttlmtpp = 0;
                        echo idr($ttljtpp).','.$ttlmtpp;
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        $ttljtp = carijam($jmp);
                        $ttlmtp = carimenit($jmp, $ttljtp);
                        $ttlmtp == 30 ? $ttlmpp = 5 : $ttlmtp = 0;
                        echo idr($ttljtp).','.$ttlmtp;
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:10.5px;"><b>UTILITY</b></td>                    
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td>
                  </tr>
                  <?php
                  if(!empty($d) || !empty($dd)){
                  for ($k=0; $k < count($d['utility']); $k++) { 
                  ?>
                  <tr>
                    <td style="font-size:10px;"><?php echo ' - '.strtoupper($d['utility'][$k]['dept']).' ';echo $d['utility'][$k]['grup'] == 'S' ? 'SHIFT' : 'N';?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['jml_kar'])){ $jkuu += $dd['utility'][$k]['jml_kar']; 
                          echo $dd['utility'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['jml_kar'])){ $jku += $d['utility'][$k]['jml_kar']; 
                          echo $d['utility'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['gp'])){ $gpuu += $dd['utility'][$k]['gp']; 
                          echo idr($dd['utility'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['gp'])){ $gpu += $d['utility'][$k]['gp']; 
                          echo idr($d['utility'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['masakerja'])){ $mkuu += $dd['utility'][$k]['masakerja']; 
                          echo idr($dd['utility'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['masakerja'])){ $mku += $d['utility'][$k]['masakerja']; 
                          echo idr($d['utility'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['insentif'])){ $insuu += $dd['utility'][$k]['insentif']; 
                          echo idr($dd['utility'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['insentif'])){ $insu += $d['utility'][$k]['insentif']; 
                          echo idr($d['utility'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['lembur'])){ $lemuu += $dd['utility'][$k]['lembur']; 
                          echo idr($dd['utility'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['lembur'])){ $lemu += $d['utility'][$k]['lembur']; 
                          echo idr($d['utility'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['premi'])){ $premuu += $dd['utility'][$k]['premi']; 
                          echo idr($dd['utility'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['premi'])){ $premu += $d['utility'][$k]['premi']; 
                          echo idr($d['utility'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['tjab'])){ $tjabuu += $dd['utility'][$k]['tjab']; 
                          echo idr($dd['utility'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['tjab'])){ $tjabu += $d['utility'][$k]['tjab']; 
                          echo idr($d['utility'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['tahli'])){ $tahuu += $dd['utility'][$k]['tahli']; 
                          echo idr($dd['utility'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['tahli'])){ $tahu += $d['utility'][$k]['tahli']; 
                          echo idr($d['utility'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['tprestasi'])){ $tpresuu += $dd['utility'][$k]['tprestasi']; 
                          echo idr($dd['utility'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['tprestasi'])){ $tpresu += $d['utility'][$k]['tprestasi']; 
                          echo idr($d['utility'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['u_makan'])){ $jumuu += $dd['utility'][$k]['u_makan']; 
                          echo idr($dd['utility'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['u_makan'])){ $jumu += $d['utility'][$k]['u_makan']; 
                          echo idr($d['utility'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['utility'][$k]['gj_bruto'])){ $gbuu += $dd['utility'][$k]['gj_bruto']; 
                          echo idr($dd['utility'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['utility'][$k]['gj_bruto'])){ $gbu += $d['utility'][$k]['gj_bruto']; 
                          echo idr($d['utility'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right">
                      <?php 
                        if(empty($dd)){
                          echo '0,0';
                        }else{
                          if(empty($dd['utility'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttljuu = carijam($dd['utility'][$k]['jml_lembur']);
                            $ttlmuu = carimenit($dd['utility'][$k]['jml_lembur'], $ttljuu);                            
                            $ttlmuu == 30 ? $ttlmuu = 5 : $ttlmuu = 0;
                            echo idr($ttljuu).','.$ttlmuu;
                            $jmuu += $dd['utility'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        if(empty($d)){
                          echo '0,0';
                        }else{
                          if(empty($d['utility'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttlju = carijam($d['utility'][$k]['jml_lembur']);
                            $ttlmu = carimenit($d['utility'][$k]['jml_lembur'], $ttlju);
                            $ttlmu == 30 ? $ttlmu = 5 : $ttlmu = 0;
                            echo idr($ttlju).','.$ttlmu;
                            $jmu += $d['utility'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <tr>
                    <td></td>
                    <td align="right"><?php echo $jkuu; ?></td>
                    <td align="right"><?php echo $jku; ?></td>
                    <td align="right"><?php echo idr($gpuu); ?></td>
                    <td align="right"><?php echo idr($gpu); ?></td>
                    <td align="right"><?php echo idr($mkuu); ?></td>
                    <td align="right"><?php echo idr($mku); ?></td>
                    <td align="right"><?php echo idr($insuu); ?></td>
                    <td align="right"><?php echo idr($insu); ?></td>
                    <td align="right"><?php echo idr($lemuu); ?></td>
                    <td align="right"><?php echo idr($lemu); ?></td>
                    <td align="right"><?php echo idr($premuu); ?></td>
                    <td align="right"><?php echo idr($premu); ?></td>
                    <td align="right"><?php echo idr($tjabuu); ?></td>
                    <td align="right"><?php echo idr($tjabu); ?></td>
                    <td align="right"><?php echo idr($tahuu); ?></td>
                    <td align="right"><?php echo idr($tahu); ?></td>
                    <td align="right"><?php echo idr($tpresuu); ?></td>
                    <td align="right"><?php echo idr($tpresu); ?></td>
                    <td align="right"><?php echo idr($jumuu); ?></td>
                    <td align="right"><?php echo idr($jumu); ?></td>
                    <td align="right"><?php echo idr($gbuu); ?></td>
                    <td align="right"><?php echo idr($gbu); ?></td>
                    <td align="right">
                      <?php 
                        $ttljtuu = carijam($jmuu);
                        $ttlmtuu = carimenit($jmuu, $ttljtuu);
                        $ttlmtuu == 30 ? $ttlmtuu = 5 : $ttlmtuu = 0;
                        echo idr($ttljtuu).','.$ttlmtuu;
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        $ttljtu = carijam($jmu);
                        $ttlmtu = carimenit($jmu, $ttljtu);
                        $ttlmtu == 30 ? $ttlmtu = 5 : $ttlmtu = 0;
                        echo idr($ttljtu).','.$ttlmtu;
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:10.5px;"><b>UMUM & PEMASARAN</b></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td>
                  </tr>
                  <?php
                  if(!empty($d) || !empty($dd)){
                  for ($k=0; $k < count($d['umum']); $k++) { 
                  ?>
                  <tr>
                    <td style="font-size:10px;"><?php echo ' - '.strtoupper($d['umum'][$k]['dept']); ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['jml_kar'])){ $jkii += $dd['umum'][$k]['jml_kar']; 
                          echo $dd['umum'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['jml_kar'])){ $jki += $d['umum'][$k]['jml_kar']; 
                          echo $d['umum'][$k]['jml_kar'];} else { echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['gp'])){ $gpii += $dd['umum'][$k]['gp']; 
                          echo idr($dd['umum'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['gp'])){ $gpi += $d['umum'][$k]['gp']; 
                          echo idr($d['umum'][$k]['gp']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['masakerja'])){ $mkii += $dd['umum'][$k]['masakerja']; 
                          echo idr($dd['umum'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['masakerja'])){ $mki += $d['umum'][$k]['masakerja']; 
                          echo idr($d['umum'][$k]['masakerja']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['insentif'])){ $insii += $dd['umum'][$k]['insentif']; 
                          echo idr($dd['umum'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['insentif'])){ $insi += $d['umum'][$k]['insentif']; 
                          echo idr($d['umum'][$k]['insentif']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['lembur'])){ $lemii += $dd['umum'][$k]['lembur']; 
                          echo idr($dd['umum'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; } 
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['lembur'])){ $lemi += $d['umum'][$k]['lembur']; 
                          echo idr($d['umum'][$k]['lembur']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['premi'])){ $premii += $dd['umum'][$k]['premi']; 
                          echo idr($dd['umum'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['premi'])){ $premi += $d['umum'][$k]['premi']; 
                          echo idr($d['umum'][$k]['premi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['tjab'])){ $tjabii += $dd['umum'][$k]['tjab']; 
                          echo idr($dd['umum'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['tjab'])){ $tjabi += $d['umum'][$k]['tjab']; 
                          echo idr($d['umum'][$k]['tjab']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['tahli'])){ $tahii += $dd['umum'][$k]['tahli']; 
                          echo idr($dd['umum'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['tahli'])){ $tahi += $d['umum'][$k]['tahli']; 
                          echo idr($d['umum'][$k]['tahli']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['tprestasi'])){ $tpresii += $dd['umum'][$k]['tprestasi']; 
                          echo idr($dd['umum'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['tprestasi'])){ $tpresi += $d['umum'][$k]['tprestasi']; 
                          echo idr($d['umum'][$k]['tprestasi']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['u_makan'])){ $jumii += $dd['umum'][$k]['u_makan']; 
                          echo idr($dd['umum'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['u_makan'])){ $jumi += $d['umum'][$k]['u_makan']; 
                          echo idr($d['umum'][$k]['u_makan']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($dd)){ 
                          if(!empty($dd['umum'][$k]['gj_bruto'])){ $gbii += $dd['umum'][$k]['gj_bruto']; 
                          echo idr($dd['umum'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right"><?php 
                        if(!empty($d)){ 
                          if(!empty($d['umum'][$k]['gj_bruto'])){ $gbi += $d['umum'][$k]['gj_bruto']; 
                          echo idr($d['umum'][$k]['gj_bruto']);}else{ echo '0'; } }else { echo '0'; }
                    ?></td>
                    <td align="right">
                      <?php 
                        if(empty($dd)){
                          echo '0,0';
                        }else{
                          if(empty($dd['umum'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttljii = carijam($dd['umum'][$k]['jml_lembur']);
                            $ttlmii = carimenit($dd['umum'][$k]['jml_lembur'], $ttljii);
                            $ttlmii == 30 ? $ttlmii = 5 : $ttlmii = 0;
                            echo idr($ttljii).','.$ttlmii;
                            $jmii += $dd['umum'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        if(empty($d)){
                          echo '0,0';
                        }else{
                          if(empty($d['umum'][$k]['jml_lembur'])){
                            echo '0,0';
                          }else{
                            $ttlji = carijam($d['umum'][$k]['jml_lembur']);
                            $ttlmi = carimenit($d['umum'][$k]['jml_lembur'], $ttlji);
                            $ttlmi == 30 ? $ttlmi = 5 : $ttlmi = 0;
                            echo idr($ttlji).','.$ttlmi;
                            $jmi += $d['umum'][$k]['jml_lembur'];
                          }
                        }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <tr>
                    <td></td>
                    <td align="right"><?php echo $jkii; ?></td>
                    <td align="right"><?php echo $jki; ?></td>
                    <td align="right"><?php echo idr($gpii); ?></td>
                    <td align="right"><?php echo idr($gpi); ?></td>
                    <td align="right"><?php echo idr($mkii); ?></td>
                    <td align="right"><?php echo idr($mki); ?></td>
                    <td align="right"><?php echo idr($insii); ?></td>
                    <td align="right"><?php echo idr($insi); ?></td>
                    <td align="right"><?php echo idr($lemii); ?></td>
                    <td align="right"><?php echo idr($lemi); ?></td>
                    <td align="right"><?php echo idr($premii); ?></td>
                    <td align="right"><?php echo idr($premi); ?></td>
                    <td align="right"><?php echo idr($tjabii); ?></td>
                    <td align="right"><?php echo idr($tjabi); ?></td>
                    <td align="right"><?php echo idr($tahii); ?></td>
                    <td align="right"><?php echo idr($tahi); ?></td>
                    <td align="right"><?php echo idr($tpresii); ?></td>
                    <td align="right"><?php echo idr($tpresi); ?></td>
                    <td align="right"><?php echo idr($jumii); ?></td>
                    <td align="right"><?php echo idr($jumi); ?></td>
                    <td align="right"><?php echo idr($gbii); ?></td>
                    <td align="right"><?php echo idr($gbi); ?></td>
                    <td align="right"><?php 
                        $ttljtii = carijam($jmii);
                        $ttlmtii = carimenit($jmii, $ttljtii);
                        $ttlmtii == 30 ? $ttlmtii = 5 : $ttlmtii = 0;
                        echo idr($ttljtii).','.$ttlmtii;
                      ?>
                    </td>
                    <td align="right">
                      <?php 
                        $ttljti = carijam($jmi);
                        $ttlmti = carimenit($jmi, $ttljti);
                        $ttlmti == 30 ? $ttlmti = 5 : $ttlmti = 0;
                        echo idr($ttljti).','.$ttlmti;
                      ?>
                    </td>
                  </tr>
                  <tr><td colspan="25"></td></tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th style="font-size: 10.5px">T O T A L</th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'999'*/$jkii+$jkuu+$jkpp; ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'999'*/$jki+$jku+$jkp; ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999.999'*/idr($gppp+$gpuu+$gpii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999.999'*/idr($gpp+$gpu+$gpi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($mkpp+$mkuu+$mkii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($mkp+$mku+$mki); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'99.999.999'*/idr($inspp+$insuu+$insii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'99.999.999'*/idr($insp+$insu+$insi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'999.999.999'*/idr($lempp+$lemuu+$lemii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'999.999.999'*/idr($lemp+$lemu+$lemi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($prempp+$premuu+$premii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($premp+$premu+$premi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'99.999.999'*/idr($tjabpp+$tjabuu+$tjabii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'99.999.999'*/idr($tjabp+$tjabu+$tjabi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($tahpp+$tahuu+$tahii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($tahp+$tahu+$tahi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($tprespp+$tpresuu+$tpresii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($tpresp+$tpresu+$tpresi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($jumpp+$jumuu+$jumii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999'*/idr($jump+$jumu+$jumi); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999.999'*/idr($gbpp+$gbuu+$gbii); ?></th>
                    <th style="font-size: 10.5px" align="right"><?php echo /*'9.999.999.999'*/idr($gbp+$gbu+$gbi); ?></th>
                    <th style="font-size: 10.5px" align="right">
                      <?php 
                        $ttljj = carijam($jmpp+$jmuu+$jmii);
                        $ttlmm = carimenit($jmpp+$jmuu+$jmii, $ttljj);
                        $ttlmm == 30 ? $ttlmm = 5 : $ttlmm = 0;
                        echo /*'99.999,00'*/idr($ttljj).','.$ttlmm;
                      ?>
                    </th>
                    <th style="font-size: 10.5px" align="right">
                      <?php 
                        $ttlj = carijam($jmp+$jmu+$jmi);
                        $ttlm = carimenit($jmp+$jmu+$jmi, $ttlj);
                        $ttlm == 30 ? $ttlm = 5 : $ttlm = 0;
                        echo /*'99.999,00'*/idr($ttlj).','.$ttlm;
                      ?>
                    </th>
                  </tr>
                </tfoot>
              </table>
              <div style="float:right; font-size:11pt; margin-right:5px"><?php echo 'Jaten, '.date('d').' '.namaBulan(date('m')).' '.date('Y'); ?></div>

	</body>
	</html>
	<?php
/* */
	$html = ob_get_clean();

	$subdir = 'pdf/rincian/';
	$dir = $pubdir . '/' . $subdir;

	$exists = false;
	do {
	    $name = $thn.'_'.$bln.'_Perbandingan_Gaji.pdf';
	    $exists = file_exists($dir . $name);
	} while($exists);

	$dompdf = new \Dompdf\Dompdf();
	$dompdf->loadHtml($html);
	$dompdf->setPaper('A3', 'landscape');
	$dompdf->render();

	$pdf_gen = $dompdf->output();

	if(!file_put_contents($dir . $name, $pdf_gen)){
	    header("HTTP/1.0 500 Internal Server Error");
	    echo 'Generate PDF Failed';
	    exit();
	} else {
	    $mval->addRincian($thn, $bln, $subdir . $name);

	    header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => $name,
	        'filepath' => url($subdir . $name)
	    ]);
	}
}else{
	header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => 'x',
	        'filepath' => url($file)
	    ]);
}
/**/
?>