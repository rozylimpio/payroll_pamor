<?php

$jml = $app->input->post('jml');
$tgl = dateCreate($app->input->post('tgl'));
$shift = $app->input->post('shift');
$grup = $app->input->post('grup');

$n = 6;
$tglaw = strtotime($tgl);
$day = date('l', strtotime($tgl));
$dayke = date('N', strtotime($tgl));
$tglp = date('Y-m-d', strtotime('+'.$n.' days', strtotime($tgl)));
$tglak = strtotime($tglp);
$dayp = date('l', strtotime($tglp));
$daypke = date('N', strtotime($tglp));

$grup = array_filter($grup);
//print_r($grup); die();
if($shift == 'Pagi'){
  $shift = array('Pagi', 'Siang', 'Malam', 0);
}

for ($is=0; $is < count($shift); $is++) { 
  for ($ig=0; $ig < count($grup); $ig++) { 
    $shift_p[] = $shift[$is];
  }
}

//print_r($shift_p);die();

$a = 0; $b = $grup[0]; $c=0;
do {    
  for($j=0; $j<(count($grup)*3);$j++){
    $hari[$a][$j]['berlaku'] = $tgl;
    if($b<=$jml){
      $maxdata = strtoupper(chr(96+$b));
      $hari[$a][$j]['grup'] = $maxdata;
    }
    $b==($jml) ? $b=1 : $b+=1;
    //$hari[$a][$j][] = date('Y-m-d', $tglaw);
    $hari[$a][$j]['harike'] = date('N', $tglaw);
    if ($c<(count($shift_p)-count($grup))) {
      $hari[$a][$j]['shift'] = $shift_p[$c];//."<br>";
    }
    $c==count($shift_p)-(count($grup)+1) ? $c=0 : $c+=1;
  }
  $a+=1;
  $tglaw = strtotime('+1 day', $tglaw);
} while ($tglaw<=$tglak);

//print_r($hari);die();

$jadwal = new \App\Models\Jadwal7GrupS($app);
if($insert_id = $jadwal->add($hari)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal_7_grup_s')); 
