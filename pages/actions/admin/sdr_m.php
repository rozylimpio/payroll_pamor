<?php

$id = $app->input->post('id');
$page = $app->input->post('page');
$awal = $app->input->postDate('awal');
$akhir = $app->input->postDate('akhir');
$ijin = $app->input->post('ijin');

$departemen = $app->input->post('departemen');
$bagian = $app->input->post('bagian');
//$ket_bag = $app->input->post('ket_bag');
$jgrup = $app->input->post('jgrup');
$grup = null.$app->input->post('grup');
$libur = null.$app->input->post('libur');
$jamkerja = $app->input->post('jamkerja');

$mtki = new \App\Models\Karyawan($app);

$queries = [
    'page' => $page,
    'departemen' => $departemen,
    'bagi' => $bagian,
    'bagian' => $bagian,
    //'ket_bag' => $ket_bag,
    'jgrup' => $jgrup,
    'grup' => $grup,
    'libur' => $libur,
    'jam' => $jamkerja,
    'tinjau' => ''
];

if(empty($id)){
	$app->addError('sdr_list', 'Maaf pilih Karyawan terlebih dahulu');
	header('Location: ' . url('a/sdr_m') .'?'. queryString($queries));
}else{
	if($ijin=='kk'){
		if($mtki->kkAdd($id, $awal, $akhir)) {
		    $app->addMessage('sdr_list', 'Proses Ijin Sdr/KK Berhasil');
		    header('Location: ' . url('a/sdr_m') .'?'. queryString($queries));
		} else {
		    $app->addError('sdr_list', 'Proses Ijin Sdr/KK Gagal');
		    header('Location: ' . url('a/sdr_m') .'?'. queryString($queries));
		}
	}else{
		if($mtki->sdrAdd($id, $awal, $akhir)) {
		    $app->addMessage('sdr_list', 'Proses Ijin Sdr/KK Berhasil');
		    header('Location: ' . url('a/sdr_m') .'?'. queryString($queries));
		} else {
		    $app->addError('sdr_list', 'Proses Ijin Sdr/KK Gagal');
		    header('Location: ' . url('a/sdr_m') .'?'. queryString($queries));
		}
	}
}