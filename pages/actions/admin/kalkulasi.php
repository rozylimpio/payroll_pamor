<?php 
$mkal = new \App\Models\Kalkulasi($app);
$mper = new \App\Models\Persen($app);
$mum = new \App\Models\UangMakan($app);

$a = 0;
$data = [];
$bulan = $app->input->post('bulan');
$bulann = $app->input->post('bulan') - 1;
$thn = date('Y');
if($bulann == 0){
  $bulann = 12;
  $thnn = $thn - 1;
}else{
  $thnn = $thn;
}
$data['tgl_aw'] = date($thnn.'-'.$bulann.'-21');
$data['tgl_ak'] = date($thn.'-'.$bulan.'-20');

//print_r($data);die();
//$data['departemen'] = $app->input->get('departemen');
//$data['bagian'] = $app->input->get('bagian');
//echo $data['tgl_aw'].' '.$data['tgl_ak'].'<br>';

$mkal->cek($bulan, $thn);
$list = $mkal->getData($data);

// header('Content-Type: application/json');
// echo json_encode($list, JSON_PRETTY_PRINT);
// echo "<br><br>";die();

for ($i=0; $i < count($list['nik']); $i++) { 
  $nik = $list['nik'][$i];
  $gaji = $list['gaji'][$i];
  $persen = $list['persen'][$i];
  $sts = $list['status'][$i];
  
  if($sts == 4){
    $status = $list['status_lama'][$i];
  }elseif ($sts == 1 OR $sts == 2) {
    $status = $sts;
  }

  $drmhkn = 0;
  $rmh = 0; 
  $unpd = 0;
  $grumah = 0;
  $insentif = 0;
  $tgtl = 0;
  $talij = 0;
  $det_potlain = null;
  $potlain = 0;
  $n_tlw = 0;
  $n_tlb = 0;
  $n_tll= 0;
  $jbiasa='';
  $jlibur='';
  $jmasakerja = 0;
  $rumah = 0;
  $rumahi = 0;
  $unpaid = 0;
  $tmakan = 0;
  $tumakan = 0;
  $umakan = $mum->get();
  $nTunj = 0;

  //uang makan//
  $tmakan = empty($list[$nik]['masuk']['tmakan']) ? 0 : $list[$nik]['masuk']['tmakan'];
  $tumakan = $tmakan * $umakan;
  
  //masa kerja//
  $jmasakerja = $list[$nik]['tunjangan']['j_masakerja']*$list[$nik]['tunjangan']['t_masakerja'];

  //tunj tunj
  $nTunj = $list[$nik]['tunjangan']['t_jabatan'] + $list[$nik]['tunjangan']['t_keahlian'] + $list[$nik]['tunjangan']['t_prestasi'];

  //Insentif//
  $insentif = 0; $alpha = 0; $sdr = 0; $ijin = 0; $rumah = 0;
  if(($list[$nik]['hadir']['alpha'] + $list[$nik]['hadir']['alpha_r'])>=2 || $list[$nik]['hadir']['sdr']>=10 || $list[$nik]['hadir']['ijin']>=10 || $list[$nik]['hadir']['dirumahkan']>=10 || $list[$nik]['hadir']['rumahi']>=10 || $list[$nik]['hadir']['unpaid']>=10){
    $alpha = 20000;
  }elseif($list[$nik]['hadir']['alpha']==1){
    $alpha = 8000;
  }
  if ($list[$nik]['hadir']['sdr']>0 && $list[$nik]['hadir']['sdr']<10){
    $sdr = $list[$nik]['hadir']['sdr']*2000;
  }
  if ($list[$nik]['hadir']['ijin']>0 && $list[$nik]['hadir']['ijin']<10){
    $ijin = $list[$nik]['hadir']['ijin']*2000;
  }
  if ($list[$nik]['hadir']['dirumahkan']>0 && $list[$nik]['hadir']['dirumahkan']<10){
    $rumah = $list[$nik]['hadir']['dirumahkan']*2000;
  }
  if ($list[$nik]['hadir']['rumahi']>0 && $list[$nik]['hadir']['rumahi']<10){
    $unpaid = $list[$nik]['hadir']['rumahi']*2000;
  }
  if ($list[$nik]['hadir']['unpaid']>0 && $list[$nik]['hadir']['unpaid']<10){
    $unpaid = $list[$nik]['hadir']['unpaid']*2000;
  }
  $insentif = 20000 - ($alpha+$sdr+$ijin+$rumah+$rumahi+$unpaid);
  
  if($insentif<0){
    $insentif = 0;
  }



  //Tarif Lembur//
   $tarif = round(($gaji + $jmasakerja + $list[$nik]['tunjangan']['t_jabatan'] + $list[$nik]['tunjangan']['t_keahlian'])/173);
   
   //echo $tarif."<br>";
  
  
  //Lembur Wajib//
  //if($list['bagian'][$i]=='104'){//104 scurity
    $jamwaji = (($list[$nik]['masuk']['wajib']*5)/6)*1.5; //-> rumus jika full dlm 1 bulan normal
  /*}else{
    $jamwaji = ($list[$nik]['masuk']['wajib']*45)/60;
  }*/
  $jamwajib = toleransi((60*60)*($jamwaji));

  $tlwj = carijam($jamwajib);
  $tlwm = carimenit($jamwajib, $tlwj);
  empty($tlwm) ? $menitlmw = 0 : $menitlmw = 0.5;
  $jwajib = $tlwj+$menitlmw;
  $n_tlw = round($jwajib * $tarif);


  // lembur perintah/biasa + libur//
  $jambiasa = $list[$nik]['lembur']['l_biasa'];
  $tlbj = carijam($jambiasa);
  $tlbm = carimenit($jambiasa, $tlbj);
  empty($tlbm) ? $menitlmb = 0 : $menitlmb = 0.5;
  $jbiasa = $tlbj+$menitlmb;
  $n_tlb = round($jbiasa*$tarif);
  //$n_tlb = round(($tlbj * $tarif) + ($menitlmb  * $tarif));


  // lembur nasional //
  $jamlibur = $list[$nik]['lembur']['l_libur'];
  $tllj = carijam($jamlibur);
  $tllm = carimenit($jamlibur, $tllj);
  empty($tllm) ? $menitlml = 0 : $menitlml = 0.5;
  $jlibur = $tllj+$menitlml;
  $n_tll = round($jlibur*$tarif);
  //$n_tll = round(($tllj * $tarif) + ($menitlml  * $tarif));


  //total lembur
  $ttl = $list[$nik]['masuk']['tlembur']+$jamwajib;
  $tgtl = $n_tlw+$n_tlb+$n_tll;
  /*
  $ttlj = carijam($ttl);
  $ttlm = carimenit($ttl, $ttlj);
  empty($ttlm) ? $menittlml = 0 : $menittlml = 0.5;
  $tgtl = round(($ttlj+$menittlml)*$tarif);
  //$tgtl = round(($ttlj * $tarif) + ($menittlml  * $tarif));
  */

  //$totallb = $ttlj." jam ".$ttlm." menit";  

  /*hitung lembur
  $gtl = toleransi($jamwajib + $list[$nik]['masuk']['tlembur']);
  //echo $gtl.'<br>';
  $tgtlj = carijam($gtl);
  $tgtlm = carimenit($gtl, $tgtlj);
  empty($tgtlm) ? $menitlm = 0 : $menitlm = 0.5;
  //echo $tgtlj.' jam '.$tgtlm.' menit<br>';
  $tgtl = round(($tgtlj * $tarif) + ($menitlm  * $tarif));
  /**/
  //$tgtl = $n_tlw + $n_tlb + $n_tll;
  //$tgtl = ($jwajib+$jbiasa+$jlibur)*$tarif;



  //Tarif Potongan Alpha dan Ijin//                  
  $tpalpha = round(($gaji+$jmasakerja)/30);
  $talij = round($tpalpha * ($list[$nik]['hadir']['ijin'] + $list[$nik]['hadir']['alpha'] + $list[$nik]['hadir']['alpha_r']));

  /*penghitungan gaji karyawan masuk pertengahan
  if($list[$nik]['newbie'] == 'yes'){
    if($list['bagian'][$i]=='1'){
      $talijj = round($tpalpha * ($list[$nik]['hadir']['ijin'] + $list[$nik]['hadir']['alpha']));
    }
  }
  */

  //dirumahkan/rumah/unpaid
  $jmld = get_object_vars(date_diff(date_create($data['tgl_aw']), date_create($data['tgl_ak'])));
  $jmlhari = $jmld['d'] - $list[$nik]['hadir']['libur'] + 1; //+ 1 dimaksudkan untuk menambah 1 hari di tanggal 21

  if($status==1){ //kontrak
        //dirumahkan
        $persdrmhkn= $mper->getpersen('diliburkan',$status);
        if($list[$nik]['masuk']['tmasuk'] == 0 && $list[$nik]['hadir']['dirumahkan'] >= $jmlhari){
          $drmhkn = round(($gaji + $nTunj) * ($persdrmhkn/100));
        }else{
          $drmhkn = round($list[$nik]['hadir']['dirumahkan']  *((($gaji + $nTunj)/30) * ($persdrmhkn/100)));
        }

        //rumah
        $persrmh = $mper->getpersen('rumah',$status);
        if($list[$nik]['masuk']['tmasuk'] == 0 && $list[$nik]['hadir']['rumahi'] >= $jmlhari){
          $rmh = round(($gaji + $nTunj) * ($persrmh/100));
        }else{
          $rmh = round($list[$nik]['hadir']['rumahi'] * ((($gaji + $nTunj)/30) * ($persrmh/100)));
        }

        //unpaid
        $persunpd = $mper->getpersen('unpaid',$status);
        if($list[$nik]['masuk']['tmasuk'] == 0 && $list[$nik]['hadir']['unpaid'] >= $jmlhari){
          $unpd = round(($gaji + $nTunj) * ($persunpd/100));
        }else{
          $unpd = round($list[$nik]['hadir']['unpaid'] * ((($gaji + $nTunj)/30) * ($persunpd/100)));
        }
  }else{
        //dirumahkan
        $persdrmhkn = $mper->getpersen('diliburkan',$status);
        if($list[$nik]['masuk']['tmasuk'] == 0 && $list[$nik]['hadir']['dirumahkan'] >= $jmlhari){
          if($list['bagian'][$i]=='1'){
            $drmhkn = round(($gaji + $nTunj) * ($persdrmhkn/100));
          }else{
            $drmhkn = round(($gaji + $jmasakerja + $nTunj) * ($persdrmhkn/100));
          }
        }else{
          if($list['bagian'][$i]=='1'){
            $drmhkn = round($list[$nik]['hadir']['dirumahkan'] * ((($gaji + $nTunj)/30) * ($persdrmhkn/100)));
          }else{
            $drmhkn = round($list[$nik]['hadir']['dirumahkan'] * ((($gaji + $jmasakerja + $nTunj)/30) * ($persdrmhkn/100)));
          }
        }

        //rumah
        $persrmh = $mper->getpersen('rumah',$status);
        if($list[$nik]['masuk']['tmasuk'] == 0 && $list[$nik]['hadir']['rumahi'] >= $jmlhari){
          if($list['bagian'][$i]=='1'){
            $rmh = round(($gaji + $nTunj) * ($persrmh/100));
          }else{
            $rmh = round(($gaji + $jmasakerja + $nTunj) * ($persrmh/100));
          }
        }else{
          if($list['bagian'][$i]=='1'){
            $rmh = round($list[$nik]['hadir']['rumahi'] * ((($gaji + $nTunj)/30) * ($persrmh/100)));
          }else{
            $rmh = round($list[$nik]['hadir']['rumahi'] * ((($gaji + $jmasakerja + $nTunj)/30) * ($persrmh/100)));
          }
        }

        //unpaid
        $persunpd = $mper->getpersen('unpaid',$status);
        if($list[$nik]['masuk']['tmasuk'] == 0 && $list[$nik]['hadir']['unpaid'] >= $jmlhari){
          if($list['bagian'][$i]=='1'){
            $undp = round(($gaji + $nTunj) * ($persunpd/100));
          }else{
            $unpd = round(($gaji + $jmasakerja + $nTunj) * ($persunpd/100));
          }
        }else{
          if($list['bagian'][$i]=='1'){
            $unpd = round($list[$nik]['hadir']['unpaid'] * ((($gaji + $nTunj)/30) * ($persunpd/100)));
          }else{      
            $unpd = round($list[$nik]['hadir']['unpaid'] * ((($gaji + $jmasakerja + $nTunj)/30) * ($persunpd/100)));
          }
        }
  }

  $grumah = $drmhkn + $rmh + $unpd;


  //total ijin
  $tij = carijam($list[$nik]['masuk']['tijin']);
  $tim = carimenit($list[$nik]['masuk']['tijin'], $tij);
  $t_ijin = $tij." jam ".$tim." menit";

  if($list['bagian'][$i]=='1'){
      $gaji_kotor = $gaji+$tumakan;
      $insentif = 0;
      // $talij = 0;
      $jmasakerja = 0;
      // $list[$nik]['masuk']['pot_ijin'] = 0;
  }else{
    $gaji_kotor = ((($gaji+$list[$nik]['tunjangan']['t_jabatan']+$list[$nik]['tunjangan']['t_keahlian']+$list[$nik]['tunjangan']['t_prestasi']+$jmasakerja+$list[$nik]['tunjangan']['t_premi']))+$insentif+$tgtl+$tumakan);
  }

  
  if($persen<100){
    $p = 100 - $persen;
    $potgajikotor = $gaji_kotor*($p/100);
    $grumah = $grumah + $potgajikotor;
  }
  
  //tambahan keluarga BPJS
  $bpjskes = $list[$nik]['potongan']['bpjs_kes'] + ($list[$nik]['potongan']['bpjs_kes_plus']*$list[$nik]['potongan']['jml_bpjs_kes_plus']);

  // if($list['bagian'][$i]=='1'){
  //   $potongan = $list[$nik]['potongan']['bpjs_ket']+$bpjskes+$list[$nik]['potongan']['lainnya']+$list[$nik]['potongan']['koperasi']+round($list[$nik]['potongan']['spt_karyawan'])+$list[$nik]['potongan']['kspn']+$grumah;
  // }else{
    $potongan = $talij+$list[$nik]['potongan']['bpjs_ket']+$bpjskes+$list[$nik]['potongan']['lainnya']+$list[$nik]['potongan']['koperasi']+round($list[$nik]['potongan']['spt_karyawan'])+$list[$nik]['masuk']['pot_ijin']+$list[$nik]['potongan']['kspn']+$grumah;
  // }

  $totalgaji = pembulatan($gaji_kotor-$potongan);
/*
GAJI Rp.0 tetapi masih dapat BPJS
*/ 
  if($sts == 5){
    
    $list[$nik]['tunjangan']['t_jabatan'] = 0;
    $list[$nik]['tunjangan']['t_keahlian'] = 0;
    $list[$nik]['tunjangan']['t_prestasi'] = 0;
    
    $jmasakerja = 0;
    $insentif = 0;
    $list[$nik]['potongan']['koperasi'] = 0;
    $list[$nik]['potongan']['spt_karyawan'] = 0;
    $list[$nik]['potongan']['spt_perusahaan'] = 0;
    $list[$nik]['potongan']['lainnya'] = 0;

    $grumah = (($list['gaji'][$i] + $list[$nik]['tunjangan']['t_jabatan'] + $list[$nik]['tunjangan']['t_keahlian'] + $list[$nik]['tunjangan']['t_prestasi']) - ($list[$nik]['potongan']['bpjs_ket']+$list[$nik]['potongan']['bpjs_kes']+$list[$nik]['potongan']['kspn']));

    $gaji_kotor = $list['gaji'][$i] + $list[$nik]['tunjangan']['t_jabatan'] + $list[$nik]['tunjangan']['t_keahlian'] + $list[$nik]['tunjangan']['t_prestasi'];
    $potongan = $list[$nik]['potongan']['bpjs_ket']+$list[$nik]['potongan']['bpjs_kes']+$list[$nik]['potongan']['kspn']+$grumah;

    $totalgaji = pembulatan($gaji_kotor-$potongan);
  }
/**/  
  $d['bulan'] = $bulan;
  $d['tahun'] = $thn;
  $d['kd_departemen'] = $list['departemen'][$i];
  $d['kd_bagian'] = $list['bagian'][$i];
  $d['nik'] = $list['nik'][$i];
  $d['gaji'] = $list['gaji'][$i];
  $d['persen'] = $list['persen'][$i];
  $d['tarif_l'] = $tarif;
  $d['tarif_ai'] = $tpalpha;
  $d['masuk'] = 0+$list[$nik]['masuk']['tmasuk'];
  $d['pagi'] = 0+$list[$nik]['jadwal']['pagi'];
  $d['siang'] = 0+$list[$nik]['jadwal']['siang'];
  $d['malam'] = 0+$list[$nik]['jadwal']['malam'];
  $d['libur'] = 0+$list[$nik]['hadir']['libur'];
  $d['ijin'] = 0+$list[$nik]['hadir']['ijin'];
  $d['sdr'] = 0+$list[$nik]['hadir']['sdr'];
  $d['cuti'] = 0+$list[$nik]['hadir']['cuti'];
  $d['cuti_h'] = 0+$list[$nik]['hadir']['cutihamil'];
  $d['cuti_r'] = 0+$list[$nik]['hadir']['cuti_r'];
  $d['rumah'] = 0+$list[$nik]['hadir']['dirumahkan'];
  $d['alpha'] = 0+$list[$nik]['hadir']['alpha'];
  $d['alpha_r'] = 0+$list[$nik]['hadir']['alpha_r'];
  $d['kk'] = 0+$list[$nik]['hadir']['kk'];
  $d['pijin'] = 0+$list[$nik]['masuk']['pijin'];
  $d['t_ijin'] = 0+$list[$nik]['masuk']['tijin'];
  $d['lembur_w'] = 0+$list[$nik]['masuk']['wajib'];
  $d['t_lembur_w'] = 0+$jamwajib;
  $d['t_biasa'] = 0+$jbiasa;
  $d['t_libur'] = 0+$jlibur;
  $d['t_lembur_lain'] = 0+$list[$nik]['masuk']['tlembur'];
  $d['t_lembur'] = 0+$ttl;
  $d['nt_jabatan'] = 0+$list[$nik]['tunjangan']['t_jabatan'];
  $d['nt_keahlian'] = 0+$list[$nik]['tunjangan']['t_keahlian'];
  $d['nt_prestasi'] = 0+$list[$nik]['tunjangan']['t_prestasi'];
  $d['nt_masakerja'] = 0+$jmasakerja;
  $d['nt_premi'] = 0+$list[$nik]['tunjangan']['t_premi'];
  $d['nt_insentif'] = 0+$insentif;
  $d['nt_l_wajib'] = 0+$n_tlw;
  $d['nt_l_biasa'] = 0+$n_tlb;
  $d['nt_l_libur'] = 0+$n_tll;
  $d['nt_lembur'] = 0+$tgtl;
  $d['n_rumah'] = 0+$grumah;
  $d['n_ai'] = 0+$talij;
  $d['n_psw'] = 0+$list[$nik]['masuk']['pot_ijin'];
  $d['n_bpjs_ket'] = 0+$list[$nik]['potongan']['bpjs_ket'];
  $d['n_bpjs_kes'] = 0+$bpjskes;
  $d['tn_lain'] = 0+$list[$nik]['potongan']['lainnya'];
  $d['n_koperasi'] = 0+$list[$nik]['potongan']['koperasi'];
  $d['n_kspn'] = 0+$list[$nik]['potongan']['kspn'];
  $d['n_spt'] = 0+$list[$nik]['potongan']['spt_karyawan'];
  $d['n_spt_p'] = 0+$list[$nik]['potongan']['spt_perusahaan'];
  $d['gaji_kotor'] = $gaji_kotor;
  $d['n_potongan'] = $potongan;
  $d['total_gaji'] = $totalgaji; 
  $d['sd'] = $list[$nik]['sd']['tgl_absensi'];
  $d['tmakan'] = $tumakan;
  
  //print_r($d); echo '<br><br>';
  
  if($insert_id = $mkal->add($d)) {
    $a += 1;
  }
  /**/
} 

//die();

if($a == count($list['nik'])){
    $mkal->cekGaji($bulan, $thn);
    if($mkal->addGaji($d)){
      if($mkal->addRekap($d['bulan'], $d['tahun'])){
        $app->addMessage('kalkulasi', 'Kalkulasi Telah Berhasil Diproses');  
      }else{
        $app->addError('kalkulasi', 'Terdapat Data Yang Belum Tersimpan');  
      }
    }else{
      $app->addError('kalkulasi', 'Terdapat Data Yang Belum Tersimpan');  
    }    
}elseif($a<count($list['nik']) && $a>0){
    $app->addError('kalkulasi', 'Terdapat Data Yang Belum Tersimpan');
}else {
    $app->addError('kalkulasi', 'Gagal');
}

header('Location: ' . url('a/kalkulasi'));
?>