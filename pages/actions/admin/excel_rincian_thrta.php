<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$thn = $app->input->post('tahun');
$kode = $app->input->post('kode');

$mval = new \App\Models\Validasi($app);
$mkal = new \App\Models\Kalkulasi($app);


require $sysdir . '/excelstyle.php';

$datas = $mkal->getRekapTHRTA($thn, $kode);

$da = [];
for($w=0; $w<count($datas);$w++){
	if($datas[$w]['dept'] == 'produksi'){
	    $da['produksi'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'produksi n'){
        $da['produksi'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'pemasaran gudang'){
	    $da['produksi'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'logistik gudang'){
	    $da['produksi'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'exim'){
	    $da['produksi'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'utility'){
	    $da['utility'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'pemasaran adm'){
	    $da['pemasaran'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'keuangan'){
	    $da['keuangan'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'personalia'){
	    $da['personalia'][] = $datas[$w];
	}else{
	    $da['umum'][] = $datas[$w];
	}
}

$gbbp=0;$gbbu=0;$gbbi=0;$gbba=0;$gbbk=0;$gbbe=0;
$ttp=0;$ttu=0;$tti=0;$tta=0;$ttk=0;$tte=0;


$filename = $thn.'_Lap_Rincian_'.$kode;
$formname = 'PT. PAMOR SPINNING MILLS';
$formnamee = 'Jaten, Karanganyar';
$formnameee = 'S U R A K A R T A';
$kode == 'thr' ? $title = 'T.H.R KARYAWAN PT. PAMOR SPINNING MILLS' : $title = 'TALI ASIH KARYAWAN PT. PAMOR SPINNING MILLS'; 
$sub = ''; 
$subtitle = 'TAHUN '.$thn;

$excel_data = [];
$total_data = [];
$excel_data[] = ['1', 'PRODUKSI', '',''];
if(!empty($da)){
	for ($q=0; $q < count($da['produksi']); $q++) {
		$dept = '  -  '.strtoupper($da['produksi'][$q]['dept']);
		if($da['produksi'][$q]['dept']=='produksi'){ 
		  if($da['produksi'][$q]['grup']!='N'){
		    $dept .= ' SHIFT '.$da['produksi'][$q]['grup'];
		  }else{
		    $dept .= ' '.$da['produksi'][$q]['grup'];
		  }
		}else{ 
		  $dept .= ''; 
		}
		$dgbbp = $da['produksi'][$q]['total']; $gbbp += $da['produksi'][$q]['total'];

		$excel_data[] = [
			'',
			$dept,
			$dgbbp,
			''
		];
	}
	$ttp += $gbbp;
	$excel_data[] = [
		'',
		'',
		'',
		$ttp
	];

	$excel_data[] = ['2', 'UTILITY', '',''];
	for ($q=0; $q < count($da['utility']); $q++) {
		$dept = '  -  '.strtoupper($da['utility'][$q]['dept']);
        		$da['utility'][$q]['grup']=='S' ? $dept .= ' SHIFT' : $dept .= ' N';		
		$dgbbu = $da['utility'][$q]['total']; $gbbu += $da['utility'][$q]['total'];

		$excel_data[] = [
			'',
			$dept,
			$dgbbu,
			''
		];
	}
	$ttu += $gbbu;
	$excel_data[] = [
		'',
		'',
		'',
		$ttu
	];

	$dept = strtoupper($da['pemasaran'][0]['dept']);
	$dgbba = $da['pemasaran'][0]['total']; $gbba += $da['pemasaran'][0]['total'];
	$tta += $gbba;

	$excel_data[] = [
		'3',
		$dept,
		$dgbba,
		$tta
	];

	$dept = strtoupper($da['keuangan'][0]['dept']);
	$dgbbk = $da['keuangan'][0]['total']; $gbbk += $da['keuangan'][0]['total'];
	$ttk += $gbbk;

	$excel_data[] = [
		'4',
		$dept,
		$dgbbk,
		$ttk
	];

	$dept = strtoupper($da['personalia'][0]['dept']);
	$dgbbe= $da['personalia'][0]['total']; $gbbe+= $da['personalia'][0]['total'];
	$tte+= $gbbe;

	$excel_data[] = [
		'5',
		$dept,
		$dgbbe,
		$tte
	];

	$excel_data[] = ['6', 'UMUM', '',''];
	for ($q=0; $q < count($da['umum']); $q++) {
		$dept = '  -  '.strtoupper($da['umum'][$q]['dept']);
		$dgbbi = $da['umum'][$q]['total']; $gbbi += $da['umum'][$q]['total'];

		$excel_data[] = [
			'',
			$dept,
			$dgbbi,
			''
		];
	}
	$tti += $gbbi;
	$excel_data[] = [
		'',
		'',
		'',
		$tti
	];

}

$total_data = [
	'',
	'',
    $gbbp+$gbbu+$gbbi+$gbba+$gbbk+$gbbe,
    $ttp+$ttu+$tti+$tta+$ttk+$tte
];

$rowcount = count($excel_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$sheet = $spreadsheet->getActiveSheet()->setTitle('Rekap Gaji');

$sheet->mergeCells('A1:D1');
$sheet->mergeCells('A2:D2');
$sheet->mergeCells('A3:D3');
$sheet->mergeCells('A4:D4');
$sheet->mergeCells('A5:D5');
$sheet->mergeCells('A6:D6');

$sheet->getStyle('A1')->applyFromArray($formnameStyleL);
$sheet->getCell('A1')->setValue($formname);
$sheet->getStyle('A2')->applyFromArray($formnameStyleL);
$sheet->getCell('A2')->setValue($formnamee);
$sheet->getStyle('A3')->applyFromArray($formnameStyleL);
$sheet->getCell('A3')->setValue($formnameee);
$sheet->getStyle('A4')->applyFromArray($titleStyle);
$sheet->getCell('A4')->setValue($title);
$sheet->getStyle('A5')->applyFromArray($subtitleStyle);
$sheet->getCell('A5')->setValue($subtitle);

// header
$sheet->mergeCells('A7:A8');
$sheet->mergeCells('B7:B8');
$sheet->mergeCells('C7:C8');
$sheet->mergeCells('D7:D8');

$sheet->getStyle('A7:D8')->applyFromArray($headerStyle);
$sheet->fromArray([
    'NO.',
    'BAGIAN',
    'GAJI KOTOR',
    'TOTAL'
], null, 'A7');

// data
$sheet->getStyle('A9:B'.(9+$rowcount))->applyFromArray($contentStyle);
$sheet->getStyle('C9:D' . (9 + $rowcount))->applyFromArray($contentStyleR);
$sheet->fromArray($excel_data, null, 'A9');

//footer
$sheet->getStyle('A'. (9+$rowcount+1).':B'. (9+$rowcount+1))->applyFromArray($footerStyleC);
$sheet->getStyle('C'. (9+$rowcount+1) .':D' . (9+$rowcount+1))->applyFromArray($footerStyleR);
$sheet->fromArray($total_data, null, 'A'. (9+$rowcount+1));

$sheet->mergeCells('A'. (9+$rowcount+2).':D'. (9+$rowcount+2));
$sheet->getStyle('A'. (9+$rowcount+2))->applyFromArray($contentStyleN);
$sheet->getCell('A'. (9+$rowcount+2))->setValue('( '.terbilang($ttp+$ttu+$tti+$tta+$ttk+$tte).' )');
/*
$sheet->mergeCells('E'. (9+$rowcount+6).':F'. (9+$rowcount+6));
$sheet->getStyle('E'. (9+$rowcount+6))->applyFromArray($contentStyleClear);
$sheet->getCell('E'. (9+$rowcount+6))->setValue('Mengetahui,');

$sheet->mergeCells('E'. (9+$rowcount+10).':F'. (9+$rowcount+10));
$sheet->getStyle('E'. (9+$rowcount+10))->applyFromArray($contentStyleClear);
$sheet->getCell('E'. (9+$rowcount+10))->setValue('Moh. Meftakhurreza');

$sheet->mergeCells('E'. (9+$rowcount+11).':F'. (9+$rowcount+11));
$sheet->getStyle('E'. (9+$rowcount+11))->applyFromArray($contentStyleClear);
$sheet->getCell('E'. (9+$rowcount+11))->setValue('GM Umum & Keuangan');

$sheet->mergeCells('J'. (9+$rowcount+6).':K'. (9+$rowcount+6));
$sheet->getStyle('J'. (9+$rowcount+6))->applyFromArray($contentStyleClear);
$sheet->getCell('J'. (9+$rowcount+6))->setValue('Mengetahui,');

$sheet->mergeCells('J'. (9+$rowcount+10).':K'. (9+$rowcount+10));
$sheet->getStyle('J'. (9+$rowcount+10))->applyFromArray($contentStyleClear);
$sheet->getCell('J'. (9+$rowcount+10))->setValue('Bony Hendrianto');

$sheet->mergeCells('J'. (9+$rowcount+11).':K'. (9+$rowcount+11));
$sheet->getStyle('J'. (9+$rowcount+11))->applyFromArray($contentStyleClear);
$sheet->getCell('J'. (9+$rowcount+11))->setValue('Manager HRD');
*/
$sheet->mergeCells('C'. (9+$rowcount+5).':D'. (9+$rowcount+5));
$sheet->getStyle('C'. (9+$rowcount+5))->applyFromArray($contentStyleClear);
$sheet->getCell('C'. (9+$rowcount+5))->setValue('Jaten, '.date('d').' '.namaBulan(date('m')).' '.date('Y'));

$sheet->mergeCells('C'. (9+$rowcount+6).':D'. (9+$rowcount+6));
$sheet->getStyle('C'. (9+$rowcount+6))->applyFromArray($contentStyleClear);
$sheet->getCell('C'. (9+$rowcount+6))->setValue('Bag. Personalia');


$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setWidth(30);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);





//========================================================================================================================================//
//==============================================================  SHEET 2  ===============================================================//
//========================================================================================================================================//




$datar = $mkal->getRincianThrTa($thn, $kode);
$i=1;
$data = [];
$q = 1; $r = 1;
foreach ($datar as $dd) {
	if($dd['bagian']=='produksi'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'adm produksi'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'maintenance'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'QC/PACK/CC/B STORE'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'pemasaran gudang'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'logistik gudang'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'exim'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'utility'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'pemasaran adm'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'keuangan'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'umum'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'personalia'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'sipil'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'keamanan'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'e.d.p'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}
}

	//print_r($d[2][1]);
for($e=1;$e<=count($d);$e++) {
	$objWorkSheet = $spreadsheet->createSheet($e);
	$settitle = '';
	$rincian_data = [];
	$rincian_total = [];
	$settitle = str_replace('/', '-', $d[$e][1]['devisi']);
				/*if($d['grup']=='N'){
	                $settitle .= '';
	            }elseif ($d['grup']=='S') {
	                $settitle .= ' SHIFT';
	            }elseif ($d['grup']=='R') {
	                $settitle .= " DIRUMAHKAN";
	            }else{
	                $settitle .= ' '.$d['grup'];
	            }*/
	$objWorkSheet->setTitle($settitle);

	$kode == 'thr' ? $judul = 'DAFTAR PERINCIAN PEMBAYARAN T.H.R' : $judul = 'DAFTAR PERINCIAN PEMBAYARAN TALI ASIH';
	$subjudul = 'BAGIAN : '.str_replace('-', '/', $settitle);
	$subsubjudul = 'TAHUN '.$thn;

    $gp=0;$mk=0;$tjab=0;$tah=0;$tpres=0;$tgb=0;$tg=0;
    $n=1;
    for ($i=1; $i <= count($d[$e]); $i++) { 
    	
	    for ($k=0; $k < count($d[$e][$i]['data']) ; $k++) {
	    	$rule = '';
		    $gp+=$d[$e][$i]['data'][$k]['gaji'];
		    $mk+=$d[$e][$i]['data'][$k]['t_masakerja'];
			$tjab+=$d[$e][$i]['data'][$k]['t_jabatan'];
			$tah+=$d[$e][$i]['data'][$k]['t_keahlian'];
			$tpres+=$d[$e][$i]['data'][$k]['t_prestasi'];
			$tgb+=$d[$e][$i]['data'][$k]['gaji_kotor'];
			$tg+=$d[$e][$i]['data'][$k]['total'];

			//$d[$e][$i]['data'][$k]['tgl_angkat']=='0000-00-00' || empty($d[$e][$i]['data'][$k]['tgl_angkat']) ? $tgl_in = $d[$e][$i]['data'][$k]['tgl_in'] : $tgl_in = $d[$e][$i]['data'][$k]['tgl_angkat'];

			$dep = $d[$e][$i]['bagian'];
			if($d[$e][$i]['grup']=='N'){
	            $dep .= '';
	        }elseif ($d[$e][$i]['grup']=='S') {
	            $dep .= ' SHIFT';
	        }else{
	            $dep .= ' '.$d[$e][$i]['grup'];
	        }

	        if($kode == 'thr'){
	          $d[$e][$i]['data'][$k]['rule']<=12 ? $rule = $d[$e][$i]['data'][$k]['rule'].'/12 x JUMLAH x 100%' : $rule = '12/12 x JUMLAH x 100%';
	        }elseif($kode = 'ta'){
	          $rule = 'JUMLAH x '.$d[$e][$i]['data'][$k]['rule'].'%';
	        }
			$rincian_data[] = [
				$n,
				substr($d[$e][$i]['data'][$k]['nik'], 1),
				strtoupper($d[$e][$i]['data'][$k]['nama']),
				$d[$e][$i]['data'][$k]['gaji'],
				$d[$e][$i]['data'][$k]['t_masakerja'],
				$d[$e][$i]['data'][$k]['t_jabatan'],
				$d[$e][$i]['data'][$k]['t_keahlian'],
				$d[$e][$i]['data'][$k]['t_prestasi'],
				$d[$e][$i]['data'][$k]['gaji_kotor'],
				$rule,
				$d[$e][$i]['data'][$k]['total']
			];

			$n++;
		}
	}

	//print_r($rincian_data);die();

	$rincian_total[] = [
		'T O T A L',
		'',
		'',
		$gp,
		$mk,
		$tjab,
		$tah,
		$tpres,
		$tgb,
		'',
		$tg
	];
	$rowcount = count($rincian_data);

	$objWorkSheet->mergeCells('A1:K1');
	$objWorkSheet->mergeCells('A2:k2');
	$objWorkSheet->mergeCells('A3:K3');
	$objWorkSheet->mergeCells('A4:K4');
	$objWorkSheet->mergeCells('A5:K5');
	$objWorkSheet->mergeCells('A6:K6');
	$objWorkSheet->mergeCells('A7:K7');

	$objWorkSheet->getStyle('A1')->applyFromArray($formnameStyleL);
	$objWorkSheet->getCell('A1')->setValue($formname);
	$objWorkSheet->getStyle('A2')->applyFromArray($formnameStyleL);
	$objWorkSheet->getCell('A2')->setValue($formnamee);
	$objWorkSheet->getStyle('A3')->applyFromArray($formnameStyleL);
	$objWorkSheet->getCell('A3')->setValue($formnameee);
	$objWorkSheet->getStyle('A4')->applyFromArray($titleStyle);
	$objWorkSheet->getCell('A4')->setValue($judul);
	$objWorkSheet->getStyle('A5')->applyFromArray($subtitleStyle);
	$objWorkSheet->getCell('A5')->setValue($subjudul);
	$objWorkSheet->getStyle('A6')->applyFromArray($subtitleStyle);
	$objWorkSheet->getCell('A6')->setValue($subsubjudul);

	// header
	$objWorkSheet->mergeCells('A8:A9');
	$objWorkSheet->mergeCells('B8:B9');
	$objWorkSheet->mergeCells('C8:C9');
	$objWorkSheet->mergeCells('D8:I8');
	$objWorkSheet->mergeCells('J8:J9');
	$objWorkSheet->mergeCells('K8:K9');

	$objWorkSheet->getStyle('A8:K9')->applyFromArray($headerStyle);
	$objWorkSheet->fromArray([
	    'NO.',
	    'NIK',
	    "NAMA\nKARYAWAN",
	    "GAJI DAN TUNJANGAN",
	    "",
	    '',
	    '',
	    '',
	    '',
	    "PERHITUNGAN",
	    'TOTAL'
	], null, 'A8');

	$objWorkSheet->fromArray([
		'GAJI POKOK',
        'MASA KERJA',
        'TUNJ. JABATAN',
        'TUNJ. KEAHLIAN',
        'TUNJ. PRESTASI',
        'JUMLAH'
	], null, 'D9');

	// data
	$objWorkSheet->getStyle('A10:C'.(10+$rowcount))->applyFromArray($contentStyle);
	$objWorkSheet->getStyle('D10:K' . (10 + $rowcount))->applyFromArray($contentStyleR);
	$objWorkSheet->fromArray($rincian_data, null, 'A10');

	//footer
	$objWorkSheet->mergeCells('A'. (10+$rowcount+1).':C'. (10+$rowcount+1));
	$objWorkSheet->getStyle('A'. (10+$rowcount+1).':C'. (10+$rowcount+1))->applyFromArray($footerStyleC);
	$objWorkSheet->getStyle('D'. (10+$rowcount+1) .':K' . (10+$rowcount+1))->applyFromArray($footerStyleR);
	$objWorkSheet->fromArray($rincian_total, null, 'A'. (10+$rowcount+1));

	$objWorkSheet->mergeCells('A'. (10+$rowcount+3).':K'. (10+$rowcount+3));
	$objWorkSheet->getStyle('A'. (10+$rowcount+3))->applyFromArray($contentStyleN);
	$objWorkSheet->getCell('A'. (10+$rowcount+3))->setValue('JUMLAH YANG DIBAYARKAN  ## '.strtoupper(terbilang($tg)).' ##');
/*
	$objWorkSheet->mergeCells('F'. (10+$rowcount+6).':I'. (10+$rowcount+6));
	$objWorkSheet->getStyle('F'. (10+$rowcount+6))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('F'. (10+$rowcount+6))->setValue('MENGETAHUI,');

	$objWorkSheet->mergeCells('F'. (10+$rowcount+10).':I'. (10+$rowcount+10));
	$objWorkSheet->getStyle('F'. (10+$rowcount+10))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('F'. (10+$rowcount+10))->setValue('........................................');
*/
	$objWorkSheet->mergeCells('H'. (10+$rowcount+5).':J'. (10+$rowcount+5));
	$objWorkSheet->getStyle('H'. (10+$rowcount+5))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('H'. (10+$rowcount+5))->setValue('Surakarta, .....................................');

	$objWorkSheet->mergeCells('H'. (10+$rowcount+6).':J'. (10+$rowcount+6));
	$objWorkSheet->getStyle('H'. (10+$rowcount+6))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('H'. (10+$rowcount+6))->setValue('KA. SIE PERSONALIA');


	$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('C')->setWidth(35);
	$objWorkSheet->getColumnDimension('D')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('J')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('K')->setAutoSize(true);

	$i++;
}




//========================================================================================================================================//
//==============================================================  SHEET 3  ===============================================================//
//========================================================================================================================================//




$datac = $mkal->getRincianThrTa($thn, $kode);
$objWorkSheet = $spreadsheet->createSheet(3);
$settitle = '';
$rincian_data = [];
$rincian_total = [];
$kode=='thr' ? $settitle = 'T.H.R Bersih' : $settitle = 'TALI ASIH Bersih';
$objWorkSheet->setTitle($settitle);

$kode=='thr' ? $judul = 'DAFTAR RINCIAN T.H.R BERSIH KARYAWAN' : $judul = 'DAFTAR RINCIAN TALI ASIH BERSIH KARYAWAN';
$subsubjudul = 'TAHUN : '.$thn;

foreach ($datac as $c) {
    for ($k=0; $k < count($c['data']) ; $k++) {
		$dep = $c['bagian'];
		if($c['grup']=='N'){
            $dep .= '';
        }elseif ($c['grup']=='S') {
            $dep .= ' SHIFT';
        }else{
            $dep .= ' '.$c['grup'];
        }

		$rincian_data[] = [
			strtoupper($dep),
			substr($c['data'][$k]['nik'], 1),
			strtoupper($c['data'][$k]['nama']),
			$c['data'][$k]['total']
		];
	}
}

$rowcount = count($rincian_data);

$objWorkSheet->mergeCells('A1:D1');
$objWorkSheet->mergeCells('A2:D2');
$objWorkSheet->mergeCells('A3:D3');
$objWorkSheet->mergeCells('A4:D4');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);
$objWorkSheet->getStyle('A2')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A2')->setValue($subsubjudul);

$objWorkSheet->getStyle('A5:D5')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'DEPARTEMEN',
    'NIK',
    "NAMA\nKARYAWAN",
    "TOTAL"
], null, 'A5');

// data
$objWorkSheet->getStyle('A6:C'.(6+$rowcount))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('D6:D' . (6 + $rowcount))->applyFromArray($contentStyleR);
$objWorkSheet->fromArray($rincian_data, null, 'A6');

$objWorkSheet->getColumnDimension('A')->setWidth(25);
$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
$objWorkSheet->getColumnDimension('C')->setWidth(35);
$objWorkSheet->getColumnDimension('D')->setAutoSize(true);


//========================================================================================================================================//
//==============================================================  SHEET 4  ===============================================================//
//========================================================================================================================================//


$dc = $mkal->getMasterThrTa($thn, $kode);
$objWorkSheet = $spreadsheet->createSheet(4);
$settitle = '';
$rincian_data = [];
$rincian_total = [];
$kode=='thr' ? $settitle = 'Master THR' : $settitle = 'Master Tali Asih';
$objWorkSheet->setTitle($settitle);

$kode=='thr' ? $judul = 'DAFTAR RINCIAN MASTER T.H.R KARYAWAN' : $judul = 'DAFTAR RINCIAN MASTER TALI ASIH KARYAWAN';
$subsubjudul = 'TAHUN  '.$thn;

for ($k=0; $k < count($dc) ; $k++) {
	$dep = $dc[$k]['nm_bagian'];
	if($dc[$k]['grup']=='N'){
        $dep .= '';
    }elseif ($dc[$k]['grup']=='S') {
        $dep .= ' SHIFT';
    }else{
        $dep .= ' '.$dc[$k]['grup'];
    }

	$rincian_data[] = [
		$k+1,
		strtoupper($dc[$k]['nama']),
		substr($dc[$k]['id'], 1),
		strtoupper($dep),
		$dc[$k]['gaji'],
		$dc[$k]['t_masakerja'],
		$dc[$k]['t_jabatan'],
		$dc[$k]['t_keahlian'],
		$dc[$k]['t_prestasi'],
		$dc[$k]['total']
	];
}

$rowcount = count($rincian_data);

$objWorkSheet->mergeCells('A1:J1');
$objWorkSheet->mergeCells('A2:J2');
$objWorkSheet->mergeCells('A3:J3');
$objWorkSheet->mergeCells('A4:J4');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);
$objWorkSheet->getStyle('A2')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A2')->setValue($subsubjudul);

$objWorkSheet->getStyle('A5:J5')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'NO',
    "NAMA\nKARYAWAN",
    "NIK",
    "BAGIAN",
    "GAJI POKOK",
    "TUNJ. MASA KERJA",
    "TUNJ. JABATAN",
    "TUNJ. KEAHLIAN",
    "TUNJ. PRESTASI",
    "TOTAL"
], null, 'A5');

// data
$objWorkSheet->getStyle('A6:D'.(6+$rowcount))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('E6:J' . (6 + $rowcount))->applyFromArray($contentStyleR);
$objWorkSheet->fromArray($rincian_data, null, 'A6');

$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
$objWorkSheet->getColumnDimension('B')->setWidth(35);
$objWorkSheet->getColumnDimension('C')->setAutoSize(true);
$objWorkSheet->getColumnDimension('D')->setWidth(25);
$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
$objWorkSheet->getColumnDimension('J')->setAutoSize(true);


// ERROR
//die();
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>