<?php

$id = $app->input->post('id');
$page = $app->input->post('page');
$awal = $app->input->postDate('awal');
$akhir = $app->input->postDate('akhir');

$departemen = $app->input->post('departemen');
$bagian = $app->input->post('bagian');
//$ket_bag = $app->input->post('ket_bag');
$jgrup = $app->input->post('jgrup');
$grup = null.$app->input->post('grup');
$libur = null.$app->input->post('libur');
$jamkerja = $app->input->post('jamkerja');

$mtki = new \App\Models\Karyawan($app);

$queries = [
    'page' => $page,
    'departemen' => $departemen,
    'bagi' => $bagian,
    'bagian' => $bagian,
    //'ket_bag' => $ket_bag,
    'jgrup' => $jgrup,
    'grup' => $grup,
    'libur' => $libur,
    'jam' => $jamkerja,
    'tinjau' => ''
];

if(empty($id)){
	$app->addError('cuti_d_list', 'Maaf pilih Karyawan terlebih dahulu');
	header('Location: ' . url('a/cuti_d_m') .'?'. queryString($queries));
}else{
	if($mtki->cutiDAdd($id, $awal, $akhir)) {
	    $app->addMessage('cuti_d_list', 'Proses Cuti Dispensasi Berhasil');
	    header('Location: ' . url('a/cuti_d_m') .'?'. queryString($queries));
	}
	else {
	    $app->addError('cuti_d_list', 'Proses Cuti Dispensasi Gagal');
	    header('Location: ' . url('a/cuti_d_m') .'?'. queryString($queries));
	}
}