<?php

$kd = $app->input->post('jam');
$ket = $app->input->post('ket');
$mulai = $app->input->post('mulai');
$akhir = $app->input->post('akhir');
$bagian = $app->input->post('bagian');
$shift = $app->input->post('shift');
$hari = $app->input->post('hari');
$kd == 1 ? $jam = ' ' : $jam=' Shift ';
$nmhari = '';
if($hari==5){
	$nmhari = namaHari($hari);	
}elseif($hari == 0.5){
	$nmhari = 'Setengah Hari';
}
$ket = $bagian.$jam.$shift." ".$nmhari;
$tgl = dateCreate($app->input->post('tgl'));

$jamkerja_aktif = new \App\Models\Jamkerja($app);
if($insert_id = $jamkerja_aktif->addAktif($kd, $bagian, $shift, $hari, $ket, $mulai, $akhir, $tgl)) {
    $app->addMessage('jamkerja_aktif', 'Jam Kerja Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jamkerja_aktif', 'Jam Kerja Baru Gagal Disimpan');
}

header('Location: ' . url('a/jamkerja_aktif'));
