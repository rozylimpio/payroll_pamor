<?php

$tgl = dateCreate($app->input->post('tgl'));
$shift = $app->input->post('shift');

$jadwal = new \App\Models\JadwalShift($app);
if($insert_id = $jadwal->add($tgl, $shift)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal_shift')); 
