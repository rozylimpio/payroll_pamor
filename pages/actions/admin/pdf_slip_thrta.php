<?php
$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$thn = $app->input->post('thn');
$bagian = $app->input->post('bag');
$kode = $app->input->post('kode');
$bag_ex = explode('_', $bagian);
$bag = $bag_ex[0];
$grup = $bag_ex[1];

$file = $mval->getSlipTHRTA($thn, $bagian, $kode);

if(empty($file)){
	ob_start();
	?><html>
	<head>
	<meta charset='utf-8'>
	<style>
	    @page { margin: 10px 0 0 0; }
	    body {
	        line-height: 1.3;
	        font-size: 12pt;
	        font-family: Helvetica, Arial, sans-serif;/*Georgia, serif;*/
	        color: black;
	        margin: 0px;
	    }
	    table {
		  	border-collapse: collapse;
		  }
	    table, tr, th, td { 
	    	line-height: 1.2;
	    	border: 0px; 
	    	padding: 2px;
	    }
	    td.tinggi{
	    	/*height: 726pt; 736pt*/
	    	width: 303pt !important;
	    	min-width: 303pt; /*fix 303pt*/
	    	max-width: 303pt;
	    	vertical-align: top;
	    	padding: 15px 20px 0px 20px;
	    }
	    .colom{
	    	height: 369pt; /*376pt*/
	    	margin: 0px 10px 0px 0px;
	    }
		.sisi{
			border-left: solid 1px;
			border-right: solid 1px;
			border-top: 0px;
			border-bottom: 0px;
		}
		.atas{
			border-left: 0px;
			border-right: 0px;
			border-top: dashed 1px;
			border-bottom: dashed 1px;	
		}
		.kosong{
			border-left: 0px;
			border-right: 0px;
			border-top: 0px;
			border-bottom: 0px;
		}
	    /*.page_break { page-break-after: always; }*/
	</style>
	</head>
	<body>
		<?php
	   $data = $mkal->getRincianThrTa($thn, $kode);
	   $i=1;
	   foreach ($data as $d) {
	    	if($d['bagian'] == $bag && $d['grup'] == $grup){
		      $br = count($d['data']);
		      $hall = ceil($br/3);
		      $hall==0 ? $hal = 1 : $hal = $hall;
		      $min = 0;

		      for ($j=1; $j <= $hal; $j++) { 
		        $max_data = 3*$j;
		        $max_data>count($d['data']) ? $max = count($d['data']) : $max = $max_data;
		        ?>
		        <div class="<?php echo $i<=count($data) ? 'page_break' : ''; ?>">
					<table width="100% auto" border="0" class="kosong" table-layout: fixed>
						<tr>
							<?php
							for ($k=$min; $k < $max ; $k++) {
							$gb=0;$tp=0;$gt=0;
			            ?>
							<td class="tinggi">
								<div class="colom">
									<br><br><br><br><br><br>
									<table width="100%" border="0" class="kosong">
										<tr>
											<td colspan="2" align="center" style="font-size: 18; padding: 10px;" class="atas">
												<B>PT. PAMOR SPINNING MILLS</B>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center" style="font-size: 14; padding: 7px;" class="atas">
												<?php if($kode=='thr'){ ?>
												<B>TUNJANGAN HARI RAYA TAHUN <?php echo $thn ?></B>
												<?php }else{ ?>
												<B>TALI ASIH KARYAWAN TAHUN <?php echo $thn ?></B>
												<?php }?>
											</td>
										</tr>
										<tr>
											<td class="kosong" width="30%" style="font-size:18px">
												<br style="margin-bottom: 10px;">
												No. Urut
											</td>
											<td class="kosong" style="font-size:21px">
												<br>
												: &nbsp; <?php echo $k+1; ?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:18px">
												N a m a
											</td>
											<td class="kosong" style="font-size:21px">
												: &nbsp; <?php echo strtoupper(substr($d['data'][$k]['nama'], 0, 17)); ?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:18px">
												N . I . K
											</td>
											<td class="kosong" style="font-size:21px">
												: &nbsp; <?php echo substr($d['data'][$k]['nik'], 1); ?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:18px">
												Departement
												<br><br>
											</td>
											<td class="kosong" style="font-size:21px">
												: &nbsp; <?php
											              echo strtoupper($d['bagian']);
											              if($d['grup']=='N'){
											                echo '';
											              }elseif ($d['grup']=='S') {
											                echo ' SHIFT';
											              }else{
											                echo ' '.$d['grup'];
											              }

											              $nm = '_'.strtoupper(str_replace('/', '-', $d['bagian']));
											              if($d['grup']=='N'){
											                $nm .= '';
											              }elseif ($d['grup']=='S') {
											                $nm .= '_SHIFT';
											              }else{
											                $nm .= '_'.$d['grup'];
											              }
											            ?>
												<br><br>
											</td>
										</tr>
										<tr><td colspan="2" class="atas"></td></tr>
									</table>
								</div>
								<div class="colom" style="margin-bottom: -7px">
									<div style="margin-bottom: 12px">&nbsp;</div>
									<br><br>
									<table width="100%" border="0" class="kosong">
										<tr>
											<td colspan="5"align="center" style="padding: 5px;" class="atas">
												<B style="font-size: 12;">PT. PAMOR SPINNING MILLS</B><br>
												<?php if($kode=='thr'){ ?>
													<B style="font-size: 10;">TUNJANGAN HARI RAYA TAHUN <?php echo $thn ?></B>
												<?php }else{ ?>
													<B style="font-size: 10;">TALI ASIH TAHUN <?php echo $thn ?></B>
												<?php }?>
											</td>
										</tr>
										<tr>
											<td class="kosong" style="font-size:13px">
												<div style="height:15px"></div>
												No. Urut<br>
												N a m a<br>
												N . I . K<br>
												Departement
												<br>
											</td>
											<td colspan="4" class="kosong" style="font-size:13px">
												<div style="height:15px"></div>
												: &nbsp; <?php echo $k+1; ?><br>
												: &nbsp; <?php echo strtoupper(substr($d['data'][$k]['nama'], 0, 23)); ?><br>
												: &nbsp; <?php echo substr($d['data'][$k]['nik'],1); ?><br>
												: &nbsp; <?php
											              echo strtoupper($d['bagian']);
											              if($d['grup']=='N'){
											                echo '';
											              }elseif ($d['grup']=='S') {
											                echo ' SHIFT';
											              }else{
											                echo ' '.$d['grup'];
											              }
											            ?>
												<br>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:14px;" class="atas">
												<div style="height:15px"></div>
												Gaji<br>
												Masa Kerja<br>
												Tunjangan Jabatan<br>
												Tunjangan Keahlian<br>
												Insentif Prestasi Kerja
											</td>
											<td style="font-size:14px;" class="atas">
												<div style="height:15px"></div>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.<br>
												Rp.
											</td>
											<td style="font-size:14px" align="right" class="atas">
												<div style="height:15px"></div>
												<?php echo idr($d['data'][$k]['gaji']); $gb += $d['data'][$k]['gaji']; ?><br>
												<?php echo idr($d['data'][$k]['t_masakerja']); $gb += $d['data'][$k]['t_masakerja']; ?><br>
												<?php echo idr($d['data'][$k]['t_jabatan']); $gb += $d['data'][$k]['t_jabatan']; ?><br>
												<?php echo idr($d['data'][$k]['t_keahlian']); $gb += $d['data'][$k]['t_keahlian']; ?><br>
												<?php echo idr($d['data'][$k]['t_prestasi']); $gb += $d['data'][$k]['t_prestasi']; ?>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:14px" align="right">
												JUMLAH&nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; 
											</td>
											<td style="font-size:14px;">
												Rp.
											</td>
											<td style="font-size:14px" align="right">
												<?php echo idr($gb); ?>
											</td>
										</tr>
										<tr>
											<td style="font-size:14px;" class="atas">
												<div style="height:15px"></div>
												Perhitungan <?php echo $kode=='thr' ? 'T.H.R' : 'Tali Asih' ?>
											</td>
											<td style="font-size:14px;" class="atas">
												<div style="height:15px"></div>
												:
											</td>
											<td style="font-size:14px" align="right" class="atas">
												<div style="height:15px"></div>&nbsp;
											</td>
											<td colspan="2" class="atas"> &nbsp;</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:14px" align="center" class="atas">
												<br>
												<?php
												if($kode == 'thr'){
													echo $d['data'][$k]['rule']<=12 ? $d['data'][$k]['rule'].'/12 x JUMLAH x 100%' : '12/12 x JUMLAH x 100%';
												}elseif($kode = 'ta'){
													echo 'JUMLAH x '.$d['data'][$k]['rule'].'%';
												}
												?>
												<br><br>
											</td>
											<td style="font-size:14px;" class="atas">
												<br>Rp.<br><br>
											</td>
											<td style="font-size:14px" align="right" class="atas">
												<br>
												<?php echo idr($d['data'][$k]['total']); ?>
												<br><br>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="font-size:16px" align="center">
												<div style="height:15px"></div>
												<b><?php echo $kode=='thr' ? 'T.H.R' : 'TALI ASIH' ?> BERSIH</b>
											</td>
											<td style="font-size:16px;">
												<div style="height:15px"></div>
												<b>Rp.</b>
											</td>
											<td style="font-size:16px" align="right">
												<div style="height:15px"></div>
												<b><u><?php echo idr(pembulatan($d['data'][$k]['total'])); ?></u></b>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<?php
							}
							if ($j==$hal){
								$jtd = $max_data - count($d['data']);
								for ($n=0; $n < $jtd; $n++) { 
									?>
									<td class="tinggi"><div class="colom"></div></td>
									<?php
								}
							}
							?>
						</tr>
					</table>
				</div>
	        <?php
	        $i+=1;
	        $min+=3;

		     }
	      }

	   }
	   ?>
	</body>
	</html>
	<?php

	$html = ob_get_clean();

	$subdir = 'pdf/slip_'.$kode.'/';
	$dir = $pubdir . '/' . $subdir;

	$exists = false;
	do {
	    $name = $thn.'_Slip_'.strtoupper($kode).$nm.'.pdf';
	    $exists = file_exists($dir . $name);
	} while($exists);
  	$customPaper = array(0,0,1006.29,791.811026); //dikurangi 0.17mm. awal 792.283465);//
	$dompdf = new \Dompdf\Dompdf();
	$dompdf->loadHtml($html);
	$dompdf->setPaper($customPaper);
	$dompdf->render();

	$pdf_gen = $dompdf->output();

	if(!file_put_contents($dir . $name, $pdf_gen)){
	    header("HTTP/1.0 500 Internal Server Error");
	    echo 'Generate PDF Failed';
	    exit();
	} else {
	    $mval->addSlipThrta($thn, $subdir . $name, $bagian, $kode);

	    header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => $name,
	        'filepath' => url($subdir . $name)
	    ]);
	}
}else{
	header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => 'x',
	        'filepath' => url($file)
	    ]);
}
?>