<?php
$id = '';
$ids = '';
$ids = $app->input->post('id');
$data['dep'] = $app->input->post('dep');
$data['bag'] = $app->input->post('bag');
$data['jgrup'] = ''.$app->input->post('jgrup');
$data['grup'] = null.$app->input->post('grup');
$data['libur'] = null.$app->input->post('libur');
$data['jamkerja'] = $app->input->post('jamkerja');
$data['pilihan'] = $app->input->post('pilihan');
$data['kt'] = $app->input->post('kt');
$data['jmlh'] = $app->input->post('jmlh');

$mkar = new \App\Models\Karyawan($app);

if(!empty($ids)){
	for ($i=0; $i < count($ids); $i++) { 
		$id .= ','.$ids[$i];
	}
	$data['id'] = substr($id, 1);
}else{
	$data['id'] = null;
}

//print_r($data);die();

if($data['pilihan']=='all'){
	if($mkar->geserLiburAll($data)) {
	    $app->addMessage('geser', 'Geser Libur Berhasil diperbaharui');
	    header('Location: ' . url("a/geser_libur?departemen=".$data['dep']."&bagian=".$data['bag']."&jam=".$data['jamkerja']."&pilihan=".$data['pilihan']."&jgrup=".$data['jgrup']."&grup=".$data['grup']."&libur=".$data['libur']."&tinjau="));
	}
	else {
	    $app->addError('geser', ' Gagal Perbaharui Geser Libur');
	    header('Location: ' . url("a/geser_libur?departemen=".$data['dep']."&bagian=".$data['bag']."&jam=".$data['jamkerja']."&pilihan=".$data['pilihan']."&jgrup=".$data['jgrup']."&grup=".$data['grup']."&libur=".$data['libur']."&tinjau="));
	}
}elseif($data['pilihan']=='not'){
	if($mkar->geserLiburCustom($data)) {
	    $app->addMessage('geser', 'Geser Libur Berhasil diperbaharui');
	    header('Location: ' . url("a/geser_libur?departemen=".$data['dep']."&bagian=".$data['bag']."&jam=".$data['jamkerja']."&pilihan=".$data['pilihan']."&jgrup=".$data['jgrup']."&grup=".$data['grup']."&libur=".$data['libur']."&tinjau="));
	}
	else {
	    $app->addError('geser', ' Gagal Perbaharui Geser Libur');
	    header('Location: ' . url("a/geser_libur?departemen=".$data['dep']."&bagian=".$data['bag']."&jam=".$data['jamkerja']."&pilihan=".$data['pilihan']."&jgrup=".$data['jgrup']."&grup=".$data['grup']."&libur=".$data['libur']."&tinjau="));
	}
}