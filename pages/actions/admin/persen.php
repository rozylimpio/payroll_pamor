<?php

$ket = $app->input->post('ket');
$status = $app->input->post('status');
$pers = $app->input->post('persen');
$persen = 100-$pers;

$per = new \App\Models\Persen($app);
if($insert_id = $per->add($ket, $status, $persen)) {
    $app->addMessage('persen', 'Data Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('persen', 'Data Baru Gagal Disimpan');
}

header('Location: ' . url('a/persen'));