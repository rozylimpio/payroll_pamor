<?php

$id = $app->input->get('id');

$jamkerja_istirahat = new \App\Models\Jamkerja($app);
if($jamkerja_istirahat->deleteIstirahat($id)) {
    $app->addMessage('jamkerja_istirahat_list', 'Jam Istirahat Berhasil Dihapus');
}
else {
    $app->addError('jamkerja_istirahat_list', 'Jam Istirahat Gagal Dihapus');
}

$redirect = url('a/jamkerja_istirahat');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);