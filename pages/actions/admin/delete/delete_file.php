<?php
$id = $app->input->get('id');
$tbl = $app->input->get('tbl');
$direct = $app->input->get('dir');

$absensi = new \App\Models\Absensi($app);
$abs = $absensi->getFile($thn=null, $bln=null, $bag=null, $tbl, $id);
if($abs){
	$name = $abs['file'];

	if($absensi->deleteFile($id, $tbl)) {
		$exists = file_exists($name);
		if($exists){ unlink($name); $exists = false; }
    	if(!$exists){ 
    		$app->addMessage('rincian', 'File Berhasil Dihapus'); 
    	}else{
    		$app->addError('rincian', 'File Gagal Dihapus'); 
    	}
	}
	else {
	    $app->addError('rincian', 'Data Tabel Gagal Dihapus');
	}	
}else{
	$app->addError('rincian', 'Data Tabel Tidak Ditemukan');
}


$redirect = url('a/'.$direct);
header('Location: ' . $redirect);
?>