<?php

$id = $app->input->get('id');

$potongan = new \App\Models\Potongan($app);
if($potongan->delete($id)) {
    $app->addMessage('potongan_list', 'Potongan Berhasil Dihapus');
}
else {
    $app->addError('potongan_list', 'Potongan Gagal Dihapus');
}

$redirect = url('a/potongan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);