<?php

$tgl_aw = dateCreate(str_replace('/', '-', $app->input->post('tgl_aw')));
$tgl_ak = dateCreate(str_replace('/', '-', $app->input->post('tgl_ak')));

$absensi = new \App\Models\Absensi($app);
if($absensi->deleteAbsBetween($tgl_aw, $tgl_ak)) {
    $app->addMessage('absensi', 'Absensi Berhasil Dihapus');
}
else {
    $app->addError('absensi', 'Absensi Gagal Dihapus');
}

$redirect = url('a/absensi_hapus_b');
header('Location: ' . $redirect);