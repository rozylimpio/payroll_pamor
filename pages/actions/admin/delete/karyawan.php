<?php

$id = $app->input->get('id');

$karyawan = new \App\Models\Karyawan($app);
if($karyawan->delete($id)) {
    $app->addMessage('karyawan', 'Karyawan Berhasil Dihapus');
}
else {
    $app->addError('karyawan', 'Karyawan Gagal Dihapus');
}

$redirect = url('a/karyawan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);