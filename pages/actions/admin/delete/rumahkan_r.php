<?php

$link['ta'] = $app->input->post('tgl');
$id = $app->input->get('id');

$mkar = new \App\Models\Karyawan($app);
if($mkar->deleteRumah($id)) {
    $app->addMessage('rumah_list', 'Berhasil Dihapus');
}
else {
    $app->addError('rumah_list', 'Gagal Dihapus');
}

$redirect = url('a/rumah_r?tanggal='.$link['ta'].'&tinjau=');
header('Location: ' . $redirect);