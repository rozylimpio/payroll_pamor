<?php

$id = $app->input->get('id');

$depp = new \App\Models\Tkeahlian($app);
if($depp->delete($id)) {
    $app->addMessage('t_keahlian_list', 'Tunjangan Keahlian Berhasil Dihapus');
}
else {
    $app->addError('t_keahlian_list', 'Tunjangan Keahlian Gagal Dihapus');
}

$redirect = url('a/t_keahlian');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);