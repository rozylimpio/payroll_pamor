<?php

$id = $app->input->get('id');
$file = $app->input->get('file');
$direct = $app->input->get('redirect');

$ssp = explode('/', $file);

//print_r($ssp);echo $id.' '.$file.' '.$redirect;die();

$surat = new \App\Models\Surat($app);
if($ssp[2]=='sp'){
	if($surat->deletesp($id)) {
	    $app->addMessage('surat', 'Surat Berhasil Dihapus');
	    unlink($file);
	}
	else {
	    $app->addError('surat', 'Surat Gagal Dihapus');
	}
}else{
	if($surat->delete($id)) {
	    $app->addMessage('surat', 'Surat Berhasil Dihapus');
	    unlink($file);
	}
	else {
	    $app->addError('surat', 'Surat Gagal Dihapus');
	}
}

$redirect = url($direct);
header('Location: ' . $redirect);