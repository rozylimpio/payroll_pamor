<?php

$link['ta'] = $app->input->post('nik');
$id = $app->input->get('id');

$mkar = new \App\Models\Karyawan($app);
if($mkar->deleteCutiD($id)) {
    $app->addMessage('cuti_d_list', 'Berhasil Dihapus');
}
else {
    $app->addError('cuti_d_list', 'Gagal Dihapus');
}

$redirect = url('a/cuti_d_r?tanggal='.$link['ta'].'&tinjau=');
header('Location: ' . $redirect);