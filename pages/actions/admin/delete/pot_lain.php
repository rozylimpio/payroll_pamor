<?php

$id = $app->input->get('id');

$potongan = new \App\Models\Potongan($app);
if($potongan->deleteLain($id)) {
    $app->addMessage('Pot_list', 'Potongan Telah Berhasil Dihapus');
}
else {
    $app->addError('Pot_list', 'Potongan Gagal Dihapus');
}
header('Location: ' . url('a/pot_lain'));