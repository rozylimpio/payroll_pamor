<?php

$id = $app->input->get('id');

$jadwal = new \App\Models\JadwalShift($app);
if($jadwal->delete($id)) {
    $app->addMessage('jadwal_list', 'Jadwal Berhasil Dihapus');
}
else {
    $app->addError('jadwal_list', 'Jadwal Gagal Dihapus');
}

$redirect = url('a/jadwal_shift');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);