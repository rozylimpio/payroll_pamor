<?php

$id = $app->input->get('id');

$depp = new \App\Models\Tprestasi($app);
if($depp->delete($id)) {
    $app->addMessage('t_prestasi_list', 'Tunjangan Prestasi Berhasil Dihapus');
}
else {
    $app->addError('t_prestasi_list', 'Tunjangan Prestasi Gagal Dihapus');
}

$redirect = url('a/t_prestasi');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);