<?php

$id = $app->input->get('id');

$persen = new \App\Models\Persen($app);
if($persen->delete($id)) {
    $app->addMessage('persen_list', 'Data Berhasil Dihapus');
}
else {
    $app->addError('persen_list', 'Data Gagal Dihapus');
}

$redirect = url('a/persen');
header('Location: ' . $redirect);