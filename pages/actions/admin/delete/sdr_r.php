<?php

$link['ta'] = $app->input->get('nik');
$ijin = $app->input->get('ijin');
$id = $app->input->get('id');

//echo $link['ta'].' '.$ijin.' '.$id;

$mkar = new \App\Models\Karyawan($app);

if($ijin=='kk'){
	if($mkar->deleteKK($id)) {
	    $app->addMessage('sdr_list', 'Berhasil Dihapus');
	}else{
	    $app->addError('sdr_list', 'Gagal Dihapus');
	}
}else{
	if($mkar->deleteSdr($id)) {
	    $app->addMessage('sdr_list', 'Berhasil Dihapus');
	}
	else {
	    $app->addError('sdr_list', 'Gagal Dihapus');
	}
}

$redirect = url('a/sdr_r?nik='.$link['ta'].'&ijin='.$ijin.'&tinjau=');
header('Location: ' . $redirect);