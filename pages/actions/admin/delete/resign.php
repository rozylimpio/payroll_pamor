<?php

$id = $app->input->get('id');

$karyawan = new \App\Models\Resign($app);
if($karyawan->delete($id)) {
    $app->addMessage('resign', 'Karyawan Berhasil Dihapus');
}
else {
    $app->addError('resign', 'Karyawan Gagal Dihapus');
}

$redirect = url('a/resign');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);