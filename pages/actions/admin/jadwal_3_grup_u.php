<?php

$tgl = dateCreate($app->input->post('tgl'));
$shift = $app->input->post('shift');
array_push($shift, 0);
$grup = $app->input->post('grupAwal');
$jdwlGrup = $app->input->post('grup');

foreach($jdwlGrup as $item){
   $grp[] = numToAlphabet($item);
}

// echo 'grup: ';print_r($grup); echo '<br>'; 
// echo 'jdwl: ';print_r($jdwlGrup); echo '<br>';
// echo 'grp: ';print_r($grp); echo '<br>';
// echo 'shift: ';print_r($shift); echo '<br>';
// die();

$tglaw = strtotime($tgl);
$thntgl = explode('-',$tgl);
$tglak = strtotime(date('Y-m-d', strtotime('+1 year', $tglaw)));

$hr = date('N', $tglaw);
$tglp = $tglaw;
$tglp_n = date('N', $tglp);

if ($hr > 1){
   $hrtglak = 7 - $hr;
}else{
   $hrtglak = 6;
}
// if($shift == 'Pagi'){
//    $shift = array('Pagi', 'Malam', 'Siang', 0);
// }

// $grp = ['A', 'C', 'B'];

$a = 0; $b = $grup-1; $c=0; $d=0;
do {    
   $tgll = $tgl;              
   if($hr==$tglp_n){
      for($j=0; $j<(1*3);$j++){ 
         $hrtglak = $a > 0 ? 6 : $hrtglak; 
         //Tanggal berlaku begin              
         $hari[$a][$j]['berlaku'] = $tgll;
         //grup shift begin
         if($b<3){
            $hari[$a][$j]['grup'] = $grp[$b];
         }
         $b==(2) ? $b=0 : $b+=1;
         //grup shift end

         //minggu ke- begin
         $hari[$a][$j]['bulan_ke'] = (int)date('n', $tglaw);
         $hari[$a][$j]['tglaw'] = date('Y-m-d', $tglp);
         $hari[$a][$j]['tglak'] = date('Y-m-d', strtotime('+'.$hrtglak.' day', $tglp));

         //shift pagi, malam, siang begin
         if ($c<(count($shift)-1)) {
            $hari[$a][$j]['shift'] = $shift[$c];//.'<br>';
         }
         $c>=count($shift)-(1+1) ? $c=0+$d : $c+=1;
         //shift pagi, malam, siang end
      }
      $a+=1; $c+=1;
      $c>=count($shift)-(1) ? $c=0 : $c=$c;
   }
   $hr = 1; //senin (perpindahan shift dimulai dari senin, dan tidak melihat kapan hari berlakunya)
   $tglp = strtotime('+1 day', $tglp);
   $tglp_n = date('N', $tglp);
   $tglaw = strtotime('+1 day', $tglaw);
} while ($tglaw<=$tglak);

// print_r($hari);die();
$jadwal = new \App\Models\Jadwal3GrupU($app);
if($insert_id = $jadwal->add($hari)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal_3_grup_u')); 
