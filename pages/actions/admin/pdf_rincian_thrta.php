<?php
$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$thn = $app->input->post('thn');
$kode = $app->input->post('kode');

ob_start();
?><html>
<head>
<meta charset='utf-8'>
<style>
    @page { margin: 15px 5px 10px 30px; }
    body {
        line-height: 1.1;
        font-size: 12pt;
        font-family: system-ui;
        /*font-family: "Arial Narrow", Arial, sans-serif;*/
        color: black;
        margin: 15px 5px 10px 30px;
    }
    .kar{
      line-height: 1.1;
      /*font-family: Calibri;*/
      font-family: sans-serif;
    }
    table {
	  	border-collapse: collapse;
	  }
    table, tr, th, td { 
    	border: solid 1px; 
    	padding: 2.7px;
      font-size: 12px;
    }
    .sisi{
      border-left: solid 1px;
      border-right: solid 1px;
      border-top: 0px;
      border-bottom: 0px;
    }
    .kosong{
      border-left: 0px;
      border-right: 0px;
      border-top: 0px;
      border-bottom: 0px;
    }
    .page_break { page-break-after: always; }
    /*br{
        display: block; /* makes it have a width */
      /*  content: ""; /* clears default height */
       /*  margin-top: 20; /* change this to whatever height you want it */
    /* }*/
</style>
</head>
<body>
  <?php
  $data = $mkal->getRincianThrTa($thn, $kode);
    $i=1;$n=1;
  foreach ($data as $d) {
    $br = count($d['data']);
    $hall = ceil($br/40);
    $hall==0 ? $hal = 1 : $hal = $hall;
    $min = 0;

    for ($j=1; $j <= $hal; $j++) { 
      $max_data = 40*$j;
      $max_data>count($d['data']) ? $max = count($d['data']) : $max = $max_data;
      ?>
      <div class="<?php echo $j<=$hal ? 'page_break kar' : ''; ?>">
        <b style="font-size: 14px">
        PT. Pamor Spinning Mills<br>
        Jaten, Karanganyar<br>
        <u>S U R A K A R T A</u>
        </b>
        <br><br><?php //echo 'i:'.$i.' cd:'.$cd ?>
        <div align="center" style="font-size: 14px">
          <b>
            DAFTAR PERINCIAN <?php echo $kode=='thr'?'T.H.R.':'TALI ASIH' ?> KARYAWAN<br>
            BAGIAN : 
            <?php
            echo strtoupper($d['bagian']);
            if($d['grup']=='N'){
              echo '';
            }elseif ($d['grup']=='S') {
              echo ' SHIFT';
            }else{
              echo ' '.$d['grup'];
            }
            echo '<br>';
            echo 'TAHUN '.$thn; 
            ?>
          </b>
        </div>      
        <div style="float: right; font-size:12px;">Halaman : <?php echo $j.' / '.$hal; ?></div><br>
        <table width="100% auto" class="kar">
          <thead>
            <tr>
              <th align="center" rowspan="2">No</th>
              <th align="center" rowspan="2">NIK</th>
              <th align="center" rowspan="2">N A M A<br>KARYAWAN</th>
              <th align="center" colspan="6">GAJI DAN TUNJANGAN</th>
              <th align="center" rowspan="2">PERHITUNGAN<br><?php echo $kode=='thr'?'T.H.R':'TALI ASIH' ?></th>
              <th align="center" rowspan="2">TOTAL<br><?php echo $kode=='thr'?'T.H.R':'TALI ASIH' ?></th>
            </tr>
            <tr>
              <th align="center" style="font-size: 10px;">GAJI POKOK</th>
              <th align="center" style="font-size: 10px;">MASA KERJA</th>
              <th align="center" style="font-size: 10px;">TUNJ. JABATAN</th>
              <th align="center" style="font-size: 10px;">TUNJ. KEAHLIAN</th>
              <th align="center" style="font-size: 10px;">TUNJ. PRESTASI</th>
              <th align="center" style="font-size: 10px;">JUMLAH</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $gp=0;$mk=0;$tjab=0;$tah=0;$tpres=0;$tgb=0;$tg=0;
            for ($k=$min; $k < $max ; $k++) {
            $d['data'][$k]['tgl_angkat']=='0000-00-00' || empty($d['data'][$k]['tgl_angkat']) ? $tgl_in = $d['data'][$k]['tgl_in'] : $tgl_in = $d['data'][$k]['tgl_angkat'];
            ?>
            <tr>
              <td align="right"><?php echo $k+1; ?></td>
              <td align="center" style="font-size:11px"><?php echo substr($d['data'][$k]['nik'], 1); ?></td>
              <td style="font-size:11px"><?php echo strtoupper(substr($d['data'][$k]['nama'], 0, 25)); ?></td>
              <td align="right"><?php echo idr($d['data'][$k]['gaji']); $gp+=$d['data'][$k]['gaji']; ?></td>
              <td align="right"><?php echo idr($d['data'][$k]['t_masakerja']); $mk+=$d['data'][$k]['t_masakerja']; ?></td>
              <td align="right"><?php echo idr($d['data'][$k]['t_jabatan']); $tjab+=$d['data'][$k]['t_jabatan']; ?></td>
              <td align="right"><?php echo idr($d['data'][$k]['t_keahlian']); $tah+=$d['data'][$k]['t_keahlian']; ?></td>
              <td align="right"><?php echo idr($d['data'][$k]['t_prestasi']); $tpres+=$d['data'][$k]['t_prestasi']; ?></td>
              <td align="right"><?php echo idr($d['data'][$k]['gaji_kotor']); $tgb+=$d['data'][$k]['gaji_kotor']; ?></td>
              <td align="center">
                <?php
                if($kode == 'thr'){
                  echo $d['data'][$k]['rule']<=12 ? $d['data'][$k]['rule'].'/12 x JUMLAH x 100%' : '12/12 x JUMLAH x 100%';
                }elseif($kode = 'ta'){
                  echo 'JUMLAH x '.$d['data'][$k]['rule'].'%';
                }
                ?>
              </td>
              <td align="right"><?php echo idr($d['data'][$k]['total']); $tg+=$d['data'][$k]['total']; ?></td>
            </tr>
            <?php
            $n==count($d['data']) ? $n=1 : $n++;
            }
            ?>
            <tr>
              <td colspan="11"></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th align="center" colspan="3">T O T A L</th>
              <th align="right"><?php echo idr($gp); ?></th>
              <th align="right"><?php echo idr($mk); ?></th>
              <th align="right"><?php echo idr($tjab); ?></th>
              <th align="right"><?php echo idr($tah); ?></th>
              <th align="right"><?php echo idr($tpres); ?></th>
              <th align="right"><?php echo idr($tgb); ?></th>
              <th>&nbsp;</th>
              <th align="right"><?php echo idr($tg); ?></th>
            </tr>
          </tfoot>
        </table>
        <div style="font-size: 12px; float: right; border-top: dashed 1px; border-bottom: dashed 1px; padding: 5px; margin-top: 10px;">
          <i><?php echo 'JUMLAH YANG DIBAYARKAN &nbsp; ### '.strtoupper(terbilang($tg)); ?> ###</i>
        </div>
        <br>
            <table border="0" width="100% auto" style="font-size:12px; border: none 0px; margin-top: 17px">
              <tr>
                <td align="center" class="kosong" width="30%">&nbsp;</td>
                <td align="center" class="kosong">
                  <br><br>
                  MENGETAHUI,
                  <br><br><br><br><br>
                  (...........................................)
                  <br>
                </td>
                <td align="center" class="kosong">
                  SURAKARTA, ..........................................
                  <br><br>
                  KA. SIE PERSONALIA
                  <br><br><br><br><br>
                  (...........................................)
                  <br>
                </td>
              </tr>
            </table> 
      </div>
      <?php
      $i==count($d['data']) ? $i=1 : $i++;
      $min+=40;
    }

  }
  ?>
</body>
</html>
<?php

$html = ob_get_clean();

$subdir = 'pdf/rincian/';
$dir = $pubdir . '/' . $subdir;

$exists = false;
do {
    $name = $thn.'_Rincian_'.$kode.'.pdf';
    $exists = file_exists($dir . $name);
    if($exists){ unlink($dir . $name); $exists = false; }
} while($exists);

$customPaper = array(0,0,992.13,930.00);
$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml($html);
$dompdf->setPaper($customPaper);
$dompdf->render();

$pdf_gen = $dompdf->output();

if(!file_put_contents($dir . $name, $pdf_gen)){
    header("HTTP/1.0 500 Internal Server Error");
    echo 'Generate PDF Failed';
    exit();
} else {
    $mval->addRincianTHRTA($thn, $subdir . $name, $kode);

    header("Content-Type: application/json");
    echo json_encode([
        'filename' => $name,
        'filepath' => url($subdir . $name)
    ]);
}

?>