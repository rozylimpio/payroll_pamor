<?php

$id = $app->input->post('id');
$pers = $app->input->post('persen');
$persen = 100-$pers;

$perr = new \App\Models\Persen($app);
if($perr->update($id, $persen)) {
    $app->addMessage('persen_list', 'Data Berhasil Diubah');
}
else {
    $app->addError('persen_list', 'Data Gagal Diubah');
}

$redirect = url('a/persen');
header('Location: ' . $redirect);