<?php

$id = $app->input->post('id');
$tgl = dateCreate($app->input->post('tgl'));
$ket = $app->input->post('ket');

$libur = new \App\Models\Libur($app);
if($libur->update($id, $tgl, $ket)) {
    $app->addMessage('libur_list', 'Libur Nasional Berhasil Diubah');
}
else {
    $app->addError('libur_list', 'Libur Nasional Gagal Diubah');
}

$redirect = url('a/libur');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);