<?php

$id = $app->input->post('id');
$pot = $app->input->post('potongan');
$nom = str_replace('.', '', $app->input->post('nominal'));
$ket = $app->input->post('ket');

$potongan = new \App\Models\Potongan($app);
if($potongan->update($id, $pot, $nom, $ket)) {
    $app->addMessage('potongan_list', 'Potongan Berhasil Diubah');
}
else {
    $app->addError('potongan_list', 'Potongan Gagal Diubah');
}

$redirect = url('a/potongan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);