<?php
$id = $app->input->post('id');
$kd = $app->input->post('jam');
$mulai = $app->input->post('mulai');
$akhir = $app->input->post('akhir');
$shift = $app->input->post('shift');
$hari = $app->input->post('hari');
$ke = $app->input->post('ke');
!empty($ke) ? $kett=" ke ".$ke : $kett = '';
$ket = "jam istirahat".$kett;
$tgl = dateCreate($app->input->post('tgl'));

//echo $id.' kd:'.$kd.' mul:'.$mulai.' akh:'.$akhir.' shift:'.$shift.' $hari:'.$hari.' ke:'.$ke.' ket:'.$ket.' tgl:'.$tgl;die();

$jamkerja_istirahat = new \App\Models\Jamkerja($app);
if($jamkerja_istirahat->updateIstirahat($id, $kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl)) {
    $app->addMessage('jamkerja_istirahat_list', 'Jam Istirahat Berhasil Diubah');
}
else {
    $app->addError('jamkerja_istirahat_list', 'Jam Istirahat Gagal Diubah');
}

$redirect = url('a/jamkerja_istirahat');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);