<?php

$id = $app->input->post('id');
$jab = $app->input->post('jabatan');
$grade = $app->input->post('grade');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_prestasi = new \App\Models\Tprestasi($app);
if($t_prestasi->update($id, $jab, $grade, $nom)) {
    $app->addMessage('t_prestasi_list', 'Tunjangan Prestasi Berhasil Diubah');
}
else {
    $app->addError('t_prestasi_list', 'Tunjangan Prestasi Gagal Diubah');
}

$redirect = url('a/t_prestasi');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);