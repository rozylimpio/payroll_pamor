<?php
$id = $app->input->post('id');
$kd = $app->input->post('jam');
$mulai = $app->input->post('mulai');
$akhir = $app->input->post('akhir');
$bagian = $app->input->post('bagian');
$shift = $app->input->post('shift');
$hari = $app->input->post('hari');
$kd == 1 ? $jam = '' : $jam='Shift';
$ket = $bagian." ".$jam." ".$shift." ".namaHari($hari);
$tgl = dateCreate($app->input->post('tgl'));

$jamkerja_aktif = new \App\Models\Jamkerja($app);
if($jamkerja_aktif->updateAktif($id, $kd, $bagian, $shift, $hari, $ket, $mulai, $akhir, $tgl)) {
    $app->addMessage('jamkerja_aktif_list', 'Jam Kerja Berhasil Diubah');
}
else {
    $app->addError('jamkerja_aktif_list', 'Jam Kerja Gagal Diubah');
}

$redirect = url('a/jamkerja_aktif');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);