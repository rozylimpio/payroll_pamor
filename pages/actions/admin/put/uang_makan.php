<?php

$id = $app->input->post('id');
$nom = str_replace('.','',$app->input->post('nominal'));
$date = date('Y-m-d');

$um = new \App\Models\UangMakan($app);
if($um->update($id, $nom, $date)) {
    $app->addMessage('uangmakan_list', 'Uang Makan Berhasil Diubah');
}
else {
    $app->addError('uangmakan_list', 'Uang Makan Gagal Diubah');
}

$redirect = url('a/uang_makan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);