<?php

$id = $app->input->post('id');
$dep = $app->input->post('departemen');

$departemen = new \App\Models\Departemen($app);
if($departemen->update($id, $dep)) {
    $app->addMessage('departemen_list', 'Departemen Berhasil Diubah');
}
else {
    $app->addError('departemen_list', 'Departemen Gagal Diubah');
}

$redirect = url('a/departemen');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);