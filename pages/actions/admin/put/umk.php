<?php

$id = $app->input->post('id');
$umk = str_replace('.', '', $app->input->post('umk'));
$tgl = dateCreate($app->input->post('tgl'));

$mumk = new \App\Models\Umk($app);
if($mumk->update($id, $umk, $tgl)) {
    $app->addMessage('umk_list', 'UMK Berhasil Diubah');
}
else {
    $app->addError('umk_list', 'UMK Gagal Diubah');
}

$redirect = url('a/umk');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);