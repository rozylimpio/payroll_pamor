<?php

$id = $app->input->post('id');
$awal = $app->input->post('awal');
$akhir = $app->input->post('akhir');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_masakerja = new \App\Models\Masakerja($app);
if($t_masakerja->update($id, $awal, $akhir, $nom)) {
    $app->addMessage('t_masakerja_list', 'Berhasil Diubah');
}
else {
    $app->addError('t_masakerja_list', 'Gagal Diubah');
}

$redirect = url('a/t_masakerja');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);