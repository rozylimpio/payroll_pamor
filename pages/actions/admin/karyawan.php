<?php
$data['id'] = $app->input->post('nik');
$data['ktp'] = $app->input->post('ktp');
$data['nama'] = strtoupper($app->input->post('nama'));
$data['tempat'] = strtoupper($app->input->post('tempat'));
$data['tgl_lhr'] = dateCreate($app->input->post('tgl_lhr'));
$data['jk'] = $app->input->post('jk');
$data['alamat'] = strtoupper($app->input->post('alamat'));
$data['status'] = $app->input->post('status');
$data['tgl_in'] = dateCreate($app->input->post('tgl_in'));
!empty($app->input->post('tgl_ang')) ? $data['tgl_ang'] = dateCreate($app->input->post('tgl_ang')) : $data['tgl_ang'] = null;
$data['gaji'] = str_replace('.', '', $app->input->post('gaji'));
$data['bpjskes'] = $app->input->post('bpjskes');
$data['bpjskesplus'] = $app->input->post('bpjskesplus');
$data['kelas'] = $app->input->post('kelas');
$data['bpjsket'] = $app->input->post('bpjsket');
$data['bpjsket_plus'] = $app->input->post('pensiun');
$data['jamkerja'] = $app->input->post('jam');
$data['jabatan'] = $app->input->post('jabatan');
$data['departemen'] = $app->input->post('departemen');
$data['bagian'] = $app->input->post('bagian');
$data['ket_bagian'] = $app->input->post('ket_bag');
$data['jgrup'] = $app->input->post('jgrup');
$data['grup'] = $app->input->post('grup');
$data['libur'] = 0 + $app->input->post('libur');
$data['tgl_m'] = dateCreate($app->input->post('tgl_mulai'));
$app->input->post('setengah') > 0 ? $data['setengah'] = $app->input->post('setengah') : $data['setengah'] = null;
$data['t_jabatan'] = $app->input->post('t_jabatan');
$data['t_keahlian'] = str_replace('.', '', $app->input->post('t_keahlian'));
$data['t_prestasi'] = $app->input->post('tprestasi');
$data['spt'] = $app->input->post('spt');
$data['ket_kar'] = $app->input->post('ket_kar');

$karyawan = new \App\Models\Karyawan($app);
if($insert_id = $karyawan->add($data)) {
    $app->addMessage('karyawan_add', 'Karyawan Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('karyawan_add', 'Karyawan Baru Gagal Disimpan');
}
header('Location: ' . url('a/add_karyawan'));
