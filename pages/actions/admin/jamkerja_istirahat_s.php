<?php

$kd = $app->input->post('jam');
$mulai = $app->input->post('mulai');
$akhir = $app->input->post('akhir');
$shift = $app->input->post('shift');
$hari = 0+$app->input->post('hari');
$ke = $app->input->post('ke');
!empty($ke) ? $kett=" ke ".$ke : $kett = '';
$ket = "jam istirahat".$kett;
$tgl = dateCreate($app->input->post('tgl'));

$jamkerja_istirahat = new \App\Models\Jamkerja($app);
if($insert_id = $jamkerja_istirahat->addIstirahatS($kd, $hari, $shift, $ke, $ket, $mulai, $akhir, $tgl)) {
    $app->addMessage('jamkerja_istirahat', 'Jam Istirahat Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jamkerja_istirahat', 'Jam Istirahat Baru Gagal Disimpan');
}

header('Location: ' . url('a/jamkerja_istirahat_s'));
