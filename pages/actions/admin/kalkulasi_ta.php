<?php
$bln = $app->input->post('bulan');
$thn = $app->input->post('tahun');
$persen = $app->input->post('persen');
$a = 0;


$mkal = new \App\Models\Kalkulasi($app);
$mkal->cekTA($thn);
$d = $mkal->kalTA($bln, $thn, $persen);

for($i=0;$i<count($d);$i++){
	if($insert_id = $mkal->addTA($d[$i], $persen)) {
	  $a += 1;
	}
}

if($a == count($d)){
	if($mkal->addRekapTHRTA($d, 'kalkulasi_ta', 'rekap_ta')){
    	$app->addMessage('kalkulasi_ta', 'Kalkulasi Tali Asih Telah Berhasil Diproses');  
    }else{
    	$app->addError('kalkulasi_ta', 'Data Rekap Tali Asih Gagap Tersimpan');  
	}  
}elseif($a<count($d) && $a>0){
    $app->addError('kalkulasi_ta', 'Terdapat Data Yang Belum Tersimpan');
}else {
    $app->addError('kalkulasi_ta', 'Gagal');
}

header('Location: ' . url('a/kalkulasi_ta'));
?>