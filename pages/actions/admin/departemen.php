<?php

$dep = $app->input->post('departemen');

$depp = new \App\Models\Departemen($app);
if($insert_id = $depp->add($dep)) {
    $app->addMessage('departemen', 'Departemen Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('departemen', 'Departemen Baru Gagal Disimpan');
}

header('Location: ' . url('a/departemen'));
