<?php

$tglm = dateCreate($app->input->post('tglm'));
$tgl = dateCreate($app->input->post('tgl'));
$shift = $app->input->post('shift');
$grup = $app->input->post('grup');

$n = 6;
$tglaw = strtotime($tglm);
$thntgl = explode('-',$tglm);
$tglak = strtotime(date('Y-m-d', strtotime('+1 year', $tglaw)));

$hr = date('N', $tglaw);
$tglp = $tglaw;
$tglp_n = date('N', $tglp);

if($shift == 'Pagi'){
   $shift = array('Pagi', 'Malam', 'Siang', 0);
}elseif($shift == 'Siang'){
   $shift = array('Siang', 'Pagi', 'Malam', 0);
}elseif($shift == 'Malam'){
   $shift = array('Malam', 'Siang', 'Pagi', 0);
}

//$grp = ['A', 'C', 'B'];

$a = 0; $b = 0; $c=0; $d=0;
do {    
   $tgll = $tgl;              
   if($hr==$tglp_n){
      //for($j=0; $j<(1*3);$j++){ 
         //Tanggal berlaku begin              
         $hari[$a]['berlaku'] = $tgll;
         //grup shift begin
         //if($b<3){
            $hari[$a]['grup'] = $grup;
         //}
         //$b==(2) ? $b=0 : $b+=1;
         //grup shift end

         //minggu ke- begin
         $hari[$a]['bulan_ke'] = (int)date('n', $tglaw);
         $hari[$a]['tglaw'] = date('Y-m-d', $tglp);
         $hari[$a]['tglak'] = date('Y-m-d', strtotime('+6 day', $tglp));

         //shift pagi, malam, siang begin
         if ($c<(count($shift)-1)) {
            $hari[$a]['shift'] = $shift[$c];//.'<br>';
         }
         //$c>=count($shift)-(1+1) ? $c=0+$d : $c+=1;
         //shift pagi, malam, siang end
      //}
      $a+=1; $c+=1;
      $c>=count($shift)-(1) ? $c=0 : $c=$c;
   }
   $tglp = strtotime('+1 day', $tglp);
   $tglp_n = date('N', $tglp);
   $tglaw = strtotime('+1 day', $tglaw);
} while ($tglaw<=$tglak);
if($tglm<=$tgl){
   $hari[0]['tglaw'] = $tgl;
}
//print_r($hari);die();
$jadwal = new \App\Models\Jadwal3GrupL($app);
if($insert_id = $jadwal->add($hari)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal_3_grup_l')); 
