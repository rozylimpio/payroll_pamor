<?php
//coba push
$tgl = dateCreate($app->input->post('tgl'));
$shift = $app->input->post('shift');
$grup = $app->input->post('grup');

$n = 6;
$tglaw = strtotime($tgl);
$thntgl = explode('-',$tgl);
$tglak = strtotime(date($thntgl[0].'-12-31'));
$hari = date('N', $tglaw);
$tglp = $tglaw;
$tglp_n = date('N', $tglp);

if($shift == 'Pagi'){
   $shift = array('Pagi', 'Malam', 'Siang', 0);
}

$grp = ['A', 'C', 'B'];

$a = 0; $b = $grup-1; $c=0; $d=0;
do {    
   $tgll = $tgl; 
   for($j=0; $j<(1*3);$j++){              
      //Tanggal berlaku begin              
      $hari[$a][$j]['berlaku'] = $tgll;

      /*if($shift[0] != 'Pagi'){
         if($a==0){
            $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));
         }
      }*/
      //tanggal berlaku end

      //grup shift begin
      if($b<3){
         //$maxdata = strtoupper(chr(96+$b));
         $hari[$a][$j]['grup'] = $grp[$b];//$maxdata;
      }
      //$b==(3) ? $b=1 : $b+=1;
      $b==(2) ? $b=0 : $b+=1;
      //grup shift end

      //minggu ke- begin
      $hari[$a][$j]['minggu'] = (int)date('W', $tglaw);
      $hari[$a][$j]['bulan_ke'] = (int)date('n', $tglaw);
      $hari[$a][$j]['minggu_ke'] = (int)date('W', $tglaw) - (int)date('W', strtotime(date('Y-m-01', $tglaw))) + 1;
      //minggu ke- end

      //shift pagi, malam, siang begin
      if ($c<(count($shift)-1)) {
         $hari[$a][$j]['shift'] = $shift[$c];
      }
      $c>=count($shift)-(1+1) ? $c=0+$d : $c+=1;
      //shift pagi, malam, siang end
   }
   $a+=1; $c+=1;
   $c>=count($shift)-(1) ? $c=0 : $c=$c;
   $tglaw = strtotime('+1 week', $tglaw);
} while ($tglaw<=$tglak);

$jadwal = new \App\Models\Jadwal($app);
if($insert_id = $jadwal->add($hari)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal')); 
