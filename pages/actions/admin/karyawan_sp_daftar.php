<style type="text/css">
  table, tr, td .bo{
    border: none;
    vertical-align: top;
    line-height: 1.15;
  }
</style>
<?php
$surat = new \App\Models\Surat($app);

$nik = $app->input->post('nik');
$data = $surat->getByIdSP($nik);
$tgl = date('Y-m-d');

?>
<div align="center">
    History Surat Peringatan Karyawan:
    <hr>
      <?php
      foreach($data as $d){
        ?>
        <table width="100%" class="bo">
          <tr>
            <td width="15%">No Surat</td>
            <td>&nbsp;: &nbsp; </td>
            <td><?php echo $d['no_surat']; ?></td>
          </tr>
          <tr>
            <td>Tanggal</td>
            <td>&nbsp;: &nbsp; </td>
            <td><?php echo dateFormat(dateResolver($d['tgl'])); ?></td>
          </tr>
          <tr>
            <td>SP ke</td>
            <td>&nbsp;: &nbsp; </td>
            <?php $bul = get_object_vars(date_diff(date_create($d['tgl']), date_create($tgl))); ?>
            <td><?php echo namaBulanRomawi($d['ke'])." (".$bul['m']." bulan yang lalu)"; ?></td>
          </tr>
          <tr>
            <td>Ket</td>
            <td>&nbsp;: &nbsp; </td>
            <td align="justify"><?php echo $d['ket']; ?></td>
          </tr>
        </table>
        <a href="<?php echo url().$d['file'];?>" target="_blank" class="btn btn-default pull-right btn-xs">Lihat SP</a>

        <button title="Hapus" type="button" data-url="<?php echo url('a/surat?_method=delete&id=' . $d['id'] . '&file=' . $d['file'] . '&redirect=a/karyawan_sp');?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger pull-left btn-xs" style="margin-left: 5px"><i class="fa fa-trash"></i></button>
        <br>
        <hr>
        <?php
      }
      ?>
</div>