<?php

$id = $app->input->post('id');
$page = $app->input->post('page');
$awal = $app->input->postDate('awal');
$akhir = $app->input->postDate('akhir');

$departemen = $app->input->post('departemen');
$bagian = $app->input->post('bagian');
//$ket_bag = $app->input->post('ket_bag');
$jgrup = $app->input->post('jgrup');
$grup = null.$app->input->post('grup');
$libur = null.$app->input->post('libur');
$jamkerja = $app->input->post('jamkerja');

$mtki = new \App\Models\Karyawan($app);

$queries = [
    'page' => $page,
    'departemen' => $departemen,
    'bagi' => $bagian,
    'bagian' => $bagian,
    //'ket_bag' => $ket_bag,
    'jgrup' => $jgrup,
    'grup' => $grup,
    'libur' => $libur,
    'jam' => $jamkerja,
    'tinjau' => ''
];

if(empty($id)){
	$app->addError('hamil_list', 'Maaf pilih Karyawan terlebih dahulu');
	header('Location: ' . url('a/cutihamil') .'?'. queryString($queries));
}else{
	if($mtki->hamilAdd($id, $awal, $akhir)) {
	    $app->addMessage('hamil_list', 'Proses Cuti Hamil Berhasil');
	    header('Location: ' . url('a/cutihamil') .'?'. queryString($queries));
	}
	else {
	    $app->addError('hamil_list', 'Proses Cuti Hamil Gagal');
	    header('Location: ' . url('a/cutihamil') .'?'. queryString($queries));
	}
}