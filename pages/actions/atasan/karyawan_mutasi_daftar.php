<?php
$mkar = new \App\Models\Karyawan($app);

$nik = $app->input->post('nik');
$bln = $app->input->post('bln');

$list = $mkar->cekMutasiDetail($bln, $nik);

?>
<div class="table-responsive" align="center">
  <hr>
  <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableDet">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>Nama</th>
        <th>Bagian</th>
        <th>Status</th>
        <th>Jabatan</th>
        <th>Tunj Jabatan</th>
        <th>Tunj Keahlian</th>
        <th>Tunj Prestasi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($list as $index => $kar) { ?>
      <tr>
        <td><?php echo $index+1;?></td>
        <td><?php echo "(".$kar['id'].")";?>
            <br><?php echo $kar['nama'];?>
            <br><?php echo $kar['ktp'];?>
        </td>
        <td>
            <?php 
            $tgl = date("Y-m-d");
            $karmut = $mkar->getDataMutasi($kar['id'], $tgl);
            if(empty($karmut)){
              echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
              echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
              echo !empty($kar['libur']) ? $kar['libur'] : "";
              echo '<br><small>'.$kar['bagian'].'</small>';
            }else{
              echo $karmut['nm_departemen']."<br>".$karmut['nm_bagian'];
              echo !empty($karmut['grup']) ? " - ".$karmut['grup'] : "";
              echo !empty($karmut['libur']) ? $karmut['libur'] : "";
              echo '<br><br>';
              echo '<i><small style="font-size: 9px">Berlaku : '.dateResolver($karmut['berlaku']).'<br>';
              echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
              echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
              echo !empty($kar['libur']) ? $kar['libur'] : "";
              echo '</i></small>';
            }
            ?>    
        </td>
        <td>Karyawan <?php echo $kar['nm_status'];?>
            <br>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
            <br><br><b>
            Sebelum Tgl : <?php echo dateResolver($kar['tgl_update']);?><br>
            <?php
            if(empty($karmut)){
              echo 'Gaji Pokok : '.indo_number($kar['gaji']);
            }else{
              echo 'Gaji Pokok : '.indo_number($karmut['gaji']);
            }
            ?>
        	</b>
        </td>
        <td><?php echo $kar['nm_jabatan'];?></td>
        <td><?php echo indo_number($kar['nom_jabatan']);?></td>
        <td><?php echo indo_number($kar['nom_keahlian']);?></td>
        <td><?php echo indo_number($kar['nom_prestasi']);?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>