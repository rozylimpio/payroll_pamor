<?php
$data['nik'] = $app->input->post('nik');
$data['ktp'] = $app->input->post('ktp');
$data['nama'] = strtoupper($app->input->post('nama'));
$data['tempat'] = strtoupper($app->input->post('tempat'));
$data['tgl_lhr'] = dateCreate($app->input->post('tgl_lhr'));
$data['alamat'] = strtoupper($app->input->post('alamat'));
$data['pendidikan'] = strtoupper($app->input->post('pendidikan'));
$data['bpjskes'] = $app->input->post('bpjskes');
$data['bpjsket'] = $app->input->post('bpjsket');
$data['tgl_in'] = dateCreate($app->input->post('tgl_in'));
$data['sisa_cuti'] = $app->input->post('sisa_cuti');
$data['bagian'] = $app->input->post('bagian');
$data['jabatan'] = $app->input->post('jabatan');
$data['status_pajak'] = $app->input->post('pajak');

$staff = new \App\Models\Staff($app);
if($insert_id = $staff->update($data)) {
    $app->addMessage('staff', 'Staff Baru Telah Berhasil Diubah');
}
else {
    $app->addError('staff', 'Staff Baru Gagal Diubah');
}
header('Location: ' . url('i/staff'));
