<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$mkar = new \App\Models\Resign($app);
$d = $mkar->get();

require $sysdir . '/excelstyle.php';

$filename = 'Daftar_Karyawan_Resign';
$settitle = '';
$judul = 'DAFTAR KARYAWAN RESIGN';

for ($i=0; $i < count($d) ; $i++) {
	$rincian_data[] = [
		$i+1,
		$d[$i]['id'],
		$d[$i]['nama']."\n".$d[$i]['nm_bagian'],
		$d[$i]['grup'].' '.$d[$i]['libur']."\n".$d[$i]['nm_jamkerja'].' '.$d[$i]['bagian'],
		dateFormat(dateResolver($d[$i]['tgl_in'])),
		dateFormat(dateResolver($d[$i]['tgl_update'])),
		'Ket : '.$d[$i]['bpjs_ket']."\nKes : ".$d[$i]['bpjs_kes']."\nKelas : ".$d[$i]['kelas']
	];
}

$rowcount = count($rincian_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$objWorkSheet = $spreadsheet->getActiveSheet()->setTitle('SP');

$objWorkSheet->mergeCells('A1:G1');
$objWorkSheet->mergeCells('A2:G2');
$objWorkSheet->mergeCells('A3:G3');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);

// header
$objWorkSheet->mergeCells('A4:A5');
$objWorkSheet->mergeCells('B4:B5');
$objWorkSheet->mergeCells('C4:C5');
$objWorkSheet->mergeCells('D4:D5');
$objWorkSheet->mergeCells('E4:E5');
$objWorkSheet->mergeCells('F4:F5');
$objWorkSheet->mergeCells('G4:G5');

$objWorkSheet->getStyle('A4:G5')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'No',
	'No Induk',
	'Nama Karyawan',
	'Grup',
	'Tgl Masuk',
	'Tgl Resign',
	'BPJS'
], null, 'A4');

// data
$objWorkSheet->getStyle('A6:A'.(6+$rowcount-1))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('B6:B'.(6+$rowcount-1))->applyFromArray($contentStyleC);
$objWorkSheet->getStyle('C6:D'.(6+$rowcount-1))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('E6:F'.(6+$rowcount-1))->applyFromArray($contentStyleC);
$objWorkSheet->getStyle('G6:G'.(6+$rowcount-1))->applyFromArray($contentStyle);
$objWorkSheet->fromArray($rincian_data, null, 'A6');

//
//
$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
$objWorkSheet->getColumnDimension('C')->setWidth(30);
$objWorkSheet->getColumnDimension('D')->setAutoSize(true);
$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
$objWorkSheet->getColumnDimension('G')->setAutoSize(true);


// ERROR
//die();
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>