<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$mstaf = new \App\Models\Staff($app);
$d = $mstaf->get();

require $sysdir . '/excelstyle.php';

$filename = 'Daftar_Staff_Perusahaan';
$settitle = '';
$judul = 'DAFTAR STAFF PAMOR';

for ($i=0; $i < count($d) ; $i++) {
	$rincian_data[] = [
		$i+1,
		$d[$i]['id'],
		$d[$i]['nama'],
		$d[$i]['tempat'].", ".dateResolver($d[$i]['tgl_lahir']),
		$d[$i]['jabatan']."\n".$d[$i]['bagian'],
		dateResolver($d[$i]['tgl_in']),
		$d[$i]['pendidikan'],
		$d[$i]['pajak'],
		"Ket : ".$d[$i]['bpjs_ket']."\n"."Kes : ".$d[$i]['bpjs_kes'],
		$d[$i]['sisa_cuti'],
		$d[$i]['alamat']	
	];
}

$rowcount = count($rincian_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$objWorkSheet = $spreadsheet->getActiveSheet()->setTitle('SP');

$objWorkSheet->mergeCells('A1:K1');
$objWorkSheet->mergeCells('A2:K2');
$objWorkSheet->mergeCells('A3:K3');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);

// header
$objWorkSheet->mergeCells('A4:A5');
$objWorkSheet->mergeCells('B4:B5');
$objWorkSheet->mergeCells('C4:C5');
$objWorkSheet->mergeCells('D4:D5');
$objWorkSheet->mergeCells('E4:E5');
$objWorkSheet->mergeCells('F4:F5');
$objWorkSheet->mergeCells('G4:G5');
$objWorkSheet->mergeCells('H4:H5');
$objWorkSheet->mergeCells('I4:I5');
$objWorkSheet->mergeCells('J4:J5');
$objWorkSheet->mergeCells('K4:K5');

$objWorkSheet->getStyle('A4:K5')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'No',
	'No Induk',
	'Nama Karyawan',
	'TTL',
	'Jabatan',
	'Tgl Masuk',
	'Pendidikan',
	'Status Pajak',
	'BPJS',
	'Cuti',
	'Alamat'
], null, 'A4');

// data
$objWorkSheet->getStyle('A6:K'.(6+$rowcount-1))->applyFromArray($contentStyle);
$objWorkSheet->fromArray($rincian_data, null, 'A6');

//
//
$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
$objWorkSheet->getColumnDimension('C')->setWidth(30);
$objWorkSheet->getColumnDimension('D')->setAutoSize(true);
$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
$objWorkSheet->getColumnDimension('J')->setAutoSize(true);
$objWorkSheet->getColumnDimension('K')->setAutoSize(true);


// ERROR
//die();
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>