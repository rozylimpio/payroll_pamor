<?php

$dep = $app->input->post('departemen');
$bag = $app->input->post('bagian');

$depp = new \App\Models\Bagian($app);
if($insert_id = $depp->add($dep, $bag)) {
    $app->addMessage('bagian', 'Bagian Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('bagian', 'Bagian Baru Gagal Disimpan');
}

header('Location: ' . url('c/bagian'));