<?php
$bln = $app->input->post('bulan');

$mkop = new \App\Models\Potongan($app);

$judul = 'Potongan_Koperasi_Bulan_-'.$bln.'-_';


if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('koperasi', 'File harus .csv');
        header('Location: ' . url('a/koperasi'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    //menampilkan hasil upload
    //for($j=0;$j<count($data);$j++){
    //    print_r($data[$j]);
    //    echo "<br>";
    //}
    $tgl = dateCreate(str_replace('/', '-', $data[0][2]));
    $blnn = date('n', strtotime($tgl));

    if($blnn != $bln){
      $app->addError('koperasi', 'Maaf Bulan Potongan Koperasi Tidak Sama');
      unlink(substr($file->fullname, 1));
      header('Location: ' . url('a/koperasi'));
      exit();
    }

    //print_r($data);die();

    for($j=0;$j<count($data);$j++){
      $datar['nik'] = $data[$j][0];
      $datar['nominal'] = $data[$j][1];
      $datar['tgl'] = $tgl;

      $hasil = $mkop->addKoperasi($datar);
      if($hasil){
          $a += 1;
      }
    }

    if($a == count($data)){
        $app->addMessage('koperasi', 'Berhasil Upload');
        header('Location: ' . url('a/koperasi'));
    }else{
        $app->addError('koperasi', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/koperasi'));    
    }
    
} else {
    $app->addError('koperasi', 'File Upload Gagal');
    header('Location: ' . url('c/koperasi'));
}
?>