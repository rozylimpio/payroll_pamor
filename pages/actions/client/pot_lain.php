<?php
$bln = $app->input->post('bulan');

$mkop = new \App\Models\Potongan($app);

$judul = 'Potongan_Lain_Bulan_-'.$bln.'-_';


if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('pot_lain', 'File harus .csv');
        header('Location: ' . url('c/pot_lain'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    //menampilkan hasil upload
    //for($j=0;$j<count($data);$j++){
    //    print_r($data[$j]);
    //    echo "<br>";
    //}
    $tgl = dateCreate(str_replace('/', '-', $data[0][2]));
    $blnn = date('n', strtotime($tgl));

    if($blnn != $bln){
      $app->addError('pot_lain', 'Maaf Bulan Potongan Tidak Sama');
      unlink(substr($file->fullname, 1));
      header('Location: ' . url('c/pot_lain'));
      exit();
    }

    //print_r($data);die();

    for($j=0;$j<count($data);$j++){
      $datar['nik'] = $data[$j][0];
      $datar['nominal'] = $data[$j][1];
      $datar['tgl'] = $tgl;

      $hasil = $mkop->addLain($datar);
      if($hasil){
          $a += 1;
      }
    }

    if($a == count($data)){
        $app->addMessage('pot_lain', 'Berhasil Upload');
        header('Location: ' . url('c/pot_lain'));
    }else{
        $app->addError('pot_lain', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('c/pot_lain'));    
    }
    
} else {
    $app->addError('pot_lain', 'File Upload Gagal');
    header('Location: ' . url('c/pot_lain'));
}
?>