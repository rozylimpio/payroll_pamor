<?php

$jab = $app->input->post('jabatan');
$jam = $app->input->post('jam');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_jabatan = new \App\Models\Tjabatan($app);
if($insert_id = $t_jabatan->add($jab, $jam, $nom)) {
    $app->addMessage('t_jabatan', 'Tunjangan Jabatan Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('t_jabatan', 'Tunjangan jabatan Baru Gagal Disimpan');
}

header('Location: ' . url('c/t_jabatan'));