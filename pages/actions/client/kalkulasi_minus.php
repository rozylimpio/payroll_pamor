<?php

$data['id'] = $app->input->post('id');
$data['tgl'] = $app->input->post('tgl');
$data['nik'] = $app->input->post('nik');
$data['bulan'] = $app->input->post('bulan');
$data['kop'] = $app->input->post('n_kop');
$data['bayar'] = $app->input->post('bayar_hide');
$data['sisa'] = $app->input->post('sisa');

//var_dump($data);echo"<br>";die();

$akun = new \App\Models\Potongan($app);
if($akun->updateKoperasiMinus($data)) {
    $app->addMessage('kalkulasi_minus', 'Potongan Koperasi Berhasil Diperbaharui. <b>SILAHKAN KALKULASI ULANG</b>');
}
else {
    $app->addError('kalkulasi_minus', 'Potongan Koperasi Gagal Diperbaharui');
}

header('Location: ' . url('c/kalkulasi_minus?bulan='.$data['bulan'].'&tinjau='));