<?php

$pot = $app->input->post('potongan');
$nom = str_replace('.', '', $app->input->post('nominal'));
$ket = $app->input->post('ket');

$potongan = new \App\Models\Potongan($app);
if($insert_id = $potongan->add($pot, $nom, $ket)) {
    $app->addMessage('potongan', 'Potongan Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('potongan', 'Potongan Baru Gagal Disimpan');
}

header('Location: ' . url('c/potongan'));
