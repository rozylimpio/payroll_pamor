<?php

$id = $app->input->get('id');

$umkp = new \App\Models\Umk($app);
if($umkp->delete($id)) {
    $app->addMessage('umk_list', 'UMK Berhasil Dihapus');
}
else {
    $app->addError('umk_list', 'UMK Gagal Dihapus');
}

$redirect = url('c/umk');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);