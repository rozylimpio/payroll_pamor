<?php

$id = $app->input->get('id');

$premi = new \App\Models\Premi($app);
if($premi->delete($id)) {
    $app->addMessage('premi_list', 'Premi Absen Shift Berhasil Dihapus');
}
else {
    $app->addError('premi_list', 'Premi Absen Shift Gagal Dihapus');
}

$redirect = url('c/premi');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);