<?php

$id = $app->input->get('id');

$depp = new \App\Models\Bagian($app);
if($depp->delete($id)) {
    $app->addMessage('bagian_list', 'Bagian Berhasil Dihapus');
}
else {
    $app->addError('bagian_list', 'Bagian Gagal Dihapus');
}

$redirect = url('c/bagian');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);