<?php

$id = $app->input->get('id');

$depp = new \App\Models\Masakerja($app);
if($depp->delete($id)) {
    $app->addMessage('t_masakerja_list', 'Berhasil Dihapus');
}
else {
    $app->addError('t_masakerja_list', 'Gagal Dihapus');
}

$redirect = url('c/t_masakerja');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);