<?php

$id = $app->input->get('id');

$insentif = new \App\Models\Insentif($app);
if($insentif->delete($id)) {
    $app->addMessage('insentif_list', 'Insentif Absen Berhasil Dihapus');
}
else {
    $app->addError('insentif_list', 'Insentif Absen Gagal Dihapus');
}

$redirect = url('c/insentif');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);