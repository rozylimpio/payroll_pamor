<?php

$id = $app->input->post('id');
$ins = $app->input->post('insentif');
$nom = str_replace('.', '', $app->input->post('nominal'));

$insentif = new \App\Models\Insentif($app);
if($insentif->update($id, $ins, $nom, $ket)) {
    $app->addMessage('insentif_list', 'Insentif Absen Berhasil Diubah');
}
else {
    $app->addError('insentif_list', 'Insentif Gagal Diubah');
}

$redirect = url('c/insentif');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);