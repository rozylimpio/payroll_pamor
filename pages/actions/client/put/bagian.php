<?php

$id = $app->input->post('id');
$dep = $app->input->post('departemen');
$bag = $app->input->post('bagian');

$bagian = new \App\Models\Bagian($app);
if($bagian->update($id, $dep, $bag)) {
    $app->addMessage('bagian_list', 'Bagian Berhasil Diubah');
}
else {
    $app->addError('bagian_list', 'Bagian Gagal Diubah');
}

$redirect = url('c/bagian');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);