<?php

$data['id'] = $app->input->post('id');
$bln = date('Y-'.$app->input->post('bln').'-01');
$data['tgl'] = date('Y-m-d', strtotime($bln));
$data['nik'] = $app->input->post('nik');
$data['nominal'] = str_replace('.', '', $app->input->post('nom'));


$koperasi = new \App\Models\Potongan($app);
if($koperasi->updateKoperasi($data)) {
    $app->addMessage('koperasi_list', 'Potongan Berhasil Diubah');
}
else {
    $app->addError('koperasi_list', 'Potongan Gagal Diubah');
}

$redirect = url('c/koperasi');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);