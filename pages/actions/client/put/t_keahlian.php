<?php

$id = $app->input->post('id');
$dep = $app->input->post('departemen');
$jam = $app->input->post('jam');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_keahlian = new \App\Models\Tkeahlian($app);
if($t_keahlian->update($id, $dep, $jam, $nom)) {
    $app->addMessage('t_keahlian_list', 'Tunjangan Keahlian Berhasil Diubah');
}
else {
    $app->addError('t_keahlian_list', 'Tunjangan Keahlian Gagal Diubah');
}

$redirect = url('c/t_keahlian');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);