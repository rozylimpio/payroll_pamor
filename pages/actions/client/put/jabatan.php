<?php

$id = $app->input->post('id');
$jab = $app->input->post('jabatan');
$ket = $app->input->post('ket');

$jabatan = new \App\Models\Jabatan($app);
if($jabatan->update($id, $jab, $ket)) {
    $app->addMessage('jabatan_list', 'Jabatan Pekerjaan Berhasil Diubah');
}
else {
    $app->addError('jabatan_list', 'Jabatan Gagal Diubah');
}

$redirect = url('c/jabatan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);