<?php
$bln = date('Y-'.$app->input->post('bln').'-01');
$data['tgl'] = date('Y-m-d', strtotime($bln));
$data['nik'] = $app->input->post('nik');
$data['nominal'] = str_replace('.', '', $app->input->post('nom'));

$potongan = new \App\Models\Potongan($app);
if($insert_id = $potongan->addKoperasi($data)) {
    $app->addMessage('koperasi_list', 'Potongan Koperasi Telah Berhasil Disimpan');
}
else {
    $app->addError('koperasi_list', 'Potongan Koperasi Gagal Disimpan');
}
header('Location: ' . url('c/koperasi'));
