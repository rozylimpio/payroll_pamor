<?php

$jab = $app->input->post('jabatan');
$grade = $app->input->post('grade');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_prestasi = new \App\Models\Tprestasi($app);
if($insert_id = $t_prestasi->add($jab, $grade, $nom)) {
    $app->addMessage('t_prestasi', 'Tunjangan Prestasi Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('t_prestasi', 'Tunjangan Prestasi Baru Gagal Disimpan');
}

header('Location: ' . url('c/t_prestasi'));