<?php 
require '_base_head.php';

$tgll = date('Y-m-d', strtotime('-6 month', strtotime(date('Y-m-d'))));

$mkal = new \App\Models\Kalkulasi($app);
$ys = $mkal->getTahun();

$mab = new \App\Models\Absensi($app);
$deps = $mab->getDepartemen($tgll);
$bags = $mab->getBagian($tgll);
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Gaji</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php foreach($ys as $y) { ?>
                    <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
                      <?php echo $y['tahun']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                  <select name="departemen" id="departemen" class="form-control select2_single" style="cursor:pointer">
                    <option value=""></option>
                    <?php foreach($deps as $dep) { ?>
                    <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
                      <?php echo $dep['nm_departemen']?></option>
                    <?php } ?>
                  </select>  
                </div>         
                <div class="col-md-3 col-sm-5 col-xs-12">
                  <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
                  <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                  </select>
                </div>
                <div class="form-group col-md-2 col-sm-2 col-xs-12">
                  <button name="tinjau" type="submit" class="btn btn-info">
                    <i class="glyphicon glyphicon-search"></i>
                    &nbsp;view&nbsp;
                  </button>
                </div>
              </div>
              
            </form>

            
            <?php 
            $defmsg_category = 'kalkulasi';
            require '../pages/defmsg.php'; 
            ?>
              <?php
              if(isset($_GET['tinjau'])){
                $tahun = $app->input->get('tahun');
                $bulan = $app->input->get('bulan');
                $departemen = $app->input->get('departemen');
                $bagian = $app->input->get('bagian');

                $d = $mkal->getFullData($tahun, $bulan, $departemen, $bagian);
                ?>
                <br><br><br>
                <div align="center">
                  <b>
                    DAFTAR PERINCIAN GAJI DAN LEMBUR<br>
                    <?php echo !empty($bagian) && !empty($departemen)?'Bagian : '.$d[0]['nm_bagian'].'<br>':'' ?>
                    <?php echo empty($bagian) && !empty($departemen)?'Departemen : '.$d[0]['nm_departemen'].'<br>':'' ?>
                  </b>
                    <?php echo !empty($bulan)?'Bulan : '.namaBulan($bulan).' - ':'Tahun ' ?> <?php echo $tahun ?>
                </div>
                <br><br>
                <table id="myTablex" class="display table-bordered table-hover table-striped hoverr" style="width:100%; font-size: 11px;">
                  <thead align="center">
                    <tr>
                      <th rowspan="2">No</th>
                      <?php
                      if(empty($bulan)){
                      ?>
                      <th rowspan="2">Bulan</th>
                      <?php  
                      }
                      ?>
                      <th rowspan="2">No Induk</th>
                      <th rowspan="2">Nama Karyawan</th>
                      <th rowspan="2">Tanggal Masuk</th>
                      <th rowspan="2">Gaji Pokok</th>
                      <th rowspan="2">Masa Kerja</th>
                      <th rowspan="2">Insentif</th>
                      <th colspan="3">Lembur</th>
                      <th rowspan="2">Premi Shift</th>
                      <th colspan="3">Tunjangan</th>
                      <th rowspan="2">Total Gaji Bruto</th>
                      <th colspan="7">Potongan</th>
                      <th rowspan="2">Total Gaji Bersih</th>
                      <th colspan="5">Absensi</th>
                    </tr>
                    <tr>
                      <th>Jam</th>
                      <th>Tarif</th>
                      <th>Jumlah</th>
                      <th>Jabatan</th>
                      <th>Keahlian</th>
                      <th>IPK</th>
                      <th>Astek</th>
                      <th>S.P.T</th>
                      <th>KSPN</th>
                      <th>Lainnya</th>
                      <th>Absensi</th>
                      <th>P.S.W</th>
                      <th>Koperasi</th>
                      <th>A</th>
                      <th>I</th>
                      <th>S</th>
                      <th>P</th>
                      <th>R</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $gp = 0;
                      $mk = 0;
                      $ins = 0;
                      $jm = 0;
                      $tar = 0;
                      $jml = 0;
                      $ps = 0;
                      $tj = 0;
                      $tk = 0;
                      $tip = 0;
                      $tgk = 0;
                      $ast = 0;
                      $spt = 0;
                      $kspn = 0;
                      $lain = 0;
                      $la = [];
                      $abs = 0;
                      $psw = 0;
                      $kop = 0;
                      $tgb = 0;
                      $a = 0;
                      $ij = 0;
                      $s = 0;
                      $p = 0;
                      $r = 0;

                    for ($i=0; $i < count($d) ; $i++) { 
                    ?>
                    <tr>
                      <td><?php echo $i+1; ?></td>
                      <?php
                      if(empty($bulan)){
                      ?>
                      <td><?php echo namaBulan($d[$i]['bulan']); ?></th>
                      <?php  
                      }
                      ?>
                      <td>
                        <?php 
                          echo $d[$i]['nik']; 
                          $bulann = $bulan - 1;
                          if($bulann == 0){
                            $bulann = 12;
                            $thnn = $tahun - 1;
                          }else{
                            $thnn = $tahun;
                          }
                          $start_date = new Datetime("$thnn-$bulann-21");
                          $end_date = new Datetime("$tahun-$bulan-20");
                          $interval = $start_date->diff($end_date);

                          echo $d[$i]['rumah']==$interval->days ? '<br><i><small>dirumahkan</small></i>' : ''; 
                        ?>
                      </td>
                      <td><?php echo $d[$i]['nama']; ?></td>
                      <td>
                        <?php 
                        $d[$i]['tgl_angkat']=='0000-00-00' || empty($d[$i]['tgl_angkat']) ? $tgl_in = $d[$i]['tgl_in'] : $tgl_in = $d[$i]['tgl_angkat'];
                        echo dateResolver($tgl_in);
                        if(empty($departemen) && empty($bagian)){
                          echo '<br>';
                          echo $d[$i]['nm_departemen'].' - '.$d[$i]['nm_bagian'];
                        }elseif(!empty($departemen) && empty($bagian)){
                          echo '<br>';
                          echo $d[$i]['nm_bagian'];
                        }
                        ?>
                      </td>
                      <td><?php echo idr($d[$i]['gaji']); $gp = $gp + $d[$i]['gaji']; ?></td>
                      <td><?php echo idr($d[$i]['nt_masakerja']); $mk = $mk + $d[$i]['nt_masakerja']; ?></td>
                      <td><?php echo idr($d[$i]['nt_insentif']); $ins = $ins + $d[$i]['nt_insentif']; ?></td>
                      <td>
                        <?php 
                        $ttlj = carijam($d[$i]['t_lembur']);
                        $ttlm = carimenit($d[$i]['t_lembur'], $ttlj);
                        echo $ttlj." jam ".$ttlm." menit";
                        $jm = $jm + $d[$i]['t_lembur'];
                        ?>    
                      </td>
                      <td><?php echo idr($d[$i]['tarif_l']); $tar = $tar + $d[$i]['tarif_l']; ?></td>
                      <td><?php echo idr($d[$i]['nt_lembur']); $jml = $jml + $d[$i]['nt_lembur']; ?></td>
                      <td><?php echo idr($d[$i]['nt_premi']); $ps = $ps + $d[$i]['nt_premi'];?></td>
                      <td><?php echo idr($d[$i]['nt_jabatan']); $tj = $tj + $d[$i]['nt_jabatan']; ?></td>
                      <td><?php echo idr($d[$i]['nt_keahlian']); $tk = $tk + $d[$i]['nt_keahlian']; ?></td>
                      <td><?php echo idr($d[$i]['nt_prestasi']); $tip = $tip + $d[$i]['nt_prestasi']; ?></td>
                      <td><?php echo idr($d[$i]['gaji_kotor']); $tgk = $tgk + $d[$i]['gaji_kotor'] ?></td>
                      <td><?php echo idr($d[$i]['n_bpjs_ket'] + $d[$i]['n_bpjs_kes']); 
                                $ast = $ast + $d[$i]['n_bpjs_ket'] + $d[$i]['n_bpjs_kes'];
                          ?>
                      </td>
                      <td><?php echo idr($d[$i]['n_spt']); $spt = $spt + $d[$i]['n_spt']; ?></td>
                      <td><?php echo idr($d[$i]['n_kspn']); $kspn = $kspn + $d[$i]['n_kspn']; ?></td>
                      <td><?php echo idr($d[$i]['tn_lain']+$d[$i]['n_rumah']); $lain = $lain + $d[$i]['tn_lain'] + $d[$i]['n_rumah']; ?></td>
                      <td><?php echo idr($d[$i]['n_ai']); $abs = $abs + $d[$i]['n_ai']; ?></td>
                      <td><?php echo idr($d[$i]['n_psw']); $psw = $psw + $d[$i]['n_psw']; ?></td>
                      <td><?php echo idr($d[$i]['n_koperasi']); $kop = $kop + $d[$i]['n_koperasi']; ?></td>
                      <td><?php echo idr(pembulatan($d[$i]['total_gaji'])); $tgb = $tgb + $d[$i]['total_gaji']; ?></td>
                      <td><?php echo $d[$i]['alpha']; $a = $a + $d[$i]['alpha']; ?></td>
                      <td><?php echo $d[$i]['ijin']; $ij = $ij + $d[$i]['ijin']; ?></td>
                      <td><?php echo $d[$i]['sdr']; $s = $s + $d[$i]['sdr']; ?></td>
                      <td><?php echo $d[$i]['pijin']; $p = $p + $d[$i]['pijin']; ?></td>
                      <td><?php echo $d[$i]['rumah']; $r = $r + $d[$i]['rumah']; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  <tfoot align="center">
                    <th colspan="<?php echo empty($bulan) ? '5' : '4'  ?>">T O T A L</th>
                    <th><?php echo idr($gp); ?></th>
                    <th><?php echo idr($mk); ?></th>
                    <th><?php echo idr($ins); ?></th>
                    <th colspan="2">&nbsp;</th>
                    <th><?php echo idr($jml); ?></th>
                    <th><?php echo idr($ps); ?></th>
                    <th><?php echo idr($tj); ?></th>
                    <th><?php echo idr($tk); ?></th>
                    <th><?php echo idr($tip); ?></th>
                    <th><?php echo idr($tgk); ?></th>
                    <th><?php echo idr($ast); ?></th>
                    <th><?php echo idr($spt); ?></th>
                    <th><?php echo idr($kspn); ?></th>
                    <th><?php echo idr($lain); ?></th>
                    <th><?php echo idr($abs); ?></th>
                    <th><?php echo idr($psw); ?></th>
                    <th><?php echo idr($kop); ?></th>
                    <th><?php echo idr($tgb); ?></th>
                    <th><?php echo $a; ?></th>
                    <th><?php echo $ij; ?></th>
                    <th><?php echo $s; ?></th>
                    <th><?php echo $p; ?></th>
                    <th><?php echo $r; ?></th>
                  </tfoot>
                </table>
            <?php
            }
            ?>
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<script>
$(document).ready(function() {
  var emptyOption = '<option value=""></option>';
  var bags = JSON.parse('<?php echo json_encode($bags);?>');

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

  $("#departemen.select2_single").select2({
    placeholder: "Pilih Departemen",
    allowClear: true
  });


  
  $("#departemen.select2_single").select2({
      placeholder: "Pilih Departemen",
      allowClear: true
  }).on('change', function(e) {
    var kd_departemen = parseInt(e.currentTarget.value);
    var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
    
    $('#bagian').html(emptyOption);
    $(list_bagian).each(function(i, item) {
      $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
    });

    $("#bagian.select2_single").select2('destroy').select2({
      placeholder: "Pilih Bagian",
      allowClear: true,
      disabled: false
    });
  });



  $("#bagian.select2_single").select2({
    placeholder: "Pilih Bagian",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  }).show(function(e){
    var bagi = parseInt(document.getElementById('bagi').value);
    var ed = document.getElementById('departemen');
    var kd_departemen = parseInt(ed.options[ed.selectedIndex].value);
    var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
    $('#bagian').html(emptyOption);
    $(list_bagian).each(function(i, item) {
      $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
    });
  });


  var table = $('#myTablex').DataTable({
        scrollX:        true
    });
  $('#myTablex tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('row_selected') ) {
          $(this).removeClass('row_selected');
      }
      else {
          table.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
      }
  } );



});
</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>