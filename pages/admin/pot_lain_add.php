<?php 
require '_base_head.php';
$mpot = new \App\Models\Potongan($app);
$edit = false;
$blnn = null;
if($app->input->get('edit')) {
  $edit = $mpot->getLainById($app->input->get('edit'));
  $tgl = dateCreate(str_replace('/', '-', $edit['tgl']));
  $blnn = date('n', strtotime($tgl));
}
$redirect = url('a/pot_lain');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Potongan Lain</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/pot_lain_add' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'pot_lain';
        require '../pages/defmsg.php';
        
        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="tgl">
            Bulan
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input type="hidden" name="bln" value="<?php echo $edit ? $blnn : date('n', strtotime(date('Y-m-d')));?>">
            <input type="text" readonly class="form-control" value="<?php echo $edit ? namaBulan($blnn) : namaBulan(date('n', strtotime(date('Y-m-d'))));?>" >
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            NIK
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="nik" id="nik" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nik'] : '' ;?>" placeholder="Nomor Induk Karyawan" <?php echo $edit ? 'readonly' : '' ;?>>
          </div>
        </div> 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Nominal Potongan
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="nom" id="nom" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nominal'] : '' ;?>" placeholder="Nominal Potongan">
          </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->                   

      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();

    $("#bulan.select2_single").select2({
        placeholder: "Pilih Bulan",
        allowClear: true
    });

    var rupiah = document.getElementById("nom");
    rupiah.addEventListener("keyup", function(e) {
      // tambahkan 'Rp.' pada saat form di ketik
      // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
      rupiah.value = formatRupiah(this.value, "");
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
      var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
      }

      rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
      return prefix == undefined ? rupiah : rupiah ? "" + rupiah : "";
    }
  });
</script>

<?php require '_base_foot.php';?>