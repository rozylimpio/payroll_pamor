<?php 
require '_base_head.php';
$mum = new \App\Models\UangMakan($app);
$ums = $mum->getAll();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mum->getById($app->input->get('edit'));
}

$redirect = url('a/uang_makan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Uang Makan</h2>
            <div class="clearfix"></div>
            <input type="hidden" id="var" value="<?php echo $edit ? 'Ubah' : 'Input' ;?>">
        </div>
      <div class="x_content">
        <form action="<?php echo url('a/uang_makan' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'uangmakan';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="nominal">
            nominal
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="nominal" id="nominal" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nominal'] : '' ;?>">
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
          </div>
        </div>
        <?php } ?>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
          <?php
            $defmsg_category = 'uangmakan_list';
            require '../pages/defmsg.php';
          ?>
          <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th>Nominal Uang Makan</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($ums as $index => $um) { ?>
                    <tr>
                      <td><?php echo indo_number($um['nominal']);?></td>
                      <td>
                        <a href="<?php echo url('a/uang_makan?edit=' . $um['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {

    var v = document.getElementById("var");
    if(v=='ubah'){
      var rupiah = document.getElementById("nominal");
      rupiah.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "");
      });
    }
    

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
      var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
      }

      rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
      return prefix == undefined ? rupiah : rupiah ? "" + rupiah : "";
    }
  });
</script>
<?php require '_base_foot.php';?>