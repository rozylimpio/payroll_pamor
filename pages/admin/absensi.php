<?php 
require '_base_head.php';

$tgll = date('Y-m-d', strtotime('-6 month', strtotime(date('Y-m-d'))));
$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();

$mkar = new \App\Models\Karyawan($app);
$mabsen = new \App\Models\AbsensiUpload($app);
$mab = new \App\Models\Absensi($app);
$deps = $mab->getDepartemen($tgll);
$bags = $mab->getBagian($tgll);
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Absensi</h2>
          <div class="clearfix"></div>
          <?php 
          $defmsg_category = 'absensi';
          require '../pages/defmsg.php'; 
          ?>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-6 col-xs-12">
                  <input id="tanggal" name="tanggal" class="date-picker form-control" type="text" value="<?php echo isset($_GET['tanggal']) ? dateResolver($_GET['tanggal']) : '' ?>">
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <select name="departemen" id="departemen" class="form-control select2_single" required  style="cursor:pointer">
                    <option value=""></option>
                    <?php foreach($deps as $dep) { ?>
                    <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
                      <?php echo $dep['nm_departemen']?></option>
                    <?php } ?>
                  </select>  
                </div>         
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
                  <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                  </select>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                  <select name="jam" id="jam" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php foreach($jams as $jam) { ?>
                    <option <?php echo isset($_GET['jam']) && $_GET['jam'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
                    <?php }?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <!--
                <div class="col-md-2 col-sm-6 col-xs-12">
                  <select name="ket_bag" id="ket_bag" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <option <?php //echo isset($_GET['ket_bag']) && $_GET['ket_bag'] == 'Produksi' ? 'selected' : '' ;?> value="Produksi">Produksi</option>
                    <option <?php //echo isset($_GET['ket_bag']) && $_GET['ket_bag'] == 'Produksi-7' ? 'selected' : '' ;?> value="Produksi-7">Produksi Jam 7</option>
                    <option <?php //echo isset($_GET['ket_bag']) && $_GET['ket_bag'] == 'Produksi-8' ? 'selected' : '' ;?> value="Produksi-8">Produksi Jam 8</option>
                    <option <?php //echo isset($_GET['ket_bag']) && $_GET['ket_bag'] == 'Produksi-9' ? 'selected' : '' ;?> value="Produksi-9">Produksi Jam 9</option>
                    <option <?php //echo isset($_GET['ket_bag']) && $_GET['ket_bag'] == 'Non-Produksi' ? 'selected' : '' ;?> value="Non-Produksi">Non-Produksi</option>
                  </select>
                </div>
              -->
                <div class="col-md-2 col-sm-3 col-xs-12">
                  <select name="jgrup" id="jgrup" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '7' ? 'selected' : '' ;?> value='7'>7 Grup</option>
                    <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '3' ? 'selected' : '' ;?> value='3'>3 Grup</option>
                    <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '4' ? 'selected' : '' ;?> value='4'>3 Grup QC Lab</option>
                    <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '2' ? 'selected' : '' ;?> value='2'>2 Grup</option>
                    <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '1' ? 'selected' : '' ;?> value='1'>Pagi/Siang</option>
                  </select>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12">
                  <select name="grup" id="grup" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                  </select>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12">
                  <select name="libur" id="libur" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '1' ? 'selected' : '' ;?> value='1'>1</option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '2' ? 'selected' : '' ;?> value='2'>2</option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '3' ? 'selected' : '' ;?> value='3'>3</option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '4' ? 'selected' : '' ;?> value='4'>4</option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '5' ? 'selected' : '' ;?> value='5'>5</option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '6' ? 'selected' : '' ;?> value='6'>6</option>
                    <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '7' ? 'selected' : '' ;?> value='7'>7</option>
                  </select>
                </div>
              </div>
              <div class="row"></div><hr>
              <div class="form-group col-md-11 col-sm-11 col-xs-11">
                <button name="tinjau" type="submit" class="btn btn-info">
                  <i class="glyphicon glyphicon-search"></i>
                  &nbsp;Cari&nbsp;
                </button>
              </div>
            </form>
              <?php
              if(isset($_GET['tinjau'])){
                $data = [];
                $data['tanggal'] = dateCreate($app->input->get('tanggal'));
                $data['departemen'] = $app->input->get('departemen');
                $data['bagian'] = $app->input->get('bagian');
                //$data['ket_bag'] = $app->input->get('ket_bag');
                $data['jgrup'] = ''.$app->input->get('jgrup');
                $data['grup'] = ''.$app->input->get('grup');
                $data['libur'] = ''.$app->input->get('libur');
                $data['jamkerja'] = $app->input->get('jam');
                //print_r($data);
                $list = $mab->getData($data);
              ?>
                <div class="col-md-12">
                  <div class="table-responsive ">
                  <hr>
                  <?php 
                  $defmsg_category = 'absensi_list';
                  require '../pages/defmsg.php'; 
                  ?>
                    <table width="100%" class="table-responsive">
                      <tr>
                        <td align="center">
                          <h4>
                            <b>
                              Daftar Karyawan 
                            </b><br>

                        </td>
                      </tr>
                    </table>
                    <table class="table table-bordered table-hover table-striped" id="myTable" style="width: 100%">
                      <thead>
                        <tr>
                          <th rowspan="2">Proses</th>
                          <th width="5%" rowspan="2">No</th>
                          <th rowspan="2">Nama</th>
                          <th rowspan="2">Departemen</th>
                          <th rowspan="2">Kehadiran</th>
                          <th rowspan="2">Ijin</th>
                          <th rowspan="2">Lembur</th>
                          <th colspan="4">Finger</th>
                        </tr>
                        <tr>
                          <th>Masuk</th>
                          <th>Ist.Mulai</th>
                          <th>Ist.Akhir</th>
                          <th>Pulang</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($list as $index => $isi) { ?>
                        <tr>
                          <td><form id="f<?php echo $isi['id'] ?>" method="post" id="f<?php echo $isi['id'] ?>" action="<?php echo url('a/absensi') ?>">
                              <button title="Submit" type="submit" name="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                              <button title="Delete" type="button" data-url="<?php echo url('a/absensi?_method=delete&id=' . $isi['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                              <input type="hidden" name="id" value="<?php echo $isi['id'] ?>">
                              <input type="hidden" name="tgl" value="<?php echo $isi['tgl_absensi'] ?>">
                              <input type="hidden" name="nik" value="<?php echo $isi['nik'] ?>">
    
                              <input type="hidden" name="tangga" value="<?php echo $data['tanggal'] ?>">
                              <input type="hidden" name="departeme" value="<?php echo $data['departemen'] ?>">
                              <input type="hidden" name="bagia" value="<?php echo $data['bagian'] ?>">
                              <input type="hidden" name="kbagia" value="<?php echo $data['ket_bag'] ?>">
                              <input type="hidden" name="jgru" value="<?php echo $data['jgrup'] ?>">
                              <input type="hidden" name="gru" value="<?php echo $data['grup'] ?>">
                              <input type="hidden" name="libu" value="<?php echo $data['libur'] ?>">
                              <input type="hidden" name="jamkerj" value="<?php echo $data['jamkerja'] ?>">
                            </form>
                          </td>
                          <td><?php echo $index+1;?></td>
                          <td><?php echo $isi['nama']."<br>".$isi['nik'];?></td>
                          <td>
                            <?php 
                            $karmut = $mkar->getDataMutasi($isi['nik'], $data['tanggal']);
                            if(empty($karmut)){
                              echo $isi['nm_departemen']."<br>".$isi['nm_bagian'];
                              echo !empty($isi['grup']) ? " - ".$isi['grup'] : "";
                              echo !empty($isi['libur']) ? $isi['libur'] : "";
                              echo '<br><small>'.$isi['bagian'].'</small>';
                            }else{
                              echo $karmut['nm_departemen']."<br>".$karmut['nm_bagian'];
                              echo !empty($karmut['grup']) ? " - ".$karmut['grup'] : "";
                              echo !empty($karmut['libur']) ? $karmut['libur'] : "";
                              echo '<br><br>';
                              echo '<i><small style="font-size: 9px">Berlaku : '.dateResolver($karmut['berlaku']).'<br>';
                              echo $isi['nm_departemen']."<br>".$isi['nm_bagian'];
                              echo !empty($isi['grup']) ? " - ".$isi['grup'] : "";
                              echo !empty($isi['libur']) ? $isi['libur'] : "";
                              echo '</i></small>';
                            }
                            ?>    
                          </td>
                          <td width="10%">
                            <select form="f<?php echo $isi['id'] ?>" name="hadir" id="hadir<?php echo $isi['id'] ?>" class="form-control select2_single" data-idk="<?php echo $isi['id'] ?>" style="cursor: pointer">
                              <option></option>
                              <option <?php echo $isi['kehadiran']=='masuk'? 'selected' : '' ?> value="masuk">Masuk</option>
                              <option <?php echo $isi['kehadiran']=='ijin'? 'selected' : '' ?> value="ijin">Surat Ijin</option>
                              <option <?php echo $isi['kehadiran']=='sdr'? 'selected' : '' ?> value="sdr">Surat Dokter</option>
                              <option <?php echo $isi['kehadiran']=='cuti'? 'selected' : '' ?> value="cuti">Cuti</option>
                              <option <?php echo $isi['kehadiran']=='libur'? 'selected' : '' ?> value="libur">Libur</option>
                              <option <?php echo $isi['kehadiran']=='cutihamil'? 'selected' : '' ?> value="cutihamil">Cuti Hamil</option>
                              <option <?php echo $isi['kehadiran']=='dirumahkan'? 'selected' : '' ?> value="dirumahkan">Dirumahkan</option>
                              <option <?php echo $isi['kehadiran']=='alpha'? 'selected' : '' ?> value="alpha">Alpha</option>
                              <option <?php echo $isi['kehadiran']=='kk'? 'selected' : '' ?> value="kk">Kecelakaan Kerja</option>
                              <option <?php echo $isi['kehadiran']=='cuti_r'? 'selected' : '' ?> value="cuti_r">Cuti_r</option>
                              <option <?php echo $isi['kehadiran']=='alpha_r'? 'selected' : '' ?> value="alpha_r">Alpha_r</option>
                              <option <?php echo $isi['kehadiran']=='rumah' || $isi['kehadiran']=='Rumah'? 'selected' : '' ?> value="rumah">Rumah</option>                              
                              <option <?php echo $isi['kehadiran']=='unpaid' ? 'selected' : '' ?> value="unpaid">Unpaid</option>
                            </select>
                            <input form="f<?php echo $isi['id'] ?>" name="jdwl" id="jdwl<?php echo $isi['id'] ?>" 
                            value="<?php echo $isi['jadwal_shift']; ?>" type="text" style="width: 100%" />

                            <input form="f<?php echo $isi['id'] ?>" name="kode" id="kode<?php echo $isi['id'] ?>" 
                            value="<?php echo $isi['kode']; ?>" type="hidden" style="width: 100%" />
                            <?php
                            if($isi['jml_grup']==7){
                              $jdwl = '';
                              $jdwl = carimasuk($mabsen, $data['tanggal'], $isi);
                                if(empty($jdwl)){
                                  echo "LIBUR";
                                }
                            }else{
                              $day = (int)date('N', strtotime($data['tanggal']));
                              if($day == $isi['libur']){
                                echo "LIBUR";
                              }
                            }
                            ?>
                            <small><br>Uang Makan : <?php echo $isi['u_makan']."x" ?></small>
                          </td>
                          <td width="14%">
                              <input id="idf<?php echo $isi['id'] ?>" value="1" type="hidden" />
                              <div class="form-group">
                                <select form="f<?php echo $isi['id'] ?>" name="ijin[]" id="ijin<?php echo $isi['id'] ?>" class="form-control select2_single" data-idk="<?php echo $isi['id'] ?>" style="cursor: pointer">
                                  <option></option>
                                  <option value="mtk">Keluar</option>
                                  <option value="tmk">Terlambat</option>
                                  <option value="psw">Pulang</option>
                                </select>
                                <div align="center" style="margin-top: 10px" id="ijam<?php echo $isi['id'] ?>" class='ijam'>
                                  <input form="f<?php echo $isi['id'] ?>" type="text" name="imulai[]" id="mulai" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Mulai" style="width: 45%">
                                  &nbsp;
                                  <input form="f<?php echo $isi['id'] ?>" type="text" name="iakhir[]" id="akhir" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Akhir" style="width: 45%">
                                </div>
                              </div>
                              <div id="divP<?php echo $isi['id'] ?>" style="width:100%"></div>
                              <button type="button" class="btn btn-info btn-xs" id="append"
                              onclick="tambahP(<?php echo $isi['id'] ?>); return false;" style="margin-top: 5px">
                              <i class="fa fa-plus"></i></button><i>tambah ijin</i>


                              <script type="text/javascript">
                              function tambahP(idk) {
                                var idf = document.getElementById("idf"+idk).value;
                                //console.log(idk);
                                var stre;
                                stre="<div id='srow" + idf + "' style='width:100%;margin-top:10px'>"
                                  +"<div class='form-group' style='width:100%'>"
                                    +"<select form='f"+idk+"' name='ijin[]' id='ijin" + idf + "' class='form-control select2_single' style='cursor: pointer'>"
                                      +"<option></option>"
                                      +"<option value='mtk'>Keluar</option>"
                                      +"<option value='tmk'>Terlambat</option>"
                                      +"<option value='psw'>Pulang</option>"
                                    +"</select>"
                                    +"&nbsp;"
                                    +"<button type='button' class='btn btn-round btn-danger btn-xs' id='remove' onclick='hapusElemen(\"#srow" + idf + "\"); return false;'><i class='fa fa-times'></i></button>"
                                    +"<div align='center' style='margin-top: 10px' id='ijam" +idk+ idf + "'>"
                                      +"<input form='f"+idk+"' type='text' name='imulai[]' id='mulai' class='form-control' data-inputmask=\"'mask': '99:99'\" placeholder='Mulai' style='width: 45%'>"
                                      +"&nbsp;"
                                      +"<input form='f"+idk+"' type='text' name='iakhir[]' id='akhir' class='form-control' data-inputmask=\"'mask': '99:99'\" placeholder='Akhir' style='width: 45%'>"
                                    +"</div>"
                                  +"</div>"
                                +"</div>"

                                +"<script> "
                                  +"$(document).ready(function() { "
                                    +"$(':input').inputmask();  "
                                    +"$('#ijam" +idk+ idf + "').hide();  "
                                    +"$('#ijin" + idf + "').select2({  "
                                        +"placeholder: 'Pilih Ijin',  "
                                        +"allowClear: true "
                                    +"}).on('change', function(e) {  "
                                        +"var ijin = e.currentTarget.value;  "
                                        +"if(ijin=='mtk'){  "
                                            +"$('#ijam" +idk+ idf + "').show(); "
                                        +"} "
                                        +"if(ijin=='tmk' || ijin=='psw' || ijin == ''){  "
                                            +"$('#ijam"+idk + idf + "').hide(); "
                                        +"}  "
                                    +"});  "
                                  +"}); "
                                +"<"+"/script>";

                                $("#divP"+idk).append(stre);
                                idf = (idf-1) + 2;
                                document.getElementById("idf"+idk).value = idf;
                              }
                              function hapusElemen(idf) {
                                $(idf).remove();
                              }
                              </script>
                            <?php
                            if(!empty($isi['ijin'])){
                                echo "<br><i>* ".$isi['ijin']." ";
                                echo $mab->lamamtk($isi['id'], $isi['tgl_absensi']);
                                echo "</i>";
                            }
                            ?>
                          </td>
                          <td width="14%">
                              <input id="idl<?php echo $isi['id'] ?>" value="1" type="hidden" />
                              <input form="f<?php echo $isi['id'] ?>" type="hidden" name="lemburww" id="lemburww<?php echo $isi['id'] ?>" value="<?php echo $isi['lembur_wajib'] ?>">
                              <input form="f<?php echo $isi['id'] ?>" type="hidden" name="lemburw" id="lemburw<?php echo $isi['id'] ?>" value="<?php echo $isi['lembur_wajib'] ?>">
                              <?php
                              $dl = $mab->cariLemburbyId($isi['id']);
                              if(!empty($dl)){
                                for ($i=0; $i < count($dl); $i++) { 
                                  if ($dl[$i]['jl_mulai']!='00:00:00' && $dl[$i]['jl_akhir']!='00:00:00') {
                                    ?>
                                    <div id="divfor<?php echo $isi['id']+$i ?>" style="width:100%; margin-bottom: 10px"  class='form-group'>
                                      <select form="f<?php echo $isi['id'] ?>" name="lembur[]" id="lembur<?php echo $isi['id'] ?>" class="form-control select2_single" style="cursor: pointer;width:74.6%" data-idk="<?php echo $isi['id'] ?>">
                                        <option></option>
                                        <option <?php echo $isi['lembur_wajib']=='wajib'? 'selected' : '' ?> value="wajib">Wajib</option>
                                        <option <?php echo $dl[$i]['lembur']=='jumat' ? 'selected' : '' ?> value="jumat">Jum'at</option>
                                        <option <?php echo $dl[$i]['lembur']=='biasa' ? 'selected' : '' ?> value="biasa">Lembur</option>
                                        <option <?php echo $dl[$i]['lembur']=='libur' ? 'selected' : '' ?> value="libur">Hari Libur</option>
                                        <option <?php echo $dl[$i]['lembur']=='nasional' ? 'selected' : '' ?> value="nasional">Nasional</option>
                                        <option <?php echo $dl[$i]['lembur']=='serentak' ? 'selected' : '' ?> value="serentak">Serentak</option>
                                      </select>
                                      &nbsp;
                                      <button type='button' class='btn btn-round btn-danger btn-xs' id='remove' onclick='hapusElemen("#divfor"+<?php echo $isi['id']+$i ?>); return false;'><i class='fa fa-times'></i></button>
                                      <div class="form-group" align="center" style="margin-top: 10px" id="lemjam<?php echo $isi['id'] ?>" class="lemjam">
                                          <input form="f<?php echo $isi['id'] ?>" type="text" name="lmulai[]" id="lmulai" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Mulai" style="width: 45%" value="<?php echo substr($dl[$i]['jl_mulai'], 0, 5); ?>">
                                          &nbsp;
                                          <input form="f<?php echo $isi['id'] ?>" type="text" name="lakhir[]" id="lakhir" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Akhir" style="width: 45%" value="<?php echo substr($dl[$i]['jl_akhir'], 0, 5); ?>">
                                      </div>
                                    </div>

                                    <?php
                                  }elseif($dl[$i]['jl_mulai']=='00:00:00' && $dl[$i]['jl_akhir']=='00:00:00' && count($dl)==1){
                                    ?>

                                    <select form="f<?php echo $isi['id'] ?>" name="lembur[]" id="lembur<?php echo $isi['id'] ?>" class="form-control select2_single" style="cursor: pointer" style="width: 100%" data-idk="<?php echo $isi['id'] ?>">
                                      <option></option>
                                      <option <?php echo $isi['lembur_wajib']=='wajib'? 'selected' : '' ?> value="wajib">Wajib</option>
                                      <?php 
                                      $day = (int)date('N', strtotime($data['tanggal']));
                                      if($isi['kd_departemen']== 12 && $isi['kd_bagian']==104 && $day==5){ 
                                      //12=umum, 104=security pamor, 5=jum'at
                                      ?>
                                      <option value="jumat">Lembur Jum'at</option>
                                      <?php }?>
                                      <option value="biasa">Lembur</option>
                                      <option value="libur">Hari Libur</option>
                                      <option value="nasional">Libur Nasional</option>
                                      <option value="serentak">Serentak</option>
                                    </select>
                                    <div class="form-group" align="center" style="margin-top: 10px" id="lemjam<?php echo $isi['id'] ?>" class="lemjam">
                                        <input form="f<?php echo $isi['id'] ?>" type="text" name="lmulai[]" id="lmulai" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Mulai" style="width: 45%">
                                        &nbsp;
                                        <input form="f<?php echo $isi['id'] ?>" type="text" name="lakhir[]" id="lakhir" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Akhir" style="width: 45%">
                                    </div>

                                    <?php
                                  }
                                }
                              }else{
                                ?>
                                <select form="f<?php echo $isi['id'] ?>" name="lembur[]" id="lembur<?php echo $isi['id'] ?>" class="form-control select2_single" style="cursor: pointer" style="width: 100%" data-idk="<?php echo $isi['id'] ?>">
                                  <option></option>
                                  <option <?php echo $isi['lembur_wajib']=='wajib'? 'selected' : '' ?> value="wajib">Wajib</option>
                                  <?php 
                                  $day = (int)date('N', strtotime($data['tanggal']));
                                  if($isi['kd_departemen']== 12 && $isi['kd_bagian']==104 && $day==5){ 
                                  //12=umum, 104=security pamor, 5=jum'at
                                  ?>
                                  <option value="jumat">Lembur Jum'at</option>
                                  <?php }?>
                                  <option value="biasa">Lembur</option>
                                  <option value="libur">Hari Libur</option>
                                  <option value="nasional">Libur Nasional</option>
                                  <option value="serentak">Serentak</option>
                                </select>
                                <div class="form-group" align="center" style="margin-top: 10px" id="lemjam<?php echo $isi['id'] ?>" class="lemjam">
                                    <input form="f<?php echo $isi['id'] ?>" type="text" name="lmulai[]" id="lmulai" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Mulai" style="width: 45%">
                                    &nbsp;
                                    <input form="f<?php echo $isi['id'] ?>" type="text" name="lakhir[]" id="lakhir" class="form-control" data-inputmask="'mask': '99:99'" placeholder="Akhir" style="width: 45%">
                                </div>
                                <?php 
                              }
                              /*
                              $day = (int)date('N', strtotime($data['tanggal']));
                              if($isi['jadwal_shift']== 'Pagi' && $isi['kd_departemen']== 12 && $isi['kd_bagian']==104 && $day==6){ 
                              //12=umum, 104=security pamor, 6=jum'at
                              ?>
                              <div class="form-group" align="center" style="margin-top: 10px">
                                <label>
                                  <input form="f<?php echo $isi['id'] ?>" type="checkbox" class="js-switch" name="ljumat" value="9000" /> Lembur Jum'at
                                </label>
                              </div>
                              <?php }/**/?>
                              <div id="divL<?php echo $isi['id'] ?>" style="width:100%"></div>
                              <button type="button" class="btn btn-info btn-xs" id="append"
                              onclick="tambahL(<?php echo $isi['id'] ?>); return false;" style="margin-top: 5px">
                              <i class="fa fa-plus"></i></button><i>tambah lembur</i>
                              
                              <script type="text/javascript">
                              function tambahL(idk) {
                                var idl = document.getElementById("idl"+idk).value;
                                //console.log(idk);
                                var strel;
                                strel="<div id='srowl" + idl + "' style='width:100%;margin-top:10px'>"
                                  +"<div class='form-group' style='width:100%'>"
                                    +"<select form='f"+idk+"' name='lembur[]' id='lembur"+idl+"' class='form-control select2_single' style='cursor: pointer'>"
                                      +"<option></option>"
                                      +"<"+"?php "
                                      +"$day = (int)date('N', strtotime($data['tanggal']));"
                                      +"if($isi['kd_bagian']==104 && $day==5){ "
                                      +"?>"
                                      +"<option value='jumat'>Jum'at</option>"
                                      +"<"+"?php }?>"
                                      +"<option value='biasa'>Lembur</option>"
                                      +"<option value='libur'>Hari Libur</option>"
                                      +"<option value='nasional'>Nasional</option>"
                                      +"<option value='serentak'>Serentak</option>"
                                    +"</select>"
                                    +"&nbsp;&nbsp;&nbsp;"
                                    +"<button type='button' class='btn btn-round btn-danger btn-xs' id='remove' onclick='hapusElemen(\"#srowl"+idl+"\"); return false;'><i class='fa fa-times'></i></button>"
                                    +"<div align='center' style='margin-top: 10px' id='lemjam" +idk+ idl + "'>"
                                      +"<input form='f"+idk+"' type='text' name='lmulai[]' id='mulai' class='form-control' data-inputmask=\"'mask': '99:99'\" placeholder='Mulai' style='width: 45%'>"
                                      +"&nbsp;"
                                      +"<input form='f"+idk+"' type='text' name='lakhir[]' id='akhir' class='form-control' data-inputmask=\"'mask': '99:99'\" placeholder='Akhir' style='width: 45%'>"
                                    +"</div>"
                                  +"</div>"
                                +"</div>"

                                +"<script> "
                                  +"$(document).ready(function() { "
                                    +"$(':input').inputmask();"
                                    +"$('#lemjam" +idk+ idl + "').hide();"
                                    +"$('#lembur" + idl + "').select2({"
                                      +"placeholder: 'Pilihan',"
                                      +"allowClear: true"
                                    +"}).on('change', function(e) {"
                                        +"var lembur = e.currentTarget.value;"
                                        +"if(lembur=='biasa' || lembur=='libur' || lembur=='jumat' || lembur=='nasional'){ "
                                            +"$('#lemjam" +idk+ idl + "').show();"
                                        +"}else if(lembur=='wajib' || lembur=='serentak' || lembur==''){ "
                                            +"$('#lemjam"+idk + idl + "').hide();"
                                        +"} "
                                    +"});"
                                  +"});"
                                +"<"+"/script>";

                                $("#divL"+idk).append(strel);
                                idl = (idl-1) + 2;
                                document.getElementById("idl"+idk).value = idl;
                              }
                              function hapusElemen(idl) {
                                $(idl).remove();
                              }
                              </script>
                              <?php
                              if(!empty($isi['lembur_lain'])){
                                $tllj = carijam($isi['t_lembur']);
                                $tllm = carimenit($isi['t_lembur'], $tllj);
                                echo "<br><i>* ".$tllj." jam ".$tllm." menit"."</i>";
                              }
                              ?>

                          </td>
                          <td width="7%">
                            <input form="f<?php echo $isi['id'] ?>" type="text" name="masuk" required class="form-control" data-inputmask="'mask': '99:99'" style="width: 100%" value="<?php echo substr($isi['waktu_masuk'],0,-3);?>">
                          </td>
                          <td width="7%">
                            <input form="f<?php echo $isi['id'] ?>" type="text" name="istaw" required class="form-control" data-inputmask="'mask': '99:99'" style="width: 100%" value="<?php echo substr($isi['ist_keluar'],0,-3);?>">
                          </td>
                          <td width="7%">
                            <input form="f<?php echo $isi['id'] ?>" type="text" name="istak" required class="form-control" data-inputmask="'mask': '99:99'" style="width: 100%" value="<?php echo substr($isi['ist_masuk'],0,-3);?>">
                            <input form="f<?php echo $isi['id'] ?>" type="hidden" name="istke" value="<?php echo $isi['ist_ke']; ?>">
                          </td>
                          <td width="7%">
                            <input form="f<?php echo $isi['id'] ?>" type="text" name="pulang" required class="form-control" data-inputmask="'mask': '99:99'" style="width: 100%" value="<?php echo substr($isi['waktu_pulang'],0,-3);?>">
                            <input form="f<?php echo $isi['id'] ?>" type="hidden" name="t_jam" value="<?php echo $isi['t_jam']; ?>">
                          </td>
                        </tr>

                        <script type="text/javascript">                        
                          $('#lemjam<?php echo $isi['id'] ?>').hide();
                          $('#ijam<?php echo $isi['id'] ?>').hide();


                          $("#hadir<?php echo $isi['id'] ?>.select2_single").select2({
                            placeholder: "Kehadiran",
                            allowClear: true,
                            disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
                          }).on('change', function(e) {
                              var id = $(e.currentTarget).data('idk');
                              var hadir = e.currentTarget.value;
                              var lwajib = $('#lemburww'+id).val();
                              var ed = document.getElementById('lembur'+id);
                              var lembur = ed.options[ed.selectedIndex].value;
                              //console.log(lembur);
                              if(hadir!='masuk'){
                                if(lembur=='wajib'){
                                  $('#lembur'+id).val("").trigger("change");
                                  $('#lemburw'+id).val("");
                                  $('#jdwl'+id).val("");
                                }
                              }
                              if(hadir=='masuk'){
                                $('#lemburw'+id).val(lwajib);
                              }
                          });

                          $("#ijin<?php echo $isi['id'] ?>.select2_single").select2({
                            placeholder: "Pilih Ijin",
                            allowClear: true,
                            disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
                          }).on('change', function(e) {
                              var id = $(e.currentTarget).data('idk');
                              var ijin = e.currentTarget.value;
                              var lwajib = $('#lemburww'+id).val();
                              var ed = document.getElementById('lembur'+id);
                              var lembur = ed.options[ed.selectedIndex].value;
                              //console.log(lembur);
                              if(ijin=='mtk' || ijin=='tmk' || ijin=='psw'){
                                if(lembur=='wajib'){
                                  $('#lembur'+id).val("").trigger("change");
                                  $('#lemburw'+id).val("");
                                }
                              }
                              if(ijin=='mtk'){
                                $('#ijam'+id).show();        
                              }
                              if(ijin=='tmk' || ijin=='psw' || ijin == ''){
                                $('#ijam'+id).hide();        
                              }
                              if(ijin==''){
                                $('#lemburw'+id).val(lwajib);
                              }
                          });
                          
                          $("#lembur<?php echo $isi['id'] ?>.select2_single").select2({
                            placeholder: "Pilihan",
                            allowClear: true,
                            disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
                          }).on('change', function(ee) {
                              var id = $(ee.currentTarget).data('idk');
                              var lembur = ee.currentTarget.value;
                              if(lembur=='biasa' || lembur=='libur' || lembur=='jumat' || lembur == 'nasional'){
                                $('#lemjam'+id).show();        
                                $('#hadir'+id).val("masuk").trigger("change");
                              }else if(lembur=='wajib' || lembur=='serentak' || lembur==''){
                                $('#lemjam'+id).hide();
                                $('#hadir'+id).val("masuk").trigger("change");
                              }else if(lembur==''){
                                $('#lemjam'+id).hide();
                              }
                          }).show(function(e){
                            var ed = document.getElementById('lembur<?php echo $isi['id'] ?>');
                            var id = $(ed).data('idk');
                            var lembur = ed.options[ed.selectedIndex].value;
                            //console.log(id);
                            //console.log(lembur);
                            if(lembur=='biasa' || lembur=='libur' || lembur=='jumat' || lembur == 'nasional'){
                              $('#lemjam'+id).show();        
                            }else if(lembur=='wajib' || lembur=='serentak'){
                              $('#lemjam'+id).hide();
                            }
                          });
                        </script>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
      
                </div>
            <?php
            }
            ?>
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>
<!-- Switchery -->
<script src="<?php echo url();?>js/switchery.min.js"></script>
<script>
$(document).ready(function() {
  $(":input").inputmask();

  var emptyOption = '<option value=""></option>';
  var bags = JSON.parse('<?php echo json_encode($bags);?>');

  $("#departemen.select2_single").select2({
    placeholder: "Pilih Departemen",
    allowClear: true
  });
  $("#jgrup.select2_single").select2({
    placeholder: "Jumlah Grup",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  });
  $("#grup.select2_single").select2({
    placeholder: "Grup",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  });
  $("#libur.select2_single").select2({
    placeholder: "Libur",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  });
  $("#ket_bag.select2_single").select2({
    placeholder: "Keterangan Bagian",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  });
  $("#jam.select2_single").select2({
    placeholder: "Jam Kerja",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  });
  $("#hadir.select2_single").select2({
    placeholder: "Kehadiran",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  });

  $('#tanggal').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    locale: {
      format: "DD-MM-YYYY",
      separator: "-",
    }
  });


  
  $("#departemen.select2_single").select2({
      placeholder: "Pilih Departemen",
      allowClear: true
  }).on('change', function(e) {
    var kd_departemen = parseInt(e.currentTarget.value);
    var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
    
    $('#bagian').html(emptyOption);
    $(list_bagian).each(function(i, item) {
      $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
    });

    $("#bagian.select2_single").select2('destroy').select2({
      placeholder: "Pilih Bagian",
      allowClear: true,
      disabled: false
    });
    $("#ket_bag.select2_single").select2({
      placeholder: "Keterangan Bagian",
      allowClear: true,
      disabled: false
    });
    $("#jam.select2_single").select2({
      placeholder: "Jam Kerja",
      allowClear: true,
      disabled: false
    });
  });



  $("#bagian.select2_single").select2({
    placeholder: "Pilih Bagian",
    allowClear: true,
    disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
  }).show(function(e){
    var bagi = parseInt(document.getElementById('bagi').value);
    var ed = document.getElementById('departemen');
    var kd_departemen = parseInt(ed.options[ed.selectedIndex].value);
    var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
    $('#bagian').html(emptyOption);
    $(list_bagian).each(function(i, item) {
      $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
    });
  });

  $("#jam.select2_single").select2({
      placeholder: "Jam Kerja",
      allowClear: true
  }).on('change', function(e){
    var kd_jamkerja = e.currentTarget.value;

    if(kd_jamkerja==2){
      $("#jgrup.select2_single").select2('destroy').select2({
        placeholder: "Jumlah Grup",
        allowClear: true,
        disabled: false
      });
    }else{
      $("#jgrup.select2_single").select2('destroy').select2({
        placeholder: "Jumlah Grup",
        allowClear: true,
        disabled: true
      });
    }
  });




  $("#jgrup.select2_single").select2({
    placeholder: "Jumlah Grup",
    allowClear: true
  }).on('change', function(e) {
    var jgrup = e.currentTarget.value;
    if(jgrup>2){
      if(jgrup>4){
        //$('#libur').html(emptyOption);
        $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: true
        });
      }else{
        $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: false
        });
      }
      $("#grup.select2_single").select2('destroy').select2({
        placeholder: "Pilih Grup",
        allowClear: true,
        disabled: false
      });
      $('#grup').html(emptyOption);
      for(i=0;i<jgrup;i++){
        $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '">' + (i+10).toString(36).toUpperCase() + '</option>');
      }if(jgrup==4){
        $("#grup").append('<option value="Rp">Repliver</option>');
      }
    }else if(jgrup==2){
      $("#grup.select2_single").select2('destroy').select2({
        placeholder: "Jadwal Awal Masuk",
        allowClear: true,
        disabled: false
      });
      $("#tgl_m").show();
      $('#grup').html(emptyOption);
      $("#grup").append('<option value="P">Pagi</option>');
      $("#grup").append('<option value="S">Siang</option>');
      $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: false
        });
    }else if(jgrup==1){
      $("#grup.select2_single").select2('destroy').select2({
        placeholder: "Pilih Grup",
        allowClear: true,
        disabled: false
      });
      var p = 'Pagi';
      $('#grup').html(emptyOption);
      $("#grup").append('<option value="P">Pagi</option>');
      $("#grup").append('<option value="S">Siang</option>');
      $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: false
        });
    }

    
  });

  var edit = JSON.parse('<?php echo json_encode($_GET);?>');
  $("#grup.select2_single").select2({
    placeholder: "Pilih Grup",
    allowClear: true
  }).show(function(e){
    var ed = document.getElementById('jgrup');
    var jgrup = ed.options[ed.selectedIndex].value;          
    //console.log(edit);
    
    if(jgrup>2){
      $('#grup').html(emptyOption);
      for(i=0;i<jgrup;i++){
        $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '" '+((i+10).toString(36).toUpperCase() == edit['grup'] ? 'selected' : '') +'>' + (i+10).toString(36).toUpperCase() + '</option>');
      }
    }else if(jgrup==2){
      $('#grup').html(emptyOption);
      $("#grup").append('<option value="P" '+(edit['grup']=='P' ? 'selected' : '') +'>Pagi</option>');      
      $("#grup").append('<option value="S" '+(edit['grup']=='S' ? 'selected' : '') +'>Siang</option>');      
    }else if(jgrup==1){
      $('#grup').html(emptyOption);
      $("#grup").append('<option value="P" '+(edit['grup']=='P' ? 'selected' : '') +'>Pagi</option>');      
      $("#grup").append('<option value="S" '+(edit['grup']=='S' ? 'selected' : '') +'>Siang</option>');    
    }
  });




});
</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>