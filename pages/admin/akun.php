<?php 
require '_base_head.php';
$makun = new \App\Models\User($app);
$akuns = $makun->get();
$dkars = $makun->getnama();

$edit = false;
if($app->input->get('edit')) {
  $edit = $makun->getById($app->input->get('edit'));
}

$redirect = url('a/akun');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Akun User</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('a/akun' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'akun';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="akun">
            NIK
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="nik" id="nik" class="form-control select2_single" style="cursor:pointer" <?php echo $edit ? 'disabled' : '' ;?> >
              <option></option>
              <?php foreach($dkars as $kar) { ?>
              <option <?php echo $edit['nik'] == $kar['id'] ? 'selected' : '' ;?> value="<?php echo $kar['id'];?>"><?php echo $kar['id'];?> - <?php echo $kar['nama'];?></option>
              <?php }?>
            </select>
            <input type="hidden" name="nama" id="nama" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nama'] : '' ;?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="akun">
            Password
          </label>
          <div class="col-md-6 col-sm-6 col-xs-8">
            <input type="password" name="pass" id="pass" <?php echo $edit ? '' : 'require' ;?> autofocus class="form-control col-md-7 col-xs-12" placeholder="Password">
            <input type="checkbox" onclick="myFunction()"> Show Password
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="akun">
            Posisi
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="posisi" id="posisi" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['posisi'] == 'admin' ? 'selected' : '' ;?> value="admin">Admin</option>
              <option <?php echo $edit['posisi'] == 'atasan' ? 'selected' : '' ;?> value="atasan">Atasan</option>
              <option <?php echo $edit['posisi'] == 'client' ? 'selected' : '' ;?> value="client">Client</option>
            </select>
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'akun_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Posisi</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($akuns as $index => $akun) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo $akun['nama'];?></td>
                      <td><?php echo $akun['posisi'];?></td>
                      <td>
                        <a href="<?php echo url('a/akun?edit=' . $akun['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('a/akun?_method=delete&id=' . $akun['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $("#nik.select2_single").select2({
            placeholder: "Pilih Nomor Induk Karyawan",
            allowClear: true
        }).on('change', function(e) {          
          var id = e.currentTarget.value;
          var list_id = _.filter(dkars, {id:id});
          $(list_id).each(function(i, itemkj) {
            document.getElementById("nama").value = itemkj.nama;
          });
        });

        var dkars = JSON.parse('<?php echo json_encode($dkars);?>');
        $("#posisi.select2_single").select2({
            placeholder: "Pilih Posisi",
            allowClear: true
        });

      });

      function myFunction() {
        var x = document.getElementById("pass");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>