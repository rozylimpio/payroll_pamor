<?php 
require '_base_head.php';
$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();
$jamks = $mjam->getIstirahatS();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mjam->getIstirahatByIdS($app->input->get('edit'));
}

$redirect = url('a/jamkerja_istirahat_s');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Jam Kerja Istirahat Security</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('a/jamkerja_istirahat_s' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'jamkerja_istirahat';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jam Kerja
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="jam" id="jam" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($jams as $jam) { ?>
              <option <?php echo $edit['kd_jamkerja'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
           
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="mulai" id="mulai" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99:99'" value="<?php echo $edit ? $edit['mulai'] : '' ;?>" placeholder="Jam Mulai">
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="akhir" id="akhir" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99:99'" value="<?php echo $edit ? $edit['akhir'] : '' ;?>" placeholder="Jam Akhir">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Shift
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="shift" id="shift" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['shift'] == 'Pagi' ? 'selected' : '' ;?> value="Pagi">Pagi</option>
              <option <?php echo $edit['shift'] == 'Siang' ? 'selected' : '' ;?> value="Siang">Siang</option>
              <option <?php echo $edit['shift'] == 'Malam' ? 'selected' : '' ;?> value="Malam">Malam</option>
            </select>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="hari" id="hari" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['hari'] == '5' ? 'selected' : '' ;?> value="5">Jum'at</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jam Kerja ke-
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="ke" id="ke" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['ist_ke'] == 1 ? 'selected' : '' ;?> value="1">1</option>
              <option <?php echo $edit['ist_ke'] == 2 ? 'selected' : '' ;?> value="2">2</option>
              <option <?php echo $edit['ist_ke'] == 3 ? 'selected' : '' ;?> value="3">3</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tanggal Berlaku
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit ? dateResolver($edit['berlaku']) : '' ;?>" placeholder="Berlaku Mulai">
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'jamkerja_istirahat_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Shift</th>
                      <th>Keterangan</th>
                      <th>Hari</th>
                      <th>Jam Kerja</th>
                      <th>Tanggal Berlaku</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($jamks as $index => $jamk) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo $jamk['nm_jamkerja'].' '.$jamk['shift'];?></td>
                      <td><?php echo $jamk['ket'];?></td>
                      <td><?php echo $jamk['hari']==5 ? "Jum'at" : '';?></td>
                      <td><?php echo $jamk['mulai'];?> WIB - <?php echo $jamk['akhir'];?> WIB</td>
                      <td><?php echo dateResolver($jamk['berlaku']);?></td>
                      <td>
                        <a href="<?php echo url('a/jamkerja_istirahat_s?edit=' . $jamk['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('a/jamkerja_istirahat_s?_method=delete&id=' . $jamk['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<script>
  $(document).ready(function() {
    $(":input").inputmask();

    $("#jam.select2_single").select2({
        placeholder: "Pilih Jam Kerja",
        allowClear: true
    }).on('change', function(e){        
          var kd_jamkerja = e.currentTarget.value;
          if(kd_jamkerja==2){
            $("#shift.select2_single").select2({
                placeholder: "Pilih Shift",
                allowClear: true,
                disabled: false
            });
            $("#ke.select2_single").select2({
                placeholder: "Pilih Jam Istirahat Ke-",
                allowClear: true,
                disabled: false
            });
          }else{
            $("#shift.select2_single").select2({
                placeholder: "Pilih Shift",
                allowClear: true,
                disabled: true
            });
            $("#ke.select2_single").select2({
                placeholder: "Pilih Jam Istirahat Ke-",
                allowClear: true,
                disabled: true
            });
          }
    });
    $("#shift.select2_single").select2({
        placeholder: "Pilih Shift",
        allowClear: true,
        disabled: <?php echo $edit ? 'false' : 'true' ?>
    }).on('change', function(e){        
          var shift = e.currentTarget.value;
          if(shift=='Pagi'){
            $("#hari.select2_single").select2({
                placeholder: "Pilih Hari Istirahat",
                allowClear: true,
                disabled: false
            });
          }else{
            $("#hari.select2_single").select2({
                placeholder: "Pilih Hari Istirahat",
                allowClear: true,
                disabled: true
            });
          }
    });
    $("#ke.select2_single").select2({
        placeholder: "Pilih Jam Istirahat Ke-",
        allowClear: true,
        disabled: <?php echo $edit ? 'false' : 'true' ?>
    });
    $("#hari.select2_single").select2({
        placeholder: "Pilih Hari Istirahat",
        allowClear: true
    });
  });
</script>
<?php require '_base_foot.php';?>