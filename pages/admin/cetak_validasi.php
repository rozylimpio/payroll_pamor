<!--<script src="<?php //echo url();?>js/jquery.min.js"></script>
<link href="<?php //echo url();?>css/bootstrap.min.css" rel="stylesheet">
<script src="<?php //echo url();?>js/bootstrap.min.js"></script>-->
<?php
$surat = new \App\Models\Surat($app);
$nosrt = $surat->get();

$nik = $app->input->post('idkar');
$ket = $app->input->post('ket');
$dep = $app->input->post('dep');
$bag = $app->input->post('bag');
$res = $app->input->post('res');
$res == 1 ? $direct = 'a/resign' : $direct = 'a/resign_in';
//echo $ket;

if($ket == 'sk'){
?>
<form class="form-horizontal form-label-left">
	<div class="form-group col-md-12">
		<label class="control-label col-md-5 col-sm-6 col-xs-12">
			No Surat Terakhir
		</label>
		<div class="col-md-5 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" value="<?php echo $nosrt['nosurat']; ?>" readonly>
		</div>
	</div>
	<div class="form-group col-md-12">
		<input type="hidden" id="id" name="id" value="<?php echo $nik;?>">
		<input type="hidden" id="ket" name="ket" value="<?php echo $ket;?>">
		<input type="hidden" id="dep" name="dep" value="<?php echo $dep;?>">
		<input type="hidden" id="bag" name="bag" value="<?php echo $bag;?>">
		<input type="hidden" id="res" name="res" value="<?php echo $res;?>">
		<label class="control-label col-md-5 col-sm-6 col-xs-12">
			No Surat
		</label>
		<div class="col-md-5 col-sm-6 col-xs-12">
			<input type="text" name="no" id="no" required  autofocus class="form-control col-md-7 col-xs-12"placeholder="Nomor Surat">
			<input type="hidden" name="ketsp" id="ketsp" value="">
		</div>
	</div>	
	<div class="form-group col-md-12">
		<hr>
		<div class="col-md-3 col-sm-6 col-xs-12 pull-right">
			<button id="simpan" name="simpan" type="button" class="btn btn-success">
				<i class="glyphicon glyphicon-ok"></i>
				&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;
			</button>
		</div>
	</div>
</form>
<?php
}elseif($ket == 'sp'){
	$nosursp = $surat->getBySP();
	$data = $surat->getByIdSP($nik);
	$tgl = date('Y-m-d');
	$ke = 0;
	$nol = '';
	$direct = 'a/cari';
	empty($data) ? $k = 0 : $k = count($data)-1;
	if(empty($data)){
	?>
		<div align="center">Karyawan belum pernah mendapatkan "Surat Peringatan" sebelumnya<br></div>
		<hr>
	<?php
	}else{
		$ke = $data[0]['ke'];
	}
	?>
	<form class="form-horizontal form-label-left"><!-- method="post" action="pdf_surat_sp">-->
		<div class="form-group col-md-12">
			<input type="hidden" id="id" name="id" value="<?php echo $nik;?>">
			<input type="hidden" id="ket" name="ket" value="<?php echo $ket;?>">
			<input type="hidden" id="dep" name="dep" value="<?php echo $dep;?>">
			<input type="hidden" id="bag" name="bag" value="<?php echo $bag;?>">
			<input type="hidden" id="res" name="res" value="<?php echo $res;?>">
			<input type="hidden" id="tgl" name="tgl" value="<?php echo $tgl;?>">
			<label class="control-label col-md-3 col-sm-6 col-xs-12">
				No Surat
			</label>
			<div class="col-md-5 col-sm-6 col-xs-12">
				<?php 
				if(empty($nosursp)){							
					$thn = date('Y');
					$bln = namaBulanRomawi(date('m'));
					$no = 1;
					if (strlen($no) == 3) { $nol = ''; }elseif (strlen($no) == 2) { $nol = '0'; }elseif (strlen($no) == 1) { $nol = '00'; }
					$nosurat = $nol.$no.'/PER/'.$bln.'/PSM/'.$thn;
				}else{
					$thn_srt = date('Y', strtotime($nosursp['tgl']));
					$thn = date('Y');
					$bln = namaBulanRomawi(date('m'));
					if($thn_srt < $thn){
						$no = 1;
					}else{
						$no = $nosursp['no']+1;
					}
					if (strlen($no) == 3) { $nol = ''; }elseif (strlen($no) == 2) { $nol = '0'; }elseif (strlen($no) == 1) { $nol = '00'; }
					$nosurat = $nol.$no.'/PER/'.$bln.'/PSM/'.$thn;
				}
				?>
				<input type="hidden" name="no" id="no" required class="form-control col-md-7 col-xs-12" value="<?php echo $no; ?>">
				<input type="text" name="nosrt" id="nosrt" readonly required class="form-control col-md-7 col-xs-12" value="<?php echo $nosurat; ?>">
			</div>
		</div>
		<div class="form-group col-md-12">
			<label class="control-label col-md-3 col-sm-6 col-xs-12">
				Peringatan Ke-
			</label>
			<?php
			if(!empty($ke)){
				$bulan = get_object_vars(date_diff(date_create($data[0]['tgl']), date_create($tgl)));
				if($bulan['m']>6){// AND $bulan['d']>0){
					$ke = 0;
				}
			}
			?>
			<div class="col-md-9 col-sm-6 col-xs-12">
				<select name="ke" id="ke" class="form-control select2_single" style="cursor:pointer" <?php echo $ke == 3 ? 'disabled' : '' ?>>
					<option></option>
					<option <?php echo $ke==0?'selected':'' ?> value="1">I</option>
					<option <?php echo $ke==1?'selected':'' ?> value="2">II</option>
					<option <?php echo $ke==2?'selected':'' ?> value="3">III</option>
				</select>
			</div>
		</div>
		<div class="form-group col-md-12">
			<label class="control-label col-md-3 col-sm-6 col-xs-12">
				Keterangan
			</label>
			<div class="col-md-9 col-sm-6 col-xs-12">
				<textarea autofocus id="ketsp" name="ketsp" class="form-group" rows="3"  style="width:100%;" <?php echo $ke == 3 ? 'disabled' : '' ?>></textarea>
			</div>
		</div>
		<div class="form-group col-md-12">
			<hr>
			<div class="col-md-3 col-sm-6 col-xs-12 pull-right">
				<button id="simpan" name="simpan" type="button" class="btn btn-success" <?php echo $ke == 3 ? 'disabled' : '' ?>>
					<i class="glyphicon glyphicon-ok"></i>
					&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;
				</button>
			</div>
		</div>
	</form>
	<?php
	if(!empty($data)){
		?>
		<hr>
		History Surat Peringatan Karyawan:
		<hr>
			<?php
			foreach($data as $d){
				?>
				<table width="100%">
					<tr>
						<td style="border: none;" width="15%">No Surat</td>
						<td style="border: none;"> : <?php echo $d['no_surat']; ?></td>
					</tr>
					<tr>
						<td style="border: none;">Tanggal</td>
						<td style="border: none;"> : <?php echo dateFormat(dateResolver($d['tgl'])); ?></td>
					</tr>
					<tr>
						<td style="border: none;">SP ke</td>
						<?php $bul = get_object_vars(date_diff(date_create($d['tgl']), date_create($tgl))); ?>
						<td style="border: none;"> : <?php echo namaBulanRomawi($d['ke'])." (".$bul['m']." bulan yang lalu)"; ?></td>
					</tr>
					<tr>
						<td style="border: none;vertical-align: top">Ket</td>
						<td align="justify" style="border: none;"> : <?php echo $d['ket']; ?></td>
					</tr>
				</table>
				<div style="float: right">
					<a href="<?php echo url().$d['file'];?>" target="_blank" class="btn btn-default pull-right btn-xs">Lihat SP</a>
				</div>
				<div style="float: left">
					<button title="Hapus" type="button" data-url="<?php echo url('a/surat?_method=delete&id=' . $d['id'] . '&file=' . $d['file'] . '&redirect='.$direct);?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger pull-left btn-xs" style="margin-left: 5px"><i class="fa fa-trash"></i></button>
				</div>
				<br>
				<hr>
				<?php
			}
			?>
		<?php
	}
}else{
	$data = $surat->getById($nik, $ket);
	if(empty($data)){
		?>
		<form class="form-horizontal form-label-left">
			<div class="form-group col-md-12">
				<label class="control-label col-md-5 col-sm-6 col-xs-12">
					No Surat Terakhir
				</label>
				<div class="col-md-5 col-sm-6 col-xs-12">
					<input type="text" class="form-control col-md-7 col-xs-12" value="<?php echo $nosrt['nosurat']; ?>" readonly>
				</div>
			</div>
			<div class="form-group col-md-12">
				<input type="hidden" id="id" name="id" value="<?php echo $nik;?>">
				<input type="hidden" id="ket" name="ket" value="<?php echo $ket;?>">
				<input type="hidden" id="dep" name="dep" value="<?php echo $dep;?>">
				<input type="hidden" id="bag" name="bag" value="<?php echo $bag;?>">
				<input type="hidden" id="res" name="res" value="<?php echo $res;?>">
				<input type="hidden" name="ketsp" id="ketsp" value="">
				<label class="control-label col-md-5 col-sm-6 col-xs-12">
					No Surat
				</label>
				<div class="col-md-5 col-sm-6 col-xs-12">
					<input type="text" name="no" id="no" required  autofocus class="form-control col-md-7 col-xs-12"placeholder="Nomor Surat">
				</div>
			</div>	
			<div class="form-group col-md-12">
				<hr>
				<div class="col-md-3 col-sm-6 col-xs-12 pull-right">
					<button id="simpan" name="simpan" type="button" class="btn btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;
					</button>
				</div>
			</div>
		</form>
		<?php
	}else{
		?>
		<form class="form-horizontal form-label-left">
			<div class="form-group">
				<input type="hidden" id="filee" name="filee" value="<?php echo $data['file'];?>">
				<label class="control-label col-md-8 col-sm-6 col-xs-12">
					Surat Keterangan Sudah Dibuat Sebelumnya
				</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<button title="view" id="view" name="view" type="button" class="btn btn-success">
						<i class="fa fa-search"></i>
					</button>					
                    <button title="Hapus" type="button" data-url="<?php echo url('a/surat?_method=delete&id=' . $data['id'] . '&file=' . $data['file'] . '&redirect='.$direct);?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>
				</div>
			</div>
		</form>
		<?php
	}
?>
<?php
}
?>


<script type="text/javascript">
        $(document).ready(function(){
        	function reload(){
        	    var container = document.getElementById("modalPdf");
        	    var content = container.innerHTML;
        	    container.innerHTML= content; 
        	    
        	   //this line is to watch the result in console , you can remove it later  
        	    console.log("Refreshed"); 
        	}

        	$("#ke.select2_single").select2({
        	    placeholder: "SP Ke",
        	    allowClear: true
        	});


            function resetPDF() {
              $('#simpan').removeClass('buttonDisabled').html('Simpan');
            }
            $('#simpan').click(function() {
              var postData = {
                'id': $('#id').val(),
                'ket': $('#ket').val(),
                'dep': $('#dep').val(),
                'bag': $('#bag').val(),
                'res': $('#res').val(),
                'no': $('#no').val(),
                'nosrt': $('#nosrt').val(),
                'tgl': $('#tgl').val(),
                'ke': $('#ke').val(),
                'ketsp': $('#ketsp').val().replace(/\r\n|\r|\n/g,"<br />"),
              };
              console.log(postData);
              if(postData['ket'] == 'sk'){
              	ur = 'a/pdf_surat_sk';
              }else if(postData['ket'] == 'spk'){
              	ur = 'a/pdf_surat_spk';
              }else if(postData['ket'] == 'disnaker'){
              	ur = 'a/pdf_surat_disnaker';
              }else if(postData['ket'] == 'sp'){
              	ur = 'a/pdf_surat_sp';
              }
              
              $('#simpan').addClass('buttonDisabled').html('<em>Memuat File...</em>');
              $.ajax({
                url: $('#baseurl').val() + ur,
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function(d) {
                  $("#detail_active .close").click();
                  $('#modalPdf').find('#pdf-object').attr('data', d.filepath);
                  $('#modalPdf').modal('show');
                  resetPDF();
                  reload();
                },
                error: function(a,b,c) {
                  console.log('error pdf',a,b,c);
                  resetPDF();
                  reload();
                  <?php //$app->addError('karyawan', 'Maaf, No Surat Sudah dipakai Sebelumnya'); ?>
                  window.location = a.responseText;
                }
              });
            });

            $('#view').click(function() {
              var filepath = $('#filee').val();
              $("#detail_active .close").click();
              $('#modalPdf').find('#pdf-object').attr('data', '../'+filepath);
              $('#modalPdf').modal('show');
              reload();
              console.log(filepath);
            });
        });
    </script>