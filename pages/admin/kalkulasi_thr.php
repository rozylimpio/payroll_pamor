<?php 
require '_base_head.php';
$thn = date('Y');
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Perhitungan THR Karyawan</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" action="<?php echo url('a/kalkulasi_thr');?>"" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="tahun" id="tahun" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <option value="<?php echo $thn-1;?>"><?php echo $thn-1;?></option>
                    <option value="<?php echo $thn;?>"><?php echo $thn;?></option>
                    <!--<option value="<?php //echo $thn+1;?>"><?php //echo $thn+1;?></option>-->
                  </select>
                </div>
                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                  <button name="tinjau" type="submit" id="tinjau" class="btn btn-info">
                    <i class="glyphicon glyphicon-check"></i>
                    &nbsp;Proses&nbsp;
                  </button>
                </div>
              </div>              
            </form>
            
            <?php 
            $defmsg_category = 'kalkulasi_thr';
            require '../pages/defmsg.php'; 
            ?>
             
        </div>
    
    </div>
  </div>
</div>


<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>
<!-- Switchery -->
<script src="<?php echo url();?>js/switchery.min.js"></script>

<!-- Select2 -->
<script>
$(document).ready(function() {

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });
  
  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan Kalkulasi",
      allowClear: true
  });
  
});
</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>