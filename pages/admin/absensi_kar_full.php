<?php 
require '_base_head.php';

$mabs = new \App\Models\Absensi($app);
$mkal = new \App\Models\Kalkulasi($app);
$ys = $mkal->getTahun();

$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();

$mkar = new \App\Models\Karyawan($app);

$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Form Cari Absensi Karyawan</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<form method="get" class="form-horizontal form-label-left" id="f1">
		        <div class="form-group">
			        <div class="col-md-2 col-sm-4 col-xs-12">
		              <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
		                <option></option>
		                <?php foreach($ys as $y) { ?>
		                <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
		                  <?php echo $y['tahun']?></option>
		                <?php } ?>
		              </select>
		            </div>
		            <div class="col-md-2 col-sm-4 col-xs-12">
		              <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
		                <option></option>
		                <?php for ($i=1; $i < 13; $i++) { ?>
		                <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
		                    <?php echo namaBulan($i);?>
		                </option>
		                <?php }?>
		              </select>
		            </div>
			     </div>
			     <hr>
				  <div class="form-group">
				    <div class="col-md-3 col-sm-6 col-xs-12">
				      <select name="departemen" id="departemen" class="form-control select2_single" required  style="cursor:pointer">
				        <option value=""></option>
				        <?php foreach($deps as $dep) { ?>
				        <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
				          <?php echo $dep['nm_departemen']?></option>
				        <?php } ?>
				        <!-- tampil tujuan sesuai pemberangkatan -->
				      </select>  
				    </div>         
				    <div class="col-md-3 col-sm-6 col-xs-12">
				      <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
				      <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
				        <option></option>
				      </select>
				    </div>
				    <div class="col-md-2 col-sm-6 col-xs-12">
				      <select name="jam" id="jam" class="form-control select2_single" style="cursor:pointer">
				        <option></option>
				        <?php foreach($jams as $jam) { ?>
				        <option <?php echo isset($_GET['jam']) && $_GET['jam'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
				        <?php }?>
				      </select>
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="col-md-2 col-sm-3 col-xs-12">
				      <select name="jgrup" id="jgrup" class="form-control select2_single" style="cursor:pointer">
				        <option></option>
				        <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '7' ? 'selected' : '' ;?> value='7'>7 Grup</option>
				        <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '3' ? 'selected' : '' ;?> value='3'>3 Grup</option>
				        <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '4' ? 'selected' : '' ;?> value='4'>3 Grup QC Lab</option>
				        <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '2' ? 'selected' : '' ;?> value='2'>2 Grup</option>
				        <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '1' ? 'selected' : '' ;?> value='1'>Pagi</option>
				      </select>
				    </div>
				    <div class="col-md-2 col-sm-3 col-xs-12">
				      <select name="grup" id="grup" class="form-control select2_single" style="cursor:pointer">
				        <option></option>
				      </select>
				    </div>
				    <div class="col-md-2 col-sm-3 col-xs-12">
				      <select name="libur" id="libur" class="form-control select2_single" style="cursor:pointer">
				        <option></option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '1' ? 'selected' : '' ;?> value='1'>1</option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '2' ? 'selected' : '' ;?> value='2'>2</option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '3' ? 'selected' : '' ;?> value='3'>3</option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '4' ? 'selected' : '' ;?> value='4'>4</option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '5' ? 'selected' : '' ;?> value='5'>5</option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '6' ? 'selected' : '' ;?> value='6'>6</option>
				        <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '7' ? 'selected' : '' ;?> value='7'>7</option>
				      </select>
				    </div>
				  </div>
				  <div class="row"></div><hr>
				  <div class="form-group col-md-11 col-sm-11 col-xs-11">
				    <button name="tinjau" type="submit" class="btn btn-info">
				      <i class="glyphicon glyphicon-search"></i>
				      &nbsp;Cari&nbsp;
				    </button>
				  </div>
				</form> 
				<?php
				if(isset($_GET['tinjau'])){
				  $data = [];
				  $data['tahun'] = $app->input->get('tahun');
				  $data['bulan'] = $app->input->get('bulan');
				  $data['departemen'] = $app->input->get('departemen');
				  $data['bagian'] = $app->input->get('bagian');
				  $data['jgrup'] = ''.$app->input->get('jgrup');
				  $data['grup'] = null.$app->input->get('grup');
				  $data['libur'] = null.$app->input->get('libur');
				  $data['jamkerja'] = $app->input->get('jam');

				  $data['tgl_ak'] = $data['tahun'].'-'.$data['bulan'].'-20';
				  $blnn = date('m', strtotime("-1 month", strtotime($data['tgl_ak'])));
				  $thnn = date('Y', strtotime("-1 month", strtotime($data['tgl_ak'])));
				  $data['tgl_aw'] = $thnn.'-'.$blnn.'-21';

				  //echo $tgl_aw.' - '.$tgl_ak;
				  //print_r($data);
				  $list = $mabs->getDataAbsFull($data);
          //print_r($list);

          //$jmld = get_object_vars(date_diff(date_create($data['tgl_aw']), date_create($data['tgl_ak'])));
          $jmlhari = count($list[0]);//$jmld['d'] + 1; //+ 1 dimaksudkan untuk menambah 1 hari di tanggal 21

				?>
				<div class="ln_solid"></div>

				<table class="display table-bordered table-hover table-striped" id="myTablett">
					<thead>
            <th>NO</th>
						<th>NIK</th>
						<th style="width: 150px">NAMA</th>
						<?php
						for($i=0; $i < $jmlhari; $i++)
						{
							echo "<th>". date('d', strtotime("+". $i ." day", strtotime($data['tgl_aw']))) ."</th>";
						}
						?>
            <th>Detail</th>
					</thead>
					<tbody>
          <?php
          $n=1;
          foreach ($list as $index => $d) {
            $jum = 0;
          ?>
					<tr>
            <td><?php echo $n;?></td>
						<td><?php echo $d[0]['nik']; ?></td>
						<td><?php echo $d[0]['nama']; ?></td>

					<?php
					for($i=0; $i < $jmlhari; $i++)
					{
						if($i % $jmlhari == 0 && $i != 0)
						{
						echo "</tr>";
						echo "<tr>";
						}
            if($d[$i]['kehadiran']=='libur'){
						  echo "<td align='center' style='background:DarkGray'>";
            }else{
              echo "<td align='center' style='font-size:11px'>";
            }

            if(!empty($d[$i]['jadwal_shift']) || !empty($d[$i]['u_makan'])){
              if(!empty($d[$i]['jadwal_shift'])){
                if($d[$i]['jadwal_shift']=='Pagi'){
                  $jdwl = 'P';
                }elseif($d[$i]['jadwal_shift']=='Siang'){
                  $jdwl = 'S';
                }elseif($d[$i]['jadwal_shift']=='Malam'){
                  $jdwl = 'M';
                }
                echo "<div style='font-size:7px;' class='pull-left'>".$jdwl."</div>";
              }
              if(!empty($d[$i]['u_makan'])){
                echo "<div style='font-size:7px;' class='pull-right'>".$d[$i]['u_makan']."</div>";
                $jum = $jum + $d[$i]['u_makan'];
              }
              echo "<br>";
            }else{
              echo "<br>";
            }

            if($d[$i]['kehadiran']=='masuk'){
              echo "<div style='color:RED'><b>";
              if($d[$i]['ijin']=='tmk'){
                  echo "TM ";
                  //munculkan menit dan jam ijin
                  $jamtmk = $d[$i]['t_ijin'];
                  $tltj = carijam($jamtmk);
                  $tltm = carimenit($jamtmk, $tltj);
                  empty($tltm) ? $menitlmt = 0 : $menitlmt = 0.5;
                  $jtmk = $tltj+$menitlmt;
                  echo $jtmk;
              }elseif($d[$i]['ijin']=='mtk'){
                  echo "MT ";
                  //munculkan menit dan jam ijin
                  $jammtk = $d[$i]['t_ijin'];
                  $tlmj = carijam($jammtk);
                  $tlmm = carimenit($jammtk, $tlmj);
                  empty($tlmm) ? $menitlmm = 0 : $menitlmm = 0.5;
                  $jmtk = $tlmj+$menitlmm;
                  echo $jmtk;
              }elseif($d[$i]['ijin']=='psw'){
                  echo "P ";
                  //munculkan menit dan jam ijin
                  $jampsw = $d[$i]['t_ijin'];
                  $tlpj = carijam($jampsw);
                  $tlpm = carimenit($jampsw, $tlpj);
                  empty($tlpm) ? $menitlmp = 0 : $menitlmp = 0.5;
                  $jpsw = $tlpj+$menitlmp;
                  echo $jpsw;
              }
              echo "</b></div>";
            }else{
              echo "<div style='color:RED'><b>";
              if($d[$i]['kehadiran']=='alpha'){
                echo "A";
              }elseif($d[$i]['kehadiran']=='ijin'){
                echo "I";
              }elseif($d[$i]['kehadiran']=='sdr'){
                echo "SDR";
              }elseif($d[$i]['kehadiran']=='cuti'){
                echo "C";
              }elseif($d[$i]['kehadiran']=='cutihamil'){
                echo "CH";
              }elseif($d[$i]['kehadiran']=='kk'){
                echo "KK";
              }elseif($d[$i]['kehadiran']=='cuti_r'){
                echo "CR";
              }elseif($d[$i]['kehadiran']=='alpha_r'){
                echo "AR";
              }
              echo "</b></div>";
              if($d[$i]['kehadiran']=='dirumahkan'){
                echo "<b>R</b><br>";
              }
            }


            if($d[$i]['t_lembur']>0){
              $jaml = $d[$i]['t_lembur'];
              $tllj = carijam($jaml);
              $tllm = carimenit($jaml, $tllj);
              empty($tllm) ? $menitlm = 0 : $menitlm = 0.5;
              $jl = $tllj+$menitlm;
              echo "<br><div style='font-size:10px;' class='pull-right'>".$jl."</div>";
            }else{
              echo "<br>";
            }
            echo "</td>";
					}
					?>
            <td>
            <a title="Detail" data-toggle="modal" herf="#" data-target="#detail_active" data-idkar="<?php echo $d[0]['nik'];?>" data-tahun="<?php echo $data['tahun']; ?>" data-bulan="<?php echo $data['bulan']; ?>" data-tmakan="<?php echo $jum; ?>" class="btn btn-info btn-xs"><i class="fa fa-search"></i></a>
            </td>
					</tr>
          <?php  $n++; } ?>
					</tbody>
				</table>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<!--Modal Detail BEGIN============================================================-->
<div id="detail_active" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modalPdfLabel">Detail Absensi</h4>
      </div>
      <div class="modal-body">

        <div class="hasil-data"></div>
        
      </div>
    </div>  
  </div>
</div>
<!--Modal Detail END============================================================-->

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<script>
$(document).ready(function() {

	var table = $('#myTablett').dataTable( {
          scrollX: true,
          "paging": false
         });
	$('#myTablett tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('row_selected') ) {
          $(this).removeClass('row_selected');
      }
      else {
          table.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
      }
    });

});
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $('#detail_active').on('show.bs.modal', function (e) {
                var idkar = $(e.relatedTarget).data('idkar');
                var thn = $(e.relatedTarget).data('tahun');
                var bln = $(e.relatedTarget).data('bulan');
                var jum = $(e.relatedTarget).data('tmakan');
                // console.log($(e.relatedTarget).data());
                $.ajax({
                    type : 'post',
                    url : 'absensi_kar_full_detail',
                    data :  'idkar='+ idkar+'&thn='+ thn+'&bln='+ bln+'&jum='+ jum,
                    success : function(data){
                    $('.hasil-data').html(data);
                    }
                });
            });
        });
    </script>
<script>
  $(document).ready(function() {
    var emptyOption = '<option value=""></option>';
    var bags = JSON.parse('<?php echo json_encode($bags);?>');


    $("#departemen.select2_single").select2({
      placeholder: "Pilih Departemen",
      allowClear: true
    });
    $("#jgrup.select2_single").select2({
      placeholder: "Jumlah Grup",
      allowClear: true,
      disabled: <?php echo !empty($_GET['jgrup']) ? 'false' : 'true'; ?>
    });
    $("#grup.select2_single").select2({
      placeholder: "Grup",
      allowClear: true,
      disabled: <?php echo !empty($_GET['grup']) ? 'false' : 'true'; ?>
    });
    $("#libur.select2_single").select2({
      placeholder: "Pilih Hari Libur",
      allowClear: true,
      disabled: <?php echo !empty($_GET['libur']) ? 'false' : 'true'; ?>
    });
    $("#jam.select2_single").select2({
      placeholder: "Jam Kerja",
      allowClear: true,
      disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
    });



    
    $("#departemen.select2_single").select2({
        placeholder: "Pilih Departemen",
        allowClear: true
    }).on('change', function(e) {
      var kd_departemen = e.currentTarget.value;
      var list_bagiann = _.filter(bags, {kd_departemen:kd_departemen});
      
      $('#bagian').html(emptyOption);
      $(list_bagiann).each(function(i, item) {
        $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
      });

      $("#bagian.select2_single").select2('destroy').select2({
        placeholder: "Pilih Bagian",
        allowClear: true,
        disabled: false
      });
      $("#jam.select2_single").select2({
        placeholder: "Jam Kerja",
        allowClear: true,
        disabled: false
      });
    });



    $("#bagian.select2_single").select2({
      placeholder: "Pilih Bagian",
      allowClear: true,
      disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
    }).show(function(e){
      var bagi = document.getElementById('bagi').value;
      var ed = document.getElementById('departemen');
      var kd_departemen = ed.options[ed.selectedIndex].value;
      var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
      $('#bagian').html(emptyOption);
      $(list_bagian).each(function(i, item) {
        $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
      });
    });

    $("#jam.select2_single").select2({
        placeholder: "Jam Kerja",
        allowClear: true
    }).on('change', function(e){
      var kd_jamkerja = e.currentTarget.value;

      if(kd_jamkerja==2){
        $("#jgrup.select2_single").select2('destroy').select2({
          placeholder: "Jumlah Grup",
          allowClear: true,
          disabled: false
        });
      }else{
        $("#jgrup.select2_single").select2('destroy').select2({
          placeholder: "Jumlah Grup",
          allowClear: true,
          disabled: true
        });
        $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: true
        });
        $("#grup.select2_single").select2('destroy').select2({
          placeholder: "Pilih Grup",
          allowClear: true,
          disabled: true
        });
      }
    });




    $("#jgrup.select2_single").select2({
      placeholder: "Jumlah Grup",
      allowClear: true
    }).on('change', function(e) {
      var jgrup = e.currentTarget.value;
      if(jgrup>2){
        $("#grup.select2_single").select2('destroy').select2({
          placeholder: "Pilih Grup",
          allowClear: true,
          disabled: false
        });
        $('#grup').html(emptyOption);
        for(i=0;i<jgrup;i++){
          $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '">' + (i+10).toString(36).toUpperCase() + '</option>');
        }
        if(jgrup==4){
          $("#grup").append('<option value="Rp">Repliver</option>');
        }
      }else if(jgrup==2){
        $("#grup.select2_single").select2('destroy').select2({
          placeholder: "Jadwal Awal Masuk",
          allowClear: true,
          disabled: false
        });
        $("#tgl_m").show();
        $('#grup').html(emptyOption);
        $("#grup").append('<option value="Pagi">Pagi</option>');
        $("#grup").append('<option value="Siang">Siang</option>');
      }else if(jgrup==1){
        $("#grup.select2_single").select2('destroy').select2({
          placeholder: "Pilih Grup",
          allowClear: true,
          disabled: false
        });
        var p = 'Pagi';
        $('#grup').html(emptyOption);
        $("#grup").append('<option value="' + p + '">' + p + '</option>');
      }

      if(jgrup>4){
        $('#libur').html(emptyOption);
        $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: true
        });
        $("#grup.select2_single").select2('destroy').select2({
          placeholder: "Pilih Grup",
          allowClear: true,
          disabled: false
        });
      }else{
        $("#libur.select2_single").select2('destroy').select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: false
        });
      }
    });

    var edit = JSON.parse('<?php echo json_encode($_GET);?>');
    $("#grup.select2_single").select2({
      placeholder: "Pilih Grup",
      allowClear: true
    }).show(function(e){
      var ed = document.getElementById('jgrup');
      var jgrup = ed.options[ed.selectedIndex].value;          
      //console.log(edit);
      
      if(jgrup>2){
        $('#grup').html(emptyOption);
        for(i=0;i<jgrup;i++){
          $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '" '+((i+10).toString(36).toUpperCase() == edit['grup'] ? 'selected' : '') +'>' + (i+10).toString(36).toUpperCase() + '</option>');
        }
      }else if(jgrup==2){
        var kg = 'I';
        $('#grup').html(emptyOption);
        for(i=1;i<=jgrup;i++){
          $("#grup").append('<option value="' + kg + '" '+(kg == edit['grup'] ? 'selected' : '') +'>' + kg + '</option>');
          kg = kg+kg;
        }
      }else if(jgrup==1){
        var p = 'Pagi';
        $('#grup').html(emptyOption);
        $("#grup").append('<option value="' + p + '" '+(p == edit['grup'] ? 'selected' : '') +'>' + p + '</option>');
      }
    });

    $("#tahun.select2_single").select2({
        placeholder: "Pilih Tahun",
        allowClear: true
    });

    $("#bulan.select2_single").select2({
        placeholder: "Pilih Bulan",
        allowClear: true
    });


  });
</script>
<?php require '_base_foot.php';?>