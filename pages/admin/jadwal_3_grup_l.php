<?php 
require '_base_head.php';
// $mjadwal = new \App\Models\Jadwal3GrupU($app);
// $jadwals = $mjadwal->get();
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Form Jadwal Shift 3 Grup Bagian QC LAB</h2>
          <div class="clearfix">
          </div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/jadwal_3_grup_l')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'jadwal';
        require '../pages/defmsg.php';
        /*
        $data = 10;
        $maxdata = strtoupper(chr(96+$data));//97 = huruf A

        for($i='A'; $i<=$maxdata; $i++){
          echo $i." ";
        }*/
        ?>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tanggal Mulai
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="tglm" id="tglm" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Berlaku Mulai">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Grup
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="grup" id="grup" class="form-control select2_single" style="cursor:pointer;width:100%">
              <option></option>
              <option value='A'>A</option>
              <option value='B'>B</option>
              <option value='C'>C</option>
              <option value='RP'>Repliver</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Shift
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="shift" id="shift" class="form-control select2_single" style="cursor:pointer;width:100%">
              <option></option>
              <option value='Pagi'>Pagi</option>
              <option value='Siang'>Siang</option>
              <option value='Malam'>Malam</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tanggal Berlaku
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Berlaku Mulai">
          </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->
        <?php 
        //print_r($jadwals[1]); //die(); ?>
        <!-- table -->
        <hr>
        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
          <div class="table-responsive" align="center">
            <table class="table table-bordered table-hover table-striped" id="example" style="width: 100%">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Bulan</th>
                </tr>
            </thead>
            </table>
          </div>
        </div>
        <!-- table -->

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<script>
  $(document).ready(function() {
    
    $(":input").inputmask();

    $("#shift.select2_single").select2({
        placeholder: "Pilih Shift",
        allowClear: true
    });
    $("#grup.select2_single").select2({
        placeholder: "Pilih Grup",
        allowClear: true
    });

  });
  function format ( d ) {
      // `d` is the original data object for the row
      
      //console.log(d.jadwal['0']);
      var trs='';
      $.each($(d.jadwal),function(key,value){
        trs+='<tr><td>'+value.tgl+'</td><td>'+value.grup+'</td><td>'+value.shift+'</td><td>'+value.berlaku+'</td></tr>';
      })

      return '<table id="detail" class="table  table-bordered table-hover table-striped table-responsive">'+
        '<thead>'+
          '<tr>'+
              '<th>Tanggal Awal Akhir</th>'+
              '<th>Grup</th>'+
              '<th>Shift</th>'+
              '<th>Berlaku Mulai</th>'+
          '</tr>'+
        '</thead>'+
        '<tbody>'+
          trs+
        '</tbody>'+
      '</table>';
  }
   
  $(document).ready(function() {
      var table = $('#example').DataTable( {
          "columnDefs": [
                          { "width": "5%", "targets": 0 }
                        ],
          "lengthMenu": [20],
          "ajax": "object_l",
          "columns": [
              { 
                "className":      'details-control',
                "data": "no" 
              },
              { 
                "className":      'details-control',
                "data": "bulan" 
              }
          ]
      } );
       
      $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
  
        if ( row.child.isShown() ) {
            row.child.hide();
        }
        else {
            row.child( format(row.data()) ).show();
            MergeGridCells();
        }
    } );

      function MergeGridCells() {
      var dimension_cells = new Array();
      var dimension_col = null;
      var columnCount = $("#detail tr:first th").length;

      // first_instance holds the first instance of identical td
      var first_instance = null;
      var rowspan = 1;
      // iterate through rows
      $("#detail").find('tr').each(function () {

          // find the td of the correct column (determined by the dimension_col set above)
          var dimension_td = $(this).find('td:nth-child(' + 1 + ')');

          if (first_instance == null) {
              // must be the first row
              first_instance = dimension_td;
          } else if (dimension_td.text() == first_instance.text()) {
              // the current td is identical to the previous
              // remove the current td
              dimension_td.remove();
              ++rowspan;
              // increment the rowspan attribute of the first instance
              first_instance.attr('rowspan', rowspan);
          } else {
              // this cell is different from the last
              first_instance = dimension_td;
              rowspan = 1;
          }
      });
  }
  } );
</script>
<?php require '_base_foot.php';?>