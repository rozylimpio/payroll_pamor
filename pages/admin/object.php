
<?php
$mjadwal = new \App\Models\Jadwal($app);
$jadwals = $mjadwal->get();
$i = 1;
$jml = count($jadwals);
//print_r($jadwals[1]); die();
echo '
{
  "data":[
    ';
    foreach ($jadwals as $key => $value) { 
    echo '{ 
        "no":"'.$key.'",
        "bulan": "'.namaBulan($key).'",';
        $jmll = count($jadwals[$key]);
        echo '"jadwal":[';
        for($j=0;$j<count($jadwals[$key]);$j++){
        echo '{
          "tgl":"'.dateFormat(dateResolver($jadwals[$key][$j]['tglaw'])).' - '.dateFormat(dateResolver($jadwals[$key][$j]['tglak'])).'",
          "grup":"'.$jadwals[$key][$j]['grup'].'",
          "shift":"'.$jadwals[$key][$j]['shift'].'", 
          "berlaku":"'.dateFormat(dateResolver($jadwals[$key][$j]['berlaku'])).'"
        }';
        echo $j+1 == $jmll ? '' : ',';    
        }
    echo ']}';
    echo $i == $jml ? '' : ',';
    $i += 1;
    }
    echo '
  ]
}';
?>