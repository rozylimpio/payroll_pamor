<?php 
require '_base_head.php';
$mper = new \App\Models\Persen($app);
$persen = $mper->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mper->getById($app->input->get('edit'));
}

$redirect = url('a/persen');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Persentase</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <?php if(count($persen)!=6 || !empty($edit)){ ?>
        <form action="<?php echo url('a/persen' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'persen';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Keterangan
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="ket" id="keterangan" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?= $edit['ket']=='diliburkan' ? 'selected' : '';?> value="diliburkan">Diliburkan</option>
              <option <?= $edit['ket']=='rumah' ? 'selected' : '';?> value="rumah">Rumah</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Status Karyawan
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="status" id="status" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?= $edit['status']=='0' ? 'selected' : '';?> value="0">Semua</option>
              <option <?= $edit['status']=='1' ? 'selected' : '';?> value="1">Kontrak</option>
              <option <?= $edit['status']=='2' ? 'selected' : '';?> value="2">Tetap</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Persentase
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="persen" id="persen" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? 100-$edit['persentase'] : '' ;?>">
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php } if(!$edit) { ?>
        <?php
          $defmsg_category = 'persen_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>keterangan</th>
                      <th>Status</th>
                      <th>Persentase</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($persen as $index => $per) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?= ucfirst($per['ket']);?></td>
                      <td><?= $per['status']==0 ? 'Semua':$per['status']==1 ? 'Kontrak':'Tetap';?></td>
                      <td><?= 100-$per['persentase'].'%'; ?></td>
                      <td>
                        <a href="<?php echo url('a/persen?edit=' . $per['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('a/persen?_method=delete&id=' . $per['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $("#keterangan.select2_single").select2({
            placeholder: "Pilih Keterangan",
            allowClear: true
        });
      });
      $(document).ready(function() {
        $("#status.select2_single").select2({
            placeholder: "Pilih Status Karyawan",
            allowClear: true
        });
      });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>