<?php 
require '_base_head.php';
$mjadwal = new \App\Models\Jadwal7Grup($app);
$jadwals = $mjadwal->get();
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Form Jadwal 7 Grup Bagian Produksi</h2>
          <div class="clearfix">
          </div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/jadwal_7_grup')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'jadwal';
        require '../pages/defmsg.php';
        /*
        $data = 10;
        $maxdata = strtoupper(chr(96+$data));//97 = huruf A

        for($i='A'; $i<=$maxdata; $i++){
          echo $i." ";
        }*/
        ?>
        <!--
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jumlah Grup
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input type="number" name="jml" id="jml" required class="form-control col-md-7 col-xs-12" placeholder="Jumlah">
          </div>
        </div>
        -->
        <input type="hidden" name="jml" id="jml" value="7">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tanggal Berlaku
          </label>
          <div class="col-md-3 col-sm-4 col-xs-6">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Berlaku Mulai">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Shift
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="shift" id="shift" required class="form-control col-md-7 col-xs-12" value="Pagi" readonly>
            <!--<select name="shift" id="shift" class="form-control select2_single" style="cursor:pointer;width:100%">
              <option></option>
              <option value='Pagi'>Pagi</option>
              <option value='Siang'>Siang</option>
              <option value='Malam'>Malam</option>
            </select>-->
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Grup
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
              <option></option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
              <option></option>
            </select>
          </div>
        </div>
        <!--
        <div class="form-group" id="bp1" style="display: block">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            
          </label>
          <div class="col-md-3 col-sm-5 col-xs-12">
            <button onclick="toggle_visibility('bp1')" type="button" class="btn btn-round btn-info btn-xs" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><i class="fa fa-plus" style="cursor: pointer"></i></button><i>tambah grup</i>
          </div>
        </div>
        <div class="collapse" id="collapse1">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
              Grup
            </label>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
                <option></option>
              </select>
            </div>
            <button type='button' class='btn btn-round btn-danger btn-xs' onclick="toggle_visibility('bp1')" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><i class='fa fa-times'></i></button><i>hapus grup</i>
          </div>
          <div class="form-group" id="bp2" style="display: block">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            
            </label>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <button onclick="toggle_visibility('bp2')" type="button" class="btn btn-round btn-info btn-xs" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><i class="fa fa-plus" style="cursor: pointer"></i></button><i>tambah grup</i>
            </div>
          </div>
        </div>        
        <div class="collapse" id="collapse2">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
              Grup
            </label>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
                <option></option>
              </select>
            </div>
            <button type='button' class='btn btn-round btn-danger btn-xs' onclick="toggle_visibility('bp2')" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><i class='fa fa-times'></i></button><i>hapus grup</i>
          </div>
          <div class="form-group" id="bp3" style="display: block">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
              
            </label>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <button onclick="toggle_visibility('bp3')" type="button" class="btn btn-round btn-info btn-xs" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><i class="fa fa-plus" style="cursor: pointer"></i></button><i>tambah grup</i>
            </div>
          </div>
        </div>        
        <div class="collapse" id="collapse3">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
              Grup
            </label>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
                <option></option>
              </select>
            </div>
            <button type='button' class='btn btn-round btn-danger btn-xs' onclick="toggle_visibility('bp3')" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><i class='fa fa-times'></i></button><i>hapus grup</i>
          </div>
        </div> 
        -->
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>

        <hr>
        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
          <div class="table-responsive" align="center">
            <table class="table table-bordered table-hover table-striped" id="Table" style="width: 100%">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Hari</th>
                    <th>Grup</th>
                    <th>Shift</th>
                    <th>Berlaku</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if(!empty($jadwals)){
                foreach($jadwals as $index => $jadwal) { 
                ?>
                <tr>
                    <td><?php echo $index+1;?></td>
                    <td><?php echo namaHari($jadwal['harike']) ?></td>
                    <td><?php echo $jadwal['grup'] ?></td>
                    <td><?php echo $jadwal['shift'] ?></td>
                    <td><?php echo dateFormat(dateResolver($jadwal['berlaku'])); ?></td>
                </tr>
                <?php }} ?>
            </tbody>
            </table>
          </div>
        </div>
        <!-- table -->

        <?php
        /*
        if(isset($_POST['simpan'])){
          $jml = $_POST['jml'];
          $tgl = dateCreate($_POST['tgl']);
          $shift = $_POST['shift'];
          $grup = $_POST['grup'];

          $n = 6;
          $tglaw = strtotime($tgl);
          $day = date('l', strtotime($tgl));
          $dayke = date('N', strtotime($tgl));
          $tglp = date('Y-m-d', strtotime('+'.$n.' days', strtotime($tgl)));
          $tglak = strtotime($tglp);
          $dayp = date('l', strtotime($tglp));
          $daypke = date('N', strtotime($tglp));

          $grup = array_filter($grup);
          //print_r($grup); die();
          if($shift == 'Pagi'){
            $shift = array('Pagi', 'Siang', 'Malam', 0);
          }elseif($shift == 'Siang'){
            $shift = array('Siang', 'Malam', 'Pagi', 1);
          }elseif($shift == 'Malam'){
            $shift = array('Malam', 'Pagi', 'Siang', 1);
          }

          for ($is=0; $is < count($shift); $is++) { 
            for ($ig=0; $ig < count($grup); $ig++) { 
              $shift_p[] = $shift[$is];
            }
          }

          //print_r($shift_p);die();

          $a = 0; $b = $grup[0]; $c=0;
          do {    
            for($j=0; $j<(count($grup)*3);$j++){
              $hari[$a][$j]['tgl'] = $tgl;
              if($b<=$jml){
                $maxdata = strtoupper(chr(96+$b));
                $hari[$a][$j]['grup'] = $maxdata;
              }
              $b==($jml) ? $b=1 : $b+=1;
              //$hari[$a][$j][] = date('Y-m-d', $tglaw);
              $hari[$a][$j]['harike'] = date('N', $tglaw);
              if ($c<(count($shift_p)-count($grup))) {
                $hari[$a][$j]['shift'] = $shift_p[$c]."<br>";
              }
              $c==count($shift_p)-(count($grup)+1) ? $c=0 : $c+=1;
            }
            $a+=1;
            $tglaw = strtotime('+1 day', $tglaw);
          } while ($tglaw<=$tglak);
          
          print_r($hari);

          echo "<br><br>Tanggal : ".$tgl.", hari : ".$day." hari ke - ".$dayke."<br> tambah ".$n." hari menjadi :<br> Tanggal :  ".$tglp.", hari : ".$dayp." hari ke - ".$daypke;
        }
        */
        ?>

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<script>
  /*function toggle_visibility(id) {
     var e = document.getElementById(id);
     if(e.style.display == 'block')
        e.style.display = 'none';
     else
        e.style.display = 'block';
  }*/

  $(document).ready(function() {
    
    $(":input").inputmask();

    /*$("#shift.select2_single").select2({
        placeholder: "Pilih Shift",
        allowClear: true
    });*/
    $("#grup.select2_single").select2({
        placeholder: "Pilih Grup",
        allowClear: true
    });

    var emptyOption = '<option value=""></option>';
    $("#jml").show(function(e){
      var jml = document.getElementById('jml').value;
      var i;

      $('#grup.c').html(emptyOption);

      for(i=1; i<= jml; i++) {
        $("#grup.c").append('<option value="'+i+'">'+(i+9).toString(36).toUpperCase()+'</option>');
      }
    });

   $('#Table').dataTable({
    "pageLength": 42,
    "ordering": false,
    "lengthChange": false
   });
   MergeGridCells();

  });

  function MergeGridCells() {
      var dimension_cells = new Array();
      var dimension_col = null;
      var columnCount = $("#Table tr:first th").length;

      // first_instance holds the first instance of identical td
      var first_instance = null;
      var rowspan = 1;
      // iterate through rows
      $("#Table").find('tr').each(function () {

          // find the td of the correct column (determined by the dimension_col set above)
          var dimension_td = $(this).find('td:nth-child(' + 2 + ')');

          if (first_instance == null) {
              // must be the first row
              first_instance = dimension_td;
          } else if (dimension_td.text() == first_instance.text()) {
              // the current td is identical to the previous
              // remove the current td
              dimension_td.remove();
              ++rowspan;
              // increment the rowspan attribute of the first instance
              first_instance.attr('rowspan', rowspan);
          } else {
              // this cell is different from the last
              first_instance = dimension_td;
              rowspan = 1;
          }
      });
  }
</script>
<?php require '_base_foot.php';?>