<?php
$absensi = new \App\Models\Absensi($app);

$thn = $app->input->post('thn'); 
$bln = $app->input->post('bln');
$tbl = $app->input->post('tbl');
$dir = $app->input->post('dir');
$bag = $app->input->post('bag');
$id = null;

$abs = $absensi->getFile($thn, $bln, $bag, $tbl, $id);
if($abs){
	echo "<button title='Hapus' type='button' data-url='".url('a/delete_file?_method=delete&id=' . $abs['id'] . '&tbl=' . $tbl.'&dir=' . $dir)."' data-toggle='modal' data-target='#confirm_delete' class='btn btn-danger' style='margin-left: 5px'><i class='fa fa-trash'></i>&nbsp;Hapus File&nbsp;</button>";
}
?>