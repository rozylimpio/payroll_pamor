<?php 
require '_base_head.php';

$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();

$mkar = new \App\Models\Karyawan($app);

$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();
?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Geser/Pindah Libur Karyawan</h2>
                    <div class="clearfix"><br></div> 
                  </div>
                  <div class="x_content">
                    <br />
                    <form method="get" class="form-horizontal form-label-left" id="f1">
                      <div class="form-group">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <select name="departemen" id="departemen" class="form-control select2_single" required  style="cursor:pointer">
                            <option value=""></option>
                            <?php foreach($deps as $dep) { ?>
                            <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
                              <?php echo $dep['nm_departemen']?></option>
                            <?php } ?>
                            <!-- tampil tujuan sesuai pemberangkatan -->
                          </select>  
                        </div>         
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
                          <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                          <select name="jam" id="jam" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <?php foreach($jams as $jam) { ?>
                            <option <?php echo isset($_GET['jam']) && $_GET['jam'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
                            <?php }?>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                          <select name="pilihan" id="pilihan" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <option <?php echo isset($_GET['pilihan']) && $_GET['pilihan'] == 'all' ? 'selected' : '' ;?> value='all'>Semua Karyawan</option>
                            <option <?php echo isset($_GET['pilihan']) && $_GET['pilihan'] == 'not' ? 'selected' : '' ;?> value='not'>Pilih Manual</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2 col-sm-3 col-xs-12">
                          <select name="jgrup" id="jgrup" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '7' ? 'selected' : '' ;?> value='7'>7 Grup</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '3' ? 'selected' : '' ;?> value='3'>3 Grup</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '4' ? 'selected' : '' ;?> value='4'>3 Grup QC Lab</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '2' ? 'selected' : '' ;?> value='2'>2 Grup</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '1' ? 'selected' : '' ;?> value='1'>Pagi</option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                          <select name="grup" id="grup" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                          <select name="libur" id="libur" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '1' ? 'selected' : '' ;?> value='1'>1</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '2' ? 'selected' : '' ;?> value='2'>2</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '3' ? 'selected' : '' ;?> value='3'>3</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '4' ? 'selected' : '' ;?> value='4'>4</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '5' ? 'selected' : '' ;?> value='5'>5</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '6' ? 'selected' : '' ;?> value='6'>6</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '7' ? 'selected' : '' ;?> value='7'>7</option>
                          </select>
                        </div>
                      </div>
                      <div class="row"></div><hr>
                      <div class="form-group">
                        <div class="col-md-8 col-sm-8 col-xs-6">
                          <button name="tinjau" type="submit" class="btn btn-info">
                            <i class="glyphicon glyphicon-search"></i>
                            &nbsp;Cari&nbsp;
                          </button>
                        </div>
                      </div> 
                    </form>
                      <?php 
                      $defmsg_category = 'geser';
                      require '../pages/defmsg.php'; 
                      

                      if(isset($_GET['tinjau'])){
                        ?>
                        <form method="post" id="f2">
                        <?php
                        $dep = $app->input->get('departemen');
                        $bag = $app->input->get('bagian');
                        $jgrup = ''.$app->input->get('jgrup');
                        $grup = null.$app->input->get('grup');
                        $libur = null.$app->input->get('libur');
                        $jamkerja = $app->input->get('jam');
                        $pilihan = $app->input->get('pilihan');

                        if($pilihan=='all'){
                        ?>
                        <hr>
                          <input type="hidden" name="dep" value="<?php echo $dep ?>">
                          <input type="hidden" name="bag" value="<?php echo $bag ?>">
                          <input type="hidden" name="jgrup" value="<?php echo $jgrup ?>">
                          <input type="hidden" name="grup" value="<?php echo $grup ?>">
                          <input type="hidden" name="libur" value="<?php echo $libur ?>">
                          <input type="hidden" name="jamkerja" value="<?php echo $jamkerja ?>">
                          <input type="hidden" name="pilihan" value="<?php echo $pilihan ?>">
                        <?php
                        }elseif($pilihan=='not'){
                          $data['departemen'] = $dep;
                          $data['bagian'] = $bag;
                          $data['jgrup'] = $jgrup;
                          $data['grup'] = $grup;
                          $data['libur'] = $libur;
                          $data['jamkerja'] = $jamkerja;

                          $list = $mkar->getByData($data);
                        ?>
                          <div class="ln_solid"></div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                              <h2 class="StepTitle">
                                Daftar Karyawan
                              </h2>
                              <br>
                              <input type="hidden" name="dep" value="<?php echo $dep ?>">
                              <input type="hidden" name="bag" value="<?php echo $bag ?>">
                              <input type="hidden" name="jgrup" value="<?php echo $jgrup ?>">
                              <input type="hidden" name="grup" value="<?php echo $grup ?>">
                              <input type="hidden" name="libur" value="<?php echo $libur ?>">
                              <input type="hidden" name="jamkerja" value="<?php echo $jamkerja ?>">
                              <input type="hidden" name="pilihan" value="<?php echo $pilihan ?>">
                            </div>
                          </div>
                          <div class=" col-md-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">                            
                            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                              <thead>
                                <tr>
                                  <th><!--<input type="checkbox" id="check-alls" class="flat">--></th>
                                  <th width="5%">No</th>
                                  <th>Nama</th>
                                  <th>Departemen</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach($list as $index => $isi) { ?>
                                <tr>
                                  <td><input value="<?php echo $isi['data']['id'];?>" type="checkbox" class="flat sub_chk icheckbox_flat-green" name="id[]"></td>
                                  <td><?php echo $index+1;?></td>
                                  <td><?php echo $isi['data']['nama']."<br>".$isi['data']['id']."<br>".$isi['data']['ktp'];?></td>
                                  <td><?php echo $isi['data']['nm_departemen']."<br>".$isi['data']['nm_bagian'];
                                            echo !empty($isi['data']['grup']) ? " - ".$isi['data']['grup'] : "";
                                            echo !empty($isi['data']['libur']) ? $isi['data']['libur'] : "";
                                      ?>    
                                  </td>
                                </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>

                          <?php
                          }
                          ?>

                          <div class="row"></div><hr>
                          <div class="form-group">
                            <div class="col-md-2 col-sm-3 col-xs-6">
                              <select name="kt" id="kt" class="form-control select2_single" style="cursor:pointer" required>
                                <option></option>
                                <option value='-'>Dikurangi</option>
                                <option value='+'>Ditambah</option>
                              </select>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                              <input type="number" name="jmlh" id="jmlh" required class="form-control col-md-7 col-xs-12" placeholder="Jumlah Hari" min="1" max="7">
                            </div>
                            <div class="col-md-7 col-sm-3 col-xs-6">
                                <button name="import" type="submit" class="btn btn-success" formaction="geser_libur_proses">
                                  <i class="glyphicon glyphicon-save"></i>
                                  &nbsp;Proses&nbsp;
                                </button>
                            </div>
                          </div>
                        </form> 
                        <?php 
                      }
                      ?>
                  </div>
                </div>
              </div>
            </div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- jquery.inputmask -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
  });
</script>
<!-- /jquery.inputmask -->

    <script>
      $(document).ready(function() {

        $('#datatable-checkbox').dataTable( {
          "paging":   false
         });
        
        $("#check-alls").on("click", function(e) {
          e.preventDefault();

          $(".sub_chk").prop('checked', $(this).is(':checked')); 
        });

        var emptyOption = '<option value=""></option>';
        var bags = JSON.parse('<?php echo json_encode($bags);?>');


        $("#departemen.select2_single").select2({
          placeholder: "Pilih Departemen",
          allowClear: true
        });
        $("#jgrup.select2_single").select2({
          placeholder: "Jumlah Grup",
          allowClear: true,
          disabled: <?php echo !empty($_GET['jgrup']) ? 'false' : 'true'; ?>
        });
        $("#grup.select2_single").select2({
          placeholder: "Grup",
          allowClear: true,
          disabled: <?php echo !empty($_GET['grup']) ? 'false' : 'true'; ?>
        });
        $("#libur.select2_single").select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: <?php echo !empty($_GET['libur']) ? 'false' : 'true'; ?>
        });
        $("#pilihan.select2_single").select2({
          placeholder: "Pilih Karyawan",
          allowClear: true
        });
        $("#ket_bag.select2_single").select2({
          placeholder: "Keterangan Bagian",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        });
        $("#jam.select2_single").select2({
          placeholder: "Jam Kerja",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        });
        $("#hadir.select2_single").select2({
          placeholder: "Kehadiran",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        });
        $("#kt.select2_single").select2({
          placeholder: "Pilihan",
          allowClear: true
        });

        $('#tanggal').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3",
          locale: {
            format: "DD-MM-YYYY",
            separator: "-",
          }
        });


        $("#pilihan.select2_single").select2({
            placeholder: "Pilih Karyawan",
            allowClear: true
        }).on('change', function(e) {
            var pilihan = e.currentTarget.value;
            if(pilihan == 'all'){
              $("#jgrup.select2_single").select2('destroy').select2({
                placeholder: "Jumlah Grup",
                allowClear: true,
                disabled: true
              });
              $("#libur.select2_single").select2('destroy').select2({
                placeholder: "Pilih Hari Libur",
                allowClear: true,
                disabled: true
              });
              $("#grup.select2_single").select2('destroy').select2({
                placeholder: "Pilih Grup",
                allowClear: true,
                disabled: true
              });
            }else{
              $("#jgrup.select2_single").select2('destroy').select2({
                placeholder: "Jumlah Grup",
                allowClear: true,
                disabled: false
              });
            }
        });
        
        $("#departemen.select2_single").select2({
            placeholder: "Pilih Departemen",
            allowClear: true
        }).on('change', function(e) {
          var kd_departemen = e.currentTarget.value;
          var list_bagiann = _.filter(bags, {kd_departemen:kd_departemen});
          
          $('#bagian').html(emptyOption);
          $(list_bagiann).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
          });

          $("#bagian.select2_single").select2('destroy').select2({
            placeholder: "Pilih Bagian",
            allowClear: true,
            disabled: false
          });
          $("#ket_bag.select2_single").select2({
            placeholder: "Keterangan Bagian",
            allowClear: true,
            disabled: false
          });
          $("#jam.select2_single").select2({
            placeholder: "Jam Kerja",
            allowClear: true,
            disabled: false
          });
        });



        $("#bagian.select2_single").select2({
          placeholder: "Pilih Bagian",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        }).show(function(e){
          var bagi = document.getElementById('bagi').value;
          var ed = document.getElementById('departemen');
          var kd_departemen = ed.options[ed.selectedIndex].value;
          var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
          $('#bagian').html(emptyOption);
          $(list_bagian).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
          });
        });

        $("#jam.select2_single").select2({
            placeholder: "Jam Kerja",
            allowClear: true
        }).on('change', function(e){
          var kd_jamkerja = e.currentTarget.value;

          if(kd_jamkerja==2){
            $("#jgrup.select2_single").select2('destroy').select2({
              placeholder: "Jumlah Grup",
              allowClear: true,
              disabled: false
            });
          }else{
            $("#jgrup.select2_single").select2('destroy').select2({
              placeholder: "Jumlah Grup",
              allowClear: true,
              disabled: true
            });
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: true
            });
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: true
            });
          }
        });




        $("#jgrup.select2_single").select2({
          placeholder: "Jumlah Grup",
          allowClear: true
        }).on('change', function(e) {
          var jgrup = e.currentTarget.value;
          if(jgrup>2){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            $('#grup').html(emptyOption);
            for(i=0;i<jgrup;i++){
              $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '">' + (i+10).toString(36).toUpperCase() + '</option>');
            }
            if(jgrup==4){
              $("#grup").append('<option value="Rp">Repliver</option>');
            }
          }else if(jgrup==2){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Jadwal Awal Masuk",
              allowClear: true,
              disabled: false
            });
            $("#tgl_m").show();
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="P">Pagi</option>');
            $("#grup").append('<option value="S">Siang</option>');
          }else if(jgrup==1){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            var p = 'Pagi';
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="P">Pagi</option>');
            $("#grup").append('<option value="S">Siang</option>');
          }

          if(jgrup>4){
            $('#libur').html(emptyOption);
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: true
            });
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
          }else{
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: false
            });
          }
        });

        var edit = JSON.parse('<?php echo json_encode($_GET);?>');
        $("#grup.select2_single").select2({
          placeholder: "Pilih Grup",
          allowClear: true
        }).show(function(e){
          var ed = document.getElementById('jgrup');
          var jgrup = ed.options[ed.selectedIndex].value;          
          //console.log(edit);
          
          if(jgrup>2){
            $('#grup').html(emptyOption);
            for(i=0;i<jgrup;i++){
              $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '" '+((i+10).toString(36).toUpperCase() == edit['grup'] ? 'selected' : '') +'>' + (i+10).toString(36).toUpperCase() + '</option>');
            }
          }else if(jgrup==2){
            var p = 'P';
            var s = 'S'
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="' + p + '" '+(p == edit['grup'] ? 'selected' : '') +'>Pagi</option>');
            $("#grup").append('<option value="' + s + '" '+(s == edit['grup'] ? 'selected' : '') +'>Siang</option>');
          }else if(jgrup==1){
            var p = 'P';
            var s = 'S'
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="' + p + '" '+(p == edit['grup'] ? 'selected' : '') +'>Pagi</option>');
            $("#grup").append('<option value="' + s + '" '+(s == edit['grup'] ? 'selected' : '') +'>Siang</option>');
          }
        });



      });
    </script>



<!-- iCheck -->
    <script src="<?php echo url();?>js/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo url();?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo url();?>js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo url();?>js/datatables.scroller.min.js"></script>
<script src="<?php echo url();?>js/dataTables.buttons.min.js"></script>
<script src="<?php echo url();?>js/buttons.bootstrap.min.js"></script>
<script src="<?php echo url();?>js/buttons.flash.min.js"></script>
<script src="<?php echo url();?>js/buttons.html5.min.js"></script>
<script src="<?php echo url();?>js/buttons.print.min.js"></script>
<script src="<?php echo url();?>js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo url();?>js/dataTables.keyTable.min.js"></script>
<script src="<?php echo url();?>js/dataTables.responsive.min.js"></script>
<script src="<?php echo url();?>js/responsive.bootstrap.js"></script>
<script src="<?php echo url();?>js/jszip.min.js"></script>
<script src="<?php echo url();?>js/pdfmake.min.js"></script>
<script src="<?php echo url();?>js/vfs_fonts.js"></script>

<!-- Datatables -->
<!-- /data table -->
<?php require '_base_foot.php';?>