<?php 
require '_base_head.php';
$mkar = new \App\Models\Absensi($app);
$list = $mkar->getSisa();
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>View Update</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <!-- TABLE -->
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Bagian</th>
                      <th>Jumlah Awal</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $index => $kar) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo "(".$kar['id'].")";?>
                          <br><?php echo $kar['nama'];?>
                          <br><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
                      </td>
                      <td>
                          <?= $kar['nm_status']."<br>";?>
                          <?php echo $kar['nm_departemen']." - ".$kar['nm_bagian']." ".$kar['grup'].$kar['libur']."<br>".$kar['nm_jamkerja'];?> 
                      </td>
                      <td><?php echo $kar['jml_aw'];?></td>
                      <td><?php echo $kar['sisa'];?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>

              <!--Modal Detail BEGIN============================================================-->
              <div id="detail_active" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body">

                      <div class="hasil-data"></div>
                      
                    </div>
                  </div>  
                </div>
              </div>
              <!--Modal Detail END============================================================-->


              <!-- Modal -->
              <div class="modal fade" id="modalPdf" tabindex="0" role="dialog" aria-labelledby="modalPdfLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button title="close" type="button"  data-dismiss="modal" class="close">&times;</button>
                            <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                        </div>
                        <div class="modal-body">
                            <object id="pdf-object" type="application/pdf" data="" width="100%" height="500">
                              No Support
                            </object>
                        </div>
                    </div>
                </div>
              </div>
              <!-- end Modal -->


          </div>
      </div>
    </div>
  </div>
</div>

<?php require '_base_foot.php';?>