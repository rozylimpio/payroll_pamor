<?php 
require '_base_head.php';
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Upload Karyawan Lembur Wajib</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/wajib_u')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'wajib';
        require '../pages/defmsg.php';
        ?>
        <br>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">
            Tanggal Awal
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input id="tgl_aw" name="tgl_aw" class="date-picker form-control" type="text" value="<?php echo isset($_GET['tgl_aw']) ? dateResolver($_GET['tgl_aw']) : '' ?>">
          </div>    
        </div>
        <div class="form-group">   
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">
            Tanggal Akhir
          </label>         
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input id="tgl_ak" name="tgl_ak" class="date-picker form-control" type="text" value="<?php echo isset($_GET['tgl_ak']) ? dateResolver($_GET['tgl_ak']) : '' ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">
            File Upload
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" name="filename" id="filename" accept=".csv"  class="form-control col-md-7 col-xs-12">
            <small>*.csv only</small>
          </div>
        </div>          
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Upload&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->  
      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>

<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
  $('#tgl_aw').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    locale: {
      format: "DD-MM-YYYY",
      separator: "-",
    }
  });
  $('#tgl_ak').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    locale: {
      format: "DD-MM-YYYY",
      separator: "-",
    }
  });
  });
</script>

<?php require '_base_foot.php';?>