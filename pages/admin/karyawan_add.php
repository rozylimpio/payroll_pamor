<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);
$nikak = $mkar->getNik();

$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();

$mjab = new \App\Models\Jabatan($app);
$jabs = $mjab->get();

$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();
$stats = $mjam->getStatus();

$mtjab = new \App\Models\Tjabatan($app);
$tjabs = $mtjab->get();

// $mtk = new \App\Models\Tkeahlian($app);
// $tks = $mtk->get();

$mtp = new \App\Models\Tprestasi($app);
$tps = $mtp->get();
$tpn = $mtp->getNull();

$edit = false;
$mutasi = false;
$id = false;
if($app->input->get('edit')) {
  $id = $app->input->get('edit');
  $edit = $mkar->getById($id);
}
if($app->input->get('mutasi')) {
  $mutasi = $app->input->get('mutasi');
  $edit = $mkar->getById($mutasi);
}

$redirect = url('a/karyawan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<style type="text/css">
  #committee{
    pointer-events: none;
  }
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $id? 'Ubah' : $mutasi ? 'Mutasi' : 'Input' ;?> Karyawan</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">        
        <?php
        $defmsg_category = 'karyawan_add';
        require '../pages/defmsg.php';
        ?>
        <form action="<?php echo url('a/karyawan' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php 
        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $edit['id'];?>">
        <input type="hidden" name="mutasi" value="<?php echo empty($mutasi) ? '' : $mutasi;?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            NIK
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="nik" id="nik" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['id'] : $nikak+1 ;?>" placeholder="Nomor Induk Karyawan" <?php //echo $edit || $mutasi ? 'readonly' : '' ;?>>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            No KTP
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="ktp" id="ktp" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['ktp'] : '' ;?>" placeholder="Nomor KTP" maxlength="16" <?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Nama Karyawan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="nama" id="nama" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nama'] : '' ;?>" placeholder="Nama Karyawan" <?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tempat Tanggal Lahir
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="tempat" id="tempat" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['tempat_lhr'] : '' ;?>" placeholder="Tempat Lahir" <?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="tgl_lhr" id="tgl_lhr" required  autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit ? dateResolver($edit['tgl_lhr']) : '' ;?>" placeholder="Tanggal Lahir" <?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Jenis Kelamin
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div id="jk" class="btn-group" data-toggle="buttons">
              <label class="btn btn-default <?php echo $edit['jk'] == 'L' ? 'active' : '' ;?>" data-toggle-class="btn-primary" 
              data-toggle-passive-class="btn-default">
                <input type="radio" name="jk" value="L" required  <?php echo $edit['jk'] == 'L' ? 'checked' : '' ;?>> &nbsp; Laki-laki &nbsp;
              </label>
              <label class="btn btn-default <?php echo $edit['jk'] == 'P' ? 'active' : '' ;?>" data-toggle-class="btn-primary" 
              data-toggle-passive-class="btn-default">
                <input type="radio" name="jk" value="P" required <?php echo $edit['jk'] == 'P' ? 'checked' : '' ;?>> Perempuan
              </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Alamat
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="alamat" id="alamat" class="form-control" required placeholder="Alamat" <?php echo $mutasi ? 'readonly' : '' ;?>><?php echo $edit ? $edit['alamat'] : '' ;?></textarea>
          </div>
        </div>
        <?php if($app->sess->nik=='10895'){?> 
        <div id="committee">
        <?php } ?>        
        <hr> 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Ketenagakerjaan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="bpjsket" id="bpjsket" class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_ket'] : '' ;?>" placeholder="No. BPJS Ketenagakerjaan" <?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <select name="pensiun" id="pensiun" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['bpjs_ket_plus'] == '0' ? 'selected' : '' ;?> value='0'>Produktif</option>
              <option <?php echo $edit['bpjs_ket_plus'] == '1' ? 'selected' : '' ;?> value='1'>Masa Pensiun</option>
            </select>
          </div>
        </div>  
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Kesehatan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="bpjskes" id="bpjskes" class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_kes'] : '' ;?>" placeholder="No. BPJS Kesehatan"<?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <select name="kelas" id="kelas" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['kelas'] == '1' ? 'selected' : '' ;?> value='1'>Kelas I</option>
              <option <?php echo $edit['kelas'] == '2' ? 'selected' : '' ;?> value='2'>Kelas II</option>
            </select>
          </div>
        </div>        
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Kes. Keluarga Lain
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="number" name="bpjskesplus" id="bpjskesplus" class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_kes_plus'] : '' ;?>" min="0" placeholder="Jumlah Keluarga Tambahan"<?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
        </div>
        <hr>   
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Status Karyawan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="hidden" name="status_lama" value="<?php echo $edit['kd_status'] ;?>">
            <select name="status" id="status" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php foreach($stats as $stat) { ?>
              <option <?php echo $edit['kd_status'] == $stat['id'] ? 'selected' : '' ;?> value="<?php echo $stat['id'];?>"><?php echo $stat['nm_status'];?></option>
              <?php }?>
            </select>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <input type="hidden" name="tgl_inl" value="<?php echo $edit['tgl_in'];?>">
            <input type="text" name="tgl_in" id="tgl_in" required  autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit ? dateResolver($edit['tgl_in']) : '' ;?>" placeholder="Tanggal Masuk">
          </div>
          <div id="ang" class="col-md-2 col-sm-3 col-xs-12">
            <input type="hidden" name="tgl_angl" value="<?php echo $edit['tgl_angkat'];?>">
            <input type="text" name="tgl_ang" id="tgl_ang" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit['tgl_angkat'] == '0000-00-00' || empty($edit['tgl_angkat']) ? '' : dateResolver($edit['tgl_angkat']) ;?>" placeholder="Tanggal Pengangkatan">
          </div>
          <div id="res" class="col-md-2 col-sm-3 col-xs-12">
            <input type="text" name="tgl_res" id="tgl_res" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit['kd_status'] == 4 ? dateResolver($edit['tgl_update']) : '' ;?>" placeholder="Tanggal Resign">
            <input type="number" name="cuti_awal" id="cuti_awal" class="form-control col-md-2 col-xs-12" value="<?php echo $edit['cuti'];?>" placeholder="Cuti Awal" readonly>
            <input type="number" name="sisa_cuti" id="sisa_cuti" class="form-control col-md-2 col-xs-12" value="<?php echo $edit['sisa_cuti'];?>" placeholder="Sisa Cuti">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jam Kerja
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input type="hidden" name="jaml" value="<?php echo $edit['kd_jamkerja'];?>">
            <select name="jam" id="jam" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php foreach($jams as $jam) { ?>
              <option <?php echo $edit['kd_jamkerja'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
              <?php }?>
            </select>
          </div>
        </div>    
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Departemen
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="hidden" name="departemenl" value="<?php echo $edit['kd_departemen'];?>">
            <select name="departemen" id="departemen" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php foreach($deps as $dep) { ?>
              <option <?php echo $edit['kd_departemen'] == $dep['id'] ? 'selected' : '' ;?> value="<?php echo $dep['id'];?>"><?php echo $dep['nm_departemen'];?></option>
              <?php }?>
            </select>
          </div>
        </div>        
        <div class="form-group">
          <input type="hidden" name="bagi" id="bagi" value="<?php echo $edit['kd_bagian'];?>">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Bagian
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <select name="bagian" id="bagian" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
            </select>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="hidden" name="ket_bagl" value="<?php echo $edit['bagian'];?>">
            <select name="ket_bag" id="ket_bag" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['bagian'] == 'Non-Produksi' ? 'selected' : '' ;?> value="Non-Produksi">Non-Produksi</option>
              <option <?php echo $edit['bagian'] == 'Produksi-6' ? 'selected' : '' ;?> value="Produksi-6">Produksi Jam 6</option>
              <option <?php echo $edit['bagian'] == 'Produksi-7' ? 'selected' : '' ;?> value="Produksi-7">Produksi Jam 7</option>
              <option <?php echo $edit['bagian'] == 'Produksi-8' ? 'selected' : '' ;?> value="Produksi-8">Produksi Jam 8</option>
              <option <?php echo $edit['bagian'] == 'Produksi-9' ? 'selected' : '' ;?> value="Produksi-9">Produksi Jam 9</option>
              <option <?php echo $edit['bagian'] == 'Produksi-87' ? 'selected' : '' ;?> value="Produksi-87">Masuk Jam 8 & 7(sabtu)</option>
              <option <?php echo $edit['bagian'] == 'Produksi-10' ? 'selected' : '' ;?> value="Produksi-10">Jam Masuk Operator Traveler</option>
              <option <?php echo $edit['bagian'] == 'Produksi' ? 'selected' : '' ;?> value="Produksi">Produksi</option>
            </select>
          </div>
        </div>         
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Grup
          </label>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <input type="hidden" name="jgrupl" value="<?php echo $edit['jml_grup'];?>">
            <select name="jgrup" id="jgrup" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['jml_grup'] == '7' ? 'selected' : '' ;?> value='7'>7 Grup</option>
              <option <?php echo $edit['jml_grup'] == '3' ? 'selected' : '' ;?> value='3'>3 Grup</option>
              <option <?php echo $edit['jml_grup'] == '4' ? 'selected' : '' ;?> value='4'>3 Grup QC LAB</option>
              <option <?php echo $edit['jml_grup'] == '2' ? 'selected' : '' ;?> value='2'>2 Grup</option>
              <option <?php echo $edit['jml_grup'] == '1' ? 'selected' : '' ;?> value='1'>Pagi atau Siang</option>
            </select>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <input type="hidden" name="grupl" value="<?php echo $edit['grup'];?>">
            <select name="grup" id="grup" class="form-control select2_single" style="cursor:pointer">
              <option></option>
            </select>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-12">            
            <input type="hidden" name="liburl" value="<?php echo $edit['libur'];?>">
            <select name="libur" id="libur" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['libur'] == '1' ? 'selected' : '' ;?> value='1'>1</option>
              <option <?php echo $edit['libur'] == '2' ? 'selected' : '' ;?> value='2'>2</option>
              <option <?php echo $edit['libur'] == '3' ? 'selected' : '' ;?> value='3'>3</option>
              <option <?php echo $edit['libur'] == '4' ? 'selected' : '' ;?> value='4'>4</option>
              <option <?php echo $edit['libur'] == '5' ? 'selected' : '' ;?> value='5'>5</option>
              <option <?php echo $edit['libur'] == '6' ? 'selected' : '' ;?> value='6'>6</option>
              <option <?php echo $edit['libur'] == '7' ? 'selected' : '' ;?> value='7'>7</option>
              <option value='0'>6 & 7</option>
            </select>
          </div>
          <div id="set" class="col-md-2 col-sm-3 col-xs-12">
            <input type="hidden" name="setengahl" value="<?php echo $edit['setengah'];?>">
            <select name="setengah" id="setengah" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['setengah'] == '1' ? 'selected' : '' ;?> value='1'>H-1</option>
              <option <?php echo $edit['setengah'] == '6' ? 'selected' : '' ;?> value='6'>Sabtu</option>
            </select>
          </div>
          <div id="tgl_m" class="col-md-2 col-sm-3 col-xs-12">
            <input type="hidden" name="tgl_milail" value="<?php echo $edit['tgl_m'];?>">
            <input type="text" name="tgl_mulai" id="tgl_mulai" autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit && $edit['tgl_m']!='0000-00-00' ? dateResolver($edit['tgl_m']) : '' ;?>" placeholder="Tanggal Mulai">
          </div>
        </div>  
        <hr>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jabatan
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="hidden" name="jabatanl" value="<?php echo $edit['kd_jabatan'];?>">
            <select name="jabatan" id="jabatan" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($jabs as $jab) { ?>
              <option <?php echo $edit['kd_jabatan'] == $jab['id'] ? 'selected' : '' ;?> value="<?php echo $jab['id'];?>"><?php echo $jab['nm_jabatan'];?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tunjangan Jabatan
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="n_jabatan" id="n_jabatan" class="form-control" readonly value="<?php echo $edit ? $edit['nom_jabatan'] : '' ;?>">          
          </div>
        </div> 
        <div class="form-group">
          <input type="hidden" name="t_jabatanl" value="<?php echo $edit ? $edit['kd_t_jabatan'] : '' ;?>">
          <input type="hidden" name="t_keahlianl"  value="<?php echo $edit ? $edit['kd_t_keahlian'] : '' ;?>">
          <input type="hidden" name="t_presl" value="<?php echo $edit['kd_t_prestasi'];?>">

          <input type="hidden" name="t_jabatan" id="t_jabatan" value="<?php echo $edit ? $edit['kd_t_jabatan'] : '' ;?>">
          <!-- <input type="hidden" name="t_keahlian" id="t_keahlian" value="<?php //echo $edit ? $edit['kd_t_keahlian'] : '' ;?>"> -->
          <input type="hidden" name="t_pres" id="t_pres" value="<?php echo $edit['kd_t_prestasi'];?>">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tunjangan Prestasi
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="tprestasi" id="tprestasi" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($tps as $tp) { ?>
              <option <?php echo $edit['kd_t_prestasi'] == $tp['id'] ? 'selected' : '' ;?> value="<?php echo $tp['id'];?>">
                <?php echo $tp['nm_jabatan'].' Grade '.$tp['kd_prestasi'].' - '.$tp['nominal'];?></option>
              <?php }?>
              <?php foreach($tpn as $tpn) { ?>
              <option <?php echo $edit['kd_t_prestasi'] == $tpn['id'] ? 'selected' : '' ;?> value="<?php echo $tpn['id'];?>">
                <?php echo $tpn['nominal'];?></option>
              <?php }?>
            </select>
          </div>
        </div>   
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tunjangan Keahlian
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="t_keahlian" id="t_keahlian"  class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['kd_t_keahlian'] : '' ;?>" placeholder="Tunjangan Keahlian">
          </div>
          <!-- <div class="col-md-2 col-sm-6 col-xs-12">
            <label>
              <input type="checkbox" class="js-switch" name="keahlian" id="keahlian" />
            </label>
          </div> -->
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Potongan SPT
          </label>
          <input type="hidden" name="sptl" value="<?php echo $edit['spt'];?>">
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="spt" id="spt" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['spt'] == '1' ? 'selected' : '' ;?> value='1'>Ya</option>
              <option <?php echo $edit['spt'] < 1 ? 'selected' : '' ;?> value='0'>Tidak</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Potongan KSPN
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="ket_kar" id="ket_kar" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['ket_kar'] == 'Belum' ? 'selected' : '' ;?> value='Belum'>Belum Potongan</option>
              <option <?php echo $edit['ket_kar'] == 'Pamor' ? 'selected' : '' ;?> value='Pamor'>Pamor</option>
              <option <?php echo $edit['ket_kar'] == 'KHS' ? 'selected' : '' ;?> value='KHS'>KHS</option>
              <option <?php echo $edit['ket_kar'] == 'KPS' ? 'selected' : '' ;?> value='KPS'>KPS</option>
            </select>
          </div>
        </div>
        <hr>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Gaji Pokok
          </label>
          <input type="hidden" name="gajil" value="<?php echo $edit['gaji'];?>">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="gaji" id="gaji" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['gaji'] : '' ;?>" placeholder="Gaji Pokok">
          </div>          
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="persen" id="persen" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['persen'] == '100' ? 'selected' : '' ;?> value='100'>100%</option>
              <option <?php echo $edit['persen'] == '75' ? 'selected' : '' ;?> value='75'>75%</option>
              <option <?php echo $edit['persen'] == '50' ? 'selected' : '' ;?> value='50'>50%</option>
            </select>
          </div>
        </div>
        <?php if($app->sess->nik=='10895'){?> 
        </div>
        <?php } ?>
        <?php if($id) { ?>
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
              Keterangan Edit
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" name="ket" required style="width: 100%"></textarea>
            </div>
          </div>
        <?php } ?>
        <?php if($mutasi) { ?>
          <hr>
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
              Berlaku Mulai
            </label>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <input type="text" name="berlaku" id="berlaku" required  autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" placeholder="Berlaku">
            </div>
          </div>
        <?php } ?>
        
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit || $mutasi) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- Switchery -->
<script src="<?php echo url();?>js/switchery.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
  
      $(document).ready(function() {
        $("#committee").each(function() {
            $(this).find('input, textarea, select').attr('readonly', true);

        });

        $(":input").inputmask();

        $("#agama.select2_single").select2({
            placeholder: "Pilih Agama",
            allowClear: true
        });
        $("#pernikahan.select2_single").select2({
            placeholder: "Pilih Status Pernikahan",
            allowClear: true
        });
        $("#kelas.select2_single").select2({
            placeholder: "Pilih Kelas",
            allowClear: true
        });
        $("#pensiun.select2_single").select2({
            placeholder: "Pilih Masa",
            allowClear: true
        });
        $("#status.select2_single").select2({
            placeholder: "Pilih Status Karyawan",
            allowClear: true
        });
        $("#pendidikan.select2_single").select2({
            placeholder: "Pilih Pendidikan Terakhir",
            allowClear: true
        });
        $("#grup.select2_single").select2({
            placeholder: "Pilih Grup",
            allowClear: true,
            disabled: <?php echo $edit ? 'false' : 'true' ?>
        });
        $("#jgrup.select2_single").select2({
            placeholder: "Jumlah Grup",
            allowClear: true,
            disabled: <?php echo $edit ? 'false' : 'true' ?>
        });
        $("#libur.select2_single").select2({
            placeholder: "Pilih Hari Libur",
            allowClear: true,
            disabled: <?php echo $edit ? 'false' : 'true' ?>
        });
        $("#setengah.select2_single").select2({
            placeholder: "Masuk Setengah Hari",
            allowClear: true,
            disabled: <?php echo $edit ? 'false' : 'true' ?>
        });
        $("#ket_bag.select2_single").select2({
            placeholder: "Keterangan Bagian",
            allowClear: true,
            disabled: <?php echo $edit ? 'false' : 'true' ?>
        });
        $("#spt.select2_single").select2({
            placeholder: "Pilih Potongan SPT",
            allowClear: true
        });
        $("#ket_kar.select2_single").select2({
            placeholder: "Pilih Potongan KSPN",
            allowClear: true
        });
        $("#persen.select2_single").select2({
            placeholder: "Pilih Persentasi Gaji",
            allowClear: true
        });
        $("#tprestasi.select2_single").select2({
            placeholder: "Pilih Prestasi Karyawan",
            allowClear: true
        })





        var emptyOption = '<option value=""></option>';
        var bags = JSON.parse('<?php echo json_encode($bags);?>');
        var tjabs = JSON.parse('<?php echo json_encode($tjabs);?>');
        // var tks = JSON.parse('<?php //echo json_encode($tks);?>');
        /*var tps = JSON.parse('<?php// echo json_encode($tps);?>');*/
        var edit = JSON.parse('<?php echo json_encode($edit);?>');



        $("#status").show(function(e) {
          var setatus = document.getElementById('status').value;
          if(setatus == '2'){
              $('#ang').show();
              $('#tgl_angkat').removeAttr('disabled');
          }else if(setatus == '4'){
              $('#res').show();
              $('#tgl_res').removeAttr('disabled');
          }else{
              $('#ang').hide();
              $('#tgl_angkat').attr('disabled','disabled');
              $('#res').hide();
              $('#tgl_res').attr('disabled','disabled');
          }
        });
        $("#status.select2_single").select2({
            placeholder: "Pilih Status Karyawan",
            allowClear: true
        }).on('change', function(e){
            var setatus = e.currentTarget.value;
            if(setatus=='2'){
              $('#ang').show();
              $('#tgl_angkat').removeAttr('disabled');
            }else if(setatus == '4'){
              $('#res').show();
              $('#tgl_res').removeAttr('disabled');
          }else{
              $('#ang').hide();
              $('#tgl_angkat').attr('disabled','disabled');
              $('#res').hide();
              $('#tgl_res').attr('disabled','disabled');
          }
        });


        $('#bpjskes').show(function(e) {
          var bpjskes = document.getElementById('bpjskes').value;
          if(bpjskes==''){
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: true
            });
          }else{
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: false
            });
          }
        });
        $('#bpjskes').on('keyup', function(e){
          var bpjskes = document.getElementById('bpjskes').value;
          if(bpjskes == ''){
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: true
            });
          }else{
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: false
            });
          }
        });


/*
        $("#tprestasi.select2_single").select2({
            placeholder: "Pilih Prestasi Karyawan",
            allowClear: true
        }).show(function(e){
          var tpr = document.getElementById('t_pres').value;
          var ej = document.getElementById('jabatan');
          var kd_jabatan = ej.options[ej.selectedIndex].value;
          var list_prestasi = _.filter(tps, {kd_jabatan:kd_jabatan});
          //console.log(JSON.stringify(list_prestasi));
          //console.log(tpr=='');
          $('#tprestasi').html(emptyOption);
          $(list_prestasi).each(function(i, item) {
            $("#tprestasi").append('<option value="'+item.id+'" '+(tpr == item.id ? 'selected' : '') +'> Grade '+item.kd_prestasi+' - '+item.nominal+'</option>');
          });
            $("#tprestasi").append('<option value="17">100.000</option>');
            $("#tprestasi").append('<option value="18">150.000</option>');
            $("#tprestasi").append('<option value="19">500.000</option>');

          $("#tprestasi.select2_single").select2('destroy').select2({
            placeholder: "Pilih Prestasi Karyawan",
            allowClear: true,
            disabled: <?php //echo $edit['kd_t_prestasi'] ? 'false' : 'true' ?>
          });
        });
/**/




        $("#jam.select2_single").select2({
            placeholder: "Pilih Jam Kerja Karyawan",
            allowClear: true
        }).on('change', function(e){
          //--- variable tunjangan jabatan ---//
          var ej = document.getElementById('jabatan');
          var kd_jabatan = ej.options[ej.selectedIndex].value;
          var kd_jamkerja = e.currentTarget.value;
          var list_t_jabatan_jk = _.filter(tjabs, {kd_jamkerja:kd_jamkerja});
          var list_t_jabatan_kj = _.filter(list_t_jabatan_jk, {kd_jabatan:kd_jabatan});
          
          //console.log(JSON.stringify(list_t_jabatan_kj));
          if(list_t_jabatan_kj.length > 0){
            $(list_t_jabatan_kj).each(function(i, itemkj) {
              document.getElementById("t_jabatan").value = itemkj.id;
              document.getElementById("n_jabatan").value = itemkj.nominal;
            });
          }else{
            document.getElementById("t_jabatan").value = '';
            document.getElementById("n_jabatan").value = 0;
          }
          //--- end ---//

          //---tunjangan keahlian---//
          // var ed = document.getElementById('departemen');
          // var kd_departemen = ed.options[ed.selectedIndex].value;
          // var ahli = document.getElementById('keahlian').checked;
          // var list_t_keahlian_jkerja = _.filter(tks, {kd_jamkerja:kd_jamkerja});
          // var list_t_keahlian_kddep = _.filter(list_t_keahlian_jkerja, {kd_departemen:kd_departemen});
          
          // //console.log(JSON.stringify(list_t_keahlian_kddep));
          // if(list_t_keahlian_kddep.length > 0 && ahli == true){
          //   $(list_t_keahlian_kddep).each(function(i, itemk) {
          //     document.getElementById("t_keahlian").value = itemk.id;
          //   });
          // }
          //--- end ---//
          if(kd_jamkerja==2){
            $("#jgrup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            $("#libur.select2_single").select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: false
            });
          }else{
            $("#tgl_m").hide();
            $("#jgrup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: true
            });
            $("#libur.select2_single").select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: false
            });
          }
        });




        
        $("#jabatan.select2_single").select2({
            placeholder: "Pilih Jabatan Karyawan",
            allowClear: true
        }).on('change', function(e){
          //--- variable tunjangan jabatan ---//
          var ej = document.getElementById('jam');
          var kd_jamkerja = ej.options[ej.selectedIndex].value;
          var kd_jabatan = e.currentTarget.value;
          var list_t_jabatan_jk = _.filter(tjabs, {kd_jamkerja:kd_jamkerja});
          var list_t_jabatan_kj = _.filter(list_t_jabatan_jk, {kd_jabatan:kd_jabatan});
          
          //console.log(JSON.stringify(list_t_jabatan_kj));
          if(list_t_jabatan_kj.length > 0){
            $(list_t_jabatan_kj).each(function(i, itemkj) {
              document.getElementById("t_jabatan").value = itemkj.id;
              document.getElementById("n_jabatan").value = itemkj.nominal;
            });
          }else{
            document.getElementById("t_jabatan").value = '';
            document.getElementById("n_jabatan").value = 0;
          }
          //--- end ---//
          
          /*--- tunjangan prestasi---//
          var list_prestasi = _.filter(tps, {kd_jabatan:kd_jabatan});
          
          $('#tprestasi').html(emptyOption);
          $(list_prestasi).each(function(i, item) {
            $("#tprestasi").append('<option value="'+item.id+'"> Grade '+item.kd_prestasi+' - '+item.nominal+'</option>');
          });          
            $("#tprestasi").append('<option value="17">100.000</option>');
            $("#tprestasi").append('<option value="18">150.000</option>');
            $("#tprestasi").append('<option value="19">500.000</option>');

          $("#tprestasi.select2_single").select2('destroy').select2({
            placeholder: "Pilih Prestasi Karyawan",
            allowClear: true,
            disabled: false
          });
          //--- end ---/**/
        });




        $("#departemen.select2_single").select2({
            placeholder: "Pilih Departemen",
            allowClear: true
        }).on('change', function(e) {
          var kd_departemen = e.currentTarget.value;
          var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
          
          $('#bagian').html(emptyOption);
          $(list_bagian).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
          });

          $("#bagian.select2_single").select2('destroy').select2({
            placeholder: "Pilih Bagian",
            allowClear: true,
            disabled: false
          });
          $("#ket_bag.select2_single").select2('destroy').select2({
            placeholder: "Keterangan Bagian",
            allowClear: true,
            disabled: false
          });

          //---tunjangan keahlian---//
          // var ed = document.getElementById('jam');
          // var kd_jamkerja = ed.options[ed.selectedIndex].value;
          // var ahli = document.getElementById('keahlian').checked;
          // var list_t_keahlian_jkerja = _.filter(tks, {kd_jamkerja:kd_jamkerja});
          // var list_t_keahlian_kddep = _.filter(list_t_keahlian_jkerja, {kd_departemen:kd_departemen});
          
          // //console.log(ahli);
          // if(list_t_keahlian_kddep.length > 0 && ahli == true){
          //   $(list_t_keahlian_kddep).each(function(i, itemk) {
          //     document.getElementById("t_keahlian").value = itemk.id;
          //   });
          // }
          //--- end ---//
        });


        // $('#keahlian').on('click', function(e){
        //   var ed = document.getElementById('jam');
        //   var kd_jamkerja = ed.options[ed.selectedIndex].value;          
        //   var edd = document.getElementById('departemen');
        //   var kd_departemen = edd.options[edd.selectedIndex].value;       
        //   var edb = document.getElementById('bagian');
        //   var kd_bagian = edb.options[edb.selectedIndex].value;
        //   var ahli = document.getElementById('keahlian').checked;
        //   var list_t_keahlian_jkerja = _.filter(tks, {kd_jamkerja:kd_jamkerja});
        //   var list_t_keahlian_kddep = _.filter(list_t_keahlian_jkerja, {kd_departemen:kd_departemen});

        //   //console.log(ahli);

        //   if(list_t_keahlian_kddep.length > 0 && ahli == true){
        //     $(list_t_keahlian_kddep).each(function(i, itemk) {
        //       document.getElementById("t_keahlian").value = itemk.id;
        //     });
        //   }else if(kd_departemen==12 && kd_bagian!=170){
        //     document.getElementById("t_keahlian").value = "0";
        //   }
        //   else{
        //     document.getElementById("t_keahlian").value = ""; 
        //   }
        // });
        // $('#keahlian').show('change', function(e){
        //   var tahli = document.getElementById('t_keahlian').value;
        //   if(tahli>0){
        //     document.getElementById("keahlian").checked = true;
        //   } 
        // });


        $("#bagian.select2_single").select2({
            placeholder: "Pilih Bagian",
            allowClear: true
        }).on('change', function(e){
          var ed = document.getElementById('departemen');
          var kd_departemen = ed.options[ed.selectedIndex].value;
          var kd_bagian = e.currentTarget.value;

          //console.log(JSON.stringify(kd_bagian));
          if(kd_departemen==12 && kd_bagian!=170){
            document.getElementById("t_keahlian").value = "0";
          }
        }).show(function(e){
          var bagi = document.getElementById('bagi').value;
          var ed = document.getElementById('departemen');
          var kd_departemen = ed.options[ed.selectedIndex].value;
          var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
          
          $('#bagian').html(emptyOption);
          $(list_bagian).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
          });
          $("#bagian.select2_single").select2('destroy').select2({
            placeholder: "Pilih Bagian",
            allowClear: true,
            disabled: <?php echo $edit ? 'false' : 'true' ?>
          });
        });

        $("#jgrup").show(function(e) {
          var jgrup = document.getElementById('jgrup').value;
          if(jgrup == 2){
            $("#tgl_m").show();
          }else{
            $("#tgl_m").hide();
          }
        });

        $("#jgrup.select2_single").select2({
          placeholder: "Jumlah Grup",
          allowClear: true
        }).on('change', function(e) {
          var jgrup = e.currentTarget.value;
          if(jgrup>2){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            $("#tgl_m").hide();
            $('#grup').html(emptyOption);
            for(i=0;i<jgrup;i++){
              $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '">' + (i+10).toString(36).toUpperCase() + '</option>');
            }
            if(jgrup==4){
              $("#grup").append('<option value="R" '+(edit['grup']=='R' ? 'selected' : '')+ '>Repliver</option>');
            }
          }else if(jgrup==2){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Jadwal Awal Masuk",
              allowClear: true,
              disabled: false
            });
            $("#tgl_m").show();
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="Pagi">Pagi</option>');
            $("#grup").append('<option value="Siang">Siang</option>');
          }else if(jgrup==1){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            $("#tgl_m").hide();
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="Pagi">Pagi</option>');
            $("#grup").append('<option value="Siang">Siang</option>');
          }

          if(jgrup>4){
            $('#libur').html(emptyOption);
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: true
            });
            $("#tgl_m").hide();
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grups",
              allowClear: true,
              disabled: false
            });
          }else{
            $("#libur.select2_single").select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: false
            });
          }
        });

        $("#grup.select2_single").select2({
          placeholder: "Pilih Grup",
          allowClear: true
        }).show(function(e){
          var ed = document.getElementById('jgrup');
          var jgrup = ed.options[ed.selectedIndex].value;          
          //console.log(edit['grup']);
          
          if(jgrup>2){
            $('#grup').html(emptyOption);
            for(i=0;i<jgrup;i++){
              $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '" '+((i+10).toString(36).toUpperCase() == edit['grup'] ? 'selected' : '') +'>' + (i+10).toString(36).toUpperCase() + '</option>');
            }
            if(jgrup==4){
              $("#grup").append('<option value="R" '+(edit['grup']=='R' ? 'selected' : '')+ '>Repliver</option>');
            }
          }else if(jgrup==2){
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="Pagi" '+(edit['grup']=='P' ? 'selected' : '')+ '>Pagi</option>');
            $("#grup").append('<option value="Siang" '+(edit['grup']=='S' ? 'selected' : '')+ '>Siang</option>');
          }else if(jgrup==1){
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="Pagi" '+(edit['grup']=='P' ? 'selected' : '')+ '>Pagi</option>');
            $("#grup").append('<option value="Siang" '+(edit['grup']=='S' ? 'selected' : '')+ '>Siang</option>');
          }
        });

        $("#libur").show(function() {
          var libur = document.getElementById('libur').value;
          var jam = document.getElementById('jam').value;
          if(libur >= 1 && jam == 1){
            $("#set").show();
            $("#setengah.select2_single").select2({
                placeholder: "Masuk Setengah Hari",
                allowClear: true,
                disabled: false
            });
          }else{
            $("#set").hide();
            $("#setengah.select2_single").select2({
                placeholder: "Masuk Setengah Hari",
                allowClear: true,
                disabled: true
            });
          }
        });

        $("#libur.select2_single").select2().on('change', function(e){
          var libur = e.currentTarget.value;
          var ed = document.getElementById('jam');
          var jam = ed.options[ed.selectedIndex].value; 
          if(libur >= 1 && jam == 1){
            $("#set").show();
            $("#setengah.select2_single").select2({
                placeholder: "Masuk Setengah Hari",
                allowClear: true,
                disabled: false
            });
          }else{
            $("#set").hide();
            $("#setengah.select2_single").select2({
                placeholder: "Masuk Setengah Hari",
                allowClear: true,
                disabled: true
            });
          }
        });




      var rupiah = document.getElementById("gaji");
      rupiah.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "");
      });

      var nominal = document.getElementById("t_keahlian");
      nominal.addEventListener("keyup", function(e) {
        nominal.value = formatRupiah(this.value, "");
      });

      /* Fungsi formatRupiah */
      function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
          split = number_string.split(","),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
          separator = sisa ? "." : "";
          rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "" + rupiah : "";
      }


      });

    $(document).on("wheel", "input[type=number]", function (e) {
        $(this).blur();
    });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>