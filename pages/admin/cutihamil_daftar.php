<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Cuti Hamil per Periode</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <button name="tinjau" type="submit" id="tinjau" class="btn btn-info">
                    &nbsp;Proses&nbsp;
                  </button>
                </div>
              </div>              
            </form>
        </div>

        <?php
        if(isset($_POST['tinjau'])){
          $thn = date('Y');
          $bln = $app->input->post('bulan');

          $tgl_ak = date($thn.'-'.$bln.'-20');
          $blnn = date('m', strtotime("-1 month", strtotime($tgl_ak)));
          $thnn = date('Y', strtotime("-1 month", strtotime($tgl_ak)));
          $tgl_aw = date($thnn.'-'.$blnn.'-21');

          $data = $mkar->getCutiHamilDaftar($tgl_aw, $tgl_ak);
          ?>
          <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
                <div class="table-responsive" align="center">
                  <hr>
                  <table width="100%" class="table-responsive">
                    <tr>
                      <td align="center">
                        <h4><b>Daftar Karyawan Cuti Hamil</b><br>
                        Periode <?php echo namaBulan($bln); ?>
                        <br>
                        <br>
                        </h4>
                      </td>
                    </tr>
                  </table>
                  <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                    <thead>
                      <tr>
                        <th width="5%">No</th>
                        <th>Nama</th>
                        <th>Bagian</th>
                        <th>Cuti Tanggal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data as $index => $kar) { ?>
                      <tr>
                        <td><?php echo $index+1;?></td>
                        <td><?php echo "(".$kar['id'].")";?>
                            <br><?php echo $kar['nama'];?>
                        </td>
                        <td>
                            <?php 
                            $tgl = date("Y-m-d");
                            $karmut = $mkar->getDataMutasi($kar['id'], $tgl);
                            if(empty($karmut)){
                              echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
                              echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                              echo !empty($kar['libur']) ? $kar['libur'] : "";
                              echo '<br><small>'.$kar['bagian'].'</small>';
                            }else{
                              echo $karmut['nm_departemen']."<br>".$karmut['nm_bagian'];
                              echo !empty($karmut['grup']) ? " - ".$karmut['grup'] : "";
                              echo !empty($karmut['libur']) ? $karmut['libur'] : "";
                              echo '<br><br>';
                              echo '<i><small style="font-size: 9px">Berlaku : '.dateResolver($karmut['berlaku']).'<br>';
                              echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
                              echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                              echo !empty($kar['libur']) ? $kar['libur'] : "";
                              echo '</i></small>';
                            }
                            ?>    
                        </td>
                        <td><?php 
                          echo dateFormatSingkat(dateResolver($kar['tgl_awal'])).' - '.dateFormatSingkat(dateResolver($kar['tgl_akhir'])); 
                        ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
            </div>
          <?php
        }
        ?>
    
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  var emptyOption = '<option value=""></option>';

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

});
</script>
<?php require '_base_foot.php';?>