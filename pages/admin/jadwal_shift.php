<?php 
require '_base_head.php';
$mjadwal = new \App\Models\JadwalShift($app);
$jadwals = $mjadwal->get();
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Form Jadwal Shift 2 Grup</h2>
          <div class="clearfix">
          </div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/jadwal_shift')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'jadwal';
        require '../pages/defmsg.php';
        /*
        $data = 10;
        $maxdata = strtoupper(chr(96+$data));//97 = huruf A

        for($i='A'; $i<=$maxdata; $i++){
          echo $i." ";
        }*/
        ?>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Tanggal Berlaku
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Berlaku Mulai">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Hari Perubahan Jadwal
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="shift" id="shift" class="form-control select2_single" style="cursor:pointer;width:100%">
              <option></option>
              <option value='1'>Senin</option>
              <option value='2'>Selasa</option>
              <option value='3'>Rabu</option>
              <option value='4'>Kamis</option>
              <option value='5'>Jumat</option>
              <option value='6'>Sabtu</option>
              <option value='7'>Minggu</option>
              <option value='0'>Libur Perorang</option>
            </select>
          </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->
        <?php 
        $defmsg_category = 'jadwal_list';
        require '../pages/defmsg.php';
        //print_r($jadwals[1]); //die(); ?>
        <!-- table -->
        <hr>
        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
          <div class="table-responsive" align="center">
            <table class="table table-bordered table-hover table-striped" id="myTable" style="width: 100%">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Hari Pergantian</th>
                    <th>Berlaku</th>
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($jadwals as $index => $jadwal) { ?>
                <tr>
                    <td><?php echo $index+1;?></td>
                    <td><?php echo $jadwal['hari']==0 ? 'Hari Libur Karyawan' : namaHari($jadwal['hari']); ?></td>
                    <td><?php echo dateFormat(dateResolver($jadwal['berlaku']))?></td>
                    <td><button type="button" data-url="<?php echo url('a/jadwal_shift?_method=delete&id=' . $jadwal['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button></td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
          </div>
        </div>
        <!-- table -->

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<script>
  $(document).ready(function() {
    
    $(":input").inputmask();

    $("#shift.select2_single").select2({
        placeholder: "Pilih Hari Ganti Jadwal",
        allowClear: true
    });
    $("#grup.select2_single").select2({
        placeholder: "Pilih Grup",
        allowClear: true
    });
  });
</script>
<?php require '_base_foot.php';?>