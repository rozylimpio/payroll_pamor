<?php 
require '_base_head.php';
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Update Absensi</h2>
            <br><br>
            <small><i>
              Ubah absensi dari (unpaid/rumah/diliburkan) ke sesuai dengan inputan file *.csv
              <br>
              diambil dari absensi terakhir periode ke belakang (ex: 20,19,18,dst)
            </i></small>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/update_absensi')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'update_absensi';
        require '../pages/defmsg.php';
        ?>
        <br>
        <br>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="foto">
            Bulan
          </label>
          <div class="col-md-2 col-sm-4 col-xs-12">
            <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php for ($i=1; $i < 13; $i++) { ?>
              <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                  <?php echo namaBulan($i);?>
              </option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="foto">
            File Absensi
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" name="filename" id="filename" accept=".csv"  class="form-control col-md-7 col-xs-12">
            <small>
              *.csv only
              <br>
              format file .csv (nik;jumlah;ket)
              <br>
              ex: 
              <br>
              11235;2;cuti_r
              <br>
              11230;5;cuti_r
            </small>
          </div>
        </div>  
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<script>
$(document).ready(function() {

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

});
</script>


<?php require '_base_foot.php';?>