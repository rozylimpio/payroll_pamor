<?php 
require '_base_head.php';
$mkar = new \App\Models\Resign($app);

$id = $app->input->get('edit');
$edit = $mkar->getById($id);

$redirect = url('a/resign');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form Edit Karyawan Resign</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">        
        <?php
        $defmsg_category = 'resign';
        require '../pages/defmsg.php';
        ?>
        <form action="<?php echo url('a/resign_edit' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php 
        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $edit['id'];?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            NIK
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="nik" id="nik" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['id'] : '' ;?>" placeholder="Nomor Induk Karyawan" <?php echo $edit ? 'readonly' : '' ;?>>
          </div>
        </div>
        <div class="col form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            No KTP
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="ktp" id="ktp" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['ktp'] : '' ;?>" placeholder="Nomor KTP" maxlength="16">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Nama Karyawan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="nama" id="nama" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nama'] : '' ;?>" placeholder="Nama Karyawan">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tempat Tanggal Lahir
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="tempat" id="tempat" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['tempat_lhr'] : '' ;?>" placeholder="Tempat Lahir">
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="tgl_lhr" id="tgl_lhr" required  autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit ? dateResolver($edit['tgl_lhr']) : '' ;?>" placeholder="Tanggal Lahir">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Jenis Kelamin
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div id="jk" class="btn-group" data-toggle="buttons">
              <label class="btn btn-default <?php echo $edit['jk'] == 'L' ? 'active' : '' ;?>" data-toggle-class="btn-primary" 
              data-toggle-passive-class="btn-default">
                <input type="radio" name="jk" value="L" required  <?php echo $edit['jk'] == 'L' ? 'checked' : '' ;?>> &nbsp; Laki-laki &nbsp;
              </label>
              <label class="btn btn-default <?php echo $edit['jk'] == 'P' ? 'active' : '' ;?>" data-toggle-class="btn-primary" 
              data-toggle-passive-class="btn-default">
                <input type="radio" name="jk" value="P" required <?php echo $edit['jk'] == 'P' ? 'checked' : '' ;?>> Perempuan
              </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Alamat
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="alamat" id="alamat" class="form-control" required placeholder="Alamat"><?php echo $edit ? $edit['alamat'] : '' ;?></textarea>
          </div>
        </div>        
        <hr> 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Ketenagakerjaan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="bpjsket" id="bpjsket" required class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_ket'] : '' ;?>" placeholder="No. BPJS Ketenagakerjaan">
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <select name="pensiun" id="pensiun" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['bpjs_ket_plus'] == '0' ? 'selected' : '' ;?> value='0'>Produktif</option>
              <option <?php echo $edit['bpjs_ket_plus'] == '1' ? 'selected' : '' ;?> value='1'>Masa Pensiun</option>
            </select>
          </div>
        </div>  
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Kesehatan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="bpjskes" id="bpjskes" class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_kes'] : '' ;?>" placeholder="No. BPJS Kesehatan">
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <select name="kelas" id="kelas" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['kelas'] == '1' ? 'selected' : '' ;?> value='1'>Kelas I</option>
              <option <?php echo $edit['kelas'] == '2' ? 'selected' : '' ;?> value='2'>Kelas II</option>
            </select>
          </div>
        </div>      
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Kes. Keluarga Lain
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="number" name="bpjskesplus" min="0" id="bpjskesplus" class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_kes_plus'] : '' ;?>" placeholder="Jumlah Keluarga Tambahan"<?php echo $mutasi ? 'readonly' : '' ;?>>
          </div>
        </div>
        
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- Switchery -->
<script src="<?php echo url();?>js/switchery.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {

        $(":input").inputmask();

        $("#kelas.select2_single").select2({
            placeholder: "Pilih Kelas",
            allowClear: true
        });
        $("#pensiun.select2_single").select2({
            placeholder: "Pilih Masa",
            allowClear: true
        });

        var edit = JSON.parse('<?php echo json_encode($edit);?>');

        $('#bpjskes').show(function(e) {
          var bpjskes = document.getElementById('bpjskes').value;
          if(bpjskes==''){
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: true
            });
          }else{
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: false
            });
          }
        });
        $('#bpjskes').on('keyup', function(e){
          var bpjskes = document.getElementById('bpjskes').value;
          if(bpjskes == ''){
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: true
            });
          }else{
            $("#kelas.select2_single").select2({
                placeholder: "Pilih Kelas",
                allowClear: true,
                disabled: false
            });
          }
        });


      });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>