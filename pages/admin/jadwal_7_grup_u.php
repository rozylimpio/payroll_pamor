<?php 
require '_base_head.php';
$mjadwal = new \App\Models\Jadwal7GrupU($app);
$jadwals = $mjadwal->get();
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Form Jadwal 7 Grup Bagian Utility</h2>
          <div class="clearfix">
          </div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/jadwal_7_grup_u')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'jadwal';
        require '../pages/defmsg.php';
        ?>
        <input type="hidden" name="jml" id="jml" value="7">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tanggal Berlaku
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Berlaku Mulai">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Shift
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="shift" id="shift" required class="form-control col-md-7 col-xs-12" value="Pagi" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Grup
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
              <option></option>
            </select>
          </div>
        </div><div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="grup[]" id="grup" class="form-control select2_single c" style="cursor:pointer;width:100%">
              <option></option>
            </select>
          </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>

        <hr>
        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
          <div class="table-responsive" align="center">
            <table class="table table-bordered table-hover table-striped" id="Table" style="width: 100%">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Hari</th>
                    <th>Grup</th>
                    <th>Shift</th>
                    <th>Berlaku</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if(!empty($jadwals)){
                foreach($jadwals as $index => $jadwal) { 
                ?>
                <tr>
                    <td><?php echo $index+1;?></td>
                    <td><?php echo namaHari($jadwal['harike']) ?></td>
                    <td><?php echo $jadwal['grup'] ?></td>
                    <td><?php echo $jadwal['shift'] ?></td>
                    <td><?php echo dateFormat(dateResolver($jadwal['berlaku'])) ?></td>
                </tr>
                <?php }} ?>
            </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<script>
  $(document).ready(function() {
    
    $(":input").inputmask();

    $("#grup.select2_single").select2({
        placeholder: "Pilih Grup",
        allowClear: true
    });

    var emptyOption = '<option value=""></option>';
    $("#jml").show(function(e){
      var jml = document.getElementById('jml').value;
      var i;

      $('#grup.c').html(emptyOption);

      for(i=1; i<= jml; i++) {
        $("#grup.c").append('<option value="'+i+'">'+(i+9).toString(36).toUpperCase()+'</option>');
      }
    });

   $('#Table').dataTable({
    "pageLength": 42,
    "ordering": false,
    "lengthChange": false
   });
   MergeGridCells();

  });

  function MergeGridCells() {
      var dimension_cells = new Array();
      var dimension_col = null;
      var columnCount = $("#Table tr:first th").length;

      // first_instance holds the first instance of identical td
      var first_instance = null;
      var rowspan = 1;
      // iterate through rows
      $("#Table").find('tr').each(function () {

          // find the td of the correct column (determined by the dimension_col set above)
          var dimension_td = $(this).find('td:nth-child(' + 2 + ')');

          if (first_instance == null) {
              // must be the first row
              first_instance = dimension_td;
          } else if (dimension_td.text() == first_instance.text()) {
              // the current td is identical to the previous
              // remove the current td
              dimension_td.remove();
              ++rowspan;
              // increment the rowspan attribute of the first instance
              first_instance.attr('rowspan', rowspan);
          } else {
              // this cell is different from the last
              first_instance = dimension_td;
              rowspan = 1;
          }
      });
  }
</script>
<?php require '_base_foot.php';?>